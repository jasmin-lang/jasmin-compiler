
type 't coq_ReverseCoercionSource = 't

type 't coq_ReverseCoercionTarget = 't

val reverse_coercion :
  'a1 -> 'a2 coq_ReverseCoercionSource -> 'a1 coq_ReverseCoercionTarget
