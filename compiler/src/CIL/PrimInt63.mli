open Datatypes

val add : Uint63.t -> Uint63.t -> Uint63.t

val eqb : Uint63.t -> Uint63.t -> bool

val compares : Uint63.t -> Uint63.t -> comparison
