open BinInt
open BinNums
open Arch_decl
open Arch_utils
open Arm_expand_imm
open Eqtype
open Expr
open Fintype
open Flag_combination
open Seq
open Shift_kind
open Ssralg
open Ssrbool
open Type
open Utils0
open Word0
open Word_ssrZ
open Wsize

val arm_reg_size : wsize

val arm_xreg_size : wsize

type register =
| R00
| R01
| R02
| R03
| R04
| R05
| R06
| R07
| R08
| R09
| R10
| R11
| R12
| LR
| SP

val register_beq : register -> register -> bool

val register_eq_dec : register -> register -> bool

val register_eq_axiom : register eq_axiom

val eqTC_register : register eqTypeC

val arm_register_eqType : Equality.coq_type

val registers : register list

val finTC_register : register finTypeC

val register_finType : Finite.coq_type

val register_to_string : register -> string

val reg_toS : register coq_ToString

type rflag =
| NF
| ZF
| CF
| VF

val rflag_beq : rflag -> rflag -> bool

val rflag_eq_dec : rflag -> rflag -> bool

val rflag_eq_axiom : rflag eq_axiom

val eqTC_rflag : rflag eqTypeC

val rflag_eqType : Equality.coq_type

val rflags : rflag list

val finTC_rflag : rflag finTypeC

val rflag_finType : Finite.coq_type

val flag_to_string : rflag -> string

val rflag_toS : rflag coq_ToString

type condt =
| EQ_ct
| NE_ct
| CS_ct
| CC_ct
| MI_ct
| PL_ct
| VS_ct
| VC_ct
| HI_ct
| LS_ct
| GE_ct
| LT_ct
| GT_ct
| LE_ct

val condt_beq : condt -> condt -> bool

val condt_eq_dec : condt -> condt -> bool

val condt_eq_axiom : condt eq_axiom

val eqTC_condt : condt eqTypeC

val condt_eqType : Equality.coq_type

val condts : condt list

val finTC_condt : condt finTypeC

val condt_finType : Finite.coq_type

val string_of_condt : condt -> string

val shift_kind_beq : shift_kind -> shift_kind -> bool

val shift_kind_eq_dec : shift_kind -> shift_kind -> bool

val shift_kind_eq_axiom : shift_kind eq_axiom

val eqTC_shift_kind : shift_kind eqTypeC

val shift_kind_eqType : Equality.coq_type

val shift_kinds : shift_kind list

val string_of_shift_kind : shift_kind -> string

val check_shift_amount : shift_kind -> coq_Z -> bool

val shift_op :
  shift_kind -> wsize -> GRing.ComRing.sort -> coq_Z -> GRing.ComRing.sort

val shift_of_sop2 : wsize -> sop2 -> shift_kind option

val arm_fc_of_cfc : combine_flags_core -> flag_combination

val arm_fcp : coq_FlagCombinationParams

val arm_check_CAimm : caimm_checker_s -> wsize -> GRing.ComRing.sort -> bool

val arm_decl : (register, empty, empty, rflag, condt) arch_decl

val arm_linux_call_conv :
  (register, empty, empty, rflag, condt) calling_convention

val is_expandable : coq_Z -> bool

val is_expandable_or_shift : coq_Z -> bool
