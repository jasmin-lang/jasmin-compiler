open BinInt
open BinNums
open Eqtype
open Ssralg
open Strings
open Word0
open Word_ssrZ
open Wsize

type expand_immediate_kind =
| EI_none
| EI_byte
| EI_pattern
| EI_shift

(** val z_to_bytes : coq_Z -> ((coq_Z * coq_Z) * coq_Z) * coq_Z **)

let z_to_bytes n =
  let (n0, b0) =
    Z.div_eucl n (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
      (Coq_xO (Coq_xO Coq_xH)))))))))
  in
  let (n1, b1) =
    Z.div_eucl n0 (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
      (Coq_xO (Coq_xO Coq_xH)))))))))
  in
  let (n2, b2) =
    Z.div_eucl n1 (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
      (Coq_xO (Coq_xO Coq_xH)))))))))
  in
  let b3 =
    Z.modulo n2 (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
      (Coq_xO Coq_xH)))))))))
  in
  (((b3, b2), b1), b0)

(** val is_ei_pattern : coq_Z -> bool **)

let is_ei_pattern n =
  let (p, b0) = z_to_bytes n in
  let (p0, b1) = p in
  let (b3, b2) = p0 in
  (||)
    ((&&)
      (eq_op coq_BinNums_Z__canonical__eqtype_Equality (Obj.magic b3)
        (Obj.magic b0))
      ((&&)
        (eq_op coq_BinNums_Z__canonical__eqtype_Equality (Obj.magic b2)
          (Obj.magic b0))
        (eq_op coq_BinNums_Z__canonical__eqtype_Equality (Obj.magic b1)
          (Obj.magic b0))))
    ((||)
      ((&&)
        (eq_op coq_BinNums_Z__canonical__eqtype_Equality (Obj.magic b3)
          (Obj.magic Z0))
        ((&&)
          (eq_op coq_BinNums_Z__canonical__eqtype_Equality (Obj.magic b2)
            (Obj.magic b0))
          (eq_op coq_BinNums_Z__canonical__eqtype_Equality (Obj.magic b1)
            (Obj.magic Z0))))
      ((&&)
        (eq_op coq_BinNums_Z__canonical__eqtype_Equality (Obj.magic b3)
          (Obj.magic b1))
        ((&&)
          (eq_op coq_BinNums_Z__canonical__eqtype_Equality (Obj.magic b2)
            (Obj.magic Z0))
          (eq_op coq_BinNums_Z__canonical__eqtype_Equality (Obj.magic b0)
            (Obj.magic Z0)))))

(** val is_ei_shift : coq_Z -> bool **)

let is_ei_shift n =
  let byte_end = Z.sub (Z.log2 n) (Zpos (Coq_xI (Coq_xI Coq_xH))) in
  eq_op coq_BinNums_Z__canonical__eqtype_Equality
    (Obj.magic Z.rem n (Z.pow (Zpos (Coq_xO Coq_xH)) byte_end)) (Obj.magic Z0)

(** val ei_kind : coq_Z -> expand_immediate_kind **)

let ei_kind n =
  if (&&) (Z.leb Z0 n)
       (Z.ltb n (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
         (Coq_xO Coq_xH))))))))))
  then EI_byte
  else if is_ei_pattern n
       then EI_pattern
       else if is_ei_shift n then EI_shift else EI_none

type wencoding =
| WE_allowed of bool
| W12_encoding
| W16_encoding

(** val wencoding_beq : wencoding -> wencoding -> bool **)

let wencoding_beq x y =
  match x with
  | WE_allowed x0 ->
    (match y with
     | WE_allowed x1 -> internal_bool_beq x0 x1
     | _ -> false)
  | W12_encoding -> (match y with
                     | W12_encoding -> true
                     | _ -> false)
  | W16_encoding -> (match y with
                     | W16_encoding -> true
                     | _ -> false)

type expected_wencoding = { on_shift : wencoding; on_none : wencoding }

(** val expected_wencoding_beq :
    expected_wencoding -> expected_wencoding -> bool **)

let expected_wencoding_beq x y =
  let { on_shift = on_shift0; on_none = on_none0 } = x in
  let { on_shift = on_shift1; on_none = on_none1 } = y in
  (&&) (wencoding_beq on_shift0 on_shift1) (wencoding_beq on_none0 on_none1)

(** val is_w12_encoding : coq_Z -> bool **)

let is_w12_encoding z =
  Z.ltb z
    (Z.pow (Zpos (Coq_xO Coq_xH)) (Zpos (Coq_xO (Coq_xO (Coq_xI Coq_xH)))))

(** val is_w16_encoding : coq_Z -> bool **)

let is_w16_encoding z =
  Z.ltb z
    (Z.pow (Zpos (Coq_xO Coq_xH)) (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO
      Coq_xH))))))

(** val check_wencoding : wencoding -> coq_Z -> bool **)

let check_wencoding we z =
  match we with
  | WE_allowed b -> b
  | W12_encoding -> is_w12_encoding z
  | W16_encoding -> is_w16_encoding z

(** val check_ei_kind :
    expected_wencoding -> wsize -> GRing.ComRing.sort -> bool **)

let check_ei_kind ewe sz w =
  let z = wunsigned sz w in
  (match ei_kind z with
   | EI_none -> check_wencoding ewe.on_none z
   | EI_shift -> check_wencoding ewe.on_shift z
   | _ -> true)

(** val string_of_ew : wencoding -> string **)

let string_of_ew = function
| WE_allowed b -> if b then "allowed" else "not allowed"
| W12_encoding -> "W12"
| W16_encoding -> "W16"
