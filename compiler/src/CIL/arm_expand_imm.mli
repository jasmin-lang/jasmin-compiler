open BinInt
open BinNums
open Eqtype
open Ssralg
open Strings
open Word0
open Word_ssrZ
open Wsize

type expand_immediate_kind =
| EI_none
| EI_byte
| EI_pattern
| EI_shift

val z_to_bytes : coq_Z -> ((coq_Z * coq_Z) * coq_Z) * coq_Z

val is_ei_pattern : coq_Z -> bool

val is_ei_shift : coq_Z -> bool

val ei_kind : coq_Z -> expand_immediate_kind

type wencoding =
| WE_allowed of bool
| W12_encoding
| W16_encoding

val wencoding_beq : wencoding -> wencoding -> bool

type expected_wencoding = { on_shift : wencoding; on_none : wencoding }

val expected_wencoding_beq : expected_wencoding -> expected_wencoding -> bool

val is_w12_encoding : coq_Z -> bool

val is_w16_encoding : coq_Z -> bool

val check_wencoding : wencoding -> coq_Z -> bool

val check_ei_kind : expected_wencoding -> wsize -> GRing.ComRing.sort -> bool

val string_of_ew : wencoding -> string
