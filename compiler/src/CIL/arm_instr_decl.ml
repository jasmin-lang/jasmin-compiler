open BinInt
open BinNums
open Bool
open Datatypes
open PeanoNat
open Arch_decl
open Arch_utils
open Arm_decl
open Arm_expand_imm
open Eqtype
open Fintype
open Sem_type
open Seq
open Shift_kind
open Sopn
open Ssralg
open Ssrbool
open Ssrnat
open Type
open Utils0
open Word0
open Word_ssrZ
open Wsize
open Xseq

let __ = let rec f _ = Obj.repr f in Obj.repr f

module E =
 struct
  (** val no_semantics : error **)

  let no_semantics =
    ErrSemUndef
 end

type arm_options = { set_flags : bool; is_conditional : bool;
                     has_shift : shift_kind option }

(** val set_flags : arm_options -> bool **)

let set_flags a =
  a.set_flags

(** val is_conditional : arm_options -> bool **)

let is_conditional a =
  a.is_conditional

(** val has_shift : arm_options -> shift_kind option **)

let has_shift a =
  a.has_shift

(** val arm_options_beq : arm_options -> arm_options -> bool **)

let arm_options_beq ao0 ao1 =
  (&&)
    (eq_op coq_Datatypes_bool__canonical__eqtype_Equality
      (Obj.magic ao0.set_flags) (Obj.magic ao1.set_flags))
    ((&&)
      (eq_op coq_Datatypes_bool__canonical__eqtype_Equality
        (Obj.magic ao0.is_conditional) (Obj.magic ao1.is_conditional))
      (eq_op
        (coq_Datatypes_option__canonical__eqtype_Equality
          shift_kind_shift_kind__canonical__eqtype_Equality)
        (Obj.magic ao0.has_shift) (Obj.magic ao1.has_shift)))

(** val arm_options_eq_axiom : arm_options eq_axiom **)

let arm_options_eq_axiom __top_assumption_ =
  let _evar_0_ =
    fun _set_flags_ _is_conditional_ _has_shift_ __top_assumption_0 ->
    let _evar_0_ = fun _set_flags1_ _is_conditional1_ _has_shift1_ ->
      iffP
        (arm_options_beq { set_flags = _set_flags_; is_conditional =
          _is_conditional_; has_shift = _has_shift_ } { set_flags =
          _set_flags1_; is_conditional = _is_conditional1_; has_shift =
          _has_shift1_ })
        (if arm_options_beq { set_flags = _set_flags_; is_conditional =
              _is_conditional_; has_shift = _has_shift_ } { set_flags =
              _set_flags1_; is_conditional = _is_conditional1_; has_shift =
              _has_shift1_ }
         then ReflectT
         else ReflectF)
    in
    let { set_flags = set_flags0; is_conditional = is_conditional0;
      has_shift = has_shift0 } = __top_assumption_0
    in
    _evar_0_ set_flags0 is_conditional0 has_shift0
  in
  let { set_flags = set_flags0; is_conditional = is_conditional0; has_shift =
    has_shift0 } = __top_assumption_
  in
  _evar_0_ set_flags0 is_conditional0 has_shift0

(** val eqTC_arm_options : arm_options eqTypeC **)

let eqTC_arm_options =
  { beq = arm_options_beq; ceqP = arm_options_eq_axiom }

(** val arm_options_eqType : Equality.coq_type **)

let arm_options_eqType =
  ceqT_eqType eqTC_arm_options

(** val arm_options_dec_eq : arm_options -> arm_options -> bool **)

let arm_options_dec_eq ao0 ao1 =
  let _evar_0_ = fun _ -> true in
  let _evar_0_0 = fun _ -> false in
  (match arm_options_eq_axiom ao0 ao1 with
   | ReflectT -> _evar_0_ __
   | ReflectF -> _evar_0_0 __)

(** val default_opts : arm_options **)

let default_opts =
  { set_flags = false; is_conditional = false; has_shift = None }

(** val set_is_conditional : arm_options -> arm_options **)

let set_is_conditional ao =
  { set_flags = ao.set_flags; is_conditional = true; has_shift =
    ao.has_shift }

(** val unset_is_conditional : arm_options -> arm_options **)

let unset_is_conditional ao =
  { set_flags = ao.set_flags; is_conditional = false; has_shift =
    ao.has_shift }

type halfword =
| HWB
| HWT

type arm_mnemonic =
| ADD
| ADC
| MUL
| MLA
| MLS
| SDIV
| SUB
| SBC
| RSB
| UDIV
| UMULL
| UMAAL
| UMLAL
| SMULL
| SMLAL
| SMMUL
| SMMULR
| SMUL_hw of halfword * halfword
| SMLA_hw of halfword * halfword
| SMULW_hw of halfword
| AND
| BFC
| BFI
| BIC
| EOR
| MVN
| ORR
| ASR
| LSL
| LSR
| ROR
| REV
| REV16
| REVSH
| ADR
| MOV
| MOVT
| UBFX
| UXTB
| UXTH
| SBFX
| CLZ
| CMP
| TST
| CMN
| LDR
| LDRB
| LDRH
| LDRSB
| LDRSH
| STR
| STRB
| STRH

(** val internal_halfword_beq : halfword -> halfword -> bool **)

let internal_halfword_beq x y =
  match x with
  | HWB -> (match y with
            | HWB -> true
            | HWT -> false)
  | HWT -> (match y with
            | HWB -> false
            | HWT -> true)

(** val arm_mnemonic_beq : arm_mnemonic -> arm_mnemonic -> bool **)

let arm_mnemonic_beq x y =
  match x with
  | ADD -> (match y with
            | ADD -> true
            | _ -> false)
  | ADC -> (match y with
            | ADC -> true
            | _ -> false)
  | MUL -> (match y with
            | MUL -> true
            | _ -> false)
  | MLA -> (match y with
            | MLA -> true
            | _ -> false)
  | MLS -> (match y with
            | MLS -> true
            | _ -> false)
  | SDIV -> (match y with
             | SDIV -> true
             | _ -> false)
  | SUB -> (match y with
            | SUB -> true
            | _ -> false)
  | SBC -> (match y with
            | SBC -> true
            | _ -> false)
  | RSB -> (match y with
            | RSB -> true
            | _ -> false)
  | UDIV -> (match y with
             | UDIV -> true
             | _ -> false)
  | UMULL -> (match y with
              | UMULL -> true
              | _ -> false)
  | UMAAL -> (match y with
              | UMAAL -> true
              | _ -> false)
  | UMLAL -> (match y with
              | UMLAL -> true
              | _ -> false)
  | SMULL -> (match y with
              | SMULL -> true
              | _ -> false)
  | SMLAL -> (match y with
              | SMLAL -> true
              | _ -> false)
  | SMMUL -> (match y with
              | SMMUL -> true
              | _ -> false)
  | SMMULR -> (match y with
               | SMMULR -> true
               | _ -> false)
  | SMUL_hw (x0, x1) ->
    (match y with
     | SMUL_hw (x2, x3) ->
       (&&) (internal_halfword_beq x0 x2) (internal_halfword_beq x1 x3)
     | _ -> false)
  | SMLA_hw (x0, x1) ->
    (match y with
     | SMLA_hw (x2, x3) ->
       (&&) (internal_halfword_beq x0 x2) (internal_halfword_beq x1 x3)
     | _ -> false)
  | SMULW_hw x0 ->
    (match y with
     | SMULW_hw x1 -> internal_halfword_beq x0 x1
     | _ -> false)
  | AND -> (match y with
            | AND -> true
            | _ -> false)
  | BFC -> (match y with
            | BFC -> true
            | _ -> false)
  | BFI -> (match y with
            | BFI -> true
            | _ -> false)
  | BIC -> (match y with
            | BIC -> true
            | _ -> false)
  | EOR -> (match y with
            | EOR -> true
            | _ -> false)
  | MVN -> (match y with
            | MVN -> true
            | _ -> false)
  | ORR -> (match y with
            | ORR -> true
            | _ -> false)
  | ASR -> (match y with
            | ASR -> true
            | _ -> false)
  | LSL -> (match y with
            | LSL -> true
            | _ -> false)
  | LSR -> (match y with
            | LSR -> true
            | _ -> false)
  | ROR -> (match y with
            | ROR -> true
            | _ -> false)
  | REV -> (match y with
            | REV -> true
            | _ -> false)
  | REV16 -> (match y with
              | REV16 -> true
              | _ -> false)
  | REVSH -> (match y with
              | REVSH -> true
              | _ -> false)
  | ADR -> (match y with
            | ADR -> true
            | _ -> false)
  | MOV -> (match y with
            | MOV -> true
            | _ -> false)
  | MOVT -> (match y with
             | MOVT -> true
             | _ -> false)
  | UBFX -> (match y with
             | UBFX -> true
             | _ -> false)
  | UXTB -> (match y with
             | UXTB -> true
             | _ -> false)
  | UXTH -> (match y with
             | UXTH -> true
             | _ -> false)
  | SBFX -> (match y with
             | SBFX -> true
             | _ -> false)
  | CLZ -> (match y with
            | CLZ -> true
            | _ -> false)
  | CMP -> (match y with
            | CMP -> true
            | _ -> false)
  | TST -> (match y with
            | TST -> true
            | _ -> false)
  | CMN -> (match y with
            | CMN -> true
            | _ -> false)
  | LDR -> (match y with
            | LDR -> true
            | _ -> false)
  | LDRB -> (match y with
             | LDRB -> true
             | _ -> false)
  | LDRH -> (match y with
             | LDRH -> true
             | _ -> false)
  | LDRSB -> (match y with
              | LDRSB -> true
              | _ -> false)
  | LDRSH -> (match y with
              | LDRSH -> true
              | _ -> false)
  | STR -> (match y with
            | STR -> true
            | _ -> false)
  | STRB -> (match y with
             | STRB -> true
             | _ -> false)
  | STRH -> (match y with
             | STRH -> true
             | _ -> false)

(** val arm_mnemonic_eq_dec : arm_mnemonic -> arm_mnemonic -> bool **)

let arm_mnemonic_eq_dec x y =
  let b = arm_mnemonic_beq x y in if b then true else false

(** val arm_mnemonic_eq_axiom : arm_mnemonic eq_axiom **)

let arm_mnemonic_eq_axiom =
  eq_axiom_of_scheme arm_mnemonic_beq

(** val eqTC_arm_mnemonic : arm_mnemonic eqTypeC **)

let eqTC_arm_mnemonic =
  { beq = arm_mnemonic_beq; ceqP = arm_mnemonic_eq_axiom }

(** val arm_mnemonic_eqType : Equality.coq_type **)

let arm_mnemonic_eqType =
  ceqT_eqType eqTC_arm_mnemonic

(** val arm_mnemonics : arm_mnemonic list **)

let arm_mnemonics =
  ADD :: (ADC :: (MUL :: (MLA :: (MLS :: (SDIV :: (SUB :: (SBC :: (RSB :: (UDIV :: (UMULL :: (UMAAL :: (UMLAL :: (SMULL :: (SMLAL :: (SMMUL :: (SMMULR :: ((SMUL_hw
    (HWB, HWB)) :: ((SMUL_hw (HWB, HWT)) :: ((SMUL_hw (HWT,
    HWB)) :: ((SMUL_hw (HWT, HWT)) :: ((SMLA_hw (HWB, HWB)) :: ((SMLA_hw
    (HWB, HWT)) :: ((SMLA_hw (HWT, HWB)) :: ((SMLA_hw (HWT,
    HWT)) :: ((SMULW_hw HWB) :: ((SMULW_hw
    HWT) :: (AND :: (BFC :: (BFI :: (BIC :: (EOR :: (MVN :: (ORR :: (ASR :: (LSL :: (LSR :: (ROR :: (REV :: (REV16 :: (REVSH :: (ADR :: (MOV :: (MOVT :: (UBFX :: (UXTB :: (UXTH :: (SBFX :: (CLZ :: (CMP :: (TST :: (CMN :: (LDR :: (LDRB :: (LDRH :: (LDRSB :: (LDRSH :: (STR :: (STRB :: (STRH :: [])))))))))))))))))))))))))))))))))))))))))))))))))))))))))))

(** val finTC_arm_mnemonic : arm_mnemonic finTypeC **)

let finTC_arm_mnemonic =
  { _eqC = eqTC_arm_mnemonic; cenum = arm_mnemonics }

(** val arm_mnemonic_finType : Finite.coq_type **)

let arm_mnemonic_finType =
  cfinT_finType finTC_arm_mnemonic

(** val set_flags_mnemonics : arm_mnemonic list **)

let set_flags_mnemonics =
  ADD :: (ADC :: (MUL :: (SUB :: (SBC :: (RSB :: (AND :: (BIC :: (EOR :: (MVN :: (ORR :: (ASR :: (LSL :: (LSR :: (ROR :: (MOV :: [])))))))))))))))

(** val has_shift_mnemonics : arm_mnemonic list **)

let has_shift_mnemonics =
  ADD :: (ADC :: (SUB :: (SBC :: (RSB :: (AND :: (BIC :: (EOR :: (MVN :: (ORR :: (CMP :: (TST :: (CMN :: []))))))))))))

(** val condition_mnemonics : arm_mnemonic list **)

let condition_mnemonics =
  CMP :: (TST :: [])

(** val always_has_shift_mnemonics : (arm_mnemonic * shift_kind) list **)

let always_has_shift_mnemonics =
  (UXTB, SROR) :: ((UXTH, SROR) :: [])

(** val wsize_uload_mn : (wsize * arm_mnemonic) list **)

let wsize_uload_mn =
  (U8, LDRB) :: ((U16, LDRH) :: ((U32, LDR) :: []))

(** val uload_mn_of_wsize : wsize -> arm_mnemonic option **)

let uload_mn_of_wsize ws =
  assoc wsize_wsize__canonical__eqtype_Equality (Obj.magic wsize_uload_mn)
    (Obj.magic ws)

(** val wsize_of_uload_mn : arm_mnemonic -> wsize option **)

let wsize_of_uload_mn mn =
  assoc arm_mnemonic_eqType
    (map (fun x -> ((snd (Obj.magic x)), (fst x))) wsize_uload_mn)
    (Obj.magic mn)

(** val wsize_sload_mn : (wsize * arm_mnemonic) list **)

let wsize_sload_mn =
  (U8, LDRSB) :: ((U16, LDRSH) :: [])

(** val sload_mn_of_wsize : wsize -> arm_mnemonic option **)

let sload_mn_of_wsize ws =
  assoc wsize_wsize__canonical__eqtype_Equality (Obj.magic wsize_sload_mn)
    (Obj.magic ws)

(** val wsize_of_sload_mn : arm_mnemonic -> wsize option **)

let wsize_of_sload_mn mn =
  assoc arm_mnemonic_eqType
    (map (fun x -> ((snd (Obj.magic x)), (fst x))) wsize_sload_mn)
    (Obj.magic mn)

(** val wsize_of_load_mn : arm_mnemonic -> wsize option **)

let wsize_of_load_mn mn =
  match wsize_of_uload_mn mn with
  | Some ws -> Some ws
  | None -> wsize_of_sload_mn mn

(** val wsize_store_mn : (wsize * arm_mnemonic) list **)

let wsize_store_mn =
  (U8, STRB) :: ((U16, STRH) :: ((U32, STR) :: []))

(** val store_mn_of_wsize : wsize -> arm_mnemonic option **)

let store_mn_of_wsize ws =
  assoc wsize_wsize__canonical__eqtype_Equality (Obj.magic wsize_store_mn)
    (Obj.magic ws)

(** val wsize_of_store_mn : arm_mnemonic -> wsize option **)

let wsize_of_store_mn mn =
  assoc arm_mnemonic_eqType
    (map (fun x -> ((snd (Obj.magic x)), (fst x))) wsize_store_mn)
    (Obj.magic mn)

(** val string_of_hw : halfword -> string **)

let string_of_hw = function
| HWB -> "B"
| HWT -> "T"

(** val string_of_arm_mnemonic : arm_mnemonic -> string **)

let string_of_arm_mnemonic mn =
  let with_hw = fun s hw -> (^) s (string_of_hw hw) in
  (match mn with
   | ADD -> "ADD"
   | ADC -> "ADC"
   | MUL -> "MUL"
   | MLA -> "MLA"
   | MLS -> "MLS"
   | SDIV -> "SDIV"
   | SUB -> "SUB"
   | SBC -> "SBC"
   | RSB -> "RSB"
   | UDIV -> "UDIV"
   | UMULL -> "UMULL"
   | UMAAL -> "UMAAL"
   | UMLAL -> "UMLAL"
   | SMULL -> "SMULL"
   | SMLAL -> "SMLAL"
   | SMMUL -> "SMMUL"
   | SMMULR -> "SMMULR"
   | SMUL_hw (hw0, hw1) -> with_hw (with_hw "SMUL" hw0) hw1
   | SMLA_hw (hw0, hw1) -> with_hw (with_hw "SMLA" hw0) hw1
   | SMULW_hw hw -> with_hw "SMULW" hw
   | AND -> "AND"
   | BFC -> "BFC"
   | BFI -> "BFI"
   | BIC -> "BIC"
   | EOR -> "EOR"
   | MVN -> "MVN"
   | ORR -> "ORR"
   | ASR -> "ASR"
   | LSL -> "LSL"
   | LSR -> "LSR"
   | ROR -> "ROR"
   | REV -> "REV"
   | REV16 -> "REV16"
   | REVSH -> "REVSH"
   | ADR -> "ADR"
   | MOV -> "MOV"
   | MOVT -> "MOVT"
   | UBFX -> "UBFX"
   | UXTB -> "UXTB"
   | UXTH -> "UXTH"
   | SBFX -> "SBFX"
   | CLZ -> "CLZ"
   | CMP -> "CMP"
   | TST -> "TST"
   | CMN -> "CMN"
   | LDR -> "LDR"
   | LDRB -> "LDRB"
   | LDRH -> "LDRH"
   | LDRSB -> "LDRSB"
   | LDRSH -> "LDRSH"
   | STR -> "STR"
   | STRB -> "STRB"
   | STRH -> "STRH")

type arm_op =
| ARM_op of arm_mnemonic * arm_options

(** val arm_op_beq : arm_op -> arm_op -> bool **)

let arm_op_beq op0 op1 =
  let ARM_op (mn0, ao0) = op0 in
  let ARM_op (mn1, ao1) = op1 in
  (&&) (eq_op arm_mnemonic_eqType (Obj.magic mn0) (Obj.magic mn1))
    (eq_op arm_options_eqType (Obj.magic ao0) (Obj.magic ao1))

(** val arm_op_eq_axiom : arm_op eq_axiom **)

let arm_op_eq_axiom __top_assumption_ =
  let _evar_0_ = fun mn0 ao0 __top_assumption_0 ->
    let _evar_0_ = fun mn1 ao1 ->
      iffP (arm_op_beq (ARM_op (mn0, ao0)) (ARM_op (mn1, ao1)))
        (if arm_op_beq (ARM_op (mn0, ao0)) (ARM_op (mn1, ao1))
         then ReflectT
         else ReflectF)
    in
    let ARM_op (a, a0) = __top_assumption_0 in _evar_0_ a a0
  in
  let ARM_op (a, a0) = __top_assumption_ in _evar_0_ a a0

(** val eqTC_arm_op : arm_op eqTypeC **)

let eqTC_arm_op =
  { beq = arm_op_beq; ceqP = arm_op_eq_axiom }

(** val arm_op_eqType : Equality.coq_type **)

let arm_op_eqType =
  ceqT_eqType eqTC_arm_op

(** val arm_op_dec_eq : arm_op -> arm_op -> bool **)

let arm_op_dec_eq op0 op1 =
  let _evar_0_ = fun _ -> true in
  let _evar_0_0 = fun _ -> false in
  (match arm_op_eq_axiom op0 op1 with
   | ReflectT -> _evar_0_ __
   | ReflectF -> _evar_0_0 __)

(** val ad_nz :
    (register, empty, empty, rflag, condt) Arch_decl.arg_desc list **)

let ad_nz =
  map (coq_F arm_decl) (NF :: (ZF :: []))

(** val ad_nzc :
    (register, empty, empty, rflag, condt) Arch_decl.arg_desc list **)

let ad_nzc =
  map (coq_F arm_decl) (NF :: (ZF :: (CF :: [])))

(** val ad_nzcv :
    (register, empty, empty, rflag, condt) Arch_decl.arg_desc list **)

let ad_nzcv =
  map (coq_F arm_decl) (NF :: (ZF :: (CF :: (VF :: []))))

(** val coq_NF_of_word : wsize -> GRing.ComRing.sort -> bool **)

let coq_NF_of_word =
  msb

(** val coq_ZF_of_word : wsize -> GRing.ComRing.sort -> bool **)

let coq_ZF_of_word ws w =
  eq_op
    (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality (word ws))
    w
    (GRing.zero
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule (word ws)))

(** val nzcv_of_aluop :
    wsize -> GRing.ComRing.sort -> coq_Z -> coq_Z -> sem_tuple **)

let nzcv_of_aluop ws res res_unsigned res_signed =
  Obj.magic ((Some (coq_NF_of_word ws res)), ((Some (coq_ZF_of_word ws res)),
    ((Some
    (negb
      (eq_op coq_BinNums_Z__canonical__eqtype_Equality
        (Obj.magic wunsigned ws res) (Obj.magic res_unsigned)))), (Some
    (negb
      (eq_op coq_BinNums_Z__canonical__eqtype_Equality
        (Obj.magic wsigned ws res) (Obj.magic res_signed)))))))

(** val nzcv_w_of_aluop :
    wsize -> GRing.ComRing.sort -> coq_Z -> coq_Z -> ltuple **)

let nzcv_w_of_aluop ws w wun wsi =
  merge_tuple
    (map (Obj.magic __)
      (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: [])))))
    (map (Obj.magic __) ((Coq_sword ws) :: [])) (nzcv_of_aluop ws w wun wsi) w

(** val drop_nz :
    (register, empty, empty, rflag, condt) instr_desc_t -> (register, empty,
    empty, rflag, condt) instr_desc_t **)

let drop_nz =
  idt_drop2 arm_decl

(** val drop_nzc :
    (register, empty, empty, rflag, condt) instr_desc_t -> (register, empty,
    empty, rflag, condt) instr_desc_t **)

let drop_nzc =
  idt_drop3 arm_decl

(** val drop_nzcv :
    (register, empty, empty, rflag, condt) instr_desc_t -> (register, empty,
    empty, rflag, condt) instr_desc_t **)

let drop_nzcv =
  idt_drop4 arm_decl

(** val mk_semi_cond :
    stype list -> stype list -> sem_tuple exec sem_prod -> sem_tuple exec
    sem_prod **)

let mk_semi_cond tin tout semi =
  let f0 = fun res cond ->
    if cond
    then sem_prod_const tout res
    else sem_prod_ok tout (sem_prod_tuple tout)
  in
  let f1 = sem_prod_app tin semi f0 in
  add_arguments tin (Coq_sbool :: tout) f1

(** val mk_cond :
    (register, empty, empty, rflag, condt) instr_desc_t -> (register, empty,
    empty, rflag, condt) instr_desc_t **)

let mk_cond idt =
  { id_valid = idt.id_valid; id_msb_flag = MSB_MERGE; id_tin =
    (cat idt.id_tin (Coq_sbool :: idt.id_tout)); id_in =
    (cat idt.id_in ((coq_Ea arm_decl idt.id_nargs) :: idt.id_out)); id_tout =
    idt.id_tout; id_out = idt.id_out; id_semi =
    (mk_semi_cond idt.id_tin idt.id_tout idt.id_semi); id_args_kinds =
    (map (fun x -> cat x ((CAcond :: []) :: [])) idt.id_args_kinds);
    id_nargs = (S idt.id_nargs); id_str_jas = idt.id_str_jas; id_safe =
    idt.id_safe; id_pp_asm = idt.id_pp_asm }

(** val mk_semi1_shifted :
    shift_kind -> 'a1 exec sem_prod -> 'a1 exec sem_prod **)

let mk_semi1_shifted sk semi =
  Obj.magic (fun wn shift_amount ->
    let sham = wunsigned U8 shift_amount in
    Obj.magic semi (shift_op sk arm_decl.reg_size wn sham))

(** val mk_semi2_2_shifted :
    stype -> shift_kind -> 'a1 exec sem_prod -> 'a1 exec sem_prod **)

let mk_semi2_2_shifted _ sk semi =
  Obj.magic (fun x wm shift_amount ->
    let sham = wunsigned U8 shift_amount in
    Obj.magic semi x (shift_op sk arm_decl.reg_size wm sham))

(** val mk_semi3_2_shifted :
    stype -> stype -> shift_kind -> 'a1 exec sem_prod -> 'a1 exec sem_prod **)

let mk_semi3_2_shifted _ _ sk semi =
  Obj.magic (fun x wm y shift_amount ->
    let sham = wunsigned U8 shift_amount in
    Obj.magic semi x (shift_op sk arm_decl.reg_size wm sham) y)

(** val mk_shifted :
    shift_kind -> (register, empty, empty, rflag, condt) instr_desc_t ->
    sem_tuple exec sem_prod -> (register, empty, empty, rflag, condt)
    instr_desc_t **)

let mk_shifted sk idt semi' =
  { id_valid = idt.id_valid; id_msb_flag = MSB_MERGE; id_tin =
    (cat idt.id_tin ((Coq_sword U8) :: [])); id_in =
    (cat idt.id_in ((coq_Ea arm_decl idt.id_nargs) :: [])); id_tout =
    idt.id_tout; id_out = idt.id_out; id_semi = semi'; id_args_kinds =
    (map (fun x ->
      cat x (((CAimm ((CAimmC_arm_shift_amout sk), U8)) :: []) :: []))
      idt.id_args_kinds); id_nargs = (S idt.id_nargs); id_str_jas =
    idt.id_str_jas; id_safe = idt.id_safe; id_pp_asm = idt.id_pp_asm }

(** val ak_reg_reg_imm_ : expected_wencoding -> arg_kind list list list **)

let ak_reg_reg_imm_ ew =
  ((CAreg :: []) :: ((CAreg :: []) :: (((CAimm ((CAimmC_arm_wencoding ew),
    arm_decl.reg_size)) :: []) :: []))) :: []

(** val ak_reg_reg_imm_shift :
    wsize -> shift_kind -> arg_kind list list list **)

let ak_reg_reg_imm_shift ws sk =
  ((CAreg :: []) :: ((CAreg :: []) :: (((CAimm ((CAimmC_arm_shift_amout sk),
    ws)) :: []) :: []))) :: []

(** val ak_reg_reg_reg_or_imm_ : expected_wencoding -> args_kinds list **)

let ak_reg_reg_reg_or_imm_ ew =
  cat ak_reg_reg_reg (ak_reg_reg_imm_ ew)

(** val ak_reg_reg_reg_or_imm :
    arm_options -> expected_wencoding -> i_args_kinds **)

let ak_reg_reg_reg_or_imm opts ew =
  if isSome opts.has_shift then ak_reg_reg_reg else ak_reg_reg_reg_or_imm_ ew

(** val ak_reg_imm_ : expected_wencoding -> arg_kind list list list **)

let ak_reg_imm_ ew =
  ((CAreg :: []) :: (((CAimm ((CAimmC_arm_wencoding ew),
    arm_decl.reg_size)) :: []) :: [])) :: []

(** val ak_reg_reg_or_imm_ : expected_wencoding -> args_kinds list **)

let ak_reg_reg_or_imm_ ew =
  cat ak_reg_reg (ak_reg_imm_ ew)

(** val ak_reg_reg_or_imm :
    arm_options -> expected_wencoding -> i_args_kinds **)

let ak_reg_reg_or_imm opts ew =
  if isSome opts.has_shift then ak_reg_reg else ak_reg_reg_or_imm_ ew

(** val chk_imm_accept_shift : expected_wencoding **)

let chk_imm_accept_shift =
  { on_shift = (WE_allowed true); on_none = (WE_allowed false) }

(** val chk_imm_accept_shift_w12 : arm_options -> expected_wencoding **)

let chk_imm_accept_shift_w12 opts =
  { on_shift = (WE_allowed true); on_none =
    (if opts.set_flags then WE_allowed false else W12_encoding) }

(** val chk_imm_w16_encoding : bool -> expected_wencoding **)

let chk_imm_w16_encoding opts =
  let allowed = if opts then WE_allowed false else W16_encoding in
  { on_shift = allowed; on_none = allowed }

(** val chk_imm_reject_shift : expected_wencoding **)

let chk_imm_reject_shift =
  { on_shift = (WE_allowed false); on_none = (WE_allowed false) }

(** val pp_arm_op :
    arm_mnemonic -> arm_options -> (register, empty, empty, rflag, condt)
    asm_arg list -> (register, empty, empty, rflag, condt) pp_asm_op **)

let pp_arm_op mn _ args =
  { pp_aop_name = (string_of_arm_mnemonic mn); pp_aop_ext = PP_name;
    pp_aop_args = (map (fun a -> (arm_decl.reg_size, a)) args) }

(** val arm_ADD_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let arm_ADD_semi wn wm =
  nzcv_w_of_aluop arm_decl.reg_size
    (GRing.add
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
        (word arm_decl.reg_size)) wn wm)
    (Z.add (wunsigned arm_decl.reg_size wn) (wunsigned arm_decl.reg_size wm))
    (Z.add (wsigned arm_decl.reg_size wn) (wsigned arm_decl.reg_size wm))

(** val arm_ADD_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_ADD_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = ADD in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: []));
    id_tout =
    (cat (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: []))))
      ((sreg arm_decl) :: [])); id_out =
    (cat ad_nzcv ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic arm_ADD_semi)); id_args_kinds =
    (ak_reg_reg_reg_or_imm opts (chk_imm_accept_shift_w12 opts)); id_nargs =
    (S (S (S O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn));
    id_safe = []; id_pp_asm = (pp_arm_op mn opts) }
  in
  let x0 =
    match opts.has_shift with
    | Some sk ->
      mk_shifted sk x (mk_semi2_2_shifted (sreg arm_decl) sk x.id_semi)
    | None -> x
  in
  if opts.set_flags then x0 else drop_nzcv x0

(** val arm_ADC_semi : sem_tuple -> sem_tuple -> bool -> sem_tuple **)

let arm_ADC_semi wn wm cf =
  let c = Z.b2z cf in
  nzcv_w_of_aluop arm_decl.reg_size
    (GRing.add
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
        (word arm_decl.reg_size))
      (GRing.add
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
          (word arm_decl.reg_size)) wn wm) (wrepr arm_decl.reg_size c))
    (Z.add
      (Z.add (wunsigned arm_decl.reg_size wn)
        (wunsigned arm_decl.reg_size wm)) c)
    (Z.add
      (Z.add (wsigned arm_decl.reg_size wn) (wsigned arm_decl.reg_size wm)) c)

(** val arm_ADC_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_ADC_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = ADC in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: (Coq_sbool :: [])) in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: ((coq_F
                                                                   arm_decl
                                                                   CF) :: [])));
    id_tout =
    (cat (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: []))))
      ((sreg arm_decl) :: [])); id_out =
    (cat ad_nzcv ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic arm_ADC_semi)); id_args_kinds =
    (ak_reg_reg_reg_or_imm opts chk_imm_accept_shift); id_nargs = (S (S (S
    O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
    id_pp_asm = (pp_arm_op mn opts) }
  in
  let x0 =
    match opts.has_shift with
    | Some sk ->
      mk_shifted sk x
        (mk_semi3_2_shifted (sreg arm_decl) Coq_sbool sk x.id_semi)
    | None -> x
  in
  if opts.set_flags then x0 else drop_nzcv x0

(** val arm_MUL_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let arm_MUL_semi wn wm =
  let res =
    GRing.mul
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
        (word arm_decl.reg_size)) wn wm
  in
  Obj.magic ((Some (coq_NF_of_word arm_decl.reg_size res)), ((Some
    (coq_ZF_of_word arm_decl.reg_size res)), res))

(** val arm_MUL_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_MUL_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = MUL in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: []));
    id_tout = (cat (Coq_sbool :: (Coq_sbool :: [])) ((sreg arm_decl) :: []));
    id_out = (cat ad_nz ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic arm_MUL_semi)); id_args_kinds =
    ak_reg_reg_reg; id_nargs = (S (S (S O))); id_str_jas =
    (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
    (pp_arm_op mn opts) }
  in
  if opts.set_flags then x else drop_nz x

(** val arm_MLA_semi : sem_tuple -> sem_tuple -> sem_tuple -> sem_tuple **)

let arm_MLA_semi wn wm wa =
  GRing.add
    (GRing.SemiRing.Exports.coq_GRing_SemiRing__to__GRing_Nmodule
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
        (word arm_decl.reg_size)))
    (GRing.mul
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
        (word arm_decl.reg_size)) wn wm) wa

(** val arm_MLA_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_MLA_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = MLA in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: ((sreg arm_decl) :: [])) in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: ((coq_Ea
                                                                 arm_decl (S
                                                                 (S (S O)))) :: [])));
  id_tout = ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []);
  id_semi = (sem_prod_ok tin (Obj.magic arm_MLA_semi)); id_args_kinds =
  ak_reg_reg_reg_reg; id_nargs = (S (S (S (S O)))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
  (pp_arm_op mn opts) }

(** val arm_MLS_semi : sem_tuple -> sem_tuple -> sem_tuple -> sem_tuple **)

let arm_MLS_semi wn wm wa =
  GRing.add
    (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
      (word arm_decl.reg_size)) wa
    (GRing.opp
      (GRing.Ring.Exports.join_GRing_Ring_between_GRing_SemiRing_and_GRing_Zmodule
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Ring
          (word arm_decl.reg_size)))
      (GRing.mul
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
          (word arm_decl.reg_size)) wn wm))

(** val arm_MLS_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_MLS_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = MLS in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: ((sreg arm_decl) :: [])) in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: ((coq_Ea
                                                                 arm_decl (S
                                                                 (S (S O)))) :: [])));
  id_tout = ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []);
  id_semi = (sem_prod_ok tin (Obj.magic arm_MLS_semi)); id_args_kinds =
  ak_reg_reg_reg_reg; id_nargs = (S (S (S (S O)))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
  (pp_arm_op mn opts) }

(** val arm_SDIV_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let arm_SDIV_semi wn wm =
  wdivi arm_decl.reg_size wn wm

(** val arm_SDIV_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_SDIV_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = SDIV in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: [])); id_tout =
  ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic arm_SDIV_semi)); id_args_kinds =
  ak_reg_reg_reg; id_nargs = (S (S (S O))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
  (pp_arm_op mn opts) }

(** val arm_SUB_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let arm_SUB_semi wn wm =
  let wmnot = wnot arm_decl.reg_size wm in
  nzcv_w_of_aluop arm_decl.reg_size
    (GRing.add
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
        (word arm_decl.reg_size))
      (GRing.add
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
          (word arm_decl.reg_size)) wn wmnot)
      (GRing.one
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
          (word arm_decl.reg_size))))
    (Z.add
      (Z.add (wunsigned arm_decl.reg_size wn)
        (wunsigned arm_decl.reg_size wmnot)) (Zpos Coq_xH))
    (Z.add
      (Z.add (wsigned arm_decl.reg_size wn) (wsigned arm_decl.reg_size wmnot))
      (Zpos Coq_xH))

(** val arm_SUB_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_SUB_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = SUB in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: []));
    id_tout =
    (cat (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: []))))
      ((sreg arm_decl) :: [])); id_out =
    (cat ad_nzcv ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic arm_SUB_semi)); id_args_kinds =
    (ak_reg_reg_reg_or_imm opts (chk_imm_accept_shift_w12 opts)); id_nargs =
    (S (S (S O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn));
    id_safe = []; id_pp_asm = (pp_arm_op mn opts) }
  in
  let x0 =
    match opts.has_shift with
    | Some sk ->
      mk_shifted sk x (mk_semi2_2_shifted (sreg arm_decl) sk x.id_semi)
    | None -> x
  in
  if opts.set_flags then x0 else drop_nzcv x0

(** val arm_SBC_semi : sem_tuple -> sem_tuple -> bool -> sem_tuple **)

let arm_SBC_semi wn wm cf =
  arm_ADC_semi wn (wnot arm_decl.reg_size wm) cf

(** val arm_SBC_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_SBC_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = SBC in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: (Coq_sbool :: [])) in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: ((coq_F
                                                                   arm_decl
                                                                   CF) :: [])));
    id_tout =
    (cat (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: []))))
      ((sreg arm_decl) :: [])); id_out =
    (cat ad_nzcv ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic arm_SBC_semi)); id_args_kinds =
    (ak_reg_reg_reg_or_imm opts chk_imm_accept_shift); id_nargs = (S (S (S
    O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
    id_pp_asm = (pp_arm_op mn opts) }
  in
  let x0 =
    match opts.has_shift with
    | Some sk ->
      mk_shifted sk x
        (mk_semi3_2_shifted (sreg arm_decl) Coq_sbool sk x.id_semi)
    | None -> x
  in
  if opts.set_flags then x0 else drop_nzcv x0

(** val arm_RSB_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_RSB_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = RSB in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  let arm_RSB_semi = fun wn wm -> arm_SUB_semi wm wn in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: []));
    id_tout =
    (cat (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: []))))
      ((sreg arm_decl) :: [])); id_out =
    (cat ad_nzcv ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic arm_RSB_semi)); id_args_kinds =
    (ak_reg_reg_reg_or_imm opts chk_imm_accept_shift); id_nargs = (S (S (S
    O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
    id_pp_asm = (pp_arm_op mn opts) }
  in
  let x0 =
    match opts.has_shift with
    | Some sk ->
      mk_shifted sk x (mk_semi2_2_shifted (sreg arm_decl) sk x.id_semi)
    | None -> x
  in
  if opts.set_flags then x0 else drop_nzcv x0

(** val arm_UDIV_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let arm_UDIV_semi wn wm =
  wdiv arm_decl.reg_size wn wm

(** val arm_UDIV_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_UDIV_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = UDIV in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: [])); id_tout =
  ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic arm_UDIV_semi)); id_args_kinds =
  ak_reg_reg_reg; id_nargs = (S (S (S O))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
  (pp_arm_op mn opts) }

(** val arm_UMULL_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let arm_UMULL_semi wn wm =
  let (hi, lo) = wumul arm_decl.reg_size wn wm in Obj.magic (lo, hi)

(** val arm_UMULL_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_UMULL_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = UMULL in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl (S (S O))) :: ((coq_Ea arm_decl (S (S (S O)))) :: []));
  id_tout = ((sreg arm_decl) :: ((sreg arm_decl) :: [])); id_out =
  ((coq_Ea arm_decl O) :: ((coq_Ea arm_decl (S O)) :: [])); id_semi =
  (sem_prod_ok tin (Obj.magic arm_UMULL_semi)); id_args_kinds =
  ak_reg_reg_reg_reg; id_nargs = (S (S (S (S O)))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
  (pp_arm_op mn opts) }

(** val arm_UMAAL_semi :
    sem_tuple -> sem_tuple -> sem_tuple -> sem_tuple -> sem_tuple **)

let arm_UMAAL_semi wa wb wn wm =
  let r =
    Z.add
      (Z.add (wunsigned arm_decl.reg_size wa)
        (wunsigned arm_decl.reg_size wb))
      (Z.mul (wunsigned arm_decl.reg_size wn)
        (wunsigned arm_decl.reg_size wm))
  in
  Obj.magic ((wrepr arm_decl.reg_size r), (high_bits arm_decl.reg_size r))

(** val arm_UMAAL_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_UMAAL_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = UMAAL in
  let tin =
    (sreg arm_decl) :: ((sreg arm_decl) :: ((sreg arm_decl) :: ((sreg
                                                                  arm_decl) :: [])))
  in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl O) :: ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S
                                                         O))) :: ((coq_Ea
                                                                    arm_decl
                                                                    (S (S (S
                                                                    O)))) :: []))));
  id_tout = ((sreg arm_decl) :: ((sreg arm_decl) :: [])); id_out =
  ((coq_Ea arm_decl O) :: ((coq_Ea arm_decl (S O)) :: [])); id_semi =
  (sem_prod_ok tin (Obj.magic arm_UMAAL_semi)); id_args_kinds =
  ak_reg_reg_reg_reg; id_nargs = (S (S (S (S O)))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
  (pp_arm_op mn opts) }

(** val arm_UMLAL_semi :
    sem_tuple -> sem_tuple -> sem_tuple -> sem_tuple -> sem_tuple **)

let arm_UMLAL_semi dlo dhi wn wm =
  let (hi, lo) = wumul arm_decl.reg_size wn wm in
  Obj.magic wdaddu arm_decl.reg_size dhi dlo hi lo

(** val arm_UMLAL_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_UMLAL_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = UMLAL in
  let tin =
    (sreg arm_decl) :: ((sreg arm_decl) :: ((sreg arm_decl) :: ((sreg
                                                                  arm_decl) :: [])))
  in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl O) :: ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S
                                                         O))) :: ((coq_Ea
                                                                    arm_decl
                                                                    (S (S (S
                                                                    O)))) :: []))));
  id_tout = ((sreg arm_decl) :: ((sreg arm_decl) :: [])); id_out =
  ((coq_Ea arm_decl O) :: ((coq_Ea arm_decl (S O)) :: [])); id_semi =
  (sem_prod_ok tin (Obj.magic arm_UMLAL_semi)); id_args_kinds =
  ak_reg_reg_reg_reg; id_nargs = (S (S (S (S O)))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
  (pp_arm_op mn opts) }

(** val arm_SMULL_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let arm_SMULL_semi wn wm =
  let (hi, lo) = wsmul arm_decl.reg_size wn wm in Obj.magic (lo, hi)

(** val arm_SMULL_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_SMULL_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = SMULL in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl (S (S O))) :: ((coq_Ea arm_decl (S (S (S O)))) :: []));
  id_tout = ((sreg arm_decl) :: ((sreg arm_decl) :: [])); id_out =
  ((coq_Ea arm_decl O) :: ((coq_Ea arm_decl (S O)) :: [])); id_semi =
  (sem_prod_ok tin (Obj.magic arm_SMULL_semi)); id_args_kinds =
  ak_reg_reg_reg_reg; id_nargs = (S (S (S (S O)))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
  (pp_arm_op mn opts) }

(** val arm_SMLAL_semi :
    sem_tuple -> sem_tuple -> sem_tuple -> sem_tuple -> sem_tuple **)

let arm_SMLAL_semi dlo dhi wn wm =
  let (hi, lo) = wsmul arm_decl.reg_size wn wm in
  Obj.magic wdadds arm_decl.reg_size dhi dlo hi lo

(** val arm_SMLAL_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_SMLAL_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = SMLAL in
  let tin =
    (sreg arm_decl) :: ((sreg arm_decl) :: ((sreg arm_decl) :: ((sreg
                                                                  arm_decl) :: [])))
  in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl O) :: ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S
                                                         O))) :: ((coq_Ea
                                                                    arm_decl
                                                                    (S (S (S
                                                                    O)))) :: []))));
  id_tout = ((sreg arm_decl) :: ((sreg arm_decl) :: [])); id_out =
  ((coq_Ea arm_decl O) :: ((coq_Ea arm_decl (S O)) :: [])); id_semi =
  (sem_prod_ok tin (Obj.magic arm_SMLAL_semi)); id_args_kinds =
  ak_reg_reg_reg_reg; id_nargs = (S (S (S (S O)))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
  (pp_arm_op mn opts) }

(** val arm_SMMUL_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let arm_SMMUL_semi wn wm =
  wmulhs arm_decl.reg_size wn wm

(** val arm_SMMUL_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_SMMUL_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = SMMUL in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: [])); id_tout =
  ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic arm_SMMUL_semi)); id_args_kinds =
  ak_reg_reg_reg; id_nargs = (S (S (S O))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
  (pp_arm_op mn opts) }

(** val arm_SMMULR_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let arm_SMMULR_semi wn wm =
  high_bits arm_decl.reg_size
    (Z.add
      (Z.mul (wsigned arm_decl.reg_size wn) (wsigned arm_decl.reg_size wm))
      (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
      (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
      (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
      (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
      Coq_xH)))))))))))))))))))))))))))))))))

(** val arm_SMMULR_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_SMMULR_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = SMMULR in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: [])); id_tout =
  ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic arm_SMMULR_semi)); id_args_kinds =
  ak_reg_reg_reg; id_nargs = (S (S (S O))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
  (pp_arm_op mn opts) }

(** val get_hw :
    halfword -> (register, empty, empty, rflag, condt) wreg ->
    GRing.ComRing.sort **)

let get_hw hw x =
  match split_vec arm_decl.reg_size (S (S (S (S (S (S (S (S (S (S (S (S (S (S
          (S (S O)))))))))))))))) x with
  | [] ->
    GRing.zero
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule (word U16))
  | lo :: l ->
    (match l with
     | [] ->
       GRing.zero
         (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
           (word U16))
     | hi :: l0 ->
       (match l0 with
        | [] -> (match hw with
                 | HWB -> Obj.magic lo
                 | HWT -> Obj.magic hi)
        | _ :: _ ->
          GRing.zero
            (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
              (word U16))))

(** val arm_smul_hw_semi :
    halfword -> halfword -> (register, empty, empty, rflag, condt) wreg ->
    (register, empty, empty, rflag, condt) wreg -> (register, empty, empty,
    rflag, condt) wreg **)

let arm_smul_hw_semi hwn hwm wn wm =
  let n = get_hw hwn wn in
  let m = get_hw hwm wm in
  let r = Z.mul (wsigned U16 n) (wsigned U16 m) in wrepr U32 r

(** val arm_smul_hw_instr :
    arm_options -> halfword -> halfword -> (register, empty, empty, rflag,
    condt) instr_desc_t **)

let arm_smul_hw_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  (fun hwn hwm ->
  let mn = SMUL_hw (hwn, hwm) in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  let semi = arm_smul_hw_semi hwn hwm in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: [])); id_tout =
  ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic semi)); id_args_kinds = ak_reg_reg_reg;
  id_nargs = (S (S (S O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn));
  id_safe = []; id_pp_asm = (pp_arm_op mn opts) })

(** val arm_smla_hw_semi :
    halfword -> halfword -> (register, empty, empty, rflag, condt) wreg ->
    (register, empty, empty, rflag, condt) wreg -> (register, empty, empty,
    rflag, condt) wreg -> (register, empty, empty, rflag, condt) wreg **)

let arm_smla_hw_semi hwn hwm wn wm acc =
  let n = get_hw hwn wn in
  let m = get_hw hwm wm in
  let r =
    Z.add (Z.mul (wsigned U16 n) (wsigned U16 m))
      (wsigned arm_decl.reg_size acc)
  in
  wrepr U32 r

(** val arm_smla_hw_instr :
    arm_options -> halfword -> halfword -> (register, empty, empty, rflag,
    condt) instr_desc_t **)

let arm_smla_hw_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  (fun hwn hwm ->
  let mn = SMLA_hw (hwn, hwm) in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: ((sreg arm_decl) :: [])) in
  let semi = arm_smla_hw_semi hwn hwm in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: ((coq_Ea
                                                                 arm_decl (S
                                                                 (S (S O)))) :: [])));
  id_tout = ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []);
  id_semi = (sem_prod_ok tin (Obj.magic semi)); id_args_kinds =
  ak_reg_reg_reg_reg; id_nargs = (S (S (S (S O)))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
  (pp_arm_op mn opts) })

(** val arm_smulw_hw_semi :
    halfword -> (register, empty, empty, rflag, condt) wreg -> (register,
    empty, empty, rflag, condt) wreg -> (register, empty, empty, rflag,
    condt) wreg **)

let arm_smulw_hw_semi hw wn wm =
  let m = get_hw hw wm in
  let res = Z.mul (wsigned arm_decl.reg_size wn) (wsigned U16 m) in
  let w = wrepr U64 res in
  winit U32 (fun i ->
    wbit_n U64 w
      (addn i (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
        O))))))))))))))))))

(** val arm_smulw_hw_instr :
    arm_options -> halfword -> (register, empty, empty, rflag, condt)
    instr_desc_t **)

let arm_smulw_hw_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  (fun hw ->
  let mn = SMULW_hw hw in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  let semi = arm_smulw_hw_semi hw in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: [])); id_tout =
  ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic semi)); id_args_kinds = ak_reg_reg_reg;
  id_nargs = (S (S (S O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn));
  id_safe = []; id_pp_asm = (pp_arm_op mn opts) })

(** val arm_bitwise_semi :
    wsize -> (GRing.ComRing.sort -> GRing.ComRing.sort) ->
    (GRing.ComRing.sort -> GRing.ComRing.sort) -> (GRing.ComRing.sort ->
    GRing.ComRing.sort -> GRing.ComRing.sort) -> sem_tuple -> sem_tuple ->
    sem_tuple **)

let arm_bitwise_semi ws op0 op1 op wn wm =
  let res = op (op0 wn) (op1 wm) in
  Obj.magic ((Some (coq_NF_of_word ws res)), ((Some (coq_ZF_of_word ws res)),
    (None, res)))

(** val arm_AND_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_AND_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = AND in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  let semi =
    arm_bitwise_semi arm_decl.reg_size (fun x -> x) (fun x -> x)
      (wand arm_decl.reg_size)
  in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin =
    ((sreg arm_decl) :: ((sreg arm_decl) :: [])); id_in =
    ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: []));
    id_tout =
    (cat (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: [])))
      ((sreg arm_decl) :: [])); id_out =
    (cat ad_nzc ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic semi)); id_args_kinds =
    (ak_reg_reg_reg_or_imm opts chk_imm_reject_shift); id_nargs = (S (S (S
    O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
    id_pp_asm = (pp_arm_op mn opts) }
  in
  let x0 =
    match opts.has_shift with
    | Some sk ->
      mk_shifted sk x (mk_semi2_2_shifted (sreg arm_decl) sk x.id_semi)
    | None -> x
  in
  if opts.set_flags then x0 else drop_nzc x0

(** val arm_BFC_semi :
    (register, empty, empty, rflag, condt) wreg -> GRing.ComRing.sort ->
    GRing.ComRing.sort -> (register, empty, empty, rflag, condt) wreg exec **)

let arm_BFC_semi x lsb width =
  let lsbit = wunsigned U8 lsb in
  let nbits = wunsigned U8 width in
  if Z.ltb lsbit (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH))))))
  then if Z.leb (Zpos Coq_xH) nbits
       then if Z.leb nbits
                 (Z.sub (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
                   Coq_xH)))))) lsbit)
            then let msbit = Z.sub (Z.add lsbit nbits) (Zpos Coq_xH) in
                 let mk = fun i ->
                   if (&&) (Nat.leb (Z.to_nat lsbit) i)
                        (Nat.leb i (Z.to_nat msbit))
                   then false
                   else wbit_n arm_decl.reg_size x i
                 in
                 Ok (winit arm_decl.reg_size mk)
            else Error E.no_semantics
       else Error E.no_semantics
  else Error E.no_semantics

(** val arm_BFC_semi_sc : safe_cond list **)

let arm_BFC_semi_sc =
  (ULt (U8, (S O), (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
    Coq_xH)))))))) :: ((UGe (U8, (Zpos Coq_xH), (S (S O)))) :: ((UaddLe (U8,
    (S (S O)), (S O), (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
    Coq_xH)))))))) :: []))

(** val arm_BFC_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_BFC_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = BFC in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin =
  ((sreg arm_decl) :: ((Coq_sword U8) :: ((Coq_sword U8) :: []))); id_in =
  ((coq_Ea arm_decl O) :: ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S
                                                         O))) :: [])));
  id_tout = ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []);
  id_semi = (Obj.magic arm_BFC_semi); id_args_kinds = ak_reg_imm8_imm8;
  id_nargs = (S (S (S O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn));
  id_safe = arm_BFC_semi_sc; id_pp_asm = (pp_arm_op mn opts) }

(** val arm_BFI_semi :
    (register, empty, empty, rflag, condt) wreg -> (register, empty, empty,
    rflag, condt) wreg -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    (register, empty, empty, rflag, condt) wreg exec **)

let arm_BFI_semi x y lsb width =
  let lsbit = wunsigned U8 lsb in
  let nbits = wunsigned U8 width in
  if Z.ltb lsbit (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH))))))
  then if Z.leb (Zpos Coq_xH) nbits
       then if Z.leb nbits
                 (Z.sub (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
                   Coq_xH)))))) lsbit)
            then let msbit = Z.sub (Z.add lsbit nbits) (Zpos Coq_xH) in
                 let mk = fun i ->
                   if (&&) (Nat.leb (Z.to_nat lsbit) i)
                        (Nat.leb i (Z.to_nat msbit))
                   then wbit_n arm_decl.reg_size y (subn i (Z.to_nat lsbit))
                   else wbit_n arm_decl.reg_size x i
                 in
                 Ok (winit arm_decl.reg_size mk)
            else Error E.no_semantics
       else Error E.no_semantics
  else Error E.no_semantics

(** val arm_BFI_semi_sc : safe_cond list **)

let arm_BFI_semi_sc =
  (ULt (U8, (S (S O)), (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
    Coq_xH)))))))) :: ((UGe (U8, (Zpos Coq_xH), (S (S (S O))))) :: ((UaddLe
    (U8, (S (S (S O))), (S (S O)), (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO
    (Coq_xO Coq_xH)))))))) :: []))

(** val arm_BFI_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_BFI_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = BFI in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin =
  ((sreg arm_decl) :: ((sreg arm_decl) :: ((Coq_sword U8) :: ((Coq_sword
  U8) :: [])))); id_in =
  ((coq_Ea arm_decl O) :: ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S
                                                         O))) :: ((coq_Ea
                                                                    arm_decl
                                                                    (S (S (S
                                                                    O)))) :: []))));
  id_tout = ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []);
  id_semi = (Obj.magic arm_BFI_semi); id_args_kinds = ak_reg_reg_imm8_imm8;
  id_nargs = (S (S (S (S O)))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = arm_BFI_semi_sc; id_pp_asm =
  (pp_arm_op mn opts) }

(** val arm_BIC_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_BIC_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = BIC in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  let semi =
    arm_bitwise_semi arm_decl.reg_size (fun x -> x) (wnot arm_decl.reg_size)
      (wand arm_decl.reg_size)
  in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: []));
    id_tout =
    (cat (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: [])))
      ((sreg arm_decl) :: [])); id_out =
    (cat ad_nzc ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic semi)); id_args_kinds =
    (ak_reg_reg_reg_or_imm opts chk_imm_reject_shift); id_nargs = (S (S (S
    O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
    id_pp_asm = (pp_arm_op mn opts) }
  in
  let x0 =
    match opts.has_shift with
    | Some sk ->
      mk_shifted sk x (mk_semi2_2_shifted (sreg arm_decl) sk x.id_semi)
    | None -> x
  in
  if opts.set_flags then x0 else drop_nzc x0

(** val arm_EOR_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_EOR_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = EOR in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  let semi =
    arm_bitwise_semi arm_decl.reg_size (fun x -> x) (fun x -> x)
      (wxor arm_decl.reg_size)
  in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: []));
    id_tout =
    (cat (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: [])))
      ((sreg arm_decl) :: [])); id_out =
    (cat ad_nzc ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic semi)); id_args_kinds =
    (ak_reg_reg_reg_or_imm opts chk_imm_reject_shift); id_nargs = (S (S (S
    O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
    id_pp_asm = (pp_arm_op mn opts) }
  in
  let x0 =
    match opts.has_shift with
    | Some sk ->
      mk_shifted sk x (mk_semi2_2_shifted (sreg arm_decl) sk x.id_semi)
    | None -> x
  in
  if opts.set_flags then x0 else drop_nzc x0

(** val arm_MVN_semi : sem_tuple -> sem_tuple **)

let arm_MVN_semi wn =
  let res = wnot arm_decl.reg_size wn in
  Obj.magic ((Some (coq_NF_of_word arm_decl.reg_size res)), ((Some
    (coq_ZF_of_word arm_decl.reg_size res)), (None, res)))

(** val arm_MVN_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_MVN_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = MVN in
  let tin = (sreg arm_decl) :: [] in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl (S O)) :: []); id_tout =
    (cat (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: [])))
      ((sreg arm_decl) :: [])); id_out =
    (cat ad_nzc ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic arm_MVN_semi)); id_args_kinds =
    (ak_reg_reg_or_imm opts chk_imm_reject_shift); id_nargs = (S (S O));
    id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
    id_pp_asm = (pp_arm_op mn opts) }
  in
  let x0 =
    match opts.has_shift with
    | Some sk -> mk_shifted sk x (mk_semi1_shifted sk x.id_semi)
    | None -> x
  in
  if opts.set_flags then x0 else drop_nzc x0

(** val arm_ORR_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_ORR_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = ORR in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  let semi =
    arm_bitwise_semi arm_decl.reg_size (fun x -> x) (fun x -> x)
      (wor arm_decl.reg_size)
  in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: []));
    id_tout =
    (cat (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: [])))
      ((sreg arm_decl) :: [])); id_out =
    (cat ad_nzc ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic semi)); id_args_kinds =
    (ak_reg_reg_reg_or_imm opts chk_imm_reject_shift); id_nargs = (S (S (S
    O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
    id_pp_asm = (pp_arm_op mn opts) }
  in
  let x0 =
    match opts.has_shift with
    | Some sk ->
      mk_shifted sk x (mk_semi2_2_shifted (sreg arm_decl) sk x.id_semi)
    | None -> x
  in
  if opts.set_flags then x0 else drop_nzc x0

(** val arm_ASR_semi : sem_tuple -> GRing.ComRing.sort -> sem_tuple **)

let arm_ASR_semi wn wsham =
  let sham = wunsigned U8 wsham in
  let res = wsar arm_decl.reg_size wn sham in
  Obj.magic ((Some (coq_NF_of_word arm_decl.reg_size res)), ((Some
    (coq_ZF_of_word arm_decl.reg_size res)), ((Some
    (msb arm_decl.reg_size res)), res)))

(** val arm_ASR_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_ASR_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = ASR in
  let tin = (sreg arm_decl) :: ((Coq_sword U8) :: []) in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: []));
    id_tout =
    (cat (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: [])))
      ((sreg arm_decl) :: [])); id_out =
    (cat ad_nzc ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic arm_ASR_semi)); id_args_kinds =
    (cat ak_reg_reg_reg (ak_reg_reg_imm_shift U8 SASR)); id_nargs = (S (S (S
    O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
    id_pp_asm = (pp_arm_op mn opts) }
  in
  if opts.set_flags then x else drop_nzc x

(** val arm_LSL_semi : sem_tuple -> GRing.ComRing.sort -> sem_tuple **)

let arm_LSL_semi wn wsham =
  let sham = wunsigned U8 wsham in
  let res = wshl arm_decl.reg_size wn sham in
  Obj.magic ((Some (coq_NF_of_word arm_decl.reg_size res)), ((Some
    (coq_ZF_of_word arm_decl.reg_size res)), ((Some
    (msb arm_decl.reg_size res)), res)))

(** val arm_LSL_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_LSL_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = LSL in
  let tin = (sreg arm_decl) :: ((Coq_sword U8) :: []) in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: []));
    id_tout =
    (cat (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: [])))
      ((sreg arm_decl) :: [])); id_out =
    (cat ad_nzc ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic arm_LSL_semi)); id_args_kinds =
    (cat ak_reg_reg_reg (ak_reg_reg_imm_shift U8 SLSL)); id_nargs = (S (S (S
    O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
    id_pp_asm = (pp_arm_op mn opts) }
  in
  if opts.set_flags then x else drop_nzc x

(** val arm_LSR_semi : sem_tuple -> GRing.ComRing.sort -> sem_tuple **)

let arm_LSR_semi wn wsham =
  let sham = wunsigned U8 wsham in
  let res = wshr arm_decl.reg_size wn sham in
  Obj.magic ((Some (coq_NF_of_word arm_decl.reg_size res)), ((Some
    (coq_ZF_of_word arm_decl.reg_size res)), ((Some
    (msb arm_decl.reg_size res)), res)))

(** val arm_LSR_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_LSR_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = LSR in
  let tin = (sreg arm_decl) :: ((Coq_sword U8) :: []) in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: []));
    id_tout =
    (cat (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: [])))
      ((sreg arm_decl) :: [])); id_out =
    (cat ad_nzc ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic arm_LSR_semi)); id_args_kinds =
    (cat ak_reg_reg_reg (ak_reg_reg_imm_shift U8 SLSR)); id_nargs = (S (S (S
    O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
    id_pp_asm = (pp_arm_op mn opts) }
  in
  if opts.set_flags then x else drop_nzc x

(** val arm_ROR_semi : sem_tuple -> GRing.ComRing.sort -> sem_tuple **)

let arm_ROR_semi wn wsham =
  let sham = wunsigned U8 wsham in
  let res = wror arm_decl.reg_size wn sham in
  Obj.magic ((Some (coq_NF_of_word arm_decl.reg_size res)), ((Some
    (coq_ZF_of_word arm_decl.reg_size res)), ((Some
    (msb arm_decl.reg_size res)), res)))

(** val arm_ROR_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_ROR_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = ROR in
  let tin = (sreg arm_decl) :: ((Coq_sword U8) :: []) in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: []));
    id_tout =
    (cat (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: [])))
      ((sreg arm_decl) :: [])); id_out =
    (cat ad_nzc ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic arm_ROR_semi)); id_args_kinds =
    (cat ak_reg_reg_reg (ak_reg_reg_imm_shift U8 SROR)); id_nargs = (S (S (S
    O))); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
    id_pp_asm = (pp_arm_op mn opts) }
  in
  if opts.set_flags then x else drop_nzc x

(** val mk_rev_instr :
    arm_options -> arm_mnemonic -> sem_tuple sem_prod -> (register, empty,
    empty, rflag, condt) instr_desc_t **)

let mk_rev_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  (fun mn semi ->
  let tin = (sreg arm_decl) :: [] in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl (S O)) :: []); id_tout = ((sreg arm_decl) :: []);
  id_out = ((coq_Ea arm_decl O) :: []); id_semi = (sem_prod_ok tin semi);
  id_args_kinds = ak_reg_reg; id_nargs = (S (S O)); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
  (pp_arm_op mn opts) })

(** val arm_REV_semi : sem_tuple -> sem_tuple **)

let arm_REV_semi w =
  wbswap arm_decl.reg_size w

(** val arm_REV16_semi : sem_tuple -> sem_tuple **)

let arm_REV16_semi w =
  lift1_vec U16 (wbswap U16) U32 w

(** val arm_REVSH_semi : sem_tuple -> sem_tuple **)

let arm_REVSH_semi w =
  sign_extend U32 U16 (wbswap U16 (zero_extend U16 arm_decl.reg_size w))

(** val arm_REV_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_REV_instr opts =
  mk_rev_instr opts REV (Obj.magic arm_REV_semi)

(** val arm_REV16_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_REV16_instr opts =
  mk_rev_instr opts REV16 (Obj.magic arm_REV16_semi)

(** val arm_REVSH_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_REVSH_instr opts =
  mk_rev_instr opts REVSH (Obj.magic arm_REVSH_semi)

(** val arm_ADR_semi : sem_tuple -> sem_tuple **)

let arm_ADR_semi wn =
  wn

(** val arm_ADR_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_ADR_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = ADR in
  let tin = (sreg arm_decl) :: [] in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ec arm_decl (S O)) :: []); id_tout = ((sreg arm_decl) :: []);
  id_out = ((coq_Ea arm_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic arm_ADR_semi)); id_args_kinds = ak_reg_addr;
  id_nargs = (S (S O)); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn));
  id_safe = []; id_pp_asm = (pp_arm_op mn opts) }

(** val arm_MOV_semi : sem_tuple -> sem_tuple **)

let arm_MOV_semi wn =
  Obj.magic ((Some (coq_NF_of_word arm_decl.reg_size wn)), ((Some
    (coq_ZF_of_word arm_decl.reg_size wn)), (None, wn)))

(** val arm_MOV_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_MOV_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = MOV in
  let tin = (sreg arm_decl) :: [] in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl (S O)) :: []); id_tout =
    (cat (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: [])))
      ((sreg arm_decl) :: [])); id_out =
    (cat ad_nzc ((coq_Ea arm_decl O) :: [])); id_semi =
    (sem_prod_ok tin (Obj.magic arm_MOV_semi)); id_args_kinds =
    (cat ak_reg_reg (ak_reg_imm_ (chk_imm_w16_encoding opts.set_flags)));
    id_nargs = (S (S O)); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn));
    id_safe = []; id_pp_asm = (pp_arm_op mn opts) }
  in
  if opts.set_flags then x else drop_nzc x

(** val arm_MOVT_semi : sem_tuple -> GRing.ComRing.sort -> sem_tuple **)

let arm_MOVT_semi wn wm =
  let hi =
    wshl arm_decl.reg_size (zero_extend arm_decl.reg_size U16 wm) (Zpos
      (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH)))))
  in
  let mask = zero_extend arm_decl.reg_size U16 (wrepr U16 (Zneg Coq_xH)) in
  wor arm_decl.reg_size hi (wand arm_decl.reg_size wn mask)

(** val arm_MOVT_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_MOVT_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = MOVT in
  let tin = (sreg arm_decl) :: ((Coq_sword U16) :: []) in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl O) :: ((coq_Ea arm_decl (S O)) :: [])); id_tout =
  ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic arm_MOVT_semi)); id_args_kinds =
  (((CAreg :: []) :: (((coq_CAimm_sz U16) :: []) :: [])) :: []); id_nargs =
  (S (S O)); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
  id_pp_asm = (pp_arm_op mn opts) }

(** val bit_field_extract_semi :
    ((register, empty, empty, rflag, condt) wreg -> coq_Z -> (register,
    empty, empty, rflag, condt) wreg) -> (register, empty, empty, rflag,
    condt) wreg -> GRing.ComRing.sort -> GRing.ComRing.sort -> (register,
    empty, empty, rflag, condt) wreg exec **)

let bit_field_extract_semi shr wn widx wwidth =
  let idx = wunsigned U8 widx in
  let width = wunsigned U8 wwidth in
  if (&&) (Z.leb (Zpos Coq_xH) width)
       (Z.ltb width
         (Z.sub (Zpos (Coq_xI (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH))))))
           idx))
  then Ok
         (shr
           (wshl arm_decl.reg_size wn
             (Z.sub
               (Z.sub (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
                 Coq_xH)))))) width) idx))
           (Z.sub (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH))))))
             width))
  else Error E.no_semantics

(** val bit_field_extract_semi_sc : safe_cond list **)

let bit_field_extract_semi_sc =
  (UGe (U8, (Zpos Coq_xH), (S (S O)))) :: ((UaddLe (U8, (S (S O)), (S O),
    (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH)))))))) :: [])

(** val ak_reg_reg_imm_imm_extr : arg_kind list list list **)

let ak_reg_reg_imm_imm_extr =
  ((CAreg :: []) :: ((CAreg :: []) :: (((CAimm ((CAimmC_arm_shift_amout
    SLSL), U8)) :: []) :: (((coq_CAimm_sz U8) :: []) :: [])))) :: []

(** val arm_UBFX_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_UBFX_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = UBFX in
  let sh = wshr arm_decl.reg_size in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin =
  ((sreg arm_decl) :: ((Coq_sword U8) :: ((Coq_sword U8) :: []))); id_in =
  ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: ((coq_Ea
                                                                 arm_decl (S
                                                                 (S (S O)))) :: [])));
  id_tout = ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []);
  id_semi = (Obj.magic bit_field_extract_semi sh); id_args_kinds =
  ak_reg_reg_imm_imm_extr; id_nargs = (S (S (S (S O)))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = bit_field_extract_semi_sc;
  id_pp_asm = (pp_arm_op mn opts) }

(** val extend_bits_semi :
    coq_Z -> (register, empty, empty, rflag, condt) wreg ->
    GRing.ComRing.sort -> (register, empty, empty, rflag, condt) wreg **)

let extend_bits_semi len wn wroram =
  let mask =
    wrepr arm_decl.reg_size
      (Z.sub (Z.pow (Zpos (Coq_xO Coq_xH)) len) (Zpos Coq_xH))
  in
  let roram = wunsigned U8 wroram in
  wand arm_decl.reg_size mask (wror arm_decl.reg_size wn roram)

(** val ak_reg_reg_imm8_0_8_16_24 : arg_kind list list list **)

let ak_reg_reg_imm8_0_8_16_24 =
  ((CAreg :: []) :: ((CAreg :: []) :: (((CAimm (CAimmC_arm_0_8_16_24,
    U8)) :: []) :: []))) :: []

(** val arm_UXTB_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_UXTB_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = UXTB in
  let tin = (sreg arm_decl) :: ((Coq_sword U8) :: []) in
  let semi = extend_bits_semi (Zpos (Coq_xO (Coq_xO (Coq_xO Coq_xH)))) in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: [])); id_tout =
  ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic semi)); id_args_kinds =
  ak_reg_reg_imm8_0_8_16_24; id_nargs = (S (S (S O))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
  (pp_arm_op mn opts) }

(** val arm_UXTH_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_UXTH_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = UXTH in
  let tin = (sreg arm_decl) :: ((Coq_sword U8) :: []) in
  let semi =
    extend_bits_semi (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH)))))
  in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: [])); id_tout =
  ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic semi)); id_args_kinds =
  ak_reg_reg_imm8_0_8_16_24; id_nargs = (S (S (S O))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = []; id_pp_asm =
  (pp_arm_op mn opts) }

(** val arm_SBFX_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_SBFX_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = SBFX in
  let sh = wsar arm_decl.reg_size in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin =
  ((sreg arm_decl) :: ((Coq_sword U8) :: ((Coq_sword U8) :: []))); id_in =
  ((coq_Ea arm_decl (S O)) :: ((coq_Ea arm_decl (S (S O))) :: ((coq_Ea
                                                                 arm_decl (S
                                                                 (S (S O)))) :: [])));
  id_tout = ((sreg arm_decl) :: []); id_out = ((coq_Ea arm_decl O) :: []);
  id_semi = (Obj.magic bit_field_extract_semi sh); id_args_kinds =
  ak_reg_reg_imm_imm_extr; id_nargs = (S (S (S (S O)))); id_str_jas =
  (pp_s (string_of_arm_mnemonic0 mn)); id_safe = bit_field_extract_semi_sc;
  id_pp_asm = (pp_arm_op mn opts) }

(** val arm_CMP_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let arm_CMP_semi wn wm =
  let wmnot = wnot arm_decl.reg_size wm in
  nzcv_of_aluop arm_decl.reg_size
    (GRing.add
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
        (word arm_decl.reg_size))
      (GRing.add
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
          (word arm_decl.reg_size)) wn wmnot)
      (GRing.one
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
          (word arm_decl.reg_size))))
    (Z.add
      (Z.add (wunsigned arm_decl.reg_size wn)
        (wunsigned arm_decl.reg_size wmnot)) (Zpos Coq_xH))
    (Z.add
      (Z.add (wsigned arm_decl.reg_size wn) (wsigned arm_decl.reg_size wmnot))
      (Zpos Coq_xH))

(** val arm_CMP_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_CMP_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = CMP in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl O) :: ((coq_Ea arm_decl (S O)) :: [])); id_tout =
    (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: [])))); id_out =
    ad_nzcv; id_semi = (sem_prod_ok tin (Obj.magic arm_CMP_semi));
    id_args_kinds = (ak_reg_reg_or_imm opts chk_imm_accept_shift); id_nargs =
    (S (S O)); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe =
    []; id_pp_asm = (pp_arm_op mn opts) }
  in
  (match opts.has_shift with
   | Some sk ->
     mk_shifted sk x (mk_semi2_2_shifted (sreg arm_decl) sk x.id_semi)
   | None -> x)

(** val arm_TST_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let arm_TST_semi wn wm =
  let res = wand arm_decl.reg_size wn wm in
  Obj.magic ((Some (coq_NF_of_word arm_decl.reg_size res)), ((Some
    (coq_ZF_of_word arm_decl.reg_size res)), (Some false)))

(** val arm_TST_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_TST_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = TST in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl O) :: ((coq_Ea arm_decl (S O)) :: [])); id_tout =
    (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: []))); id_out = ad_nzc;
    id_semi = (sem_prod_ok tin (Obj.magic arm_TST_semi)); id_args_kinds =
    (ak_reg_reg_or_imm opts chk_imm_reject_shift); id_nargs = (S (S O));
    id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
    id_pp_asm = (pp_arm_op mn opts) }
  in
  (match opts.has_shift with
   | Some sk ->
     mk_shifted sk x (mk_semi2_2_shifted (sreg arm_decl) sk x.id_semi)
   | None -> x)

(** val arm_CMN_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_CMN_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = CMN in
  let tin = (sreg arm_decl) :: ((sreg arm_decl) :: []) in
  let semi = fun wn wm ->
    rtuple_drop5th Coq_sbool Coq_sbool Coq_sbool Coq_sbool (sreg arm_decl)
      (arm_ADD_semi wn wm)
  in
  let x = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
    ((coq_Ea arm_decl O) :: ((coq_Ea arm_decl (S O)) :: [])); id_tout =
    (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: [])))); id_out =
    ad_nzcv; id_semi = (sem_prod_ok tin (Obj.magic semi)); id_args_kinds =
    (ak_reg_reg_or_imm opts chk_imm_accept_shift); id_nargs = (S (S O));
    id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
    id_pp_asm = (pp_arm_op mn opts) }
  in
  (match opts.has_shift with
   | Some sk ->
     mk_shifted sk x (mk_semi2_2_shifted (sreg arm_decl) sk x.id_semi)
   | None -> x)

(** val arm_extend_semi :
    wsize -> bool -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let arm_extend_semi ws sign ws' wn =
  if sign then sign_extend ws' ws wn else zero_extend ws' ws wn

(** val arm_load_instr :
    arm_options -> arm_mnemonic -> (register, empty, empty, rflag, condt)
    instr_desc_t **)

let arm_load_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  (fun mn ->
  let ws = match wsize_of_load_mn mn with
           | Some ws' -> ws'
           | None -> U32 in
  let tin = (Coq_sword ws) :: [] in
  let semi =
    arm_extend_semi ws (isSome (wsize_of_sload_mn mn)) arm_decl.reg_size
  in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Eu arm_decl (S O)) :: []); id_tout = ((sreg arm_decl) :: []);
  id_out = ((coq_Ea arm_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic semi)); id_args_kinds = ak_reg_addr; id_nargs =
  (S (S O)); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
  id_pp_asm = (pp_arm_op mn opts) })

(** val arm_store_instr :
    arm_options -> arm_mnemonic -> (register, empty, empty, rflag, condt)
    instr_desc_t **)

let arm_store_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  (fun mn ->
  let ws = match wsize_of_store_mn mn with
           | Some ws' -> ws'
           | None -> U32 in
  let tin = (Coq_sword ws) :: [] in
  let semi = arm_extend_semi ws false ws in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl O) :: []); id_tout = ((Coq_sword ws) :: []); id_out =
  ((coq_Eu arm_decl (S O)) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic semi)); id_args_kinds = ak_reg_addr; id_nargs =
  (S (S O)); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
  id_pp_asm = (pp_arm_op mn opts) })

(** val arm_CLZ_instr :
    arm_options -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_CLZ_instr opts =
  let string_of_arm_mnemonic0 = fun mn ->
    (^) (string_of_arm_mnemonic mn)
      ((^) (if opts.set_flags then "S" else "")
        (if opts.is_conditional then "cc" else ""))
  in
  let mn = CLZ in
  let tin = (sreg arm_decl) :: [] in
  let semi = fun z -> leading_zero arm_decl.reg_size z in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea arm_decl (S O)) :: []); id_tout = ((sreg arm_decl) :: []);
  id_out = ((coq_Ea arm_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic semi)); id_args_kinds = ak_reg_reg; id_nargs =
  (S (S O)); id_str_jas = (pp_s (string_of_arm_mnemonic0 mn)); id_safe = [];
  id_pp_asm = (pp_arm_op mn opts) }

(** val mn_desc :
    arm_options -> arm_mnemonic -> (register, empty, empty, rflag, condt)
    instr_desc_t **)

let mn_desc opts = function
| ADD -> arm_ADD_instr opts
| ADC -> arm_ADC_instr opts
| MUL -> arm_MUL_instr opts
| MLA -> arm_MLA_instr opts
| MLS -> arm_MLS_instr opts
| SDIV -> arm_SDIV_instr opts
| SUB -> arm_SUB_instr opts
| SBC -> arm_SBC_instr opts
| RSB -> arm_RSB_instr opts
| UDIV -> arm_UDIV_instr opts
| UMULL -> arm_UMULL_instr opts
| UMAAL -> arm_UMAAL_instr opts
| UMLAL -> arm_UMLAL_instr opts
| SMULL -> arm_SMULL_instr opts
| SMLAL -> arm_SMLAL_instr opts
| SMMUL -> arm_SMMUL_instr opts
| SMMULR -> arm_SMMULR_instr opts
| SMUL_hw (hw0, hw1) -> arm_smul_hw_instr opts hw0 hw1
| SMLA_hw (hw0, hw1) -> arm_smla_hw_instr opts hw0 hw1
| SMULW_hw hw -> arm_smulw_hw_instr opts hw
| AND -> arm_AND_instr opts
| BFC -> arm_BFC_instr opts
| BFI -> arm_BFI_instr opts
| BIC -> arm_BIC_instr opts
| EOR -> arm_EOR_instr opts
| MVN -> arm_MVN_instr opts
| ORR -> arm_ORR_instr opts
| ASR -> arm_ASR_instr opts
| LSL -> arm_LSL_instr opts
| LSR -> arm_LSR_instr opts
| ROR -> arm_ROR_instr opts
| REV -> arm_REV_instr opts
| REV16 -> arm_REV16_instr opts
| REVSH -> arm_REVSH_instr opts
| ADR -> arm_ADR_instr opts
| MOV -> arm_MOV_instr opts
| MOVT -> arm_MOVT_instr opts
| UBFX -> arm_UBFX_instr opts
| UXTB -> arm_UXTB_instr opts
| UXTH -> arm_UXTH_instr opts
| SBFX -> arm_SBFX_instr opts
| CLZ -> arm_CLZ_instr opts
| CMP -> arm_CMP_instr opts
| TST -> arm_TST_instr opts
| CMN -> arm_CMN_instr opts
| STR -> arm_store_instr opts STR
| STRB -> arm_store_instr opts STRB
| STRH -> arm_store_instr opts STRH
| x -> arm_load_instr opts x

(** val arm_instr_desc :
    arm_op -> (register, empty, empty, rflag, condt) instr_desc_t **)

let arm_instr_desc = function
| ARM_op (mn, opts) ->
  let x = mn_desc opts mn in if opts.is_conditional then mk_cond x else x

(** val arm_prim_string : (string * arm_op prim_constructor) list **)

let arm_prim_string =
  ("ADD", (PrimARM (fun sf ic -> Ok (ARM_op (ADD, { set_flags = sf;
    is_conditional = ic; has_shift = None }))))) :: (("ADC", (PrimARM
    (fun sf ic -> Ok (ARM_op (ADC, { set_flags = sf; is_conditional = ic;
    has_shift = None }))))) :: (("MUL", (PrimARM (fun sf ic -> Ok (ARM_op
    (MUL, { set_flags = sf; is_conditional = ic; has_shift =
    None }))))) :: (("MLA", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (MLA, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("MLS", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (MLS, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("SDIV", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (SDIV, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("SUB", (PrimARM (fun sf ic -> Ok (ARM_op (SUB,
    { set_flags = sf; is_conditional = ic; has_shift =
    None }))))) :: (("SBC", (PrimARM (fun sf ic -> Ok (ARM_op (SBC,
    { set_flags = sf; is_conditional = ic; has_shift =
    None }))))) :: (("RSB", (PrimARM (fun sf ic -> Ok (ARM_op (RSB,
    { set_flags = sf; is_conditional = ic; has_shift =
    None }))))) :: (("UDIV", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (UDIV, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("UMULL", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (UMULL, { set_flags = sf; is_conditional = ic;
           has_shift = None }))))) :: (("UMAAL", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (UMAAL, { set_flags = sf; is_conditional = ic;
           has_shift = None }))))) :: (("UMLAL", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (UMLAL, { set_flags = sf; is_conditional = ic;
           has_shift = None }))))) :: (("SMULL", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (SMULL, { set_flags = sf; is_conditional = ic;
           has_shift = None }))))) :: (("SMLAL", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (SMLAL, { set_flags = sf; is_conditional = ic;
           has_shift = None }))))) :: (("SMMUL", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (SMMUL, { set_flags = sf; is_conditional = ic;
           has_shift = None }))))) :: (("SMMULR", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (SMMULR, { set_flags = sf; is_conditional = ic;
           has_shift = None }))))) :: (("SMULBB", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op ((SMUL_hw (HWB, HWB)), { set_flags = sf; is_conditional =
           ic; has_shift = None }))))) :: (("SMULBT", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op ((SMUL_hw (HWB, HWT)), { set_flags = sf; is_conditional =
           ic; has_shift = None }))))) :: (("SMULTB", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op ((SMUL_hw (HWT, HWB)), { set_flags = sf; is_conditional =
           ic; has_shift = None }))))) :: (("SMULTT", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op ((SMUL_hw (HWT, HWT)), { set_flags = sf; is_conditional =
           ic; has_shift = None }))))) :: (("SMLABB", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op ((SMLA_hw (HWB, HWB)), { set_flags = sf; is_conditional =
           ic; has_shift = None }))))) :: (("SMLABT", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op ((SMLA_hw (HWB, HWT)), { set_flags = sf; is_conditional =
           ic; has_shift = None }))))) :: (("SMLATB", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op ((SMLA_hw (HWT, HWB)), { set_flags = sf; is_conditional =
           ic; has_shift = None }))))) :: (("SMLATT", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op ((SMLA_hw (HWT, HWT)), { set_flags = sf; is_conditional =
           ic; has_shift = None }))))) :: (("SMULWB", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op ((SMULW_hw HWB), { set_flags = sf; is_conditional = ic;
           has_shift = None }))))) :: (("SMULWT", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op ((SMULW_hw HWT), { set_flags = sf; is_conditional = ic;
           has_shift = None }))))) :: (("AND", (PrimARM (fun sf ic -> Ok
    (ARM_op (AND, { set_flags = sf; is_conditional = ic; has_shift =
    None }))))) :: (("BFC", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (BFC, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("BFI", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (BFI, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("BIC", (PrimARM (fun sf ic -> Ok (ARM_op (BIC,
    { set_flags = sf; is_conditional = ic; has_shift =
    None }))))) :: (("EOR", (PrimARM (fun sf ic -> Ok (ARM_op (EOR,
    { set_flags = sf; is_conditional = ic; has_shift =
    None }))))) :: (("MVN", (PrimARM (fun sf ic -> Ok (ARM_op (MVN,
    { set_flags = sf; is_conditional = ic; has_shift =
    None }))))) :: (("ORR", (PrimARM (fun sf ic -> Ok (ARM_op (ORR,
    { set_flags = sf; is_conditional = ic; has_shift =
    None }))))) :: (("ASR", (PrimARM (fun sf ic -> Ok (ARM_op (ASR,
    { set_flags = sf; is_conditional = ic; has_shift =
    None }))))) :: (("LSL", (PrimARM (fun sf ic -> Ok (ARM_op (LSL,
    { set_flags = sf; is_conditional = ic; has_shift =
    None }))))) :: (("LSR", (PrimARM (fun sf ic -> Ok (ARM_op (LSR,
    { set_flags = sf; is_conditional = ic; has_shift =
    None }))))) :: (("ROR", (PrimARM (fun sf ic -> Ok (ARM_op (ROR,
    { set_flags = sf; is_conditional = ic; has_shift =
    None }))))) :: (("REV", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (REV, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("REV16", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (REV16, { set_flags = sf; is_conditional = ic;
           has_shift = None }))))) :: (("REVSH", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (REVSH, { set_flags = sf; is_conditional = ic;
           has_shift = None }))))) :: (("ADR", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (ADR, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("MOV", (PrimARM (fun sf ic -> Ok (ARM_op (MOV,
    { set_flags = sf; is_conditional = ic; has_shift =
    None }))))) :: (("MOVT", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (MOVT, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("UBFX", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (UBFX, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("UXTB", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (UXTB, { set_flags = sf; is_conditional = ic; has_shift =
           (Some SROR) }))))) :: (("UXTH", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (UXTH, { set_flags = sf; is_conditional = ic; has_shift =
           (Some SROR) }))))) :: (("SBFX", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (SBFX, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("CLZ", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (CLZ, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("CMP", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (CMP, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("TST", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (TST, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("CMN", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (CMN, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("LDR", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (LDR, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("LDRB", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (LDRB, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("LDRH", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (LDRH, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("LDRSB", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (LDRSB, { set_flags = sf; is_conditional = ic;
           has_shift = None }))))) :: (("LDRSH", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (LDRSH, { set_flags = sf; is_conditional = ic;
           has_shift = None }))))) :: (("STR", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (STR, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("STRB", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (STRB, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: (("STRH", (PrimARM (fun sf ic ->
    if sf
    then let s = "this mnemonic cannot set flags" in Error s
    else Ok (ARM_op (STRH, { set_flags = sf; is_conditional = ic; has_shift =
           None }))))) :: [])))))))))))))))))))))))))))))))))))))))))))))))))))))))))))

(** val arm_op_decl :
    (register, empty, empty, rflag, condt, arm_op) asm_op_decl **)

let arm_op_decl =
  { Arch_decl._eqT = eqTC_arm_op; instr_desc_op = arm_instr_desc;
    Arch_decl.prim_string = arm_prim_string }

type arm_prog = (register, empty, empty, rflag, condt, arm_op) asm_prog
