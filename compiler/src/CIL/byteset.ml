open BinInt
open BinNums
open Datatypes
open Eqtype

let __ = let rec f _ = Obj.repr f in Obj.repr f

type interval = { imin : coq_Z; imax : coq_Z }

module I =
 struct
  (** val is_empty : interval -> bool **)

  let is_empty n =
    Z.leb n.imax n.imin

  (** val subset : interval -> interval -> bool **)

  let subset n1 n2 =
    (&&) (Z.leb n2.imin n1.imin) (Z.leb n1.imax n2.imax)

  (** val inter : interval -> interval -> interval **)

  let inter n1 n2 =
    { imin = (Z.max n1.imin n2.imin); imax = (Z.min n1.imax n2.imax) }

  (** val convex_hull : interval -> interval -> interval **)

  let convex_hull n1 n2 =
    { imin = (Z.min n1.imin n2.imin); imax = (Z.max n1.imax n2.imax) }

  (** val wf : interval -> bool **)

  let wf n =
    negb (is_empty n)
 end

module ByteSet =
 struct
  type bytes = interval list

  (** val wf_aux : coq_Z -> bytes -> bool **)

  let rec wf_aux i = function
  | [] -> true
  | n :: t1 -> (&&) (Z.ltb i n.imin) ((&&) (I.wf n) (wf_aux n.imax t1))

  (** val wf : bytes -> bool **)

  let wf = function
  | [] -> true
  | n :: t1 -> (&&) (I.wf n) (wf_aux n.imax t1)

  type coq_Bytes =
    bytes
    (* singleton inductive, whose constructor was mkBytes *)

  (** val tobytes : coq_Bytes -> bytes **)

  let tobytes b =
    b

  type t = coq_Bytes

  (** val coq_HB_unnamed_factory_1 : (bytes, coq_Bytes) Coq_isSub.axioms_ **)

  let coq_HB_unnamed_factory_1 =
    { Coq_isSub.val_subdef = (fun b -> b); Coq_isSub.coq_Sub = (fun x _ ->
      x); Coq_isSub.coq_Sub_rect = (fun _ k_S u -> k_S u __) }

  (** val coq_ByteSet_Bytes__canonical__eqtype_SubType :
      bytes SubType.coq_type **)

  let coq_ByteSet_Bytes__canonical__eqtype_SubType =
    Obj.magic coq_HB_unnamed_factory_1

  (** val empty : t **)

  let empty =
    []

  (** val is_empty : t -> bool **)

  let is_empty t0 =
    match (Obj.magic SubType.phant_on_ wf
            coq_ByteSet_Bytes__canonical__eqtype_SubType).Coq_isSub.val_subdef
            t0 with
    | [] -> true
    | _ :: _ -> false

  (** val _full : interval -> interval list **)

  let _full n =
    if I.wf n then n :: [] else []

  (** val full : interval -> t **)

  let full =
    _full

  (** val _mem : bytes -> interval -> bool **)

  let rec _mem t0 n =
    match t0 with
    | [] -> false
    | n' :: t1 ->
      (||) (I.subset n n') ((&&) (Z.ltb n'.imax n.imin) (_mem t1 n))

  (** val mem : t -> interval -> bool **)

  let mem t0 n =
    if I.is_empty n then true else _mem t0 n

  (** val _add : interval -> bytes -> interval list **)

  let rec _add n t0 = match t0 with
  | [] -> n :: []
  | n' :: t' ->
    if Z.ltb n.imax n'.imin
    then n :: t0
    else if Z.ltb n'.imax n.imin
         then n' :: (_add n t')
         else _add (I.convex_hull n n') t'

  (** val add : interval -> t -> coq_Bytes **)

  let add n t0 =
    if I.wf n then _add n t0 else t0

  (** val _push : interval -> bytes -> bytes **)

  let _push n t0 =
    if I.is_empty n then t0 else n :: t0

  (** val _remove : interval list -> interval -> interval list **)

  let rec _remove t0 e =
    match t0 with
    | [] -> t0
    | n' :: t' ->
      let n1 = { imin = n'.imin; imax = (Z.min n'.imax e.imin) } in
      let n2 = { imin = (Z.max n'.imin e.imax); imax = n'.imax } in
      let e0 = { imin = (Z.max n'.imax e.imin); imax = e.imax } in
      let t'0 = if I.is_empty e0 then t' else _remove t' e0 in
      _push n1 (_push n2 t'0)

  (** val remove : t -> interval -> coq_Bytes **)

  let remove t0 e =
    if I.wf e then _remove t0 e else t0

  (** val _subset : bytes -> bytes -> bool **)

  let rec _subset t1 t2 =
    match t1 with
    | [] -> true
    | i :: l ->
      (match t2 with
       | [] -> false
       | i0 :: l0 ->
         if I.subset i i0
         then _subset l (i0 :: l0)
         else if Z.ltb i0.imax i.imin then _subset (i :: l) l0 else false)

  (** val subset : t -> t -> bool **)

  let subset =
    _subset

  (** val _inter : bytes -> bytes -> bytes **)

  let rec _inter t1 t2 =
    match t1 with
    | [] -> []
    | i :: l ->
      (match t2 with
       | [] -> []
       | i0 :: l0 ->
         if Z.leb i.imax i0.imin
         then _inter l (i0 :: l0)
         else if Z.leb i0.imax i.imin
              then _inter (i :: l) l0
              else let n = I.inter i i0 in
                   let n1' = { imin = i0.imax; imax = i.imax } in
                   let n2' = { imin = i.imax; imax = i0.imax } in
                   n :: (_inter (_push n1' l) (_push n2' l0)))

  (** val inter : t -> t -> coq_Bytes **)

  let inter =
    _inter

  (** val _union : bytes -> bytes -> bytes **)

  let rec _union t1 t2 =
    let _evar_0_ = fun _ -> t2 in
    let _evar_0_0 = fun n1 t1' ->
      let _evar_0_0 = fun _ -> n1 :: t1' in
      let _evar_0_1 = fun n2 t2' ->
        if Z.ltb n1.imax n2.imin
        then n1 :: (_union t1' (n2 :: t2'))
        else if Z.ltb n2.imax n1.imin
             then n2 :: (_union (n1 :: t1') t2')
             else _union t1' (_add (I.convex_hull n1 n2) t2')
      in
      (match t2 with
       | [] -> _evar_0_0
       | a :: l -> (fun _ -> _evar_0_1 a l))
    in
    (match t1 with
     | [] -> _evar_0_ __
     | a :: l -> _evar_0_0 a l __)

  (** val union : t -> t -> coq_Bytes **)

  let union =
    _union
 end
