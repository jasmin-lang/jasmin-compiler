open BinInt
open BinNums
open Datatypes
open Eqtype

type interval = { imin : coq_Z; imax : coq_Z }

module I :
 sig
  val is_empty : interval -> bool

  val subset : interval -> interval -> bool

  val inter : interval -> interval -> interval

  val convex_hull : interval -> interval -> interval

  val wf : interval -> bool
 end

module ByteSet :
 sig
  type bytes = interval list

  val wf_aux : coq_Z -> bytes -> bool

  val wf : bytes -> bool

  type coq_Bytes =
    bytes
    (* singleton inductive, whose constructor was mkBytes *)

  val tobytes : coq_Bytes -> bytes

  type t = coq_Bytes

  val coq_HB_unnamed_factory_1 : (bytes, coq_Bytes) Coq_isSub.axioms_

  val coq_ByteSet_Bytes__canonical__eqtype_SubType : bytes SubType.coq_type

  val empty : t

  val is_empty : t -> bool

  val _full : interval -> interval list

  val full : interval -> t

  val _mem : bytes -> interval -> bool

  val mem : t -> interval -> bool

  val _add : interval -> bytes -> interval list

  val add : interval -> t -> coq_Bytes

  val _push : interval -> bytes -> bytes

  val _remove : interval list -> interval -> interval list

  val remove : t -> interval -> coq_Bytes

  val _subset : bytes -> bytes -> bool

  val subset : t -> t -> bool

  val _inter : bytes -> bytes -> bytes

  val inter : t -> t -> coq_Bytes

  val _union : bytes -> bytes -> bytes

  val union : t -> t -> coq_Bytes
 end
