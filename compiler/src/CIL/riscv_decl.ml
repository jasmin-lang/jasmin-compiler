open BinInt
open BinNums
open Bool
open Arch_decl
open Arch_utils
open Eqtype
open Expr
open Fintype
open Flag_combination
open Seq
open Ssralg
open Type
open Utils0
open Word0
open Wsize

(** val riscv_reg_size : wsize **)

let riscv_reg_size =
  U32

(** val riscv_xreg_size : wsize **)

let riscv_xreg_size =
  U64

type register =
| RA
| SP
| X5
| X6
| X7
| X8
| X9
| X10
| X11
| X12
| X13
| X14
| X15
| X16
| X17
| X18
| X19
| X20
| X21
| X22
| X23
| X24
| X25
| X26
| X27
| X28
| X29
| X30
| X31

(** val register_beq : register -> register -> bool **)

let register_beq x y =
  match x with
  | RA -> (match y with
           | RA -> true
           | _ -> false)
  | SP -> (match y with
           | SP -> true
           | _ -> false)
  | X5 -> (match y with
           | X5 -> true
           | _ -> false)
  | X6 -> (match y with
           | X6 -> true
           | _ -> false)
  | X7 -> (match y with
           | X7 -> true
           | _ -> false)
  | X8 -> (match y with
           | X8 -> true
           | _ -> false)
  | X9 -> (match y with
           | X9 -> true
           | _ -> false)
  | X10 -> (match y with
            | X10 -> true
            | _ -> false)
  | X11 -> (match y with
            | X11 -> true
            | _ -> false)
  | X12 -> (match y with
            | X12 -> true
            | _ -> false)
  | X13 -> (match y with
            | X13 -> true
            | _ -> false)
  | X14 -> (match y with
            | X14 -> true
            | _ -> false)
  | X15 -> (match y with
            | X15 -> true
            | _ -> false)
  | X16 -> (match y with
            | X16 -> true
            | _ -> false)
  | X17 -> (match y with
            | X17 -> true
            | _ -> false)
  | X18 -> (match y with
            | X18 -> true
            | _ -> false)
  | X19 -> (match y with
            | X19 -> true
            | _ -> false)
  | X20 -> (match y with
            | X20 -> true
            | _ -> false)
  | X21 -> (match y with
            | X21 -> true
            | _ -> false)
  | X22 -> (match y with
            | X22 -> true
            | _ -> false)
  | X23 -> (match y with
            | X23 -> true
            | _ -> false)
  | X24 -> (match y with
            | X24 -> true
            | _ -> false)
  | X25 -> (match y with
            | X25 -> true
            | _ -> false)
  | X26 -> (match y with
            | X26 -> true
            | _ -> false)
  | X27 -> (match y with
            | X27 -> true
            | _ -> false)
  | X28 -> (match y with
            | X28 -> true
            | _ -> false)
  | X29 -> (match y with
            | X29 -> true
            | _ -> false)
  | X30 -> (match y with
            | X30 -> true
            | _ -> false)
  | X31 -> (match y with
            | X31 -> true
            | _ -> false)

(** val register_eq_dec : register -> register -> bool **)

let register_eq_dec x y =
  let b = register_beq x y in if b then true else false

(** val register_eq_axiom : register eq_axiom **)

let register_eq_axiom =
  eq_axiom_of_scheme register_beq

(** val eqTC_register : register eqTypeC **)

let eqTC_register =
  { beq = register_beq; ceqP = register_eq_axiom }

(** val riscv_register_eqType : Equality.coq_type **)

let riscv_register_eqType =
  ceqT_eqType eqTC_register

(** val registers : register list **)

let registers =
  RA :: (SP :: (X5 :: (X6 :: (X7 :: (X8 :: (X9 :: (X10 :: (X11 :: (X12 :: (X13 :: (X14 :: (X15 :: (X16 :: (X17 :: (X18 :: (X19 :: (X20 :: (X21 :: (X22 :: (X23 :: (X24 :: (X25 :: (X26 :: (X27 :: (X28 :: (X29 :: (X30 :: (X31 :: []))))))))))))))))))))))))))))

(** val finTC_register : register finTypeC **)

let finTC_register =
  { _eqC = eqTC_register; cenum = registers }

(** val register_finType : Finite.coq_type **)

let register_finType =
  cfinT_finType finTC_register

(** val register_to_string : register -> string **)

let register_to_string = function
| RA -> "ra"
| SP -> "sp"
| X5 -> "x5"
| X6 -> "x6"
| X7 -> "x7"
| X8 -> "x8"
| X9 -> "x9"
| X10 -> "x10"
| X11 -> "x11"
| X12 -> "x12"
| X13 -> "x13"
| X14 -> "x14"
| X15 -> "x15"
| X16 -> "x16"
| X17 -> "x17"
| X18 -> "x18"
| X19 -> "x19"
| X20 -> "x20"
| X21 -> "x21"
| X22 -> "x22"
| X23 -> "x23"
| X24 -> "x24"
| X25 -> "x25"
| X26 -> "x26"
| X27 -> "x27"
| X28 -> "x28"
| X29 -> "x29"
| X30 -> "x30"
| X31 -> "x31"

(** val reg_toS : register coq_ToString **)

let reg_toS =
  { category = "register"; _finC = finTC_register; to_string =
    register_to_string }

type condition_kind =
| EQ
| NE
| LT of signedness
| GE of signedness

type condt = { cond_kind : condition_kind; cond_fst : register option;
               cond_snd : register option }

(** val cond_kind : condt -> condition_kind **)

let cond_kind c =
  c.cond_kind

(** val cond_fst : condt -> register option **)

let cond_fst c =
  c.cond_fst

(** val cond_snd : condt -> register option **)

let cond_snd c =
  c.cond_snd

(** val condition_kind_beq : condition_kind -> condition_kind -> bool **)

let condition_kind_beq x y =
  match x with
  | EQ -> (match y with
           | EQ -> true
           | _ -> false)
  | NE -> (match y with
           | NE -> true
           | _ -> false)
  | LT x0 ->
    (match y with
     | LT x1 -> internal_signedness_beq x0 x1
     | _ -> false)
  | GE x0 ->
    (match y with
     | GE x1 -> internal_signedness_beq x0 x1
     | _ -> false)

(** val condition_kind_eq_dec : condition_kind -> condition_kind -> bool **)

let condition_kind_eq_dec x y =
  let b = condition_kind_beq x y in if b then true else false

(** val condt_beq : condt -> condt -> bool **)

let condt_beq c1 c2 =
  (&&)
    ((&&) (condition_kind_beq c1.cond_kind c2.cond_kind)
      (eq_op
        (coq_Datatypes_option__canonical__eqtype_Equality
          riscv_register_eqType) (Obj.magic c1.cond_fst)
        (Obj.magic c2.cond_fst)))
    (eq_op
      (coq_Datatypes_option__canonical__eqtype_Equality riscv_register_eqType)
      (Obj.magic c1.cond_snd) (Obj.magic c2.cond_snd))

(** val condt_eq_axiom : condt eq_axiom **)

let condt_eq_axiom c1 c2 =
  iff_reflect (condt_beq c1 c2)

(** val eqTC_condt : condt eqTypeC **)

let eqTC_condt =
  { beq = condt_beq; ceqP = condt_eq_axiom }

(** val condt_eqType : Equality.coq_type **)

let condt_eqType =
  ceqT_eqType eqTC_condt

(** val riscv_fc_of_cfc : combine_flags_core -> flag_combination **)

let riscv_fc_of_cfc _ =
  FCVar0

(** val riscv_fcp : coq_FlagCombinationParams **)

let riscv_fcp =
  riscv_fc_of_cfc

(** val riscv_check_CAimm :
    caimm_checker_s -> wsize -> GRing.ComRing.sort -> bool **)

let riscv_check_CAimm checker ws w =
  match checker with
  | CAimmC_none -> true
  | CAimmC_riscv_12bits_signed ->
    let i = wsigned ws w in
    (&&)
      (Z.leb (Zneg (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
        (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH)))))))))))) i)
      (Z.leb i (Zpos (Coq_xI (Coq_xI (Coq_xI (Coq_xI (Coq_xI (Coq_xI (Coq_xI
        (Coq_xI (Coq_xI (Coq_xI Coq_xH))))))))))))
  | CAimmC_riscv_5bits_unsigned ->
    let i = wunsigned ws w in
    Z.leb i (Zpos (Coq_xI (Coq_xI (Coq_xI (Coq_xI Coq_xH)))))
  | _ -> false

(** val riscv_decl : (register, empty, empty, empty, condt) arch_decl **)

let riscv_decl =
  { reg_size = riscv_reg_size; xreg_size = riscv_xreg_size; cond_eqC =
    eqTC_condt; toS_r = reg_toS; toS_rx = (empty_toS (Coq_sword U32));
    toS_x = (empty_toS (Coq_sword U64)); toS_f = (empty_toS Coq_sbool);
    ad_rsp = SP; ad_fcp = riscv_fcp; check_CAimm = riscv_check_CAimm }

(** val riscv_linux_call_conv :
    (register, empty, empty, empty, condt) calling_convention **)

let riscv_linux_call_conv =
  { callee_saved =
    (map (fun x -> ARReg x)
      (SP :: (X8 :: (X9 :: (X18 :: (X19 :: (X20 :: (X21 :: (X22 :: (X23 :: (X24 :: (X25 :: (X26 :: (X27 :: []))))))))))))));
    call_reg_args =
    (X10 :: (X11 :: (X12 :: (X13 :: (X14 :: (X15 :: (X16 :: (X17 :: []))))))));
    call_xreg_args = []; call_reg_ret = (X10 :: (X11 :: [])); call_xreg_ret =
    [] }
