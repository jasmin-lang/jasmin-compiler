open BinInt
open BinNums
open Bool
open Arch_decl
open Arch_utils
open Eqtype
open Expr
open Fintype
open Flag_combination
open Seq
open Ssralg
open Type
open Utils0
open Word0
open Wsize

val riscv_reg_size : wsize

val riscv_xreg_size : wsize

type register =
| RA
| SP
| X5
| X6
| X7
| X8
| X9
| X10
| X11
| X12
| X13
| X14
| X15
| X16
| X17
| X18
| X19
| X20
| X21
| X22
| X23
| X24
| X25
| X26
| X27
| X28
| X29
| X30
| X31

val register_beq : register -> register -> bool

val register_eq_dec : register -> register -> bool

val register_eq_axiom : register eq_axiom

val eqTC_register : register eqTypeC

val riscv_register_eqType : Equality.coq_type

val registers : register list

val finTC_register : register finTypeC

val register_finType : Finite.coq_type

val register_to_string : register -> string

val reg_toS : register coq_ToString

type condition_kind =
| EQ
| NE
| LT of signedness
| GE of signedness

type condt = { cond_kind : condition_kind; cond_fst : register option;
               cond_snd : register option }

val cond_kind : condt -> condition_kind

val cond_fst : condt -> register option

val cond_snd : condt -> register option

val condition_kind_beq : condition_kind -> condition_kind -> bool

val condition_kind_eq_dec : condition_kind -> condition_kind -> bool

val condt_beq : condt -> condt -> bool

val condt_eq_axiom : condt eq_axiom

val eqTC_condt : condt eqTypeC

val condt_eqType : Equality.coq_type

val riscv_fc_of_cfc : combine_flags_core -> flag_combination

val riscv_fcp : coq_FlagCombinationParams

val riscv_check_CAimm : caimm_checker_s -> wsize -> GRing.ComRing.sort -> bool

val riscv_decl : (register, empty, empty, empty, condt) arch_decl

val riscv_linux_call_conv :
  (register, empty, empty, empty, condt) calling_convention
