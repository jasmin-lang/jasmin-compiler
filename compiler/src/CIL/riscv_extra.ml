open Datatypes
open Arch_decl
open Arch_extra
open Arch_utils
open Compiler_util
open Eqtype
open Expr
open Fexpr
open Riscv
open Riscv_decl
open Riscv_instr_decl
open Riscv_params_core
open Sem_type
open Seq
open Sopn
open Ssralg
open Type
open Utils0
open Var0
open Word0
open Wsize

type riscv_extra_op =
| SWAP of wsize
| Oriscv_add_large_imm

(** val riscv_extra_op_beq : riscv_extra_op -> riscv_extra_op -> bool **)

let riscv_extra_op_beq x y =
  match x with
  | SWAP x0 ->
    (match y with
     | SWAP x1 -> wsize_beq x0 x1
     | Oriscv_add_large_imm -> false)
  | Oriscv_add_large_imm ->
    (match y with
     | SWAP _ -> false
     | Oriscv_add_large_imm -> true)

(** val riscv_extra_op_eq_dec : riscv_extra_op -> riscv_extra_op -> bool **)

let riscv_extra_op_eq_dec x y =
  let b = riscv_extra_op_beq x y in if b then true else false

(** val riscv_extra_op_eq_axiom : riscv_extra_op eq_axiom **)

let riscv_extra_op_eq_axiom =
  eq_axiom_of_scheme riscv_extra_op_beq

(** val coq_HB_unnamed_factory_1 : riscv_extra_op Coq_hasDecEq.axioms_ **)

let coq_HB_unnamed_factory_1 =
  { Coq_hasDecEq.eq_op = riscv_extra_op_beq; Coq_hasDecEq.eqP =
    riscv_extra_op_eq_axiom }

(** val riscv_extra_riscv_extra_op__canonical__eqtype_Equality :
    Equality.coq_type **)

let riscv_extra_riscv_extra_op__canonical__eqtype_Equality =
  Obj.magic coq_HB_unnamed_factory_1

(** val eqTC_riscv_extra_op : riscv_extra_op eqTypeC **)

let eqTC_riscv_extra_op =
  { beq = riscv_extra_op_beq; ceqP = riscv_extra_op_eq_axiom }

(** val coq_Oriscv_add_large_imm_instr : instruction_desc **)

let coq_Oriscv_add_large_imm_instr =
  let ty = Coq_sword riscv_reg_size in
  let tin0 = ty :: (ty :: []) in
  let semi0 = fun x y ->
    GRing.add
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
        (word riscv_reg_size)) x y
  in
  { str = (fun _ -> "add_large_imm"); tin = tin0; i_in = ((ADExplicit ((S O),
  None)) :: ((ADExplicit ((S (S O)), None)) :: [])); tout = (ty :: []);
  i_out = ((ADExplicit (O, None)) :: []); conflicts = (((APout O), (APin
  O)) :: []); semi = (sem_prod_ok tin0 (Obj.magic semi0)); i_valid = true;
  i_safe = [] }

(** val get_instr_desc : riscv_extra_op -> instruction_desc **)

let get_instr_desc = function
| SWAP ws -> coq_Oswap_instr (Coq_sword ws)
| Oriscv_add_large_imm -> coq_Oriscv_add_large_imm_instr

(** val riscv_extra_op_decl : riscv_extra_op asmOp **)

let riscv_extra_op_decl =
  { _eqT = eqTC_riscv_extra_op; asm_op_instr = get_instr_desc; prim_string =
    [] }

module E =
 struct
  (** val pass_name : string **)

  let pass_name =
    "asmgen"

  (** val internal_error : instr_info -> string -> pp_error_loc **)

  let internal_error ii msg =
    { pel_msg = (PPEstring msg); pel_fn = None; pel_fi = None; pel_ii = (Some
      ii); pel_vi = None; pel_pass = (Some pass_name); pel_internal = true }

  (** val error : instr_info -> string -> pp_error_loc **)

  let error ii msg =
    { pel_msg = (PPEstring msg); pel_fn = None; pel_fi = None; pel_ii = (Some
      ii); pel_vi = None; pel_pass = (Some pass_name); pel_internal = false }
 end

(** val asm_args_of_opn_args :
    RISCVFopn_core.opn_args list -> (((register, empty, empty, empty, condt,
    riscv_op) asm_op_msb_t * lexpr list) * rexpr list) list **)

let asm_args_of_opn_args =
  map (fun pat ->
    let (y, res) = pat in let (les, aop) = y in (((None, aop), les), res))

(** val assemble_extra :
    instr_info -> riscv_extra_op -> lexpr list -> rexpr list -> (((register,
    empty, empty, empty, condt, riscv_op) asm_op_msb_t * lexpr list) * rexpr
    list) list cexec **)

let assemble_extra ii o outx inx =
  match o with
  | SWAP sz ->
    if eq_op wsize_wsize__canonical__eqtype_Equality (Obj.magic sz)
         (Obj.magic U32)
    then (match outx with
          | [] ->
            Error
              (E.error ii
                "only register is accepted on source and destination of the swap instruction on RISC-V")
          | l :: l0 ->
            (match l with
             | Store (_, _, _, _) ->
               Error
                 (E.error ii
                   "only register is accepted on source and destination of the swap instruction on RISC-V")
             | LLvar x ->
               (match l0 with
                | [] ->
                  Error
                    (E.error ii
                      "only register is accepted on source and destination of the swap instruction on RISC-V")
                | l1 :: l2 ->
                  (match l1 with
                   | Store (_, _, _, _) ->
                     Error
                       (E.error ii
                         "only register is accepted on source and destination of the swap instruction on RISC-V")
                   | LLvar y ->
                     (match l2 with
                      | [] ->
                        (match inx with
                         | [] ->
                           Error
                             (E.error ii
                               "only register is accepted on source and destination of the swap instruction on RISC-V")
                         | r :: l3 ->
                           (match r with
                            | Load (_, _, _, _) ->
                              Error
                                (E.error ii
                                  "only register is accepted on source and destination of the swap instruction on RISC-V")
                            | Rexpr f ->
                              (match f with
                               | Fvar z ->
                                 (match l3 with
                                  | [] ->
                                    Error
                                      (E.error ii
                                        "only register is accepted on source and destination of the swap instruction on RISC-V")
                                  | r0 :: l4 ->
                                    (match r0 with
                                     | Load (_, _, _, _) ->
                                       Error
                                         (E.error ii
                                           "only register is accepted on source and destination of the swap instruction on RISC-V")
                                     | Rexpr f0 ->
                                       (match f0 with
                                        | Fvar w ->
                                          (match l4 with
                                           | [] ->
                                             if negb
                                                  (eq_op
                                                    Var.coq_MvMake_var__canonical__eqtype_Equality
                                                    (Obj.magic x.v_var)
                                                    (Obj.magic w.v_var))
                                             then if negb
                                                       (eq_op
                                                         Var.coq_MvMake_var__canonical__eqtype_Equality
                                                         (Obj.magic y.v_var)
                                                         (Obj.magic x.v_var))
                                                  then if all (fun x0 ->
                                                            eq_op
                                                              type_stype__canonical__eqtype_Equality
                                                              (Obj.magic
                                                                Var.vtype
                                                                x0.v_var)
                                                              (Obj.magic
                                                                (Coq_sword
                                                                U32)))
                                                            (x :: (y :: (z :: (w :: []))))
                                                       then Ok ((((None,
                                                              XOR), ((LLvar
                                                              x) :: [])),
                                                              ((Rexpr (Fvar
                                                              z)) :: ((Rexpr
                                                              (Fvar
                                                              w)) :: []))) :: ((((None,
                                                              XOR), ((LLvar
                                                              y) :: [])),
                                                              ((Rexpr (Fvar
                                                              x)) :: ((Rexpr
                                                              (Fvar
                                                              w)) :: []))) :: ((((None,
                                                              XOR), ((LLvar
                                                              x) :: [])),
                                                              ((Rexpr (Fvar
                                                              x)) :: ((Rexpr
                                                              (Fvar
                                                              y)) :: []))) :: [])))
                                                       else let s =
                                                              E.error ii
                                                                "RISC-V swap only valid for register of type u32"
                                                            in
                                                            Error s
                                                  else let s =
                                                         E.internal_error ii
                                                           "bad RISC-V swap : y = x"
                                                       in
                                                       Error s
                                             else let s =
                                                    E.internal_error ii
                                                      "bad RISC-V swap : x = w"
                                                  in
                                                  Error s
                                           | _ :: _ ->
                                             Error
                                               (E.error ii
                                                 "only register is accepted on source and destination of the swap instruction on RISC-V"))
                                        | _ ->
                                          Error
                                            (E.error ii
                                              "only register is accepted on source and destination of the swap instruction on RISC-V"))))
                               | _ ->
                                 Error
                                   (E.error ii
                                     "only register is accepted on source and destination of the swap instruction on RISC-V"))))
                      | _ :: _ ->
                        Error
                          (E.error ii
                            "only register is accepted on source and destination of the swap instruction on RISC-V"))))))
    else Error (E.error ii "RISC-V swap only valid for register of type u32")
  | Oriscv_add_large_imm ->
    (match outx with
     | [] ->
       Error
         (E.internal_error ii
           "bad riscv_add_large_imm: invalid args or dests")
     | l :: l0 ->
       (match l with
        | Store (_, _, _, _) ->
          Error
            (E.internal_error ii
              "bad riscv_add_large_imm: invalid args or dests")
        | LLvar x ->
          (match l0 with
           | [] ->
             (match inx with
              | [] ->
                Error
                  (E.internal_error ii
                    "bad riscv_add_large_imm: invalid args or dests")
              | r :: l1 ->
                (match r with
                 | Load (_, _, _, _) ->
                   Error
                     (E.internal_error ii
                       "bad riscv_add_large_imm: invalid args or dests")
                 | Rexpr f ->
                   (match f with
                    | Fvar y ->
                      (match l1 with
                       | [] ->
                         Error
                           (E.internal_error ii
                             "bad riscv_add_large_imm: invalid args or dests")
                       | r0 :: l2 ->
                         (match r0 with
                          | Load (_, _, _, _) ->
                            Error
                              (E.internal_error ii
                                "bad riscv_add_large_imm: invalid args or dests")
                          | Rexpr f0 ->
                            (match f0 with
                             | Fapp1 (s, f1) ->
                               (match s with
                                | Oword_of_int _ ->
                                  (match f1 with
                                   | Fconst imm ->
                                     (match l2 with
                                      | [] ->
                                        if negb
                                             (eq_op
                                               Var.coq_MvMake_var__canonical__eqtype_Equality
                                               (Obj.magic x.v_var)
                                               (Obj.magic y.v_var))
                                        then if all (fun x0 ->
                                                  eq_op
                                                    type_stype__canonical__eqtype_Equality
                                                    (Obj.magic Var.vtype
                                                      x0.v_var)
                                                    (Obj.magic (Coq_sword
                                                      U32))) (x :: (y :: []))
                                             then Ok
                                                    (asm_args_of_opn_args
                                                      (RISCVFopn_core.smart_addi
                                                        x y imm))
                                             else let s0 =
                                                    E.error ii
                                                      "riscv_add_large_imm only valid for register of type u32"
                                                  in
                                                  Error s0
                                        else let s0 =
                                               E.internal_error ii
                                                 "bad riscv_add_large_imm: invalid register"
                                             in
                                             Error s0
                                      | _ :: _ ->
                                        Error
                                          (E.internal_error ii
                                            "bad riscv_add_large_imm: invalid args or dests"))
                                   | _ ->
                                     Error
                                       (E.internal_error ii
                                         "bad riscv_add_large_imm: invalid args or dests"))
                                | _ ->
                                  Error
                                    (E.internal_error ii
                                      "bad riscv_add_large_imm: invalid args or dests"))
                             | _ ->
                               Error
                                 (E.internal_error ii
                                   "bad riscv_add_large_imm: invalid args or dests"))))
                    | _ ->
                      Error
                        (E.internal_error ii
                          "bad riscv_add_large_imm: invalid args or dests"))))
           | _ :: _ ->
             Error
               (E.internal_error ii
                 "bad riscv_add_large_imm: invalid args or dests"))))

(** val riscv_extra :
    (register, empty, empty, empty, condt) arch_toIdent -> (register, empty,
    empty, empty, condt, riscv_op, riscv_extra_op) asm_extra **)

let riscv_extra atoI =
  { _asm = riscv; _atoI = atoI; _extra = riscv_extra_op_decl; to_asm =
    assemble_extra }

type riscv_extended_op =
  (register, empty, empty, empty, condt, riscv_op, riscv_extra_op) extended_op

(** val coq_Oriscv :
    (register, empty, empty, empty, condt) arch_toIdent -> riscv_op ->
    riscv_extended_op sopn **)

let coq_Oriscv _ o =
  Oasm (BaseOp (None, o))
