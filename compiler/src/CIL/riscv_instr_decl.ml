open BinNums
open Datatypes
open Arch_decl
open Arch_utils
open Eqtype
open Expr
open Riscv_decl
open Sem_type
open Seq
open Sopn
open Ssralg
open Type
open Utils0
open Word0
open Wsize

module E =
 struct
  (** val no_semantics : error **)

  let no_semantics =
    ErrType
 end

(** val pp_name :
    string -> (register, empty, empty, empty, condt) asm_arg list ->
    (register, empty, empty, empty, condt) pp_asm_op **)

let pp_name name args =
  { pp_aop_name = name; pp_aop_ext = PP_name; pp_aop_args =
    (map (fun a -> (riscv_decl.reg_size, a)) args) }

(** val coq_RTypeInstruction :
    wsize -> sem_tuple sem_prod -> string -> string -> (register, empty,
    empty, empty, condt) instr_desc_t **)

let coq_RTypeInstruction ws semi jazz_name asm_name =
  let tin = (sreg riscv_decl) :: ((Coq_sword ws) :: []) in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea riscv_decl (S O)) :: ((coq_Ea riscv_decl (S (S O))) :: []));
  id_tout = ((sreg riscv_decl) :: []); id_out =
  ((coq_Ea riscv_decl O) :: []); id_semi = (sem_prod_ok tin semi);
  id_args_kinds = ak_reg_reg_reg; id_nargs = (S (S (S O))); id_str_jas =
  (pp_s jazz_name); id_safe = []; id_pp_asm = (pp_name asm_name) }

(** val coq_ITypeInstruction :
    caimm_checker_s -> wsize -> sem_tuple sem_prod -> string -> string ->
    (register, empty, empty, empty, condt) instr_desc_t **)

let coq_ITypeInstruction chk_imm ws semi jazz_name asm_name =
  let tin = (sreg riscv_decl) :: ((Coq_sword ws) :: []) in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea riscv_decl (S O)) :: ((coq_Ea riscv_decl (S (S O))) :: []));
  id_tout = ((sreg riscv_decl) :: []); id_out =
  ((coq_Ea riscv_decl O) :: []); id_semi = (sem_prod_ok tin semi);
  id_args_kinds = (((CAreg :: []) :: ((CAreg :: []) :: (((CAimm (chk_imm,
  riscv_decl.reg_size)) :: []) :: []))) :: []); id_nargs = (S (S (S O)));
  id_str_jas = (pp_s jazz_name); id_safe = []; id_pp_asm =
  (pp_name asm_name) }

(** val coq_ITypeInstruction_12s :
    wsize -> sem_tuple sem_prod -> string -> string -> (register, empty,
    empty, empty, condt) instr_desc_t **)

let coq_ITypeInstruction_12s =
  coq_ITypeInstruction CAimmC_riscv_12bits_signed

(** val coq_ITypeInstruction_5u :
    wsize -> sem_tuple sem_prod -> string -> string -> (register, empty,
    empty, empty, condt) instr_desc_t **)

let coq_ITypeInstruction_5u =
  coq_ITypeInstruction CAimmC_riscv_5bits_unsigned

type riscv_op =
| ADD
| ADDI
| SUB
| SLT
| SLTI
| SLTU
| SLTIU
| AND
| ANDI
| OR
| ORI
| XOR
| XORI
| SLL
| SLLI
| SRL
| SRLI
| SRA
| SRAI
| MV
| LA
| LI
| NOT
| NEG
| LOAD of signedness * wsize
| STORE of wsize
| MUL
| MULH
| MULHU
| MULHSU
| DIV
| DIVU
| REM
| REMU

(** val riscv_op_beq : riscv_op -> riscv_op -> bool **)

let riscv_op_beq x y =
  match x with
  | ADD -> (match y with
            | ADD -> true
            | _ -> false)
  | ADDI -> (match y with
             | ADDI -> true
             | _ -> false)
  | SUB -> (match y with
            | SUB -> true
            | _ -> false)
  | SLT -> (match y with
            | SLT -> true
            | _ -> false)
  | SLTI -> (match y with
             | SLTI -> true
             | _ -> false)
  | SLTU -> (match y with
             | SLTU -> true
             | _ -> false)
  | SLTIU -> (match y with
              | SLTIU -> true
              | _ -> false)
  | AND -> (match y with
            | AND -> true
            | _ -> false)
  | ANDI -> (match y with
             | ANDI -> true
             | _ -> false)
  | OR -> (match y with
           | OR -> true
           | _ -> false)
  | ORI -> (match y with
            | ORI -> true
            | _ -> false)
  | XOR -> (match y with
            | XOR -> true
            | _ -> false)
  | XORI -> (match y with
             | XORI -> true
             | _ -> false)
  | SLL -> (match y with
            | SLL -> true
            | _ -> false)
  | SLLI -> (match y with
             | SLLI -> true
             | _ -> false)
  | SRL -> (match y with
            | SRL -> true
            | _ -> false)
  | SRLI -> (match y with
             | SRLI -> true
             | _ -> false)
  | SRA -> (match y with
            | SRA -> true
            | _ -> false)
  | SRAI -> (match y with
             | SRAI -> true
             | _ -> false)
  | MV -> (match y with
           | MV -> true
           | _ -> false)
  | LA -> (match y with
           | LA -> true
           | _ -> false)
  | LI -> (match y with
           | LI -> true
           | _ -> false)
  | NOT -> (match y with
            | NOT -> true
            | _ -> false)
  | NEG -> (match y with
            | NEG -> true
            | _ -> false)
  | LOAD (x0, x1) ->
    (match y with
     | LOAD (x2, x3) -> (&&) (internal_signedness_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | STORE x0 -> (match y with
                 | STORE x1 -> wsize_beq x0 x1
                 | _ -> false)
  | MUL -> (match y with
            | MUL -> true
            | _ -> false)
  | MULH -> (match y with
             | MULH -> true
             | _ -> false)
  | MULHU -> (match y with
              | MULHU -> true
              | _ -> false)
  | MULHSU -> (match y with
               | MULHSU -> true
               | _ -> false)
  | DIV -> (match y with
            | DIV -> true
            | _ -> false)
  | DIVU -> (match y with
             | DIVU -> true
             | _ -> false)
  | REM -> (match y with
            | REM -> true
            | _ -> false)
  | REMU -> (match y with
             | REMU -> true
             | _ -> false)

(** val riscv_op_eq_dec : riscv_op -> riscv_op -> bool **)

let riscv_op_eq_dec x y =
  let b = riscv_op_beq x y in if b then true else false

(** val riscv_op_eq_axiom : riscv_op eq_axiom **)

let riscv_op_eq_axiom =
  eq_axiom_of_scheme riscv_op_beq

(** val eqTC_riscv_op : riscv_op eqTypeC **)

let eqTC_riscv_op =
  { beq = riscv_op_beq; ceqP = riscv_op_eq_axiom }

(** val riscv_op_eqType : Equality.coq_type **)

let riscv_op_eqType =
  ceqT_eqType eqTC_riscv_op

(** val riscv_add_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let riscv_add_semi wn wm =
  GRing.add
    (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
      (word riscv_decl.reg_size)) wn wm

(** val riscv_ADD_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_ADD_instr =
  coq_RTypeInstruction riscv_decl.reg_size (Obj.magic riscv_add_semi) "ADD"
    "add"

(** val prim_ADD : string * riscv_op prim_constructor **)

let prim_ADD =
  ("ADD", (primM ADD))

(** val riscv_ADDI_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_ADDI_instr =
  coq_ITypeInstruction_12s riscv_decl.reg_size (Obj.magic riscv_add_semi)
    "ADDI" "addi"

(** val prim_ADDI : string * riscv_op prim_constructor **)

let prim_ADDI =
  ("ADDI", (primM ADDI))

(** val riscv_sub_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let riscv_sub_semi wn wm =
  GRing.add
    (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
      (word riscv_decl.reg_size)) wn
    (GRing.opp
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Zmodule
        (word riscv_decl.reg_size)) wm)

(** val riscv_SUB_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_SUB_instr =
  coq_RTypeInstruction riscv_decl.reg_size (Obj.magic riscv_sub_semi) "SUB"
    "sub"

(** val prim_SUB : string * riscv_op prim_constructor **)

let prim_SUB =
  ("SUB", (primM SUB))

(** val riscv_slt_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let riscv_slt_semi wn wm =
  if wlt riscv_decl.reg_size Signed wn wm
  then GRing.one
         (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
           (word riscv_decl.reg_size))
  else GRing.zero
         (GRing.SemiRing.Exports.coq_GRing_SemiRing__to__GRing_Nmodule
           (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
             (word riscv_decl.reg_size)))

(** val riscv_SLT_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_SLT_instr =
  coq_RTypeInstruction riscv_decl.reg_size (Obj.magic riscv_slt_semi) "SLT"
    "slt"

(** val prim_SLT : string * riscv_op prim_constructor **)

let prim_SLT =
  ("SLT", (primM SLT))

(** val riscv_SLTI_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_SLTI_instr =
  coq_ITypeInstruction_12s riscv_decl.reg_size (Obj.magic riscv_slt_semi)
    "SLTI" "slti"

(** val prim_SLTI : string * riscv_op prim_constructor **)

let prim_SLTI =
  ("SLTI", (primM SLTI))

(** val riscv_sltu_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let riscv_sltu_semi wn wm =
  if wlt riscv_decl.reg_size Unsigned wn wm
  then GRing.one
         (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
           (word riscv_decl.reg_size))
  else GRing.zero
         (GRing.SemiRing.Exports.coq_GRing_SemiRing__to__GRing_Nmodule
           (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
             (word riscv_decl.reg_size)))

(** val riscv_SLTU_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_SLTU_instr =
  coq_RTypeInstruction riscv_decl.reg_size (Obj.magic riscv_sltu_semi) "SLTU"
    "sltu"

(** val prim_SLTU : string * riscv_op prim_constructor **)

let prim_SLTU =
  ("SLTU", (primM SLTU))

(** val riscv_SLTIU_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_SLTIU_instr =
  coq_ITypeInstruction_12s riscv_decl.reg_size (Obj.magic riscv_sltu_semi)
    "SLTIU" "sltiu"

(** val prim_SLTIU : string * riscv_op prim_constructor **)

let prim_SLTIU =
  ("SLTIU", (primM SLTIU))

(** val riscv_and_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let riscv_and_semi wn wm =
  wand riscv_decl.reg_size wn wm

(** val riscv_AND_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_AND_instr =
  coq_RTypeInstruction riscv_decl.reg_size (Obj.magic riscv_and_semi) "AND"
    "and"

(** val prim_AND : string * riscv_op prim_constructor **)

let prim_AND =
  ("AND", (primM AND))

(** val riscv_ANDI_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_ANDI_instr =
  coq_ITypeInstruction_12s riscv_decl.reg_size (Obj.magic riscv_and_semi)
    "ANDI" "andi"

(** val prim_ANDI : string * riscv_op prim_constructor **)

let prim_ANDI =
  ("ANDI", (primM ANDI))

(** val riscv_or_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let riscv_or_semi wn wm =
  wor riscv_decl.reg_size wn wm

(** val riscv_OR_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_OR_instr =
  coq_RTypeInstruction riscv_decl.reg_size (Obj.magic riscv_or_semi) "OR" "or"

(** val prim_OR : string * riscv_op prim_constructor **)

let prim_OR =
  ("OR", (primM OR))

(** val riscv_ORI_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_ORI_instr =
  coq_ITypeInstruction_12s riscv_decl.reg_size (Obj.magic riscv_or_semi)
    "ORI" "ori"

(** val prim_ORI : string * riscv_op prim_constructor **)

let prim_ORI =
  ("ORI", (primM ORI))

(** val riscv_xor_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let riscv_xor_semi wn wm =
  wxor riscv_decl.reg_size wn wm

(** val riscv_XOR_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_XOR_instr =
  coq_RTypeInstruction riscv_decl.reg_size (Obj.magic riscv_xor_semi) "XOR"
    "xor"

(** val prim_XOR : string * riscv_op prim_constructor **)

let prim_XOR =
  ("XOR", (primM XOR))

(** val riscv_XORI_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_XORI_instr =
  coq_ITypeInstruction_12s riscv_decl.reg_size (Obj.magic riscv_xor_semi)
    "XORI" "xori"

(** val prim_XORI : string * riscv_op prim_constructor **)

let prim_XORI =
  ("XORI", (primM XORI))

(** val riscv_sll_semi : sem_tuple -> GRing.ComRing.sort -> sem_tuple **)

let riscv_sll_semi wn wm =
  wshl riscv_decl.reg_size wn
    (wunsigned U8
      (wand U8 wm
        (wrepr U8 (Zpos (Coq_xI (Coq_xI (Coq_xI (Coq_xI Coq_xH))))))))

(** val riscv_SLL_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_SLL_instr =
  coq_RTypeInstruction U8 (Obj.magic riscv_sll_semi) "SLL" "sll"

(** val prim_SLL : string * riscv_op prim_constructor **)

let prim_SLL =
  ("SLL", (primM SLL))

(** val riscv_SLLI_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_SLLI_instr =
  coq_ITypeInstruction_5u U8 (Obj.magic riscv_sll_semi) "SLLI" "slli"

(** val prim_SLLI : string * riscv_op prim_constructor **)

let prim_SLLI =
  ("SLLI", (primM SLLI))

(** val riscv_srl_semi : sem_tuple -> GRing.ComRing.sort -> sem_tuple **)

let riscv_srl_semi wn wm =
  wshr riscv_decl.reg_size wn
    (wunsigned U8
      (wand U8 wm
        (wrepr U8 (Zpos (Coq_xI (Coq_xI (Coq_xI (Coq_xI Coq_xH))))))))

(** val riscv_SRL_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_SRL_instr =
  coq_RTypeInstruction U8 (Obj.magic riscv_srl_semi) "SRL" "srl"

(** val prim_SRL : string * riscv_op prim_constructor **)

let prim_SRL =
  ("SRL", (primM SRL))

(** val riscv_SRLI_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_SRLI_instr =
  coq_ITypeInstruction_5u U8 (Obj.magic riscv_srl_semi) "SRLI" "srli"

(** val prim_SRLI : string * riscv_op prim_constructor **)

let prim_SRLI =
  ("SRLI", (primM SRLI))

(** val riscv_sra_semi : sem_tuple -> GRing.ComRing.sort -> sem_tuple **)

let riscv_sra_semi wn wm =
  wsar riscv_decl.reg_size wn
    (wunsigned U8
      (wand U8 wm
        (wrepr U8 (Zpos (Coq_xI (Coq_xI (Coq_xI (Coq_xI Coq_xH))))))))

(** val riscv_SRA_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_SRA_instr =
  coq_RTypeInstruction U8 (Obj.magic riscv_sra_semi) "SRA" "sra"

(** val prim_SRA : string * riscv_op prim_constructor **)

let prim_SRA =
  ("SRA", (primM SRA))

(** val riscv_SRAI_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_SRAI_instr =
  coq_ITypeInstruction_5u U8 (Obj.magic riscv_sra_semi) "SRAI" "srai"

(** val prim_SRAI : string * riscv_op prim_constructor **)

let prim_SRAI =
  ("SRAI", (primM SRAI))

(** val riscv_MV_semi : sem_tuple -> sem_tuple **)

let riscv_MV_semi wn =
  wn

(** val riscv_MV_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_MV_instr =
  let tin = (sreg riscv_decl) :: [] in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea riscv_decl (S O)) :: []); id_tout = ((sreg riscv_decl) :: []);
  id_out = ((coq_Ea riscv_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic riscv_MV_semi)); id_args_kinds = ak_reg_reg;
  id_nargs = (S (S O)); id_str_jas = (pp_s "MV"); id_safe = []; id_pp_asm =
  (pp_name "mv") }

(** val prim_MV : string * riscv_op prim_constructor **)

let prim_MV =
  ("MV", (primM MV))

(** val riscv_LA_semi : sem_tuple -> sem_tuple **)

let riscv_LA_semi wn =
  wn

(** val riscv_LA_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_LA_instr =
  let tin = (sreg riscv_decl) :: [] in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin =
  ((sreg riscv_decl) :: []); id_in = ((coq_Ec riscv_decl (S O)) :: []);
  id_tout = ((sreg riscv_decl) :: []); id_out =
  ((coq_Ea riscv_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic riscv_LA_semi)); id_args_kinds = ak_reg_addr;
  id_nargs = (S (S O)); id_str_jas = (pp_s "LA"); id_safe = []; id_pp_asm =
  (pp_name "la") }

(** val prim_LA : string * riscv_op prim_constructor **)

let prim_LA =
  ("LA", (primM LA))

(** val riscv_LI_semi : sem_tuple -> sem_tuple **)

let riscv_LI_semi wn =
  wn

(** val riscv_LI_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_LI_instr =
  let tin = (sreg riscv_decl) :: [] in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea riscv_decl (S O)) :: []); id_tout = ((sreg riscv_decl) :: []);
  id_out = ((coq_Ea riscv_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic riscv_LI_semi)); id_args_kinds =
  (ak_reg_imm riscv_decl); id_nargs = (S (S O)); id_str_jas = (pp_s "LI");
  id_safe = []; id_pp_asm = (pp_name "li") }

(** val prim_LI : string * riscv_op prim_constructor **)

let prim_LI =
  ("LI", (primM LI))

(** val riscv_NOT_semi : sem_tuple -> sem_tuple **)

let riscv_NOT_semi wn =
  wnot riscv_decl.reg_size wn

(** val riscv_NOT_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_NOT_instr =
  let tin = (sreg riscv_decl) :: [] in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea riscv_decl (S O)) :: []); id_tout = ((sreg riscv_decl) :: []);
  id_out = ((coq_Ea riscv_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic riscv_NOT_semi)); id_args_kinds = ak_reg_reg;
  id_nargs = (S (S O)); id_str_jas = (pp_s "NOT"); id_safe = []; id_pp_asm =
  (pp_name "not") }

(** val prim_NOT : string * riscv_op prim_constructor **)

let prim_NOT =
  ("NOT", (primM NOT))

(** val riscv_NEG_semi : sem_tuple -> sem_tuple **)

let riscv_NEG_semi wn =
  GRing.opp
    (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Zmodule
      (word riscv_decl.reg_size)) wn

(** val riscv_NEG_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_NEG_instr =
  let tin = (sreg riscv_decl) :: [] in
  { id_valid = true; id_msb_flag = MSB_MERGE; id_tin = tin; id_in =
  ((coq_Ea riscv_decl (S O)) :: []); id_tout = ((sreg riscv_decl) :: []);
  id_out = ((coq_Ea riscv_decl O) :: []); id_semi =
  (sem_prod_ok tin (Obj.magic riscv_NEG_semi)); id_args_kinds = ak_reg_reg;
  id_nargs = (S (S O)); id_str_jas = (pp_s "NEG"); id_safe = []; id_pp_asm =
  (pp_name "neg") }

(** val prim_NEG : string * riscv_op prim_constructor **)

let prim_NEG =
  ("NEG", (primM NOT))

(** val string_of_sign : signedness -> string **)

let string_of_sign = function
| Signed -> ""
| Unsigned -> "u"

(** val string_of_size : wsize -> string **)

let string_of_size = function
| U8 -> "b"
| U16 -> "h"
| U32 -> "w"
| _ -> ""

(** val pp_sign_sz : string -> signedness -> wsize -> unit -> string **)

let pp_sign_sz s sign sz _ =
  (^) s
    ((^) "_"
      ((^) (match sign with
            | Signed -> "s"
            | Unsigned -> "u") (string_of_wsize sz)))

(** val riscv_extend_semi :
    signedness -> wsize -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let riscv_extend_semi s ws' ws w =
  match s with
  | Signed -> sign_extend ws' ws w
  | Unsigned -> zero_extend ws' ws w

(** val riscv_LOAD_instr :
    signedness -> wsize -> (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_LOAD_instr s ws =
  let tin = (Coq_sword ws) :: [] in
  let semi = riscv_extend_semi s riscv_decl.reg_size ws in
  { id_valid =
  (match s with
   | Signed -> cmp_le wsize_cmp ws U32
   | Unsigned -> cmp_le wsize_cmp ws U16); id_msb_flag = MSB_MERGE; id_tin =
  tin; id_in = ((coq_Eu riscv_decl (S O)) :: []); id_tout =
  ((sreg riscv_decl) :: []); id_out = ((coq_Ea riscv_decl O) :: []);
  id_semi = (sem_prod_ok tin (Obj.magic semi)); id_args_kinds = ak_reg_addr;
  id_nargs = (S (S O)); id_str_jas = (pp_sign_sz "LOAD" s ws); id_safe = [];
  id_pp_asm =
  (pp_name ((^) "l" ((^) (string_of_size ws) (string_of_sign s)))) }

(** val primS :
    (signedness -> wsize -> riscv_op) -> riscv_op prim_constructor **)

let primS f =
  PrimX86
    ((cat (map (fun ws -> PVs (Signed, ws)) (U8 :: (U16 :: (U32 :: []))))
       (map (fun ws -> PVs (Unsigned, ws)) (U8 :: (U16 :: [])))), (fun s ->
    match s with
    | PVs (sg, ws) -> Some (f sg ws)
    | _ -> None))

(** val prim_LOAD : string * riscv_op prim_constructor **)

let prim_LOAD =
  ("LOAD", (primS (fun x x0 -> LOAD (x, x0))))

(** val riscv_STORE_instr :
    wsize -> (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_STORE_instr ws =
  let tin = (Coq_sword ws) :: [] in
  let semi = riscv_extend_semi Unsigned ws ws in
  { id_valid = (cmp_le wsize_cmp ws U32); id_msb_flag = MSB_MERGE; id_tin =
  ((Coq_sword ws) :: []); id_in = ((coq_Ea riscv_decl O) :: []); id_tout =
  ((Coq_sword ws) :: []); id_out = ((coq_Eu riscv_decl (S O)) :: []);
  id_semi = (sem_prod_ok tin (Obj.magic semi)); id_args_kinds = ak_reg_addr;
  id_nargs = (S (S O)); id_str_jas = (pp_sz "STORE" ws); id_safe = [];
  id_pp_asm = (pp_name ((^) "s" (string_of_size ws))) }

(** val prim_STORE : string * riscv_op prim_constructor **)

let prim_STORE =
  ("STORE", (primP (arch_pd riscv_decl) (fun x -> STORE x)))

(** val riscv_mul_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let riscv_mul_semi wn wm =
  GRing.mul
    (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
      (word riscv_decl.reg_size)) wn wm

(** val riscv_MUL_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_MUL_instr =
  coq_RTypeInstruction riscv_decl.reg_size (Obj.magic riscv_mul_semi) "MUL"
    "mul"

(** val prim_MUL : string * riscv_op prim_constructor **)

let prim_MUL =
  ("MUL", (primM MUL))

(** val riscv_mulh_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let riscv_mulh_semi wn wm =
  wmulhs riscv_decl.reg_size wn wm

(** val riscv_MULH_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_MULH_instr =
  coq_RTypeInstruction riscv_decl.reg_size (Obj.magic riscv_mulh_semi) "MULH"
    "mulh"

(** val prim_MULH : string * riscv_op prim_constructor **)

let prim_MULH =
  ("MULH", (primM MULH))

(** val riscv_mulhu_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let riscv_mulhu_semi wn wm =
  wmulhu riscv_decl.reg_size wn wm

(** val riscv_MULHU_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_MULHU_instr =
  coq_RTypeInstruction riscv_decl.reg_size (Obj.magic riscv_mulhu_semi)
    "MULHU" "mulhu"

(** val prim_MULHU : string * riscv_op prim_constructor **)

let prim_MULHU =
  ("MULHU", (primM MULHU))

(** val riscv_mulhsu_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let riscv_mulhsu_semi wn wm =
  wmulhsu riscv_decl.reg_size wn wm

(** val riscv_MULHSU_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_MULHSU_instr =
  coq_RTypeInstruction riscv_decl.reg_size (Obj.magic riscv_mulhsu_semi)
    "MULHSU" "mulhsu"

(** val prim_MULHSU : string * riscv_op prim_constructor **)

let prim_MULHSU =
  ("MULHSU", (primM MULHSU))

(** val riscv_div_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let riscv_div_semi wn wm =
  if eq_op
       (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality
         (word riscv_decl.reg_size)) wm
       (GRing.zero
         (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
           (word riscv_decl.reg_size)))
  then GRing.opp
         (GRing.Ring.Exports.join_GRing_Ring_between_GRing_SemiRing_and_GRing_Zmodule
           (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Ring
             (word riscv_decl.reg_size)))
         (GRing.one
           (GRing.Ring.Exports.coq_GRing_Ring__to__GRing_SemiRing
             (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Ring
               (word riscv_decl.reg_size))))
  else wdivi riscv_decl.reg_size wn wm

(** val riscv_DIV_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_DIV_instr =
  coq_RTypeInstruction riscv_decl.reg_size (Obj.magic riscv_div_semi) "DIV"
    "div"

(** val prim_DIV : string * riscv_op prim_constructor **)

let prim_DIV =
  ("DIV", (primM DIV))

(** val riscv_divu_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let riscv_divu_semi wn wm =
  if eq_op
       (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality
         (word riscv_decl.reg_size)) wm
       (GRing.zero
         (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
           (word riscv_decl.reg_size)))
  then GRing.opp
         (GRing.Ring.Exports.join_GRing_Ring_between_GRing_SemiRing_and_GRing_Zmodule
           (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Ring
             (word riscv_decl.reg_size)))
         (GRing.one
           (GRing.Ring.Exports.coq_GRing_Ring__to__GRing_SemiRing
             (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Ring
               (word riscv_decl.reg_size))))
  else wdiv riscv_decl.reg_size wn wm

(** val riscv_DIVU_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_DIVU_instr =
  coq_RTypeInstruction riscv_decl.reg_size (Obj.magic riscv_divu_semi) "DIVU"
    "divu"

(** val prim_DIVU : string * riscv_op prim_constructor **)

let prim_DIVU =
  ("DIVU", (primM DIVU))

(** val riscv_rem_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let riscv_rem_semi wn wm =
  wmodi riscv_decl.reg_size wn wm

(** val riscv_REM_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_REM_instr =
  coq_RTypeInstruction riscv_decl.reg_size (Obj.magic riscv_rem_semi) "REM"
    "rem"

(** val prim_REM : string * riscv_op prim_constructor **)

let prim_REM =
  ("REM", (primM REM))

(** val riscv_remu_semi : sem_tuple -> sem_tuple -> sem_tuple **)

let riscv_remu_semi wn wm =
  wmod riscv_decl.reg_size wn wm

(** val riscv_REMU_instr :
    (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_REMU_instr =
  coq_RTypeInstruction riscv_decl.reg_size (Obj.magic riscv_remu_semi) "REMU"
    "remu"

(** val prim_REMU : string * riscv_op prim_constructor **)

let prim_REMU =
  ("REMU", (primM REMU))

(** val riscv_instr_desc :
    riscv_op -> (register, empty, empty, empty, condt) instr_desc_t **)

let riscv_instr_desc = function
| ADD -> riscv_ADD_instr
| ADDI -> riscv_ADDI_instr
| SUB -> riscv_SUB_instr
| SLT -> riscv_SLT_instr
| SLTI -> riscv_SLTI_instr
| SLTU -> riscv_SLTU_instr
| SLTIU -> riscv_SLTIU_instr
| AND -> riscv_AND_instr
| ANDI -> riscv_ANDI_instr
| OR -> riscv_OR_instr
| ORI -> riscv_ORI_instr
| XOR -> riscv_XOR_instr
| XORI -> riscv_XORI_instr
| SLL -> riscv_SLL_instr
| SLLI -> riscv_SLLI_instr
| SRL -> riscv_SRL_instr
| SRLI -> riscv_SRLI_instr
| SRA -> riscv_SRA_instr
| SRAI -> riscv_SRAI_instr
| MV -> riscv_MV_instr
| LA -> riscv_LA_instr
| LI -> riscv_LI_instr
| NOT -> riscv_NOT_instr
| NEG -> riscv_NEG_instr
| LOAD (s, ws) -> riscv_LOAD_instr s ws
| STORE ws -> riscv_STORE_instr ws
| MUL -> riscv_MUL_instr
| MULH -> riscv_MULH_instr
| MULHU -> riscv_MULHU_instr
| MULHSU -> riscv_MULHSU_instr
| DIV -> riscv_DIV_instr
| DIVU -> riscv_DIVU_instr
| REM -> riscv_REM_instr
| REMU -> riscv_REMU_instr

(** val riscv_prim_string : (string * riscv_op prim_constructor) list **)

let riscv_prim_string =
  prim_ADD :: (prim_ADDI :: (prim_SUB :: (prim_SLT :: (prim_SLTI :: (prim_SLTU :: (prim_SLTIU :: (prim_AND :: (prim_ANDI :: (prim_OR :: (prim_ORI :: (prim_XOR :: (prim_XORI :: (prim_SLL :: (prim_SLLI :: (prim_SRL :: (prim_SRLI :: (prim_SRA :: (prim_SRAI :: (prim_MV :: (prim_LA :: (prim_LI :: (prim_NOT :: (prim_NEG :: (prim_LOAD :: (prim_STORE :: (prim_MUL :: (prim_MULH :: (prim_MULHU :: (prim_MULHSU :: (prim_DIV :: (prim_DIVU :: (prim_REM :: (prim_REMU :: [])))))))))))))))))))))))))))))))))

(** val riscv_op_decl :
    (register, empty, empty, empty, condt, riscv_op) asm_op_decl **)

let riscv_op_decl =
  { Arch_decl._eqT = eqTC_riscv_op; instr_desc_op = riscv_instr_desc;
    Arch_decl.prim_string = riscv_prim_string }

type riscv_prog = (register, empty, empty, empty, condt, riscv_op) asm_prog
