open BinNums
open Datatypes
open Arch_decl
open Arch_utils
open Eqtype
open Expr
open Riscv_decl
open Sem_type
open Seq
open Sopn
open Ssralg
open Type
open Utils0
open Word0
open Wsize

module E :
 sig
  val no_semantics : error
 end

val pp_name :
  string -> (register, empty, empty, empty, condt) asm_arg list -> (register,
  empty, empty, empty, condt) pp_asm_op

val coq_RTypeInstruction :
  wsize -> sem_tuple sem_prod -> string -> string -> (register, empty, empty,
  empty, condt) instr_desc_t

val coq_ITypeInstruction :
  caimm_checker_s -> wsize -> sem_tuple sem_prod -> string -> string ->
  (register, empty, empty, empty, condt) instr_desc_t

val coq_ITypeInstruction_12s :
  wsize -> sem_tuple sem_prod -> string -> string -> (register, empty, empty,
  empty, condt) instr_desc_t

val coq_ITypeInstruction_5u :
  wsize -> sem_tuple sem_prod -> string -> string -> (register, empty, empty,
  empty, condt) instr_desc_t

type riscv_op =
| ADD
| ADDI
| SUB
| SLT
| SLTI
| SLTU
| SLTIU
| AND
| ANDI
| OR
| ORI
| XOR
| XORI
| SLL
| SLLI
| SRL
| SRLI
| SRA
| SRAI
| MV
| LA
| LI
| NOT
| NEG
| LOAD of signedness * wsize
| STORE of wsize
| MUL
| MULH
| MULHU
| MULHSU
| DIV
| DIVU
| REM
| REMU

val riscv_op_beq : riscv_op -> riscv_op -> bool

val riscv_op_eq_dec : riscv_op -> riscv_op -> bool

val riscv_op_eq_axiom : riscv_op eq_axiom

val eqTC_riscv_op : riscv_op eqTypeC

val riscv_op_eqType : Equality.coq_type

val riscv_add_semi : sem_tuple -> sem_tuple -> sem_tuple

val riscv_ADD_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_ADD : string * riscv_op prim_constructor

val riscv_ADDI_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_ADDI : string * riscv_op prim_constructor

val riscv_sub_semi : sem_tuple -> sem_tuple -> sem_tuple

val riscv_SUB_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_SUB : string * riscv_op prim_constructor

val riscv_slt_semi : sem_tuple -> sem_tuple -> sem_tuple

val riscv_SLT_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_SLT : string * riscv_op prim_constructor

val riscv_SLTI_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_SLTI : string * riscv_op prim_constructor

val riscv_sltu_semi : sem_tuple -> sem_tuple -> sem_tuple

val riscv_SLTU_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_SLTU : string * riscv_op prim_constructor

val riscv_SLTIU_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_SLTIU : string * riscv_op prim_constructor

val riscv_and_semi : sem_tuple -> sem_tuple -> sem_tuple

val riscv_AND_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_AND : string * riscv_op prim_constructor

val riscv_ANDI_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_ANDI : string * riscv_op prim_constructor

val riscv_or_semi : sem_tuple -> sem_tuple -> sem_tuple

val riscv_OR_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_OR : string * riscv_op prim_constructor

val riscv_ORI_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_ORI : string * riscv_op prim_constructor

val riscv_xor_semi : sem_tuple -> sem_tuple -> sem_tuple

val riscv_XOR_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_XOR : string * riscv_op prim_constructor

val riscv_XORI_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_XORI : string * riscv_op prim_constructor

val riscv_sll_semi : sem_tuple -> GRing.ComRing.sort -> sem_tuple

val riscv_SLL_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_SLL : string * riscv_op prim_constructor

val riscv_SLLI_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_SLLI : string * riscv_op prim_constructor

val riscv_srl_semi : sem_tuple -> GRing.ComRing.sort -> sem_tuple

val riscv_SRL_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_SRL : string * riscv_op prim_constructor

val riscv_SRLI_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_SRLI : string * riscv_op prim_constructor

val riscv_sra_semi : sem_tuple -> GRing.ComRing.sort -> sem_tuple

val riscv_SRA_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_SRA : string * riscv_op prim_constructor

val riscv_SRAI_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_SRAI : string * riscv_op prim_constructor

val riscv_MV_semi : sem_tuple -> sem_tuple

val riscv_MV_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_MV : string * riscv_op prim_constructor

val riscv_LA_semi : sem_tuple -> sem_tuple

val riscv_LA_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_LA : string * riscv_op prim_constructor

val riscv_LI_semi : sem_tuple -> sem_tuple

val riscv_LI_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_LI : string * riscv_op prim_constructor

val riscv_NOT_semi : sem_tuple -> sem_tuple

val riscv_NOT_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_NOT : string * riscv_op prim_constructor

val riscv_NEG_semi : sem_tuple -> sem_tuple

val riscv_NEG_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_NEG : string * riscv_op prim_constructor

val string_of_sign : signedness -> string

val string_of_size : wsize -> string

val pp_sign_sz : string -> signedness -> wsize -> unit -> string

val riscv_extend_semi :
  signedness -> wsize -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort

val riscv_LOAD_instr :
  signedness -> wsize -> (register, empty, empty, empty, condt) instr_desc_t

val primS : (signedness -> wsize -> riscv_op) -> riscv_op prim_constructor

val prim_LOAD : string * riscv_op prim_constructor

val riscv_STORE_instr :
  wsize -> (register, empty, empty, empty, condt) instr_desc_t

val prim_STORE : string * riscv_op prim_constructor

val riscv_mul_semi : sem_tuple -> sem_tuple -> sem_tuple

val riscv_MUL_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_MUL : string * riscv_op prim_constructor

val riscv_mulh_semi : sem_tuple -> sem_tuple -> sem_tuple

val riscv_MULH_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_MULH : string * riscv_op prim_constructor

val riscv_mulhu_semi : sem_tuple -> sem_tuple -> sem_tuple

val riscv_MULHU_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_MULHU : string * riscv_op prim_constructor

val riscv_mulhsu_semi : sem_tuple -> sem_tuple -> sem_tuple

val riscv_MULHSU_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_MULHSU : string * riscv_op prim_constructor

val riscv_div_semi : sem_tuple -> sem_tuple -> sem_tuple

val riscv_DIV_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_DIV : string * riscv_op prim_constructor

val riscv_divu_semi : sem_tuple -> sem_tuple -> sem_tuple

val riscv_DIVU_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_DIVU : string * riscv_op prim_constructor

val riscv_rem_semi : sem_tuple -> sem_tuple -> sem_tuple

val riscv_REM_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_REM : string * riscv_op prim_constructor

val riscv_remu_semi : sem_tuple -> sem_tuple -> sem_tuple

val riscv_REMU_instr : (register, empty, empty, empty, condt) instr_desc_t

val prim_REMU : string * riscv_op prim_constructor

val riscv_instr_desc :
  riscv_op -> (register, empty, empty, empty, condt) instr_desc_t

val riscv_prim_string : (string * riscv_op prim_constructor) list

val riscv_op_decl :
  (register, empty, empty, empty, condt, riscv_op) asm_op_decl

type riscv_prog = (register, empty, empty, empty, condt, riscv_op) asm_prog
