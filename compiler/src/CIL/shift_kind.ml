open BinNums
open Eqtype
open Utils0

type shift_kind =
| SLSL
| SLSR
| SASR
| SROR

(** val shift_kind_beq : shift_kind -> shift_kind -> bool **)

let shift_kind_beq x y =
  match x with
  | SLSL -> (match y with
             | SLSL -> true
             | _ -> false)
  | SLSR -> (match y with
             | SLSR -> true
             | _ -> false)
  | SASR -> (match y with
             | SASR -> true
             | _ -> false)
  | SROR -> (match y with
             | SROR -> true
             | _ -> false)

(** val shift_kind_eq_axiom : shift_kind eq_axiom **)

let shift_kind_eq_axiom =
  eq_axiom_of_scheme shift_kind_beq

(** val coq_HB_unnamed_factory_1 : shift_kind Coq_hasDecEq.axioms_ **)

let coq_HB_unnamed_factory_1 =
  { Coq_hasDecEq.eq_op = shift_kind_beq; Coq_hasDecEq.eqP =
    shift_kind_eq_axiom }

(** val shift_kind_shift_kind__canonical__eqtype_Equality :
    Equality.coq_type **)

let shift_kind_shift_kind__canonical__eqtype_Equality =
  Obj.magic coq_HB_unnamed_factory_1

(** val shift_amount_bounds : shift_kind -> coq_Z * coq_Z **)

let shift_amount_bounds = function
| SLSL -> (Z0, (Zpos (Coq_xI (Coq_xI (Coq_xI (Coq_xI Coq_xH))))))
| SROR -> ((Zpos Coq_xH), (Zpos (Coq_xI (Coq_xI (Coq_xI (Coq_xI Coq_xH))))))
| _ ->
  ((Zpos Coq_xH), (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH)))))))
