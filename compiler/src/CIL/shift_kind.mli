open BinNums
open Eqtype
open Utils0

type shift_kind =
| SLSL
| SLSR
| SASR
| SROR

val shift_kind_beq : shift_kind -> shift_kind -> bool

val shift_kind_eq_axiom : shift_kind eq_axiom

val coq_HB_unnamed_factory_1 : shift_kind Coq_hasDecEq.axioms_

val shift_kind_shift_kind__canonical__eqtype_Equality : Equality.coq_type

val shift_amount_bounds : shift_kind -> coq_Z * coq_Z
