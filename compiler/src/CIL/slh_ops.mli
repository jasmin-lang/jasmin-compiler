open BinNums
open Eqtype
open Type
open Utils0
open Wsize

type slh_op =
| SLHinit
| SLHupdate
| SLHmove
| SLHprotect of wsize
| SLHprotect_ptr of positive
| SLHprotect_ptr_fail of positive

val slh_op_beq : slh_op -> slh_op -> bool

val slh_op_eq_axiom : slh_op eq_axiom

val coq_HB_unnamed_factory_1 : slh_op Coq_hasDecEq.axioms_

val slh_ops_slh_op__canonical__eqtype_Equality : Equality.coq_type
