open Eqtype
open Utils0

type stack_zero_strategy =
| SZSloop
| SZSloopSCT
| SZSunrolled

(** val stack_zero_strategy_list : stack_zero_strategy list **)

let stack_zero_strategy_list =
  SZSloop :: (SZSloopSCT :: (SZSunrolled :: []))

(** val stack_zero_strategy_beq :
    stack_zero_strategy -> stack_zero_strategy -> bool **)

let stack_zero_strategy_beq x y =
  match x with
  | SZSloop -> (match y with
                | SZSloop -> true
                | _ -> false)
  | SZSloopSCT -> (match y with
                   | SZSloopSCT -> true
                   | _ -> false)
  | SZSunrolled -> (match y with
                    | SZSunrolled -> true
                    | _ -> false)

(** val stack_zero_strategy_eq_dec :
    stack_zero_strategy -> stack_zero_strategy -> bool **)

let stack_zero_strategy_eq_dec x y =
  let b = stack_zero_strategy_beq x y in if b then true else false

(** val stack_zero_strategy_eq_axiom : stack_zero_strategy eq_axiom **)

let stack_zero_strategy_eq_axiom =
  eq_axiom_of_scheme stack_zero_strategy_beq

(** val coq_HB_unnamed_factory_1 :
    stack_zero_strategy Coq_hasDecEq.axioms_ **)

let coq_HB_unnamed_factory_1 =
  { Coq_hasDecEq.eq_op = stack_zero_strategy_beq; Coq_hasDecEq.eqP =
    stack_zero_strategy_eq_axiom }

(** val stack_zero_strategy_stack_zero_strategy__canonical__eqtype_Equality :
    Equality.coq_type **)

let stack_zero_strategy_stack_zero_strategy__canonical__eqtype_Equality =
  Obj.magic coq_HB_unnamed_factory_1
