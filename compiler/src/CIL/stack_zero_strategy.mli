open Eqtype
open Utils0

type stack_zero_strategy =
| SZSloop
| SZSloopSCT
| SZSunrolled

val stack_zero_strategy_list : stack_zero_strategy list

val stack_zero_strategy_beq :
  stack_zero_strategy -> stack_zero_strategy -> bool

val stack_zero_strategy_eq_dec :
  stack_zero_strategy -> stack_zero_strategy -> bool

val stack_zero_strategy_eq_axiom : stack_zero_strategy eq_axiom

val coq_HB_unnamed_factory_1 : stack_zero_strategy Coq_hasDecEq.axioms_

val stack_zero_strategy_stack_zero_strategy__canonical__eqtype_Equality :
  Equality.coq_type
