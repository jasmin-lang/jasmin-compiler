open BinNums
open Eqtype
open Ssralg
open Type
open Utils0
open Wsize

val syscall_t_beq :
  BinNums.positive Syscall_t.syscall_t ->
  BinNums.positive Syscall_t.syscall_t -> bool

val syscall_t_eq_axiom : BinNums.positive Syscall_t.syscall_t eq_axiom

val coq_HB_unnamed_factory_1 :
  BinNums.positive Syscall_t.syscall_t Coq_hasDecEq.axioms_

val syscall_syscall_t__canonical__eqtype_Equality : Equality.coq_type

type syscall_sig_t = { scs_tin : stype list; scs_tout : stype list }

val syscall_sig_u : BinNums.positive Syscall_t.syscall_t -> syscall_sig_t

val syscall_sig_s :
  coq_PointerData -> BinNums.positive Syscall_t.syscall_t -> syscall_sig_t

type 'syscall_state syscall_sem =
  'syscall_state -> coq_Z -> 'syscall_state * GRing.ComRing.sort list
  (* singleton inductive, whose constructor was Build_syscall_sem *)

type 'syscall_state syscall_state_t = 'syscall_state
