open BinNums
open Datatypes
open Eqtype
open Utils0

type wsize =
| U8
| U16
| U32
| U64
| U128
| U256

type velem =
| VE8
| VE16
| VE32
| VE64

(** val wsize_of_velem : velem -> wsize **)

let wsize_of_velem = function
| VE8 -> U8
| VE16 -> U16
| VE32 -> U32
| VE64 -> U64

type pelem =
| PE1
| PE2
| PE4
| PE8
| PE16
| PE32
| PE64
| PE128

type signedness =
| Signed
| Unsigned

(** val wsize_beq : wsize -> wsize -> bool **)

let wsize_beq x y =
  match x with
  | U8 -> (match y with
           | U8 -> true
           | _ -> false)
  | U16 -> (match y with
            | U16 -> true
            | _ -> false)
  | U32 -> (match y with
            | U32 -> true
            | _ -> false)
  | U64 -> (match y with
            | U64 -> true
            | _ -> false)
  | U128 -> (match y with
             | U128 -> true
             | _ -> false)
  | U256 -> (match y with
             | U256 -> true
             | _ -> false)

(** val wsize_eq_dec : wsize -> wsize -> bool **)

let wsize_eq_dec x y =
  let b = wsize_beq x y in if b then true else false

(** val wsize_axiom : wsize eq_axiom **)

let wsize_axiom =
  eq_axiom_of_scheme wsize_beq

(** val coq_HB_unnamed_factory_1 : wsize Coq_hasDecEq.axioms_ **)

let coq_HB_unnamed_factory_1 =
  { Coq_hasDecEq.eq_op = wsize_beq; Coq_hasDecEq.eqP = wsize_axiom }

(** val wsize_wsize__canonical__eqtype_Equality : Equality.coq_type **)

let wsize_wsize__canonical__eqtype_Equality =
  Obj.magic coq_HB_unnamed_factory_1

(** val wsizes : wsize list **)

let wsizes =
  U8 :: (U16 :: (U32 :: (U64 :: (U128 :: (U256 :: [])))))

(** val wsize_cmp : wsize -> wsize -> comparison **)

let wsize_cmp s s' =
  match s with
  | U8 -> (match s' with
           | U8 -> Eq
           | _ -> Lt)
  | U16 -> (match s' with
            | U8 -> Gt
            | U16 -> Eq
            | _ -> Lt)
  | U32 -> (match s' with
            | U8 -> Gt
            | U16 -> Gt
            | U32 -> Eq
            | _ -> Lt)
  | U64 -> (match s' with
            | U64 -> Eq
            | U128 -> Lt
            | U256 -> Lt
            | _ -> Gt)
  | U128 -> (match s' with
             | U128 -> Eq
             | U256 -> Lt
             | _ -> Gt)
  | U256 -> (match s' with
             | U256 -> Eq
             | _ -> Gt)

(** val size_8_16 : wsize -> bool **)

let size_8_16 sz =
  cmp_le wsize_cmp sz U16

(** val size_8_32 : wsize -> bool **)

let size_8_32 sz =
  cmp_le wsize_cmp sz U64

(** val size_8_64 : wsize -> bool **)

let size_8_64 sz =
  cmp_le wsize_cmp sz U64

(** val size_16_32 : wsize -> bool **)

let size_16_32 sz =
  (&&) (cmp_le wsize_cmp U16 sz) (cmp_le wsize_cmp sz U32)

(** val size_16_64 : wsize -> bool **)

let size_16_64 sz =
  (&&) (cmp_le wsize_cmp U16 sz) (cmp_le wsize_cmp sz U64)

(** val size_32_64 : wsize -> bool **)

let size_32_64 sz =
  (&&) (cmp_le wsize_cmp U32 sz) (cmp_le wsize_cmp sz U64)

(** val size_128_256 : wsize -> bool **)

let size_128_256 sz =
  (&&) (cmp_le wsize_cmp U128 sz) (cmp_le wsize_cmp sz U256)

(** val string_of_wsize : wsize -> string **)

let string_of_wsize = function
| U8 -> "8"
| U16 -> "16"
| U32 -> "32"
| U64 -> "64"
| U128 -> "128"
| U256 -> "256"

(** val string_of_ve_sz : velem -> wsize -> string **)

let string_of_ve_sz ve sz =
  match ve with
  | VE8 ->
    (match sz with
     | U8 -> "ERROR: please repport"
     | U16 -> "2u8"
     | U32 -> "4u8"
     | U64 -> "8u8"
     | U128 -> "16u8"
     | U256 -> "32u8")
  | VE16 ->
    (match sz with
     | U32 -> "2u16"
     | U64 -> "4u16"
     | U128 -> "8u16"
     | U256 -> "16u16"
     | _ -> "ERROR: please repport")
  | VE32 ->
    (match sz with
     | U64 -> "2u32"
     | U128 -> "4u32"
     | U256 -> "8u32"
     | _ -> "ERROR: please repport")
  | VE64 ->
    (match sz with
     | U128 -> "2u64"
     | U256 -> "4u64"
     | _ -> "ERROR: please repport")

(** val pp_s : string -> unit -> string **)

let pp_s s _ =
  s

(** val pp_sz : string -> wsize -> unit -> string **)

let pp_sz s sz _ =
  (^) s ((^) "_" (string_of_wsize sz))

(** val pp_ve_sz : string -> velem -> wsize -> unit -> string **)

let pp_ve_sz s ve sz _ =
  (^) s ((^) "_" (string_of_ve_sz ve sz))

(** val pp_ve_sz_ve_sz :
    string -> velem -> wsize -> velem -> wsize -> unit -> string **)

let pp_ve_sz_ve_sz s ve sz ve' sz' _ =
  (^) s
    ((^) "_"
      ((^) (string_of_ve_sz ve sz) ((^) "_" (string_of_ve_sz ve' sz'))))

(** val pp_sz_sz : string -> bool -> wsize -> wsize -> unit -> string **)

let pp_sz_sz s sign sz sz' _ =
  (^) s
    ((^) "_u"
      ((^) (string_of_wsize sz)
        ((^) (if sign then "s" else "u") (string_of_wsize sz'))))

type reg_kind =
| Normal
| Extra

type writable =
| Constant
| Writable

type reference =
| Direct
| Pointer of writable

type v_kind =
| Const
| Stack of reference
| Reg of (reg_kind * reference)
| Inline
| Global

type safe_cond =
| NotZero of wsize * nat
| X86Division of wsize * signedness
| InRangeMod32 of wsize * coq_Z * coq_Z * nat
| ULt of wsize * nat * coq_Z
| UGe of wsize * coq_Z * nat
| UaddLe of wsize * nat * nat * coq_Z
| AllInit of wsize * positive * nat
| ScFalse

type coq_PointerData =
  wsize
  (* singleton inductive, whose constructor was Build_PointerData *)

type coq_MSFsize =
  wsize
  (* singleton inductive, whose constructor was Build_MSFsize *)
