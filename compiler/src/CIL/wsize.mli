open BinNums
open Datatypes
open Eqtype
open Utils0

type wsize =
| U8
| U16
| U32
| U64
| U128
| U256

type velem =
| VE8
| VE16
| VE32
| VE64

val wsize_of_velem : velem -> wsize

type pelem =
| PE1
| PE2
| PE4
| PE8
| PE16
| PE32
| PE64
| PE128

type signedness =
| Signed
| Unsigned

val wsize_beq : wsize -> wsize -> bool

val wsize_eq_dec : wsize -> wsize -> bool

val wsize_axiom : wsize eq_axiom

val coq_HB_unnamed_factory_1 : wsize Coq_hasDecEq.axioms_

val wsize_wsize__canonical__eqtype_Equality : Equality.coq_type

val wsizes : wsize list

val wsize_cmp : wsize -> wsize -> comparison

val size_8_16 : wsize -> bool

val size_8_32 : wsize -> bool

val size_8_64 : wsize -> bool

val size_16_32 : wsize -> bool

val size_16_64 : wsize -> bool

val size_32_64 : wsize -> bool

val size_128_256 : wsize -> bool

val string_of_wsize : wsize -> string

val string_of_ve_sz : velem -> wsize -> string

val pp_s : string -> unit -> string

val pp_sz : string -> wsize -> unit -> string

val pp_ve_sz : string -> velem -> wsize -> unit -> string

val pp_ve_sz_ve_sz :
  string -> velem -> wsize -> velem -> wsize -> unit -> string

val pp_sz_sz : string -> bool -> wsize -> wsize -> unit -> string

type reg_kind =
| Normal
| Extra

type writable =
| Constant
| Writable

type reference =
| Direct
| Pointer of writable

type v_kind =
| Const
| Stack of reference
| Reg of (reg_kind * reference)
| Inline
| Global

type safe_cond =
| NotZero of wsize * nat
| X86Division of wsize * signedness
| InRangeMod32 of wsize * coq_Z * coq_Z * nat
| ULt of wsize * nat * coq_Z
| UGe of wsize * coq_Z * nat
| UaddLe of wsize * nat * nat * coq_Z
| AllInit of wsize * positive * nat
| ScFalse

type coq_PointerData =
  wsize
  (* singleton inductive, whose constructor was Build_PointerData *)

type coq_MSFsize =
  wsize
  (* singleton inductive, whose constructor was Build_MSFsize *)
