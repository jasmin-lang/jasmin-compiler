open Arch_decl
open Eqtype
open Fintype
open Flag_combination
open Seq
open Ssralg
open Utils0
open Wsize

(** val x86_reg_size : wsize **)

let x86_reg_size =
  U64

(** val x86_xreg_size : wsize **)

let x86_xreg_size =
  U256

type register =
| RAX
| RCX
| RDX
| RBX
| RSP
| RBP
| RSI
| RDI
| R8
| R9
| R10
| R11
| R12
| R13
| R14
| R15

type register_ext =
| MM0
| MM1
| MM2
| MM3
| MM4
| MM5
| MM6
| MM7

type xmm_register =
| XMM0
| XMM1
| XMM2
| XMM3
| XMM4
| XMM5
| XMM6
| XMM7
| XMM8
| XMM9
| XMM10
| XMM11
| XMM12
| XMM13
| XMM14
| XMM15

type rflag =
| CF
| PF
| ZF
| SF
| OF

type condt =
| O_ct
| NO_ct
| B_ct
| NB_ct
| E_ct
| NE_ct
| BE_ct
| NBE_ct
| S_ct
| NS_ct
| P_ct
| NP_ct
| L_ct
| NL_ct
| LE_ct
| NLE_ct

(** val register_beq : register -> register -> bool **)

let register_beq x y =
  match x with
  | RAX -> (match y with
            | RAX -> true
            | _ -> false)
  | RCX -> (match y with
            | RCX -> true
            | _ -> false)
  | RDX -> (match y with
            | RDX -> true
            | _ -> false)
  | RBX -> (match y with
            | RBX -> true
            | _ -> false)
  | RSP -> (match y with
            | RSP -> true
            | _ -> false)
  | RBP -> (match y with
            | RBP -> true
            | _ -> false)
  | RSI -> (match y with
            | RSI -> true
            | _ -> false)
  | RDI -> (match y with
            | RDI -> true
            | _ -> false)
  | R8 -> (match y with
           | R8 -> true
           | _ -> false)
  | R9 -> (match y with
           | R9 -> true
           | _ -> false)
  | R10 -> (match y with
            | R10 -> true
            | _ -> false)
  | R11 -> (match y with
            | R11 -> true
            | _ -> false)
  | R12 -> (match y with
            | R12 -> true
            | _ -> false)
  | R13 -> (match y with
            | R13 -> true
            | _ -> false)
  | R14 -> (match y with
            | R14 -> true
            | _ -> false)
  | R15 -> (match y with
            | R15 -> true
            | _ -> false)

(** val register_eq_dec : register -> register -> bool **)

let register_eq_dec x y =
  let b = register_beq x y in if b then true else false

(** val reg_eq_axiom : register eq_axiom **)

let reg_eq_axiom =
  eq_axiom_of_scheme register_beq

(** val coq_HB_unnamed_factory_1 : register Coq_hasDecEq.axioms_ **)

let coq_HB_unnamed_factory_1 =
  { Coq_hasDecEq.eq_op = register_beq; Coq_hasDecEq.eqP = reg_eq_axiom }

(** val x86_decl_register__canonical__eqtype_Equality : Equality.coq_type **)

let x86_decl_register__canonical__eqtype_Equality =
  Obj.magic coq_HB_unnamed_factory_1

(** val register_ext_beq : register_ext -> register_ext -> bool **)

let register_ext_beq x y =
  match x with
  | MM0 -> (match y with
            | MM0 -> true
            | _ -> false)
  | MM1 -> (match y with
            | MM1 -> true
            | _ -> false)
  | MM2 -> (match y with
            | MM2 -> true
            | _ -> false)
  | MM3 -> (match y with
            | MM3 -> true
            | _ -> false)
  | MM4 -> (match y with
            | MM4 -> true
            | _ -> false)
  | MM5 -> (match y with
            | MM5 -> true
            | _ -> false)
  | MM6 -> (match y with
            | MM6 -> true
            | _ -> false)
  | MM7 -> (match y with
            | MM7 -> true
            | _ -> false)

(** val register_ext_eq_dec : register_ext -> register_ext -> bool **)

let register_ext_eq_dec x y =
  let b = register_ext_beq x y in if b then true else false

(** val regx_eq_axiom : register_ext eq_axiom **)

let regx_eq_axiom =
  eq_axiom_of_scheme register_ext_beq

(** val coq_HB_unnamed_factory_3 : register_ext Coq_hasDecEq.axioms_ **)

let coq_HB_unnamed_factory_3 =
  { Coq_hasDecEq.eq_op = register_ext_beq; Coq_hasDecEq.eqP = regx_eq_axiom }

(** val x86_decl_register_ext__canonical__eqtype_Equality :
    Equality.coq_type **)

let x86_decl_register_ext__canonical__eqtype_Equality =
  Obj.magic coq_HB_unnamed_factory_3

(** val xmm_register_beq : xmm_register -> xmm_register -> bool **)

let xmm_register_beq x y =
  match x with
  | XMM0 -> (match y with
             | XMM0 -> true
             | _ -> false)
  | XMM1 -> (match y with
             | XMM1 -> true
             | _ -> false)
  | XMM2 -> (match y with
             | XMM2 -> true
             | _ -> false)
  | XMM3 -> (match y with
             | XMM3 -> true
             | _ -> false)
  | XMM4 -> (match y with
             | XMM4 -> true
             | _ -> false)
  | XMM5 -> (match y with
             | XMM5 -> true
             | _ -> false)
  | XMM6 -> (match y with
             | XMM6 -> true
             | _ -> false)
  | XMM7 -> (match y with
             | XMM7 -> true
             | _ -> false)
  | XMM8 -> (match y with
             | XMM8 -> true
             | _ -> false)
  | XMM9 -> (match y with
             | XMM9 -> true
             | _ -> false)
  | XMM10 -> (match y with
              | XMM10 -> true
              | _ -> false)
  | XMM11 -> (match y with
              | XMM11 -> true
              | _ -> false)
  | XMM12 -> (match y with
              | XMM12 -> true
              | _ -> false)
  | XMM13 -> (match y with
              | XMM13 -> true
              | _ -> false)
  | XMM14 -> (match y with
              | XMM14 -> true
              | _ -> false)
  | XMM15 -> (match y with
              | XMM15 -> true
              | _ -> false)

(** val xmm_register_eq_dec : xmm_register -> xmm_register -> bool **)

let xmm_register_eq_dec x y =
  let b = xmm_register_beq x y in if b then true else false

(** val xreg_eq_axiom : xmm_register eq_axiom **)

let xreg_eq_axiom =
  eq_axiom_of_scheme xmm_register_beq

(** val coq_HB_unnamed_factory_5 : xmm_register Coq_hasDecEq.axioms_ **)

let coq_HB_unnamed_factory_5 =
  { Coq_hasDecEq.eq_op = xmm_register_beq; Coq_hasDecEq.eqP = xreg_eq_axiom }

(** val x86_decl_xmm_register__canonical__eqtype_Equality :
    Equality.coq_type **)

let x86_decl_xmm_register__canonical__eqtype_Equality =
  Obj.magic coq_HB_unnamed_factory_5

(** val rflag_beq : rflag -> rflag -> bool **)

let rflag_beq x y =
  match x with
  | CF -> (match y with
           | CF -> true
           | _ -> false)
  | PF -> (match y with
           | PF -> true
           | _ -> false)
  | ZF -> (match y with
           | ZF -> true
           | _ -> false)
  | SF -> (match y with
           | SF -> true
           | _ -> false)
  | OF -> (match y with
           | OF -> true
           | _ -> false)

(** val rflag_eq_dec : rflag -> rflag -> bool **)

let rflag_eq_dec x y =
  let b = rflag_beq x y in if b then true else false

(** val rflag_eq_axiom : rflag eq_axiom **)

let rflag_eq_axiom =
  eq_axiom_of_scheme rflag_beq

(** val coq_HB_unnamed_factory_7 : rflag Coq_hasDecEq.axioms_ **)

let coq_HB_unnamed_factory_7 =
  { Coq_hasDecEq.eq_op = rflag_beq; Coq_hasDecEq.eqP = rflag_eq_axiom }

(** val x86_decl_rflag__canonical__eqtype_Equality : Equality.coq_type **)

let x86_decl_rflag__canonical__eqtype_Equality =
  Obj.magic coq_HB_unnamed_factory_7

(** val condt_beq : condt -> condt -> bool **)

let condt_beq x y =
  match x with
  | O_ct -> (match y with
             | O_ct -> true
             | _ -> false)
  | NO_ct -> (match y with
              | NO_ct -> true
              | _ -> false)
  | B_ct -> (match y with
             | B_ct -> true
             | _ -> false)
  | NB_ct -> (match y with
              | NB_ct -> true
              | _ -> false)
  | E_ct -> (match y with
             | E_ct -> true
             | _ -> false)
  | NE_ct -> (match y with
              | NE_ct -> true
              | _ -> false)
  | BE_ct -> (match y with
              | BE_ct -> true
              | _ -> false)
  | NBE_ct -> (match y with
               | NBE_ct -> true
               | _ -> false)
  | S_ct -> (match y with
             | S_ct -> true
             | _ -> false)
  | NS_ct -> (match y with
              | NS_ct -> true
              | _ -> false)
  | P_ct -> (match y with
             | P_ct -> true
             | _ -> false)
  | NP_ct -> (match y with
              | NP_ct -> true
              | _ -> false)
  | L_ct -> (match y with
             | L_ct -> true
             | _ -> false)
  | NL_ct -> (match y with
              | NL_ct -> true
              | _ -> false)
  | LE_ct -> (match y with
              | LE_ct -> true
              | _ -> false)
  | NLE_ct -> (match y with
               | NLE_ct -> true
               | _ -> false)

(** val condt_eq_dec : condt -> condt -> bool **)

let condt_eq_dec x y =
  let b = condt_beq x y in if b then true else false

(** val condt_eq_axiom : condt eq_axiom **)

let condt_eq_axiom =
  eq_axiom_of_scheme condt_beq

(** val coq_HB_unnamed_factory_9 : condt Coq_hasDecEq.axioms_ **)

let coq_HB_unnamed_factory_9 =
  { Coq_hasDecEq.eq_op = condt_beq; Coq_hasDecEq.eqP = condt_eq_axiom }

(** val x86_decl_condt__canonical__eqtype_Equality : Equality.coq_type **)

let x86_decl_condt__canonical__eqtype_Equality =
  Obj.magic coq_HB_unnamed_factory_9

(** val registers : register list **)

let registers =
  RAX :: (RCX :: (RDX :: (RBX :: (RSP :: (RBP :: (RSI :: (RDI :: (R8 :: (R9 :: (R10 :: (R11 :: (R12 :: (R13 :: (R14 :: (R15 :: [])))))))))))))))

(** val coq_HB_unnamed_factory_11 : register Choice.Countable.axioms_ **)

let coq_HB_unnamed_factory_11 =
  Obj.magic Choice.eqtype_pcan_type__canonical__choice_Countable
    Choice.coq_Datatypes_nat__canonical__choice_Countable
    (FinIsCount.pickle x86_decl_register__canonical__eqtype_Equality
      (Obj.magic registers))
    (FinIsCount.unpickle x86_decl_register__canonical__eqtype_Equality
      (Obj.magic registers))

(** val choice_Countable__to__choice_hasChoice :
    register Choice.Coq_hasChoice.axioms_ **)

let choice_Countable__to__choice_hasChoice =
  coq_HB_unnamed_factory_11.Choice.Countable.choice_hasChoice_mixin

(** val choice_Countable__to__choice_Choice_isCountable :
    register Choice.Choice_isCountable.axioms_ **)

let choice_Countable__to__choice_Choice_isCountable =
  coq_HB_unnamed_factory_11.Choice.Countable.choice_Choice_isCountable_mixin

(** val coq_HB_unnamed_mixin_15 : register Choice.Coq_hasChoice.axioms_ **)

let coq_HB_unnamed_mixin_15 =
  coq_HB_unnamed_factory_11.Choice.Countable.choice_hasChoice_mixin

(** val x86_decl_register__canonical__choice_Choice :
    Choice.Choice.coq_type **)

let x86_decl_register__canonical__choice_Choice =
  { Choice.Choice.choice_hasChoice_mixin =
    (Obj.magic coq_HB_unnamed_mixin_15);
    Choice.Choice.eqtype_hasDecEq_mixin =
    (Obj.magic coq_HB_unnamed_factory_1) }

(** val coq_HB_unnamed_mixin_16 :
    register Choice.Choice_isCountable.axioms_ **)

let coq_HB_unnamed_mixin_16 =
  coq_HB_unnamed_factory_11.Choice.Countable.choice_Choice_isCountable_mixin

(** val x86_decl_register__canonical__choice_Countable :
    Choice.Countable.coq_type **)

let x86_decl_register__canonical__choice_Countable =
  { Choice.Countable.choice_hasChoice_mixin =
    (Obj.magic coq_HB_unnamed_mixin_15);
    Choice.Countable.eqtype_hasDecEq_mixin =
    (Obj.magic coq_HB_unnamed_factory_1);
    Choice.Countable.choice_Choice_isCountable_mixin =
    (Obj.magic coq_HB_unnamed_mixin_16) }

(** val coq_HB_unnamed_factory_17 : register Coq_isFinite.axioms_ **)

let coq_HB_unnamed_factory_17 =
  registers

(** val x86_decl_register__canonical__fintype_Finite : Finite.coq_type **)

let x86_decl_register__canonical__fintype_Finite =
  { Finite.choice_hasChoice_mixin = (Obj.magic coq_HB_unnamed_mixin_15);
    Finite.choice_Choice_isCountable_mixin =
    (Obj.magic coq_HB_unnamed_mixin_16); Finite.eqtype_hasDecEq_mixin =
    (Obj.magic coq_HB_unnamed_factory_1); Finite.fintype_isFinite_mixin =
    (Obj.magic coq_HB_unnamed_factory_17) }

(** val regxs : register_ext list **)

let regxs =
  MM0 :: (MM1 :: (MM2 :: (MM3 :: (MM4 :: (MM5 :: (MM6 :: (MM7 :: [])))))))

(** val coq_HB_unnamed_factory_19 : register_ext Choice.Countable.axioms_ **)

let coq_HB_unnamed_factory_19 =
  Obj.magic Choice.eqtype_pcan_type__canonical__choice_Countable
    Choice.coq_Datatypes_nat__canonical__choice_Countable
    (FinIsCount.pickle x86_decl_register_ext__canonical__eqtype_Equality
      (Obj.magic regxs))
    (FinIsCount.unpickle x86_decl_register_ext__canonical__eqtype_Equality
      (Obj.magic regxs))

(** val choice_Countable__to__choice_hasChoice__21 :
    register_ext Choice.Coq_hasChoice.axioms_ **)

let choice_Countable__to__choice_hasChoice__21 =
  coq_HB_unnamed_factory_19.Choice.Countable.choice_hasChoice_mixin

(** val choice_Countable__to__choice_Choice_isCountable__24 :
    register_ext Choice.Choice_isCountable.axioms_ **)

let choice_Countable__to__choice_Choice_isCountable__24 =
  coq_HB_unnamed_factory_19.Choice.Countable.choice_Choice_isCountable_mixin

(** val coq_HB_unnamed_mixin_25 :
    register_ext Choice.Coq_hasChoice.axioms_ **)

let coq_HB_unnamed_mixin_25 =
  coq_HB_unnamed_factory_19.Choice.Countable.choice_hasChoice_mixin

(** val x86_decl_register_ext__canonical__choice_Choice :
    Choice.Choice.coq_type **)

let x86_decl_register_ext__canonical__choice_Choice =
  { Choice.Choice.choice_hasChoice_mixin =
    (Obj.magic coq_HB_unnamed_mixin_25);
    Choice.Choice.eqtype_hasDecEq_mixin =
    (Obj.magic coq_HB_unnamed_factory_3) }

(** val coq_HB_unnamed_mixin_26 :
    register_ext Choice.Choice_isCountable.axioms_ **)

let coq_HB_unnamed_mixin_26 =
  coq_HB_unnamed_factory_19.Choice.Countable.choice_Choice_isCountable_mixin

(** val x86_decl_register_ext__canonical__choice_Countable :
    Choice.Countable.coq_type **)

let x86_decl_register_ext__canonical__choice_Countable =
  { Choice.Countable.choice_hasChoice_mixin =
    (Obj.magic coq_HB_unnamed_mixin_25);
    Choice.Countable.eqtype_hasDecEq_mixin =
    (Obj.magic coq_HB_unnamed_factory_3);
    Choice.Countable.choice_Choice_isCountable_mixin =
    (Obj.magic coq_HB_unnamed_mixin_26) }

(** val coq_HB_unnamed_factory_27 : register_ext Coq_isFinite.axioms_ **)

let coq_HB_unnamed_factory_27 =
  regxs

(** val x86_decl_register_ext__canonical__fintype_Finite : Finite.coq_type **)

let x86_decl_register_ext__canonical__fintype_Finite =
  { Finite.choice_hasChoice_mixin = (Obj.magic coq_HB_unnamed_mixin_25);
    Finite.choice_Choice_isCountable_mixin =
    (Obj.magic coq_HB_unnamed_mixin_26); Finite.eqtype_hasDecEq_mixin =
    (Obj.magic coq_HB_unnamed_factory_3); Finite.fintype_isFinite_mixin =
    (Obj.magic coq_HB_unnamed_factory_27) }

(** val xmm_registers : xmm_register list **)

let xmm_registers =
  XMM0 :: (XMM1 :: (XMM2 :: (XMM3 :: (XMM4 :: (XMM5 :: (XMM6 :: (XMM7 :: (XMM8 :: (XMM9 :: (XMM10 :: (XMM11 :: (XMM12 :: (XMM13 :: (XMM14 :: (XMM15 :: [])))))))))))))))

(** val coq_HB_unnamed_factory_29 : xmm_register Choice.Countable.axioms_ **)

let coq_HB_unnamed_factory_29 =
  Obj.magic Choice.eqtype_pcan_type__canonical__choice_Countable
    Choice.coq_Datatypes_nat__canonical__choice_Countable
    (FinIsCount.pickle x86_decl_xmm_register__canonical__eqtype_Equality
      (Obj.magic xmm_registers))
    (FinIsCount.unpickle x86_decl_xmm_register__canonical__eqtype_Equality
      (Obj.magic xmm_registers))

(** val choice_Countable__to__choice_hasChoice__31 :
    xmm_register Choice.Coq_hasChoice.axioms_ **)

let choice_Countable__to__choice_hasChoice__31 =
  coq_HB_unnamed_factory_29.Choice.Countable.choice_hasChoice_mixin

(** val choice_Countable__to__choice_Choice_isCountable__34 :
    xmm_register Choice.Choice_isCountable.axioms_ **)

let choice_Countable__to__choice_Choice_isCountable__34 =
  coq_HB_unnamed_factory_29.Choice.Countable.choice_Choice_isCountable_mixin

(** val coq_HB_unnamed_mixin_35 :
    xmm_register Choice.Coq_hasChoice.axioms_ **)

let coq_HB_unnamed_mixin_35 =
  coq_HB_unnamed_factory_29.Choice.Countable.choice_hasChoice_mixin

(** val x86_decl_xmm_register__canonical__choice_Choice :
    Choice.Choice.coq_type **)

let x86_decl_xmm_register__canonical__choice_Choice =
  { Choice.Choice.choice_hasChoice_mixin =
    (Obj.magic coq_HB_unnamed_mixin_35);
    Choice.Choice.eqtype_hasDecEq_mixin =
    (Obj.magic coq_HB_unnamed_factory_5) }

(** val coq_HB_unnamed_mixin_36 :
    xmm_register Choice.Choice_isCountable.axioms_ **)

let coq_HB_unnamed_mixin_36 =
  coq_HB_unnamed_factory_29.Choice.Countable.choice_Choice_isCountable_mixin

(** val x86_decl_xmm_register__canonical__choice_Countable :
    Choice.Countable.coq_type **)

let x86_decl_xmm_register__canonical__choice_Countable =
  { Choice.Countable.choice_hasChoice_mixin =
    (Obj.magic coq_HB_unnamed_mixin_35);
    Choice.Countable.eqtype_hasDecEq_mixin =
    (Obj.magic coq_HB_unnamed_factory_5);
    Choice.Countable.choice_Choice_isCountable_mixin =
    (Obj.magic coq_HB_unnamed_mixin_36) }

(** val coq_HB_unnamed_factory_37 : xmm_register Coq_isFinite.axioms_ **)

let coq_HB_unnamed_factory_37 =
  xmm_registers

(** val x86_decl_xmm_register__canonical__fintype_Finite : Finite.coq_type **)

let x86_decl_xmm_register__canonical__fintype_Finite =
  { Finite.choice_hasChoice_mixin = (Obj.magic coq_HB_unnamed_mixin_35);
    Finite.choice_Choice_isCountable_mixin =
    (Obj.magic coq_HB_unnamed_mixin_36); Finite.eqtype_hasDecEq_mixin =
    (Obj.magic coq_HB_unnamed_factory_5); Finite.fintype_isFinite_mixin =
    (Obj.magic coq_HB_unnamed_factory_37) }

(** val rflags : rflag list **)

let rflags =
  CF :: (PF :: (ZF :: (SF :: (OF :: []))))

(** val coq_HB_unnamed_factory_39 : rflag Choice.Countable.axioms_ **)

let coq_HB_unnamed_factory_39 =
  Obj.magic Choice.eqtype_pcan_type__canonical__choice_Countable
    Choice.coq_Datatypes_nat__canonical__choice_Countable
    (FinIsCount.pickle x86_decl_rflag__canonical__eqtype_Equality
      (Obj.magic rflags))
    (FinIsCount.unpickle x86_decl_rflag__canonical__eqtype_Equality
      (Obj.magic rflags))

(** val choice_Countable__to__choice_hasChoice__41 :
    rflag Choice.Coq_hasChoice.axioms_ **)

let choice_Countable__to__choice_hasChoice__41 =
  coq_HB_unnamed_factory_39.Choice.Countable.choice_hasChoice_mixin

(** val choice_Countable__to__choice_Choice_isCountable__44 :
    rflag Choice.Choice_isCountable.axioms_ **)

let choice_Countable__to__choice_Choice_isCountable__44 =
  coq_HB_unnamed_factory_39.Choice.Countable.choice_Choice_isCountable_mixin

(** val coq_HB_unnamed_mixin_45 : rflag Choice.Coq_hasChoice.axioms_ **)

let coq_HB_unnamed_mixin_45 =
  coq_HB_unnamed_factory_39.Choice.Countable.choice_hasChoice_mixin

(** val x86_decl_rflag__canonical__choice_Choice : Choice.Choice.coq_type **)

let x86_decl_rflag__canonical__choice_Choice =
  { Choice.Choice.choice_hasChoice_mixin =
    (Obj.magic coq_HB_unnamed_mixin_45);
    Choice.Choice.eqtype_hasDecEq_mixin =
    (Obj.magic coq_HB_unnamed_factory_7) }

(** val coq_HB_unnamed_mixin_46 : rflag Choice.Choice_isCountable.axioms_ **)

let coq_HB_unnamed_mixin_46 =
  coq_HB_unnamed_factory_39.Choice.Countable.choice_Choice_isCountable_mixin

(** val x86_decl_rflag__canonical__choice_Countable :
    Choice.Countable.coq_type **)

let x86_decl_rflag__canonical__choice_Countable =
  { Choice.Countable.choice_hasChoice_mixin =
    (Obj.magic coq_HB_unnamed_mixin_45);
    Choice.Countable.eqtype_hasDecEq_mixin =
    (Obj.magic coq_HB_unnamed_factory_7);
    Choice.Countable.choice_Choice_isCountable_mixin =
    (Obj.magic coq_HB_unnamed_mixin_46) }

(** val coq_HB_unnamed_factory_47 : rflag Coq_isFinite.axioms_ **)

let coq_HB_unnamed_factory_47 =
  rflags

(** val x86_decl_rflag__canonical__fintype_Finite : Finite.coq_type **)

let x86_decl_rflag__canonical__fintype_Finite =
  { Finite.choice_hasChoice_mixin = (Obj.magic coq_HB_unnamed_mixin_45);
    Finite.choice_Choice_isCountable_mixin =
    (Obj.magic coq_HB_unnamed_mixin_46); Finite.eqtype_hasDecEq_mixin =
    (Obj.magic coq_HB_unnamed_factory_7); Finite.fintype_isFinite_mixin =
    (Obj.magic coq_HB_unnamed_factory_47) }

(** val eqTC_register : register eqTypeC **)

let eqTC_register =
  { beq = register_beq; ceqP = reg_eq_axiom }

(** val finC_register : register finTypeC **)

let finC_register =
  { _eqC = eqTC_register; cenum = registers }

(** val register_to_string : register -> string **)

let register_to_string = function
| RAX -> "RAX"
| RCX -> "RCX"
| RDX -> "RDX"
| RBX -> "RBX"
| RSP -> "RSP"
| RBP -> "RBP"
| RSI -> "RSI"
| RDI -> "RDI"
| R8 -> "R8"
| R9 -> "R9"
| R10 -> "R10"
| R11 -> "R11"
| R12 -> "R12"
| R13 -> "R13"
| R14 -> "R14"
| R15 -> "R15"

(** val x86_reg_toS : register coq_ToString **)

let x86_reg_toS =
  { category = "register"; _finC = finC_register; to_string =
    register_to_string }

(** val eqTC_regx : register_ext eqTypeC **)

let eqTC_regx =
  { beq = register_ext_beq; ceqP = regx_eq_axiom }

(** val finC_regx : register_ext finTypeC **)

let finC_regx =
  { _eqC = eqTC_regx; cenum = regxs }

(** val regx_to_string : register_ext -> string **)

let regx_to_string = function
| MM0 -> "MM0"
| MM1 -> "MM1"
| MM2 -> "MM2"
| MM3 -> "MM3"
| MM4 -> "MM4"
| MM5 -> "MM5"
| MM6 -> "MM6"
| MM7 -> "MM7"

(** val x86_regx_toS : register_ext coq_ToString **)

let x86_regx_toS =
  { category = "register"; _finC = finC_regx; to_string = regx_to_string }

(** val eqTC_xmm_register : xmm_register eqTypeC **)

let eqTC_xmm_register =
  { beq = xmm_register_beq; ceqP = xreg_eq_axiom }

(** val finC_xmm_register : xmm_register finTypeC **)

let finC_xmm_register =
  { _eqC = eqTC_xmm_register; cenum = xmm_registers }

(** val xreg_to_string : xmm_register -> string **)

let xreg_to_string = function
| XMM0 -> "XMM0"
| XMM1 -> "XMM1"
| XMM2 -> "XMM2"
| XMM3 -> "XMM3"
| XMM4 -> "XMM4"
| XMM5 -> "XMM5"
| XMM6 -> "XMM6"
| XMM7 -> "XMM7"
| XMM8 -> "XMM8"
| XMM9 -> "XMM9"
| XMM10 -> "XMM10"
| XMM11 -> "XMM11"
| XMM12 -> "XMM12"
| XMM13 -> "XMM13"
| XMM14 -> "XMM14"
| XMM15 -> "XMM15"

(** val x86_xreg_toS : xmm_register coq_ToString **)

let x86_xreg_toS =
  { category = "ymm_register"; _finC = finC_xmm_register; to_string =
    xreg_to_string }

(** val eqTC_rflag : rflag eqTypeC **)

let eqTC_rflag =
  { beq = rflag_beq; ceqP = rflag_eq_axiom }

(** val finC_rflag : rflag finTypeC **)

let finC_rflag =
  { _eqC = eqTC_rflag; cenum = rflags }

(** val rflag_to_string : rflag -> string **)

let rflag_to_string = function
| CF -> "CF"
| PF -> "PF"
| ZF -> "ZF"
| SF -> "SF"
| OF -> "OF"

(** val x86_rflag_toS : rflag coq_ToString **)

let x86_rflag_toS =
  { category = "rflag"; _finC = finC_rflag; to_string = rflag_to_string }

(** val eqC_condt : condt eqTypeC **)

let eqC_condt =
  { beq = condt_beq; ceqP = condt_eq_axiom }

(** val x86_fc_of_cfc : combine_flags_core -> flag_combination **)

let x86_fc_of_cfc cfc =
  let vof = FCVar0 in
  let vcf = FCVar1 in
  let vsf = FCVar2 in
  let vzf = FCVar3 in
  (match cfc with
   | CFC_B -> vcf
   | CFC_E -> vzf
   | CFC_L -> FCNot (FCEq (vof, vsf))
   | CFC_BE -> FCOr (vcf, vzf)
   | CFC_LE -> FCOr ((FCNot (FCEq (vof, vsf))), vzf))

(** val x86_fcp : coq_FlagCombinationParams **)

let x86_fcp =
  x86_fc_of_cfc

(** val x86_check_CAimm :
    caimm_checker_s -> wsize -> GRing.ComRing.sort -> bool **)

let x86_check_CAimm checker _ _ =
  match checker with
  | CAimmC_none -> true
  | _ -> false

(** val x86_decl :
    (register, register_ext, xmm_register, rflag, condt) arch_decl **)

let x86_decl =
  { reg_size = x86_reg_size; xreg_size = x86_xreg_size; cond_eqC = eqC_condt;
    toS_r = x86_reg_toS; toS_rx = x86_regx_toS; toS_x = x86_xreg_toS; toS_f =
    x86_rflag_toS; ad_rsp = RSP; ad_fcp = x86_fcp; check_CAimm =
    x86_check_CAimm }

(** val x86_linux_call_conv :
    (register, register_ext, xmm_register, rflag, condt) calling_convention **)

let x86_linux_call_conv =
  { callee_saved =
    (map (fun x -> ARReg x)
      (RBX :: (RBP :: (RSP :: (R12 :: (R13 :: (R14 :: (R15 :: []))))))));
    call_reg_args = (RDI :: (RSI :: (RDX :: (RCX :: (R8 :: (R9 :: []))))));
    call_xreg_args =
    (XMM0 :: (XMM1 :: (XMM2 :: (XMM3 :: (XMM4 :: (XMM5 :: (XMM6 :: (XMM7 :: []))))))));
    call_reg_ret = (RAX :: (RDX :: [])); call_xreg_ret =
    (XMM0 :: (XMM1 :: [])) }

(** val x86_windows_call_conv :
    (register, register_ext, xmm_register, rflag, condt) calling_convention **)

let x86_windows_call_conv =
  { callee_saved =
    (cat
      (map (fun x -> ARReg x)
        (RBX :: (RBP :: (RDI :: (RSI :: (RSP :: (R12 :: (R13 :: (R14 :: (R15 :: []))))))))))
      (map (fun x -> AXReg x)
        (XMM6 :: (XMM7 :: (XMM8 :: (XMM9 :: (XMM10 :: (XMM11 :: (XMM12 :: (XMM13 :: (XMM14 :: (XMM15 :: []))))))))))));
    call_reg_args = (RCX :: (RDX :: (R8 :: (R9 :: [])))); call_xreg_args =
    (XMM0 :: (XMM1 :: (XMM2 :: (XMM3 :: [])))); call_reg_ret = (RAX :: []);
    call_xreg_ret = (XMM0 :: []) }
