open BinInt
open BinNums
open Datatypes
open List0
open Nat0
open Prelude
open Zpower
open Arch_decl
open Eqtype
open Expr
open Sem_type
open Seq
open Sopn
open Ssralg
open Ssrbool
open Ssrnat
open Tuple
open Type
open Utils0
open Waes
open Word0
open Word
open Word_ssrZ
open Wsize
open X86_decl

type __ = Obj.t
let __ = let rec f _ = Obj.repr f in Obj.repr f

type x86_op =
| MOV of wsize
| MOVSX of wsize * wsize
| MOVZX of wsize * wsize
| CMOVcc of wsize
| XCHG of wsize
| ADD of wsize
| SUB of wsize
| MUL of wsize
| IMUL of wsize
| IMULr of wsize
| IMULri of wsize
| DIV of wsize
| IDIV of wsize
| CQO of wsize
| ADC of wsize
| SBB of wsize
| NEG of wsize
| INC of wsize
| DEC of wsize
| LZCNT of wsize
| TZCNT of wsize
| SETcc
| BT of wsize
| CLC
| STC
| LEA of wsize
| TEST of wsize
| CMP of wsize
| AND of wsize
| ANDN of wsize
| OR of wsize
| XOR of wsize
| NOT of wsize
| ROR of wsize
| ROL of wsize
| RCR of wsize
| RCL of wsize
| SHL of wsize
| SHR of wsize
| SAL of wsize
| SAR of wsize
| SHLD of wsize
| SHRD of wsize
| RORX of wsize
| SARX of wsize
| SHRX of wsize
| SHLX of wsize
| MULX_lo_hi of wsize
| ADCX of wsize
| ADOX of wsize
| BSWAP of wsize
| POPCNT of wsize
| BTR of wsize
| BTS of wsize
| PEXT of wsize
| PDEP of wsize
| MOVX of wsize
| POR
| MOVD of wsize
| MOVV of wsize
| VMOV of wsize
| VMOVDQA of wsize
| VMOVDQU of wsize
| VPMOVSX of velem * wsize * velem * wsize
| VPMOVZX of velem * wsize * velem * wsize
| VPAND of wsize
| VPANDN of wsize
| VPOR of wsize
| VPXOR of wsize
| VPADD of velem * wsize
| VPSUB of velem * wsize
| VPAVG of velem * wsize
| VPMULL of velem * wsize
| VPMULH of wsize
| VPMULHU of wsize
| VPMULHRS of wsize
| VPMUL of wsize
| VPMULU of wsize
| VPEXTR of wsize
| VPINSR of velem
| VPSLL of velem * wsize
| VPSRL of velem * wsize
| VPSRA of velem * wsize
| VPSLLV of velem * wsize
| VPSRLV of velem * wsize
| VPSLLDQ of wsize
| VPSRLDQ of wsize
| VPSHUFB of wsize
| VPSHUFD of wsize
| VPSHUFHW of wsize
| VPSHUFLW of wsize
| VPBLEND of velem * wsize
| VPBLENDVB of wsize
| BLENDV of velem * wsize
| VPACKUS of velem * wsize
| VPACKSS of velem * wsize
| VSHUFPS of wsize
| VPBROADCAST of velem * wsize
| VMOVSHDUP of wsize
| VMOVSLDUP of wsize
| VPALIGNR of wsize
| VBROADCASTI128
| VPUNPCKH of velem * wsize
| VPUNPCKL of velem * wsize
| VEXTRACTI128
| VINSERTI128
| VPERM2I128
| VPERMD
| VPERMQ
| VPMOVMSKB of wsize * wsize
| VPCMPEQ of velem * wsize
| VPCMPGT of velem * wsize
| VPSIGN of velem * wsize
| VPMADDUBSW of wsize
| VPMADDWD of wsize
| VMOVLPD
| VMOVHPD
| VPMINU of velem * wsize
| VPMINS of velem * wsize
| VPMAXU of velem * wsize
| VPMAXS of velem * wsize
| VPABS of velem * wsize
| VPTEST of wsize
| CLFLUSH
| PREFETCHT0
| PREFETCHT1
| PREFETCHT2
| PREFETCHNTA
| LFENCE
| MFENCE
| SFENCE
| RDTSC of wsize
| RDTSCP of wsize
| AESDEC
| VAESDEC of wsize
| AESDECLAST
| VAESDECLAST of wsize
| AESENC
| VAESENC of wsize
| AESENCLAST
| VAESENCLAST of wsize
| AESIMC
| VAESIMC
| AESKEYGENASSIST
| VAESKEYGENASSIST
| PCLMULQDQ
| VPCLMULQDQ of wsize

(** val x86_op_beq : x86_op -> x86_op -> bool **)

let x86_op_beq x y =
  match x with
  | MOV x0 -> (match y with
               | MOV x1 -> wsize_beq x0 x1
               | _ -> false)
  | MOVSX (x0, x1) ->
    (match y with
     | MOVSX (x2, x3) -> (&&) (wsize_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | MOVZX (x0, x1) ->
    (match y with
     | MOVZX (x2, x3) -> (&&) (wsize_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | CMOVcc x0 -> (match y with
                  | CMOVcc x1 -> wsize_beq x0 x1
                  | _ -> false)
  | XCHG x0 -> (match y with
                | XCHG x1 -> wsize_beq x0 x1
                | _ -> false)
  | ADD x0 -> (match y with
               | ADD x1 -> wsize_beq x0 x1
               | _ -> false)
  | SUB x0 -> (match y with
               | SUB x1 -> wsize_beq x0 x1
               | _ -> false)
  | MUL x0 -> (match y with
               | MUL x1 -> wsize_beq x0 x1
               | _ -> false)
  | IMUL x0 -> (match y with
                | IMUL x1 -> wsize_beq x0 x1
                | _ -> false)
  | IMULr x0 -> (match y with
                 | IMULr x1 -> wsize_beq x0 x1
                 | _ -> false)
  | IMULri x0 -> (match y with
                  | IMULri x1 -> wsize_beq x0 x1
                  | _ -> false)
  | DIV x0 -> (match y with
               | DIV x1 -> wsize_beq x0 x1
               | _ -> false)
  | IDIV x0 -> (match y with
                | IDIV x1 -> wsize_beq x0 x1
                | _ -> false)
  | CQO x0 -> (match y with
               | CQO x1 -> wsize_beq x0 x1
               | _ -> false)
  | ADC x0 -> (match y with
               | ADC x1 -> wsize_beq x0 x1
               | _ -> false)
  | SBB x0 -> (match y with
               | SBB x1 -> wsize_beq x0 x1
               | _ -> false)
  | NEG x0 -> (match y with
               | NEG x1 -> wsize_beq x0 x1
               | _ -> false)
  | INC x0 -> (match y with
               | INC x1 -> wsize_beq x0 x1
               | _ -> false)
  | DEC x0 -> (match y with
               | DEC x1 -> wsize_beq x0 x1
               | _ -> false)
  | LZCNT x0 -> (match y with
                 | LZCNT x1 -> wsize_beq x0 x1
                 | _ -> false)
  | TZCNT x0 -> (match y with
                 | TZCNT x1 -> wsize_beq x0 x1
                 | _ -> false)
  | SETcc -> (match y with
              | SETcc -> true
              | _ -> false)
  | BT x0 -> (match y with
              | BT x1 -> wsize_beq x0 x1
              | _ -> false)
  | CLC -> (match y with
            | CLC -> true
            | _ -> false)
  | STC -> (match y with
            | STC -> true
            | _ -> false)
  | LEA x0 -> (match y with
               | LEA x1 -> wsize_beq x0 x1
               | _ -> false)
  | TEST x0 -> (match y with
                | TEST x1 -> wsize_beq x0 x1
                | _ -> false)
  | CMP x0 -> (match y with
               | CMP x1 -> wsize_beq x0 x1
               | _ -> false)
  | AND x0 -> (match y with
               | AND x1 -> wsize_beq x0 x1
               | _ -> false)
  | ANDN x0 -> (match y with
                | ANDN x1 -> wsize_beq x0 x1
                | _ -> false)
  | OR x0 -> (match y with
              | OR x1 -> wsize_beq x0 x1
              | _ -> false)
  | XOR x0 -> (match y with
               | XOR x1 -> wsize_beq x0 x1
               | _ -> false)
  | NOT x0 -> (match y with
               | NOT x1 -> wsize_beq x0 x1
               | _ -> false)
  | ROR x0 -> (match y with
               | ROR x1 -> wsize_beq x0 x1
               | _ -> false)
  | ROL x0 -> (match y with
               | ROL x1 -> wsize_beq x0 x1
               | _ -> false)
  | RCR x0 -> (match y with
               | RCR x1 -> wsize_beq x0 x1
               | _ -> false)
  | RCL x0 -> (match y with
               | RCL x1 -> wsize_beq x0 x1
               | _ -> false)
  | SHL x0 -> (match y with
               | SHL x1 -> wsize_beq x0 x1
               | _ -> false)
  | SHR x0 -> (match y with
               | SHR x1 -> wsize_beq x0 x1
               | _ -> false)
  | SAL x0 -> (match y with
               | SAL x1 -> wsize_beq x0 x1
               | _ -> false)
  | SAR x0 -> (match y with
               | SAR x1 -> wsize_beq x0 x1
               | _ -> false)
  | SHLD x0 -> (match y with
                | SHLD x1 -> wsize_beq x0 x1
                | _ -> false)
  | SHRD x0 -> (match y with
                | SHRD x1 -> wsize_beq x0 x1
                | _ -> false)
  | RORX x0 -> (match y with
                | RORX x1 -> wsize_beq x0 x1
                | _ -> false)
  | SARX x0 -> (match y with
                | SARX x1 -> wsize_beq x0 x1
                | _ -> false)
  | SHRX x0 -> (match y with
                | SHRX x1 -> wsize_beq x0 x1
                | _ -> false)
  | SHLX x0 -> (match y with
                | SHLX x1 -> wsize_beq x0 x1
                | _ -> false)
  | MULX_lo_hi x0 ->
    (match y with
     | MULX_lo_hi x1 -> wsize_beq x0 x1
     | _ -> false)
  | ADCX x0 -> (match y with
                | ADCX x1 -> wsize_beq x0 x1
                | _ -> false)
  | ADOX x0 -> (match y with
                | ADOX x1 -> wsize_beq x0 x1
                | _ -> false)
  | BSWAP x0 -> (match y with
                 | BSWAP x1 -> wsize_beq x0 x1
                 | _ -> false)
  | POPCNT x0 -> (match y with
                  | POPCNT x1 -> wsize_beq x0 x1
                  | _ -> false)
  | BTR x0 -> (match y with
               | BTR x1 -> wsize_beq x0 x1
               | _ -> false)
  | BTS x0 -> (match y with
               | BTS x1 -> wsize_beq x0 x1
               | _ -> false)
  | PEXT x0 -> (match y with
                | PEXT x1 -> wsize_beq x0 x1
                | _ -> false)
  | PDEP x0 -> (match y with
                | PDEP x1 -> wsize_beq x0 x1
                | _ -> false)
  | MOVX x0 -> (match y with
                | MOVX x1 -> wsize_beq x0 x1
                | _ -> false)
  | POR -> (match y with
            | POR -> true
            | _ -> false)
  | MOVD x0 -> (match y with
                | MOVD x1 -> wsize_beq x0 x1
                | _ -> false)
  | MOVV x0 -> (match y with
                | MOVV x1 -> wsize_beq x0 x1
                | _ -> false)
  | VMOV x0 -> (match y with
                | VMOV x1 -> wsize_beq x0 x1
                | _ -> false)
  | VMOVDQA x0 -> (match y with
                   | VMOVDQA x1 -> wsize_beq x0 x1
                   | _ -> false)
  | VMOVDQU h -> (match y with
                  | VMOVDQU h0 -> wsize_beq h h0
                  | _ -> false)
  | VPMOVSX (x0, x1, x2, x3) ->
    (match y with
     | VPMOVSX (x4, x5, x6, x7) ->
       (&&) (internal_velem_beq x0 x4)
         ((&&) (wsize_beq x1 x5)
           ((&&) (internal_velem_beq x2 x6) (wsize_beq x3 x7)))
     | _ -> false)
  | VPMOVZX (x0, x1, x2, x3) ->
    (match y with
     | VPMOVZX (x4, x5, x6, x7) ->
       (&&) (internal_velem_beq x0 x4)
         ((&&) (wsize_beq x1 x5)
           ((&&) (internal_velem_beq x2 x6) (wsize_beq x3 x7)))
     | _ -> false)
  | VPAND h -> (match y with
                | VPAND h0 -> wsize_beq h h0
                | _ -> false)
  | VPANDN h -> (match y with
                 | VPANDN h0 -> wsize_beq h h0
                 | _ -> false)
  | VPOR h -> (match y with
               | VPOR h0 -> wsize_beq h h0
               | _ -> false)
  | VPXOR h -> (match y with
                | VPXOR h0 -> wsize_beq h h0
                | _ -> false)
  | VPADD (h, h0) ->
    (match y with
     | VPADD (h1, h2) -> (&&) (internal_velem_beq h h1) (wsize_beq h0 h2)
     | _ -> false)
  | VPSUB (h, h0) ->
    (match y with
     | VPSUB (h1, h2) -> (&&) (internal_velem_beq h h1) (wsize_beq h0 h2)
     | _ -> false)
  | VPAVG (x0, x1) ->
    (match y with
     | VPAVG (x2, x3) -> (&&) (internal_velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | VPMULL (h, h0) ->
    (match y with
     | VPMULL (h1, h2) -> (&&) (internal_velem_beq h h1) (wsize_beq h0 h2)
     | _ -> false)
  | VPMULH x0 -> (match y with
                  | VPMULH x1 -> wsize_beq x0 x1
                  | _ -> false)
  | VPMULHU x0 -> (match y with
                   | VPMULHU x1 -> wsize_beq x0 x1
                   | _ -> false)
  | VPMULHRS x0 -> (match y with
                    | VPMULHRS x1 -> wsize_beq x0 x1
                    | _ -> false)
  | VPMUL h -> (match y with
                | VPMUL h0 -> wsize_beq h h0
                | _ -> false)
  | VPMULU h -> (match y with
                 | VPMULU h0 -> wsize_beq h h0
                 | _ -> false)
  | VPEXTR x0 -> (match y with
                  | VPEXTR x1 -> wsize_beq x0 x1
                  | _ -> false)
  | VPINSR x0 ->
    (match y with
     | VPINSR x1 -> internal_velem_beq x0 x1
     | _ -> false)
  | VPSLL (h, h0) ->
    (match y with
     | VPSLL (h1, h2) -> (&&) (internal_velem_beq h h1) (wsize_beq h0 h2)
     | _ -> false)
  | VPSRL (h, h0) ->
    (match y with
     | VPSRL (h1, h2) -> (&&) (internal_velem_beq h h1) (wsize_beq h0 h2)
     | _ -> false)
  | VPSRA (h, h0) ->
    (match y with
     | VPSRA (h1, h2) -> (&&) (internal_velem_beq h h1) (wsize_beq h0 h2)
     | _ -> false)
  | VPSLLV (h, h0) ->
    (match y with
     | VPSLLV (h1, h2) -> (&&) (internal_velem_beq h h1) (wsize_beq h0 h2)
     | _ -> false)
  | VPSRLV (h, h0) ->
    (match y with
     | VPSRLV (h1, h2) -> (&&) (internal_velem_beq h h1) (wsize_beq h0 h2)
     | _ -> false)
  | VPSLLDQ h -> (match y with
                  | VPSLLDQ h0 -> wsize_beq h h0
                  | _ -> false)
  | VPSRLDQ h -> (match y with
                  | VPSRLDQ h0 -> wsize_beq h h0
                  | _ -> false)
  | VPSHUFB h -> (match y with
                  | VPSHUFB h0 -> wsize_beq h h0
                  | _ -> false)
  | VPSHUFD h -> (match y with
                  | VPSHUFD h0 -> wsize_beq h h0
                  | _ -> false)
  | VPSHUFHW h -> (match y with
                   | VPSHUFHW h0 -> wsize_beq h h0
                   | _ -> false)
  | VPSHUFLW h -> (match y with
                   | VPSHUFLW h0 -> wsize_beq h h0
                   | _ -> false)
  | VPBLEND (h, h0) ->
    (match y with
     | VPBLEND (h1, h2) -> (&&) (internal_velem_beq h h1) (wsize_beq h0 h2)
     | _ -> false)
  | VPBLENDVB h -> (match y with
                    | VPBLENDVB h0 -> wsize_beq h h0
                    | _ -> false)
  | BLENDV (x0, x1) ->
    (match y with
     | BLENDV (x2, x3) -> (&&) (internal_velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | VPACKUS (h, h0) ->
    (match y with
     | VPACKUS (h1, h2) -> (&&) (internal_velem_beq h h1) (wsize_beq h0 h2)
     | _ -> false)
  | VPACKSS (h, h0) ->
    (match y with
     | VPACKSS (h1, h2) -> (&&) (internal_velem_beq h h1) (wsize_beq h0 h2)
     | _ -> false)
  | VSHUFPS h -> (match y with
                  | VSHUFPS h0 -> wsize_beq h h0
                  | _ -> false)
  | VPBROADCAST (x0, x1) ->
    (match y with
     | VPBROADCAST (x2, x3) ->
       (&&) (internal_velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | VMOVSHDUP x0 ->
    (match y with
     | VMOVSHDUP x1 -> wsize_beq x0 x1
     | _ -> false)
  | VMOVSLDUP x0 ->
    (match y with
     | VMOVSLDUP x1 -> wsize_beq x0 x1
     | _ -> false)
  | VPALIGNR h -> (match y with
                   | VPALIGNR h0 -> wsize_beq h h0
                   | _ -> false)
  | VBROADCASTI128 -> (match y with
                       | VBROADCASTI128 -> true
                       | _ -> false)
  | VPUNPCKH (h, h0) ->
    (match y with
     | VPUNPCKH (h1, h2) -> (&&) (internal_velem_beq h h1) (wsize_beq h0 h2)
     | _ -> false)
  | VPUNPCKL (h, h0) ->
    (match y with
     | VPUNPCKL (h1, h2) -> (&&) (internal_velem_beq h h1) (wsize_beq h0 h2)
     | _ -> false)
  | VEXTRACTI128 -> (match y with
                     | VEXTRACTI128 -> true
                     | _ -> false)
  | VINSERTI128 -> (match y with
                    | VINSERTI128 -> true
                    | _ -> false)
  | VPERM2I128 -> (match y with
                   | VPERM2I128 -> true
                   | _ -> false)
  | VPERMD -> (match y with
               | VPERMD -> true
               | _ -> false)
  | VPERMQ -> (match y with
               | VPERMQ -> true
               | _ -> false)
  | VPMOVMSKB (x0, x1) ->
    (match y with
     | VPMOVMSKB (x2, x3) -> (&&) (wsize_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | VPCMPEQ (x0, x1) ->
    (match y with
     | VPCMPEQ (x2, x3) -> (&&) (internal_velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | VPCMPGT (x0, x1) ->
    (match y with
     | VPCMPGT (x2, x3) -> (&&) (internal_velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | VPSIGN (x0, x1) ->
    (match y with
     | VPSIGN (x2, x3) -> (&&) (internal_velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | VPMADDUBSW x0 ->
    (match y with
     | VPMADDUBSW x1 -> wsize_beq x0 x1
     | _ -> false)
  | VPMADDWD x0 -> (match y with
                    | VPMADDWD x1 -> wsize_beq x0 x1
                    | _ -> false)
  | VMOVLPD -> (match y with
                | VMOVLPD -> true
                | _ -> false)
  | VMOVHPD -> (match y with
                | VMOVHPD -> true
                | _ -> false)
  | VPMINU (x0, x1) ->
    (match y with
     | VPMINU (x2, x3) -> (&&) (internal_velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | VPMINS (x0, x1) ->
    (match y with
     | VPMINS (x2, x3) -> (&&) (internal_velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | VPMAXU (x0, x1) ->
    (match y with
     | VPMAXU (x2, x3) -> (&&) (internal_velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | VPMAXS (x0, x1) ->
    (match y with
     | VPMAXS (x2, x3) -> (&&) (internal_velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | VPABS (x0, x1) ->
    (match y with
     | VPABS (x2, x3) -> (&&) (internal_velem_beq x0 x2) (wsize_beq x1 x3)
     | _ -> false)
  | VPTEST h -> (match y with
                 | VPTEST h0 -> wsize_beq h h0
                 | _ -> false)
  | CLFLUSH -> (match y with
                | CLFLUSH -> true
                | _ -> false)
  | PREFETCHT0 -> (match y with
                   | PREFETCHT0 -> true
                   | _ -> false)
  | PREFETCHT1 -> (match y with
                   | PREFETCHT1 -> true
                   | _ -> false)
  | PREFETCHT2 -> (match y with
                   | PREFETCHT2 -> true
                   | _ -> false)
  | PREFETCHNTA -> (match y with
                    | PREFETCHNTA -> true
                    | _ -> false)
  | LFENCE -> (match y with
               | LFENCE -> true
               | _ -> false)
  | MFENCE -> (match y with
               | MFENCE -> true
               | _ -> false)
  | SFENCE -> (match y with
               | SFENCE -> true
               | _ -> false)
  | RDTSC x0 -> (match y with
                 | RDTSC x1 -> wsize_beq x0 x1
                 | _ -> false)
  | RDTSCP x0 -> (match y with
                  | RDTSCP x1 -> wsize_beq x0 x1
                  | _ -> false)
  | AESDEC -> (match y with
               | AESDEC -> true
               | _ -> false)
  | VAESDEC x0 -> (match y with
                   | VAESDEC x1 -> wsize_beq x0 x1
                   | _ -> false)
  | AESDECLAST -> (match y with
                   | AESDECLAST -> true
                   | _ -> false)
  | VAESDECLAST x0 ->
    (match y with
     | VAESDECLAST x1 -> wsize_beq x0 x1
     | _ -> false)
  | AESENC -> (match y with
               | AESENC -> true
               | _ -> false)
  | VAESENC x0 -> (match y with
                   | VAESENC x1 -> wsize_beq x0 x1
                   | _ -> false)
  | AESENCLAST -> (match y with
                   | AESENCLAST -> true
                   | _ -> false)
  | VAESENCLAST x0 ->
    (match y with
     | VAESENCLAST x1 -> wsize_beq x0 x1
     | _ -> false)
  | AESIMC -> (match y with
               | AESIMC -> true
               | _ -> false)
  | VAESIMC -> (match y with
                | VAESIMC -> true
                | _ -> false)
  | AESKEYGENASSIST -> (match y with
                        | AESKEYGENASSIST -> true
                        | _ -> false)
  | VAESKEYGENASSIST -> (match y with
                         | VAESKEYGENASSIST -> true
                         | _ -> false)
  | PCLMULQDQ -> (match y with
                  | PCLMULQDQ -> true
                  | _ -> false)
  | VPCLMULQDQ x0 ->
    (match y with
     | VPCLMULQDQ x1 -> wsize_beq x0 x1
     | _ -> false)

(** val x86_op_eq_dec : x86_op -> x86_op -> bool **)

let x86_op_eq_dec x y =
  let b = x86_op_beq x y in if b then true else false

(** val x86_op_eq_axiom : x86_op eq_axiom **)

let x86_op_eq_axiom =
  eq_axiom_of_scheme x86_op_beq

(** val coq_HB_unnamed_factory_1 : x86_op Coq_hasDecEq.axioms_ **)

let coq_HB_unnamed_factory_1 =
  { Coq_hasDecEq.eq_op = x86_op_beq; Coq_hasDecEq.eqP = x86_op_eq_axiom }

(** val x86_instr_decl_x86_op__canonical__eqtype_Equality :
    Equality.coq_type **)

let x86_instr_decl_x86_op__canonical__eqtype_Equality =
  Obj.magic coq_HB_unnamed_factory_1

(** val b_ty : stype list **)

let b_ty =
  Coq_sbool :: []

(** val b4_ty : stype list **)

let b4_ty =
  Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: [])))

(** val b5_ty : stype list **)

let b5_ty =
  Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: []))))

(** val bw_ty : wsize -> stype list **)

let bw_ty sz =
  Coq_sbool :: ((Coq_sword sz) :: [])

(** val bw2_ty : wsize -> stype list **)

let bw2_ty sz =
  Coq_sbool :: ((Coq_sword sz) :: ((Coq_sword sz) :: []))

(** val b2w_ty : wsize -> stype list **)

let b2w_ty sz =
  Coq_sbool :: (Coq_sbool :: ((Coq_sword sz) :: []))

(** val b4w_ty : wsize -> stype list **)

let b4w_ty sz =
  Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: ((Coq_sword
    sz) :: []))))

(** val b5w_ty : wsize -> stype list **)

let b5w_ty sz =
  Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: ((Coq_sword
    sz) :: [])))))

(** val b5w2_ty : wsize -> stype list **)

let b5w2_ty sz =
  Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: (Coq_sbool :: ((Coq_sword
    sz) :: ((Coq_sword sz) :: []))))))

(** val w_ty : wsize -> stype list **)

let w_ty sz =
  (Coq_sword sz) :: []

(** val w2_ty : wsize -> wsize -> stype list **)

let w2_ty sz sz' =
  (Coq_sword sz) :: ((Coq_sword sz') :: [])

(** val w3_ty : wsize -> stype list **)

let w3_ty sz =
  (Coq_sword sz) :: ((Coq_sword sz) :: ((Coq_sword sz) :: []))

(** val w8_ty : stype list **)

let w8_ty =
  (Coq_sword U8) :: []

(** val w128_ty : stype list **)

let w128_ty =
  (Coq_sword U128) :: []

(** val w256_ty : stype list **)

let w256_ty =
  (Coq_sword U256) :: []

(** val w2b_ty : wsize -> wsize -> stype list **)

let w2b_ty sz sz' =
  (Coq_sword sz) :: ((Coq_sword sz') :: (Coq_sbool :: []))

(** val ww8_ty : wsize -> stype list **)

let ww8_ty sz =
  (Coq_sword sz) :: ((Coq_sword U8) :: [])

(** val ww8b_ty : wsize -> stype list **)

let ww8b_ty sz =
  (Coq_sword sz) :: ((Coq_sword U8) :: (Coq_sbool :: []))

(** val w2w8_ty : wsize -> stype list **)

let w2w8_ty sz =
  (Coq_sword sz) :: ((Coq_sword sz) :: ((Coq_sword U8) :: []))

(** val w128w8_ty : stype list **)

let w128w8_ty =
  (Coq_sword U128) :: ((Coq_sword U8) :: [])

(** val w128ww8_ty : wsize -> stype list **)

let w128ww8_ty sz =
  (Coq_sword U128) :: ((Coq_sword sz) :: ((Coq_sword U8) :: []))

(** val w256w8_ty : stype list **)

let w256w8_ty =
  (Coq_sword U256) :: ((Coq_sword U8) :: [])

(** val w256w128w8_ty : stype list **)

let w256w128w8_ty =
  (Coq_sword U256) :: ((Coq_sword U128) :: ((Coq_sword U8) :: []))

(** val w256x2w8_ty : stype list **)

let w256x2w8_ty =
  (Coq_sword U256) :: ((Coq_sword U256) :: ((Coq_sword U8) :: []))

(** val coq_SF_of_word : wsize -> GRing.ComRing.sort -> bool **)

let coq_SF_of_word =
  msb

(** val coq_PF_of_word : wsize -> GRing.ComRing.sort -> bool **)

let coq_PF_of_word sz w =
  foldl xorb true
    (map (fun i0 -> wbit_n sz w i0)
      (iota O (S (S (S (S (S (S (S (S O))))))))))

(** val coq_ZF_of_word : wsize -> GRing.ComRing.sort -> bool **)

let coq_ZF_of_word sz w =
  eq_op
    (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality (word sz))
    w
    (GRing.zero
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule (word sz)))

(** val rflags_of_bwop : wsize -> GRing.ComRing.sort -> sem_tuple **)

let rflags_of_bwop sz w =
  Obj.magic ((Some false), ((Some false), ((Some (coq_SF_of_word sz w)),
    ((Some (coq_PF_of_word sz w)), (Some (coq_ZF_of_word sz w))))))

(** val rflags_of_aluop :
    wsize -> GRing.ComRing.sort -> coq_Z -> coq_Z -> sem_tuple **)

let rflags_of_aluop sz w vu vs =
  Obj.magic ((Some
    (negb
      (eq_op coq_BinNums_Z__canonical__eqtype_Equality
        (Obj.magic wsigned sz w) (Obj.magic vs)))), ((Some
    (negb
      (eq_op coq_BinNums_Z__canonical__eqtype_Equality
        (Obj.magic wunsigned sz w) (Obj.magic vu)))), ((Some
    (coq_SF_of_word sz w)), ((Some (coq_PF_of_word sz w)), (Some
    (coq_ZF_of_word sz w))))))

(** val rflags_of_mul : bool -> sem_tuple **)

let rflags_of_mul ov =
  Obj.magic ((Some ov), ((Some ov), (None, (None, None))))

(** val rflags_of_div : sem_tuple **)

let rflags_of_div =
  Obj.magic (None, (None, (None, (None, None))))

(** val rflags_of_andn : wsize -> GRing.ComRing.sort -> sem_tuple **)

let rflags_of_andn sz w =
  Obj.magic ((Some false), ((Some false), ((Some (coq_SF_of_word sz w)),
    (None, (Some (coq_ZF_of_word sz w))))))

(** val rflags_None_w : wsize -> sem_ot -> sem_tuple **)

let rflags_None_w _ w =
  Obj.magic (None, (None, (None, (None, (None, w)))))

(** val rflags_of_aluop_nocf :
    wsize -> GRing.ComRing.sort -> coq_Z -> sem_tuple **)

let rflags_of_aluop_nocf sz w vs =
  Obj.magic ((Some
    (negb
      (eq_op coq_BinNums_Z__canonical__eqtype_Equality
        (Obj.magic wsigned sz w) (Obj.magic vs)))), ((Some
    (coq_SF_of_word sz w)), ((Some (coq_PF_of_word sz w)), (Some
    (coq_ZF_of_word sz w)))))

(** val flags_w :
    __ list -> ltuple -> wsize -> GRing.ComRing.sort -> ltuple **)

let flags_w l1 bs sz w =
  merge_tuple l1 (map (Obj.magic __) (w_ty sz)) bs w

(** val flags_w2 : __ list -> ltuple -> wsize -> sem_tuple -> ltuple **)

let flags_w2 l1 bs sz w =
  merge_tuple l1 (map (Obj.magic __) (w2_ty sz sz)) bs w

(** val rflags_of_aluop_w :
    wsize -> GRing.ComRing.sort -> coq_Z -> coq_Z -> ltuple **)

let rflags_of_aluop_w sz w vu vs =
  flags_w (map (Obj.magic __) b5_ty) (rflags_of_aluop sz w vu vs) sz w

(** val rflags_of_aluop_nocf_w :
    wsize -> GRing.ComRing.sort -> coq_Z -> ltuple **)

let rflags_of_aluop_nocf_w sz w vs =
  flags_w (map (Obj.magic __) b4_ty) (rflags_of_aluop_nocf sz w vs) sz w

(** val rflags_of_bwop_w : wsize -> GRing.ComRing.sort -> ltuple **)

let rflags_of_bwop_w sz w =
  flags_w (map (Obj.magic __) b5_ty) (rflags_of_bwop sz w) sz w

(** val prim_8_64 : (wsize -> 'a1) -> 'a1 prim_constructor **)

let prim_8_64 f =
  let range = U64 :: (U32 :: (U16 :: (U8 :: []))) in
  PrimX86 ((map (fun x -> PVp x) range), (fun s ->
  match s with
  | PVp sz -> Some (f sz)
  | PVs (_, _) -> None
  | PVv (_, _) -> None
  | PVsv (_, _, _) -> None
  | PVx (_, _) -> None
  | PVvv (_, _, _, _) -> None))

(** val prim_16_64 : (wsize -> 'a1) -> 'a1 prim_constructor **)

let prim_16_64 f =
  let range = U64 :: (U32 :: (U16 :: [])) in
  PrimX86 ((map (fun x -> PVp x) range), (fun s ->
  match s with
  | PVp sz -> Some (f sz)
  | PVs (_, _) -> None
  | PVv (_, _) -> None
  | PVsv (_, _, _) -> None
  | PVx (_, _) -> None
  | PVvv (_, _, _, _) -> None))

(** val prim_32_64 : (wsize -> 'a1) -> 'a1 prim_constructor **)

let prim_32_64 f =
  let range = U64 :: (U32 :: []) in
  PrimX86 ((map (fun x -> PVp x) range), (fun s ->
  match s with
  | PVp sz -> Some (f sz)
  | PVs (_, _) -> None
  | PVv (_, _) -> None
  | PVsv (_, _, _) -> None
  | PVx (_, _) -> None
  | PVvv (_, _, _, _) -> None))

(** val prim_128_256 : (wsize -> 'a1) -> 'a1 prim_constructor **)

let prim_128_256 f =
  let range = U128 :: (U256 :: []) in
  PrimX86 ((map (fun x -> PVp x) range), (fun s ->
  match s with
  | PVp sz -> Some (f sz)
  | PVs (_, _) -> None
  | PVv (_, _) -> None
  | PVsv (_, _, _) -> None
  | PVx (_, _) -> None
  | PVvv (_, _, _, _) -> None))

(** val prim_movsx : (wsize -> wsize -> x86_op) -> x86_op prim_constructor **)

let prim_movsx =
  let range = (PVx (U16, U8)) :: ((PVx (U32, U8)) :: ((PVx (U64,
    U8)) :: ((PVx (U16, U16)) :: ((PVx (U32, U16)) :: ((PVx (U64,
    U16)) :: ((PVx (U32, U32)) :: ((PVx (U64, U32)) :: [])))))))
  in
  (fun f -> PrimX86 (range, (fun s ->
  match s with
  | PVp _ -> None
  | PVs (_, _) -> None
  | PVv (_, _) -> None
  | PVsv (_, _, _) -> None
  | PVx (szo, szi) -> Some (f szo szi)
  | PVvv (_, _, _, _) -> None)))

(** val prim_movzx : (wsize -> wsize -> x86_op) -> x86_op prim_constructor **)

let prim_movzx =
  let range = (PVx (U16, U8)) :: ((PVx (U32, U8)) :: ((PVx (U64,
    U8)) :: ((PVx (U32, U16)) :: ((PVx (U64, U16)) :: []))))
  in
  (fun f -> PrimX86 (range, (fun s ->
  match s with
  | PVp _ -> None
  | PVs (_, _) -> None
  | PVv (_, _) -> None
  | PVsv (_, _, _) -> None
  | PVx (szo, szi) -> Some (f szo szi)
  | PVvv (_, _, _, _) -> None)))

(** val prim_vv :
    (velem -> wsize -> velem -> wsize -> x86_op) -> x86_op prim_constructor **)

let prim_vv f =
  PrimX86 (((PVvv (VE8, U64, VE16, U128)) :: ((PVvv (VE8, U32, VE32,
    U128)) :: ((PVvv (VE8, U16, VE64, U128)) :: ((PVvv (VE16, U64, VE32,
    U128)) :: ((PVvv (VE16, U32, VE64, U128)) :: ((PVvv (VE32, U64, VE64,
    U128)) :: ((PVvv (VE8, U128, VE16, U256)) :: ((PVvv (VE8, U64, VE32,
    U256)) :: ((PVvv (VE8, U32, VE64, U256)) :: ((PVvv (VE16, U128, VE32,
    U256)) :: ((PVvv (VE16, U64, VE64, U256)) :: ((PVvv (VE32, U128, VE64,
    U256)) :: [])))))))))))), (fun s ->
    match s with
    | PVvv (ve, sz, ve', sz') -> Some (f ve sz ve' sz')
    | _ -> None))

(** val primV_range :
    prim_x86_suffix list -> (velem -> wsize -> x86_op) -> x86_op
    prim_constructor **)

let primV_range range f =
  PrimX86 (range, (fun s ->
    match s with
    | PVv (ve, sz) -> Some (f ve sz)
    | _ -> None))

(** val primV : (velem -> wsize -> x86_op) -> x86_op prim_constructor **)

let primV =
  primV_range
    (flatten
      (map (fun ve -> map (fun sz -> PVv (ve, sz)) (U128 :: (U256 :: [])))
        (VE8 :: (VE16 :: (VE32 :: (VE64 :: []))))))

(** val primV_8_16 : (velem -> wsize -> x86_op) -> x86_op prim_constructor **)

let primV_8_16 =
  primV_range
    (flatten
      (map (fun ve -> map (fun sz -> PVv (ve, sz)) (U128 :: (U256 :: [])))
        (VE8 :: (VE16 :: []))))

(** val primV_8_32 : (velem -> wsize -> x86_op) -> x86_op prim_constructor **)

let primV_8_32 =
  primV_range
    (flatten
      (map (fun ve -> map (fun sz -> PVv (ve, sz)) (U128 :: (U256 :: [])))
        (VE8 :: (VE16 :: (VE32 :: [])))))

(** val primV_16 : (velem -> wsize -> x86_op) -> x86_op prim_constructor **)

let primV_16 =
  primV_range (map (fun sz -> PVv (VE16, sz)) (U128 :: (U256 :: [])))

(** val primV_16_32 :
    (velem -> wsize -> x86_op) -> x86_op prim_constructor **)

let primV_16_32 =
  primV_range
    (flatten
      (map (fun ve -> map (fun sz -> PVv (ve, sz)) (U128 :: (U256 :: [])))
        (VE16 :: (VE32 :: []))))

(** val primV_16_64 :
    (velem -> wsize -> x86_op) -> x86_op prim_constructor **)

let primV_16_64 =
  primV_range
    (flatten
      (map (fun ve -> map (fun sz -> PVv (ve, sz)) (U128 :: (U256 :: [])))
        (VE16 :: (VE32 :: (VE64 :: [])))))

(** val primV_128 : (velem -> wsize -> x86_op) -> x86_op prim_constructor **)

let primV_128 =
  primV_range
    (map (fun ve -> PVv (ve, U128)) (VE8 :: (VE16 :: (VE32 :: (VE64 :: [])))))

(** val primSV_8_32 :
    (signedness -> velem -> wsize -> x86_op) -> x86_op prim_constructor **)

let primSV_8_32 f =
  PrimX86
    ((flatten
       (map (fun pv -> map pv (U128 :: (U256 :: [])))
         (flatten
           (map (fun sg ->
             map (fun ve x -> PVsv (sg, ve, x))
               (VE8 :: (VE16 :: (VE32 :: [])))) (Signed :: (Unsigned :: [])))))),
    (fun s ->
    match s with
    | PVsv (sg, ve, sz) -> Some (f sg ve sz)
    | _ -> None))

(** val primX : (wsize -> wsize -> x86_op) -> x86_op prim_constructor **)

let primX f =
  PrimX86
    ((flatten
       (map (fun ssz -> map (fun dsz -> PVx (ssz, dsz)) (U64 :: (U32 :: [])))
         (U128 :: (U256 :: [])))), (fun s ->
    match s with
    | PVx (ssz, dsz) -> Some (f ssz dsz)
    | _ -> None))

(** val implicit_flags :
    (register, register_ext, xmm_register, rflag, condt) Arch_decl.arg_desc
    list **)

let implicit_flags =
  map (coq_F x86_decl) (OF :: (CF :: (SF :: (PF :: (ZF :: [])))))

(** val implicit_flags_noCF :
    (register, register_ext, xmm_register, rflag, condt) Arch_decl.arg_desc
    list **)

let implicit_flags_noCF =
  map (coq_F x86_decl) (OF :: (SF :: (PF :: (ZF :: []))))

(** val iCF :
    (register, register_ext, xmm_register, rflag, condt) Arch_decl.arg_desc **)

let iCF =
  coq_F x86_decl CF

(** val reg_msb_flag : wsize -> msb_flag **)

let reg_msb_flag sz =
  if cmp_le wsize_cmp sz U16 then MSB_MERGE else MSB_CLEAR

(** val max_32 : wsize -> wsize **)

let max_32 sz =
  if cmp_le wsize_cmp sz U32 then sz else U32

(** val map_sz :
    wsize -> (register, register_ext, xmm_register, rflag, condt) asm_args ->
    (wsize * (register, register_ext, xmm_register, rflag, condt) asm_arg)
    list **)

let map_sz sz a =
  List0.map (fun a0 -> (sz, a0)) a

(** val pp_name :
    string -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    asm_args -> (register, register_ext, xmm_register, rflag, condt) pp_asm_op **)

let pp_name name sz args =
  { pp_aop_name = name; pp_aop_ext = PP_name; pp_aop_args = (map_sz sz args) }

(** val pp_name_ty :
    string -> wsize list -> (register, register_ext, xmm_register, rflag,
    condt) asm_arg list -> (register, register_ext, xmm_register, rflag,
    condt) pp_asm_op **)

let pp_name_ty name ws args =
  { pp_aop_name = name; pp_aop_ext = PP_name; pp_aop_args = (zip ws args) }

(** val pp_iname :
    string -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    asm_args -> (register, register_ext, xmm_register, rflag, condt) pp_asm_op **)

let pp_iname name sz args =
  { pp_aop_name = name; pp_aop_ext = (PP_iname sz); pp_aop_args =
    (map_sz sz args) }

(** val pp_viname_long :
    string -> velem -> wsize -> (register, register_ext, xmm_register, rflag,
    condt) asm_args -> (register, register_ext, xmm_register, rflag, condt)
    pp_asm_op **)

let pp_viname_long name ve sz args =
  { pp_aop_name = name; pp_aop_ext = (PP_viname (ve, true)); pp_aop_args =
    (map_sz sz args) }

(** val pp_viname :
    string -> velem -> wsize -> (register, register_ext, xmm_register, rflag,
    condt) asm_args -> (register, register_ext, xmm_register, rflag, condt)
    pp_asm_op **)

let pp_viname name ve sz args =
  { pp_aop_name = name; pp_aop_ext = (PP_viname (ve, false)); pp_aop_args =
    (map_sz sz args) }

(** val pp_viname_ww_128 :
    string -> velem -> wsize -> (register, register_ext, xmm_register, rflag,
    condt) asm_arg list -> (register, register_ext, xmm_register, rflag,
    condt) pp_asm_op **)

let pp_viname_ww_128 name ve sz args =
  { pp_aop_name = name; pp_aop_ext = (PP_viname (ve, false)); pp_aop_args =
    (zip (sz :: (sz :: (U128 :: []))) args) }

(** val pp_iname_w_8 :
    string -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    asm_arg list -> (register, register_ext, xmm_register, rflag, condt)
    pp_asm_op **)

let pp_iname_w_8 name sz args =
  { pp_aop_name = name; pp_aop_ext = (PP_iname sz); pp_aop_args =
    (zip (sz :: (U8 :: [])) args) }

(** val pp_iname_ww_8 :
    string -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    asm_arg list -> (register, register_ext, xmm_register, rflag, condt)
    pp_asm_op **)

let pp_iname_ww_8 name sz args =
  { pp_aop_name = name; pp_aop_ext = (PP_iname sz); pp_aop_args =
    (zip (sz :: (sz :: (U8 :: []))) args) }

(** val get_ct :
    (register, register_ext, xmm_register, rflag, condt) asm_arg list ->
    (register, register_ext, xmm_register, rflag, condt)
    pp_asm_op_ext * (register, register_ext, xmm_register, rflag, condt)
    asm_arg list **)

let get_ct args = match args with
| [] -> (PP_error, args)
| a :: args0 -> ((PP_ct a), args0)

(** val pp_ct :
    string -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    asm_arg list -> (register, register_ext, xmm_register, rflag, condt)
    pp_asm_op **)

let pp_ct name sz args =
  let (ext, args0) = get_ct args in
  { pp_aop_name = name; pp_aop_ext = ext; pp_aop_args = (map_sz sz args0) }

(** val pp_cqo :
    wsize -> (register, register_ext, xmm_register, rflag, condt) asm_args ->
    (register, register_ext, xmm_register, rflag, condt) pp_asm_op **)

let pp_cqo sz _ =
  match sz with
  | U8 ->
    let name = "CQO" in
    let ext = PP_error in
    { pp_aop_name = name; pp_aop_ext = ext; pp_aop_args = (map_sz sz []) }
  | U16 ->
    let name = "cwd" in
    let ext = PP_name in
    { pp_aop_name = name; pp_aop_ext = ext; pp_aop_args = (map_sz sz []) }
  | U32 ->
    let name = "cdq" in
    let ext = PP_name in
    { pp_aop_name = name; pp_aop_ext = ext; pp_aop_args = (map_sz sz []) }
  | U64 ->
    let name = "cqo" in
    let ext = PP_name in
    { pp_aop_name = name; pp_aop_ext = ext; pp_aop_args = (map_sz sz []) }
  | U128 ->
    let name = "CQO" in
    let ext = PP_error in
    { pp_aop_name = name; pp_aop_ext = ext; pp_aop_args = (map_sz sz []) }
  | U256 ->
    let name = "CQO" in
    let ext = PP_error in
    { pp_aop_name = name; pp_aop_ext = ext; pp_aop_args = (map_sz sz []) }

(** val c : arg_kind list **)

let c =
  CAcond :: []

(** val r : arg_kind list **)

let r =
  CAreg :: []

(** val rx : arg_kind list **)

let rx =
  CAregx :: []

(** val m : bool -> arg_kind list **)

let m b =
  (CAmem b) :: []

(** val i : wsize -> arg_kind list **)

let i sz =
  (CAimm (CAimmC_none, sz)) :: []

(** val rm : bool -> arg_kind list **)

let rm b =
  CAreg :: ((CAmem b) :: [])

(** val rxm : bool -> arg_kind list **)

let rxm b =
  CAregx :: ((CAmem b) :: [])

(** val rmi : wsize -> arg_kind list **)

let rmi sz =
  CAreg :: ((CAmem true) :: ((CAimm (CAimmC_none, sz)) :: []))

(** val ri : wsize -> arg_kind list **)

let ri sz =
  CAreg :: ((CAimm (CAimmC_none, sz)) :: [])

(** val m_r : arg_kind list list **)

let m_r =
  (m false) :: (r :: [])

(** val r_rm_false : arg_kind list list **)

let r_rm_false =
  r :: ((rm false) :: [])

(** val r_rm : arg_kind list list **)

let r_rm =
  r :: ((rm true) :: [])

(** val r_rmi : wsize -> arg_kind list list **)

let r_rmi sz =
  r :: ((rmi sz) :: [])

(** val m_ri : wsize -> arg_kind list list **)

let m_ri sz =
  (m false) :: ((ri sz) :: [])

(** val xmm : arg_kind list **)

let xmm =
  CAxmm :: []

(** val xmmm : bool -> arg_kind list **)

let xmmm b =
  CAxmm :: ((CAmem b) :: [])

(** val xmmmi : wsize -> arg_kind list **)

let xmmmi sz =
  CAxmm :: ((CAmem true) :: ((CAimm (CAimmC_none, sz)) :: []))

(** val xmm_xmmm : arg_kind list list **)

let xmm_xmmm =
  xmm :: ((xmmm true) :: [])

(** val xmmm_xmm : arg_kind list list **)

let xmmm_xmm =
  (xmmm false) :: (xmm :: [])

(** val xmm_xmm_xmmm : arg_kind list list **)

let xmm_xmm_xmmm =
  xmm :: (xmm :: ((xmmm true) :: []))

(** val xmm_xmm_xmmmi : wsize -> arg_kind list list **)

let xmm_xmm_xmmmi sz =
  xmm :: (xmm :: ((xmmmi sz) :: []))

(** val x86_MOV : wsize -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let x86_MOV _ x =
  x

(** val check_mov : wsize -> arg_kind list list list **)

let check_mov sz =
  (r_rmi sz) :: ((m_ri (max_32 sz)) :: [])

(** val coq_Ox86_MOV_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_MOV_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w_ty sz); id_in = ((coq_Eu x86_decl (S O)) :: []); id_tout =
    (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_MOV sz)); id_args_kinds =
    (check_mov sz); id_nargs = (S (S O)); id_str_jas = (pp_sz "MOV" sz);
    id_safe = []; id_pp_asm = (pp_iname "mov" sz) }), ("MOV",
    (prim_8_64 (fun x -> MOV x))))

(** val check_movx : wsize -> arg_kind list list list **)

let check_movx _ =
  (rx :: ((rm true) :: [])) :: (((rm true) :: (rx :: [])) :: [])

(** val pp_movd :
    string -> Equality.sort -> (register, register_ext, xmm_register, rflag,
    condt) asm_arg list -> (register, register_ext, xmm_register, rflag,
    condt) pp_asm_op **)

let pp_movd name sz args =
  pp_name_ty
    (if eq_op wsize_wsize__canonical__eqtype_Equality sz (Obj.magic U64)
     then (^) name "q"
     else (^) name "d")
    (match args with
     | [] -> (Obj.magic sz) :: ((Obj.magic sz) :: [])
     | y :: l ->
       (match y with
        | Arch_decl.Reg _ ->
          (match l with
           | [] -> (Obj.magic sz) :: ((Obj.magic sz) :: [])
           | a :: l0 ->
             (match a with
              | XReg _ ->
                (match l0 with
                 | [] -> (Obj.magic sz) :: (U128 :: [])
                 | _ :: _ -> (Obj.magic sz) :: ((Obj.magic sz) :: []))
              | _ -> (Obj.magic sz) :: ((Obj.magic sz) :: [])))
        | XReg _ ->
          (match l with
           | [] -> (Obj.magic sz) :: ((Obj.magic sz) :: [])
           | a :: l0 ->
             (match a with
              | Arch_decl.Reg _ ->
                (match l0 with
                 | [] -> U128 :: ((Obj.magic sz) :: [])
                 | _ :: _ -> (Obj.magic sz) :: ((Obj.magic sz) :: []))
              | _ -> (Obj.magic sz) :: ((Obj.magic sz) :: [])))
        | _ -> (Obj.magic sz) :: ((Obj.magic sz) :: []))) args

(** val x86_MOVX : wsize -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let x86_MOVX _ x =
  x

(** val coq_Ox86_MOVX_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_MOVX_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w_ty sz); id_in = ((coq_Eu x86_decl (S O)) :: []); id_tout =
    (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_MOVX sz)); id_args_kinds =
    (check_movx sz); id_nargs = (S (S O)); id_str_jas = (pp_sz "MOVX" sz);
    id_safe = []; id_pp_asm = (pp_movd "mov" (Obj.magic sz)) }), ("MOVX",
    (prim_32_64 (fun x -> MOVX x))))

(** val check_por : i_args_kinds **)

let check_por =
  (rx :: ((rxm true) :: [])) :: []

(** val x86_POR :
    (register, register_ext, xmm_register, rflag, condt) wreg -> (register,
    register_ext, xmm_register, rflag, condt) wreg -> (register,
    register_ext, xmm_register, rflag, condt) wreg **)

let x86_POR v1 v2 =
  Word0.wor x86_decl.reg_size v1 v2

(** val coq_Ox86_POR_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_POR_instr =
  let desc = { id_valid = true; id_msb_flag = MSB_MERGE; id_tin =
    (w2_ty U64 U64); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])); id_tout =
    (w_ty U64); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty U64 U64) (Obj.magic x86_POR)); id_args_kinds =
    check_por; id_nargs = (S (S O)); id_str_jas = (pp_s "POR"); id_safe = [];
    id_pp_asm = (pp_name "por" U64) }
  in
  (desc, ("POR", (primM POR)))

(** val check_movsx : wsize -> wsize -> arg_kind list list list **)

let check_movsx _ _ =
  r_rm :: []

(** val pp_movsx :
    Equality.sort -> Equality.sort -> (register, register_ext, xmm_register,
    rflag, condt) asm_arg list -> (register, register_ext, xmm_register,
    rflag, condt) pp_asm_op **)

let pp_movsx szs szd args =
  let ext =
    if (||) (eq_op wsize_wsize__canonical__eqtype_Equality szd szs)
         ((&&)
           (eq_op wsize_wsize__canonical__eqtype_Equality szd (Obj.magic U64))
           (eq_op wsize_wsize__canonical__eqtype_Equality szs (Obj.magic U32)))
    then "xd"
    else "x"
  in
  { pp_aop_name = "movs"; pp_aop_ext = (PP_iname2 (ext, (Obj.magic szs),
  (Obj.magic szd))); pp_aop_args =
  (zip ((Obj.magic szd) :: ((Obj.magic szs) :: [])) args) }

(** val size_MOVSX : wsize -> wsize -> bool **)

let size_MOVSX szi szo =
  match szi with
  | U8 -> size_16_64 szo
  | U16 -> size_16_64 szo
  | U32 -> size_32_64 szo
  | _ -> false

(** val x86_MOVSX : wsize -> wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_MOVSX szi szo x =
  sign_extend szo szi x

(** val coq_Ox86_MOVSX_instr :
    (wsize -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_MOVSX_instr =
  ((fun szo szi -> { id_valid = (size_MOVSX szi szo); id_msb_flag =
    (reg_msb_flag szo); id_tin = (w_ty szi); id_in =
    ((coq_Eu x86_decl (S O)) :: []); id_tout = (w_ty szo); id_out =
    ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty szi) (Obj.magic x86_MOVSX szi szo)); id_args_kinds =
    (check_movsx szi szo); id_nargs = (S (S O)); id_str_jas =
    (pp_sz_sz "MOVSX" true szo szi); id_safe = []; id_pp_asm =
    (pp_movsx (Obj.magic szi) (Obj.magic szo)) }), ("MOVSX",
    (prim_movsx (fun x x0 -> MOVSX (x, x0)))))

(** val pp_movzx :
    wsize -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    asm_arg list -> (register, register_ext, xmm_register, rflag, condt)
    pp_asm_op **)

let pp_movzx szs szd args =
  { pp_aop_name = "movz"; pp_aop_ext = (PP_iname2 ("x", szs, szd));
    pp_aop_args = (zip (szd :: (szs :: [])) args) }

(** val size_MOVZX : wsize -> wsize -> bool **)

let size_MOVZX szi szo =
  match szi with
  | U8 -> size_16_64 szo
  | U16 -> size_32_64 szo
  | _ -> false

(** val x86_MOVZX : wsize -> wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_MOVZX szi szo x =
  zero_extend szo szi x

(** val coq_Ox86_MOVZX_instr :
    (wsize -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_MOVZX_instr =
  ((fun szo szi -> { id_valid = (size_MOVZX szi szo); id_msb_flag =
    (reg_msb_flag szo); id_tin = (w_ty szi); id_in =
    ((coq_Eu x86_decl (S O)) :: []); id_tout = (w_ty szo); id_out =
    ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty szi) (Obj.magic x86_MOVZX szi szo)); id_args_kinds =
    (check_movsx szi szo); id_nargs = (S (S O)); id_str_jas =
    (pp_sz_sz "MOVZX" false szo szi); id_safe = []; id_pp_asm =
    (pp_movzx szi szo) }), ("MOVZX",
    (prim_movzx (fun x x0 -> MOVZX (x, x0)))))

(** val check_xchg : arg_kind list list list **)

let check_xchg =
  m_r :: (r_rm :: [])

(** val x86_XCHG :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_XCHG _ v1 v2 =
  Obj.magic (v2, v1)

(** val coq_Ox86_XCHG_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_XCHG_instr =
  let name = "XCHG" in
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
  id_tin = (w2_ty sz sz); id_in =
  ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])); id_tout =
  (w2_ty sz sz); id_out =
  ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])); id_semi =
  (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_XCHG sz)); id_args_kinds =
  check_xchg; id_nargs = (S (S O)); id_str_jas = (pp_sz name sz); id_safe =
  []; id_pp_asm = (pp_name "xchg" sz) }), (name,
  (primP (arch_pd x86_decl) (fun x -> XCHG x))))

(** val c_r_rm : arg_kind list list **)

let c_r_rm =
  c :: (r :: ((rm true) :: []))

(** val x86_CMOVcc :
    wsize -> bool -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_CMOVcc _ b w2 w3 =
  if b then w2 else w3

(** val coq_Ox86_CMOVcc_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_CMOVcc_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (bw2_ty sz); id_in =
    ((coq_Ea x86_decl O) :: ((coq_Eu x86_decl (S (S O))) :: ((coq_Ea x86_decl
                                                               (S O)) :: [])));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl (S O)) :: []); id_semi =
    (sem_prod_ok (bw2_ty sz) (Obj.magic x86_CMOVcc sz)); id_args_kinds =
    (c_r_rm :: []); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "CMOVcc" sz); id_safe = []; id_pp_asm = (pp_ct "cmov" sz) }),
    ("CMOVcc", (prim_16_64 (fun x -> CMOVcc x))))

(** val check_add : wsize -> arg_kind list list list **)

let check_add sz =
  (m_ri (max_32 sz)) :: ((r_rmi (max_32 sz)) :: [])

(** val x86_ADD :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_ADD sz v1 v2 =
  rflags_of_aluop_w sz
    (GRing.add
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule (word sz))
      v1 v2) (Z.add (wunsigned sz v1) (wunsigned sz v2))
    (Z.add (wsigned sz v1) (wsigned sz v2))

(** val coq_Ox86_ADD_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_ADD_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])); id_tout =
    (b5w_ty sz); id_out = (cat implicit_flags ((coq_Eu x86_decl O) :: []));
    id_semi = (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_ADD sz));
    id_args_kinds = (check_add sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "ADD" sz); id_safe = []; id_pp_asm = (pp_iname "add" sz) }),
    ("ADD", (prim_8_64 (fun x -> ADD x))))

(** val x86_SUB :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_SUB sz v1 v2 =
  rflags_of_aluop_w sz
    (GRing.add
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule (word sz))
      v1
      (GRing.opp
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Zmodule (word sz))
        v2)) (Z.sub (wunsigned sz v1) (wunsigned sz v2))
    (Z.sub (wsigned sz v1) (wsigned sz v2))

(** val coq_Ox86_SUB_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_SUB_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])); id_tout =
    (b5w_ty sz); id_out = (cat implicit_flags ((coq_Eu x86_decl O) :: []));
    id_semi = (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_SUB sz));
    id_args_kinds = (check_add sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "SUB" sz); id_safe = []; id_pp_asm = (pp_iname "sub" sz) }),
    ("SUB", (prim_8_64 (fun x -> SUB x))))

(** val check_mul : wsize -> arg_kind list list list **)

let check_mul _ =
  ((rm true) :: []) :: []

(** val x86_MUL :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_MUL sz v1 v2 =
  let lo =
    GRing.mul
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing (word sz))
      v1 v2
  in
  let hi = wmulhu sz v1 v2 in
  let ov = wdwordu sz hi lo in
  let ov0 = Z.gtb ov (Z.sub (wbase sz) (Zpos Coq_xH)) in
  flags_w2 (map (Obj.magic __) b5_ty) (rflags_of_mul ov0) sz
    (Obj.magic (hi, lo))

(** val coq_Ox86_MUL_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_MUL_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_R x86_decl RAX) :: ((coq_Eu x86_decl O) :: [])); id_tout =
    (b5w2_ty sz); id_out =
    (cat implicit_flags
      ((coq_R x86_decl RDX) :: ((coq_R x86_decl RAX) :: []))); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_MUL sz)); id_args_kinds =
    (check_mul sz); id_nargs = (S O); id_str_jas = (pp_sz "MUL" sz);
    id_safe = []; id_pp_asm = (pp_iname "mul" sz) }), ("MUL",
    (prim_16_64 (fun x -> MUL x))))

(** val x86_IMUL_overflow :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> bool **)

let x86_IMUL_overflow sz hi lo =
  let ov = wdwords sz hi lo in
  (||) (Z.ltb ov (wmin_signed sz)) (Z.gtb ov (wmax_signed sz))

(** val x86_IMUL :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_IMUL sz v1 v2 =
  let lo =
    GRing.mul
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing (word sz))
      v1 v2
  in
  let hi = wmulhs sz v1 v2 in
  let ov = x86_IMUL_overflow sz hi lo in
  flags_w2 (map (Obj.magic __) b5_ty) (rflags_of_mul ov) sz
    (Obj.magic (hi, lo))

(** val coq_Ox86_IMUL_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_IMUL_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_R x86_decl RAX) :: ((coq_Eu x86_decl O) :: [])); id_tout =
    (b5w2_ty sz); id_out =
    (cat implicit_flags
      ((coq_R x86_decl RDX) :: ((coq_R x86_decl RAX) :: []))); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_IMUL sz)); id_args_kinds =
    (check_mul sz); id_nargs = (S O); id_str_jas = (pp_sz "IMUL" sz);
    id_safe = []; id_pp_asm = (pp_iname "imul" sz) }), ("IMUL",
    (prim_16_64 (fun x -> IMUL x))))

(** val x86_IMULt :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_IMULt sz v1 v2 =
  let lo =
    GRing.mul
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing (word sz))
      v1 v2
  in
  let hi = wmulhs sz v1 v2 in
  let ov = x86_IMUL_overflow sz hi lo in
  flags_w (map (Obj.magic __) b5_ty) (rflags_of_mul ov) sz lo

(** val coq_Ox86_IMULr_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_IMULr_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])); id_tout =
    (b5w_ty sz); id_out = (cat implicit_flags ((coq_Eu x86_decl O) :: []));
    id_semi = (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_IMULt sz));
    id_args_kinds = (r_rm :: []); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "IMULr" sz); id_safe = []; id_pp_asm = (pp_iname "imul" sz) }),
    ("IMULr", (prim_16_64 (fun x -> IMULr x))))

(** val coq_Ox86_IMULri_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_IMULri_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (b5w_ty sz); id_out =
    (cat implicit_flags ((coq_Eu x86_decl O) :: [])); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_IMULt sz)); id_args_kinds =
    ((r :: ((rm true) :: ((i (max_32 sz)) :: []))) :: []); id_nargs = (S (S
    (S O))); id_str_jas = (pp_sz "IMULri" sz); id_safe = []; id_pp_asm =
    (pp_iname "imul" sz) }), ("IMULri", (prim_16_64 (fun x -> IMULri x))))

(** val x86_DIV :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort
    -> sem_tuple exec **)

let x86_DIV sz hi lo dv =
  let dd = wdwordu sz hi lo in
  let dv0 = wunsigned sz dv in
  let q = Z.div dd dv0 in
  let r0 = Z.modulo dd dv0 in
  let ov = Z.gtb q (wmax_unsigned sz) in
  if (||)
       (eq_op coq_BinNums_Z__canonical__eqtype_Equality (Obj.magic dv0)
         (Obj.magic Z0)) ov
  then Error ErrArith
  else Ok
         (flags_w2 (map (Obj.magic __) b5_ty) rflags_of_div sz
           (Obj.magic ((wrepr sz q), (wrepr sz r0))))

(** val coq_Ox86_DIV_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_DIV_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w3_ty sz); id_in =
    ((coq_R x86_decl RDX) :: ((coq_R x86_decl RAX) :: ((coq_Eu x86_decl O) :: [])));
    id_tout = (b5w2_ty sz); id_out =
    (cat implicit_flags
      ((coq_R x86_decl RAX) :: ((coq_R x86_decl RDX) :: []))); id_semi =
    (Obj.magic x86_DIV sz); id_args_kinds = (check_mul sz); id_nargs = (S O);
    id_str_jas = (pp_sz "DIV" sz); id_safe = ((X86Division (sz,
    Unsigned)) :: []); id_pp_asm = (pp_iname "div" sz) }), ("DIV",
    (prim_16_64 (fun x -> DIV x))))

(** val x86_IDIV :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort
    -> sem_tuple exec **)

let x86_IDIV sz hi lo dv =
  let dd = wdwords sz hi lo in
  let dv0 = wsigned sz dv in
  let q = Z.quot dd dv0 in
  let r0 = Z.rem dd dv0 in
  let ov = (||) (Z.ltb q (wmin_signed sz)) (Z.gtb q (wmax_signed sz)) in
  if (||)
       (eq_op coq_BinNums_Z__canonical__eqtype_Equality (Obj.magic dv0)
         (Obj.magic Z0)) ov
  then Error ErrArith
  else Ok
         (flags_w2 (map (Obj.magic __) b5_ty) rflags_of_div sz
           (Obj.magic ((wrepr sz q), (wrepr sz r0))))

(** val coq_Ox86_IDIV_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_IDIV_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w3_ty sz); id_in =
    ((coq_R x86_decl RDX) :: ((coq_R x86_decl RAX) :: ((coq_Eu x86_decl O) :: [])));
    id_tout = (b5w2_ty sz); id_out =
    (cat implicit_flags
      ((coq_R x86_decl RAX) :: ((coq_R x86_decl RDX) :: []))); id_semi =
    (Obj.magic x86_IDIV sz); id_args_kinds = (check_mul sz); id_nargs = (S
    O); id_str_jas = (pp_sz "IDIV" sz); id_safe = ((X86Division (sz,
    Signed)) :: []); id_pp_asm = (pp_iname "idiv" sz) }), ("IDIV",
    (prim_16_64 (fun x -> IDIV x))))

(** val x86_CQO : wsize -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let x86_CQO sz w =
  if msb sz w
  then GRing.opp
         (GRing.Ring.Exports.join_GRing_Ring_between_GRing_SemiRing_and_GRing_Zmodule
           (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Ring (word sz)))
         (GRing.one
           (GRing.Ring.Exports.coq_GRing_Ring__to__GRing_SemiRing
             (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Ring
               (word sz))))
  else GRing.zero
         (GRing.Zmodule.Exports.coq_GRing_Zmodule__to__GRing_Nmodule
           (GRing.Ring.Exports.join_GRing_Ring_between_GRing_SemiRing_and_GRing_Zmodule
             (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Ring
               (word sz))))

(** val coq_Ox86_CQO_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_CQO_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w_ty sz); id_in = ((coq_R x86_decl RAX) :: []); id_tout =
    (w_ty sz); id_out = ((coq_R x86_decl RDX) :: []); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_CQO sz)); id_args_kinds =
    ([] :: []); id_nargs = O; id_str_jas = (pp_sz "CQO" sz); id_safe = [];
    id_pp_asm = (pp_cqo sz) }), ("CQO", (prim_16_64 (fun x -> CQO x))))

(** val add_carry : wsize -> coq_Z -> coq_Z -> coq_Z -> GRing.ComRing.sort **)

let add_carry sz x y c0 =
  wrepr sz (Z.add (Z.add x y) c0)

(** val x86_ADC :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> bool -> sem_tuple **)

let x86_ADC sz v1 v2 c0 =
  let c1 = Z.b2z c0 in
  rflags_of_aluop_w sz (add_carry sz (wunsigned sz v1) (wunsigned sz v2) c1)
    (Z.add (Z.add (wunsigned sz v1) (wunsigned sz v2)) c1)
    (Z.add (Z.add (wsigned sz v1) (wsigned sz v2)) c1)

(** val coq_Ox86_ADC_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_ADC_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2b_ty sz sz); id_in =
    (cat ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])) (iCF :: []));
    id_tout = (b5w_ty sz); id_out =
    (cat implicit_flags ((coq_Eu x86_decl O) :: [])); id_semi =
    (sem_prod_ok (w2b_ty sz sz) (Obj.magic x86_ADC sz)); id_args_kinds =
    (check_add sz); id_nargs = (S (S O)); id_str_jas = (pp_sz "ADC" sz);
    id_safe = []; id_pp_asm = (pp_iname "adc" sz) }), ("ADC",
    (prim_8_64 (fun x -> ADC x))))

(** val sub_borrow :
    wsize -> coq_Z -> coq_Z -> coq_Z -> GRing.ComRing.sort **)

let sub_borrow sz x y c0 =
  wrepr sz (Z.sub (Z.sub x y) c0)

(** val x86_SBB :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> bool -> sem_tuple **)

let x86_SBB sz v1 v2 c0 =
  let c1 = Z.b2z c0 in
  rflags_of_aluop_w sz (sub_borrow sz (wunsigned sz v1) (wunsigned sz v2) c1)
    (Z.sub (wunsigned sz v1) (Z.add (wunsigned sz v2) c1))
    (Z.sub (wsigned sz v1) (Z.add (wsigned sz v2) c1))

(** val coq_Ox86_SBB_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_SBB_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2b_ty sz sz); id_in =
    (cat ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])) (iCF :: []));
    id_tout = (b5w_ty sz); id_out =
    (cat implicit_flags ((coq_Eu x86_decl O) :: [])); id_semi =
    (sem_prod_ok (w2b_ty sz sz) (Obj.magic x86_SBB sz)); id_args_kinds =
    (check_add sz); id_nargs = (S (S O)); id_str_jas = (pp_sz "SBB" sz);
    id_safe = []; id_pp_asm = (pp_iname "sbb" sz) }), ("SBB",
    (prim_8_64 (fun x -> SBB x))))

(** val check_adcx : wsize -> arg_kind list list list **)

let check_adcx _ =
  r_rm :: []

(** val x86_ADCX :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> bool -> sem_tuple **)

let x86_ADCX sz v1 v2 c0 =
  let (c1, w) = waddcarry sz v1 v2 c0 in Obj.magic ((Some c1), w)

(** val coq_Ox86_ADCX_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_ADCX_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2b_ty sz sz); id_in =
    (cat ((coq_Ea x86_decl O) :: ((coq_Eu x86_decl (S O)) :: []))
      ((coq_F x86_decl CF) :: [])); id_tout = (bw_ty sz); id_out =
    ((coq_F x86_decl CF) :: ((coq_Ea x86_decl O) :: [])); id_semi =
    (sem_prod_ok (w2b_ty sz sz) (Obj.magic x86_ADCX sz)); id_args_kinds =
    (check_adcx sz); id_nargs = (S (S O)); id_str_jas = (pp_sz "ADCX" sz);
    id_safe = []; id_pp_asm = (pp_iname "adcx" sz) }), ("ADCX",
    (prim_32_64 (fun x -> ADCX x))))

(** val coq_Ox86_ADOX_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_ADOX_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2b_ty sz sz); id_in =
    (cat ((coq_Ea x86_decl O) :: ((coq_Eu x86_decl (S O)) :: []))
      ((coq_F x86_decl OF) :: [])); id_tout = (bw_ty sz); id_out =
    ((coq_F x86_decl OF) :: ((coq_Ea x86_decl O) :: [])); id_semi =
    (sem_prod_ok (w2b_ty sz sz) (Obj.magic x86_ADCX sz)); id_args_kinds =
    (check_adcx sz); id_nargs = (S (S O)); id_str_jas = (pp_sz "ADOX" sz);
    id_safe = []; id_pp_asm = (pp_iname "adox" sz) }), ("ADOX",
    (prim_32_64 (fun x -> ADOX x))))

(** val check_mulx : arg_kind list list list **)

let check_mulx =
  (r :: (r :: ((rm true) :: []))) :: []

(** val x86_MULX_lo_hi :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_MULX_lo_hi sz v1 v2 =
  let (hi, lo) = wumul sz v1 v2 in Obj.magic (lo, hi)

(** val coq_Ox86_MULX_lo_hi_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_MULX_lo_hi_instr =
  let name = "MULX_lo_hi" in
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = (reg_msb_flag sz);
  id_tin = (w2_ty sz sz); id_in =
  ((coq_R x86_decl RDX) :: ((coq_Eu x86_decl (S (S O))) :: [])); id_tout =
  (w2_ty sz sz); id_out =
  ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl O) :: [])); id_semi =
  (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_MULX_lo_hi sz)); id_args_kinds =
  check_mulx; id_nargs = (S (S (S O))); id_str_jas = (pp_sz name sz);
  id_safe = []; id_pp_asm = (pp_iname "mulx" sz) }), (name,
  (prim_32_64 (fun x -> MULX_lo_hi x))))

(** val check_neg : wsize -> arg_kind list list list **)

let check_neg _ =
  ((rm false) :: []) :: []

(** val x86_NEG : wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_NEG sz w =
  let vs = Z.opp (wsigned sz w) in
  let v =
    GRing.opp
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Zmodule (word sz)) w
  in
  flags_w (map (Obj.magic __) b5_ty)
    (Obj.magic ((Some
      (negb
        (eq_op coq_BinNums_Z__canonical__eqtype_Equality
          (Obj.magic wsigned sz v) (Obj.magic vs)))), ((Some
      (negb
        (eq_op
          (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality
            (word sz)) w
          (GRing.zero
            (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
              (word sz)))))), ((Some (coq_SF_of_word sz v)), ((Some
      (coq_PF_of_word sz v)), (Some (coq_ZF_of_word sz v))))))) sz v

(** val coq_Ox86_NEG_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_NEG_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w_ty sz); id_in = ((coq_Eu x86_decl O) :: []); id_tout =
    (b5w_ty sz); id_out = (cat implicit_flags ((coq_Eu x86_decl O) :: []));
    id_semi = (sem_prod_ok (w_ty sz) (Obj.magic x86_NEG sz)); id_args_kinds =
    (check_neg sz); id_nargs = (S O); id_str_jas = (pp_sz "NEG" sz);
    id_safe = []; id_pp_asm = (pp_iname "neg" sz) }), ("NEG",
    (prim_8_64 (fun x -> NEG x))))

(** val x86_INC : wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_INC sz w =
  rflags_of_aluop_nocf_w sz
    (GRing.add
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule (word sz))
      w
      (GRing.one
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
          (word sz)))) (Z.add (wsigned sz w) (Zpos Coq_xH))

(** val coq_Ox86_INC_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_INC_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w_ty sz); id_in = ((coq_Eu x86_decl O) :: []); id_tout =
    (b4w_ty sz); id_out =
    (cat implicit_flags_noCF ((coq_Eu x86_decl O) :: [])); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_INC sz)); id_args_kinds =
    (check_neg sz); id_nargs = (S O); id_str_jas = (pp_sz "INC" sz);
    id_safe = []; id_pp_asm = (pp_iname "inc" sz) }), ("INC",
    (prim_8_64 (fun x -> INC x))))

(** val x86_DEC : wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_DEC sz w =
  rflags_of_aluop_nocf_w sz
    (GRing.add
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule (word sz))
      w
      (GRing.opp
        (GRing.Ring.Exports.join_GRing_Ring_between_GRing_SemiRing_and_GRing_Zmodule
          (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Ring (word sz)))
        (GRing.one
          (GRing.Ring.Exports.coq_GRing_Ring__to__GRing_SemiRing
            (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Ring
              (word sz)))))) (Z.sub (wsigned sz w) (Zpos Coq_xH))

(** val coq_Ox86_DEC_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_DEC_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w_ty sz); id_in = ((coq_Eu x86_decl O) :: []); id_tout =
    (b4w_ty sz); id_out =
    (cat implicit_flags_noCF ((coq_Eu x86_decl O) :: [])); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_DEC sz)); id_args_kinds =
    (check_neg sz); id_nargs = (S O); id_str_jas = (pp_sz "DEC" sz);
    id_safe = []; id_pp_asm = (pp_iname "dec" sz) }), ("DEC",
    (prim_8_64 (fun x -> DEC x))))

(** val x86_LZCNT : wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_LZCNT sz w =
  let v = leading_zero sz w in
  flags_w (map (Obj.magic __) b5_ty)
    (Obj.magic (None, ((Some (coq_ZF_of_word sz w)), (None, (None, (Some
      (coq_ZF_of_word sz v))))))) sz v

(** val coq_Ox86_LZCNT_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_LZCNT_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w_ty sz); id_in = ((coq_Eu x86_decl (S O)) :: []); id_tout =
    (b5w_ty sz); id_out = (cat implicit_flags ((coq_Eu x86_decl O) :: []));
    id_semi = (sem_prod_ok (w_ty sz) (Obj.magic x86_LZCNT sz));
    id_args_kinds = (r_rm :: []); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "LZCNT" sz); id_safe = []; id_pp_asm = (pp_iname "lzcnt" sz) }),
    ("LZCNT", (prim_16_64 (fun x -> LZCNT x))))

(** val x86_TZCNT : wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_TZCNT sz w =
  let v = trailing_zero sz w in
  flags_w (map (Obj.magic __) b5_ty)
    (Obj.magic (None, ((Some (coq_ZF_of_word sz w)), (None, (None, (Some
      (coq_ZF_of_word sz v))))))) sz v

(** val coq_Ox86_TZCNT_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_TZCNT_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w_ty sz); id_in = ((coq_Eu x86_decl (S O)) :: []); id_tout =
    (b5w_ty sz); id_out = (cat implicit_flags ((coq_Eu x86_decl O) :: []));
    id_semi = (sem_prod_ok (w_ty sz) (Obj.magic x86_TZCNT sz));
    id_args_kinds = (r_rm :: []); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "TZCNT" sz); id_safe = []; id_pp_asm = (pp_iname "tzcnt" sz) }),
    ("TZCNT", (prim_16_64 (fun x -> TZCNT x))))

(** val check_setcc : arg_kind list list list **)

let check_setcc =
  (c :: ((rm false) :: [])) :: []

(** val x86_SETcc : bool -> sem_tuple **)

let x86_SETcc b =
  wrepr U8 (Z.b2z b)

(** val coq_Ox86_SETcc_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_SETcc_instr =
  ({ id_valid = true; id_msb_flag = (reg_msb_flag U8); id_tin = b_ty; id_in =
    ((coq_Eu x86_decl O) :: []); id_tout = w8_ty; id_out =
    ((coq_Eu x86_decl (S O)) :: []); id_semi =
    (sem_prod_ok b_ty (Obj.magic x86_SETcc)); id_args_kinds = check_setcc;
    id_nargs = (S (S O)); id_str_jas = (pp_s "SETcc"); id_safe = [];
    id_pp_asm = (pp_ct "set" U8) }, ("SETcc", (primM SETcc)))

(** val check_bt : wsize -> arg_kind list list list **)

let check_bt _ =
  (r :: ((ri U8) :: [])) :: []

(** val x86_BT :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_BT sz x y =
  Obj.magic (Some (Word0.wbit sz x y))

(** val coq_Ox86_BT_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_BT_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])); id_tout = b_ty;
    id_out = ((coq_F x86_decl CF) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_BT sz)); id_args_kinds =
    (check_bt sz); id_nargs = (S (S O)); id_str_jas = (pp_sz "BT" sz);
    id_safe = []; id_pp_asm = (pp_iname "bt" sz) }), ("BT",
    (prim_16_64 (fun x -> BT x))))

(** val x86_CLC : sem_tuple **)

let x86_CLC =
  Obj.magic (Some false)

(** val coq_Ox86_CLC_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_CLC_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = []; id_in = [];
    id_tout = b_ty; id_out = ((coq_F x86_decl CF) :: []); id_semi =
    (sem_prod_ok [] x86_CLC); id_args_kinds = ([] :: []); id_nargs = O;
    id_str_jas = (pp_s "CLC"); id_safe = []; id_pp_asm =
    (pp_name "clc" U8) }, ("CLC", (primM CLC)))

(** val x86_STC : sem_tuple **)

let x86_STC =
  Obj.magic (Some true)

(** val coq_Ox86_STC_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_STC_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = []; id_in = [];
    id_tout = b_ty; id_out = ((coq_F x86_decl CF) :: []); id_semi =
    (sem_prod_ok [] x86_STC); id_args_kinds = ([] :: []); id_nargs = O;
    id_str_jas = (pp_s "STC"); id_safe = []; id_pp_asm =
    (pp_name "stc" U8) }, ("STC", (primM STC)))

(** val check_lea : wsize -> arg_kind list list list **)

let check_lea _ =
  (r :: ((m true) :: [])) :: []

(** val x86_LEA : wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_LEA _ addr =
  addr

(** val coq_Ox86_LEA_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_LEA_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w_ty sz); id_in = ((coq_Ec x86_decl (S O)) :: []); id_tout =
    (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_LEA sz)); id_args_kinds =
    (check_lea sz); id_nargs = (S (S O)); id_str_jas = (pp_sz "LEA" sz);
    id_safe = []; id_pp_asm = (pp_iname "lea" sz) }), ("LEA",
    (prim_16_64 (fun x -> LEA x))))

(** val check_test : wsize -> arg_kind list list list **)

let check_test sz =
  ((rm false) :: ((ri (max_32 sz)) :: [])) :: []

(** val x86_TEST :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_TEST sz x y =
  rflags_of_bwop sz (Word0.wand sz x y)

(** val coq_Ox86_TEST_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_TEST_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])); id_tout =
    b5_ty; id_out = implicit_flags; id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_TEST sz)); id_args_kinds =
    (check_test sz); id_nargs = (S (S O)); id_str_jas = (pp_sz "TEST" sz);
    id_safe = []; id_pp_asm = (pp_iname "test" sz) }), ("TEST",
    (prim_8_64 (fun x -> TEST x))))

(** val check_cmp : wsize -> arg_kind list list list **)

let check_cmp sz =
  ((rm false) :: ((ri (max_32 sz)) :: [])) :: (r_rm :: [])

(** val x86_CMP :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_CMP sz x y =
  rflags_of_aluop sz
    (GRing.add
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule (word sz))
      x
      (GRing.opp
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Zmodule (word sz))
        y)) (Z.sub (wunsigned sz x) (wunsigned sz y))
    (Z.sub (wsigned sz x) (wsigned sz y))

(** val coq_Ox86_CMP_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_CMP_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])); id_tout =
    b5_ty; id_out = implicit_flags; id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_CMP sz)); id_args_kinds =
    (check_cmp sz); id_nargs = (S (S O)); id_str_jas = (pp_sz "CMP" sz);
    id_safe = []; id_pp_asm = (pp_iname "cmp" sz) }), ("CMP",
    (prim_8_64 (fun x -> CMP x))))

(** val x86_AND :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_AND sz v1 v2 =
  rflags_of_bwop_w sz (Word0.wand sz v1 v2)

(** val coq_Ox86_AND_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_AND_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])); id_tout =
    (b5w_ty sz); id_out = (cat implicit_flags ((coq_Eu x86_decl O) :: []));
    id_semi = (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_AND sz));
    id_args_kinds = (check_cmp sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "AND" sz); id_safe = []; id_pp_asm = (pp_iname "and" sz) }),
    ("AND", (prim_8_64 (fun x -> AND x))))

(** val x86_OR :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_OR sz v1 v2 =
  rflags_of_bwop_w sz (Word0.wor sz v1 v2)

(** val coq_Ox86_OR_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_OR_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])); id_tout =
    (b5w_ty sz); id_out = (cat implicit_flags ((coq_Eu x86_decl O) :: []));
    id_semi = (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_OR sz));
    id_args_kinds = (check_cmp sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "OR" sz); id_safe = []; id_pp_asm = (pp_iname "or" sz) }), ("OR",
    (prim_8_64 (fun x -> OR x))))

(** val x86_XOR :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_XOR sz v1 v2 =
  rflags_of_bwop_w sz (Word0.wxor sz v1 v2)

(** val coq_Ox86_XOR_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_XOR_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])); id_tout =
    (b5w_ty sz); id_out = (cat implicit_flags ((coq_Eu x86_decl O) :: []));
    id_semi = (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_XOR sz));
    id_args_kinds = (check_cmp sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "XOR" sz); id_safe = []; id_pp_asm = (pp_iname "xor" sz) }),
    ("XOR", (prim_8_64 (fun x -> XOR x))))

(** val check_andn : wsize -> arg_kind list list list **)

let check_andn _ =
  (r :: (r :: ((rm true) :: []))) :: []

(** val x86_ANDN :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_ANDN sz v1 v2 =
  let w = wandn sz v1 v2 in
  flags_w (map (Obj.magic __) b5_ty) (rflags_of_andn sz w) sz w

(** val coq_Ox86_ANDN_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_ANDN_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (b5w_ty sz); id_out =
    (cat implicit_flags ((coq_Eu x86_decl O) :: [])); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_ANDN sz)); id_args_kinds =
    (check_andn sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "ANDN" sz); id_safe = []; id_pp_asm = (pp_iname "andn" sz) }),
    ("ANDN", (prim_32_64 (fun x -> ANDN x))))

(** val x86_NOT : wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_NOT =
  wnot

(** val coq_Ox86_NOT_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_NOT_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w_ty sz); id_in = ((coq_Eu x86_decl O) :: []); id_tout =
    (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_NOT sz)); id_args_kinds =
    (check_neg sz); id_nargs = (S O); id_str_jas = (pp_sz "NOT" sz);
    id_safe = []; id_pp_asm = (pp_iname "not" sz) }), ("NOT",
    (prim_8_64 (fun x -> NOT x))))

(** val check_ror : wsize -> arg_kind list list list **)

let check_ror _ =
  ((rm false) :: ((ri U8) :: [])) :: []

(** val x86_shift_mask : wsize -> GRing.ComRing.sort **)

let x86_shift_mask = function
| U64 -> wrepr U8 (Zpos (Coq_xI (Coq_xI (Coq_xI (Coq_xI (Coq_xI Coq_xH))))))
| U128 ->
  wrepr U8 (Zpos (Coq_xI (Coq_xI (Coq_xI (Coq_xI (Coq_xI (Coq_xI Coq_xH)))))))
| U256 ->
  wrepr U8 (Zpos (Coq_xI (Coq_xI (Coq_xI (Coq_xI (Coq_xI (Coq_xI (Coq_xI
    Coq_xH))))))))
| _ -> wrepr U8 (Zpos (Coq_xI (Coq_xI (Coq_xI (Coq_xI Coq_xH)))))

(** val x86_ROR :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_ROR sz v i0 =
  let i1 = Word0.wand U8 i0 (x86_shift_mask sz) in
  if eq_op
       (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality
         (word U8)) i1
       (GRing.zero
         (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
           (word U8)))
  then Obj.magic (None, (None, v))
  else let r0 = wror sz v (wunsigned U8 i1) in
       let cF = msb sz r0 in
       let oF =
         if eq_op
              (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality
                (word U8)) i1
              (GRing.one
                (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
                  (word U8)))
         then Some
                (negb
                  (eq_op coq_Datatypes_bool__canonical__eqtype_Equality
                    (Obj.magic cF) (Obj.magic msb sz v)))
         else None
       in
       Obj.magic (oF, ((Some cF), r0))

(** val coq_Ox86_ROR_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_ROR_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (ww8_ty sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Ef x86_decl (S O) RCX) :: [])); id_tout =
    (b2w_ty sz); id_out =
    ((coq_F x86_decl OF) :: ((coq_F x86_decl CF) :: ((coq_Eu x86_decl O) :: [])));
    id_semi = (sem_prod_ok (ww8_ty sz) (Obj.magic x86_ROR sz));
    id_args_kinds = (check_ror sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "ROR" sz); id_safe = []; id_pp_asm = (pp_iname_w_8 "ror" sz) }),
    ("ROR", (prim_8_64 (fun x -> ROR x))))

(** val x86_ROL :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_ROL sz v i0 =
  let i1 = Word0.wand U8 i0 (x86_shift_mask sz) in
  if eq_op
       (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality
         (word U8)) i1
       (GRing.zero
         (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
           (word U8)))
  then Obj.magic (None, (None, v))
  else let r0 = wrol sz v (wunsigned U8 i1) in
       let cF = lsb sz r0 in
       let oF =
         if eq_op
              (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality
                (word U8)) i1
              (GRing.one
                (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
                  (word U8)))
         then Some
                (negb
                  (eq_op coq_Datatypes_bool__canonical__eqtype_Equality
                    (Obj.magic msb sz r0) (Obj.magic cF)))
         else None
       in
       Obj.magic (oF, ((Some cF), r0))

(** val coq_Ox86_ROL_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_ROL_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (ww8_ty sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Ef x86_decl (S O) RCX) :: [])); id_tout =
    (b2w_ty sz); id_out =
    ((coq_F x86_decl OF) :: ((coq_F x86_decl CF) :: ((coq_Eu x86_decl O) :: [])));
    id_semi = (sem_prod_ok (ww8_ty sz) (Obj.magic x86_ROL sz));
    id_args_kinds = (check_ror sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "ROL" sz); id_safe = []; id_pp_asm = (pp_iname_w_8 "rol" sz) }),
    ("ROL", (prim_8_64 (fun x -> ROL x))))

(** val x86_rotate_with_carry :
    wsize -> (word -> nat -> word) -> (GRing.ComRing.sort -> bool -> bool) ->
    GRing.ComRing.sort -> GRing.ComRing.sort -> bool -> sem_tuple **)

let x86_rotate_with_carry sz rot ovf v i0 cf =
  let i1 = Word0.wand U8 i0 (x86_shift_mask sz) in
  let i2 =
    match sz with
    | U8 ->
      Z.modulo (wunsigned U8 i1) (Zpos (Coq_xI (Coq_xO (Coq_xO Coq_xH))))
    | U16 ->
      Z.modulo (wunsigned U8 i1) (Zpos (Coq_xI (Coq_xO (Coq_xO (Coq_xO
        Coq_xH)))))
    | _ -> wunsigned U8 i1
  in
  let r0 =
    t2w (S (S (wsize_size_minus_1 sz)))
      (tuple (S (S (wsize_size_minus_1 sz)))
        (cons_tuple (S (wsize_size_minus_1 sz)) cf
          (w2t (S (wsize_size_minus_1 sz)) (Obj.magic v))) (fun _ ->
        cf :: (w2t (S (wsize_size_minus_1 sz)) (Obj.magic v))))
  in
  let r1 = rot r0 (Z.to_nat i2) in
  let r2 = w2t (S (nat_of_wsize sz)) r1 in
  let cF = head false r2 in
  let r3 =
    t2w (pred (S (nat_of_wsize sz)))
      (tuple (pred (S (nat_of_wsize sz)))
        (behead_tuple (S (nat_of_wsize sz)) r2) (fun _ -> behead r2))
  in
  let oF =
    if eq_op coq_BinNums_Z__canonical__eqtype_Equality (Obj.magic i2)
         (GRing.one coq_BinNums_Z__canonical__GRing_SemiRing)
    then Some (Obj.magic ovf r3 cF)
    else None
  in
  Obj.magic (oF, ((Some cF), r3))

(** val x86_RCR :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> bool -> sem_tuple **)

let x86_RCR sz v i0 cf =
  x86_rotate_with_carry sz (rotr (S (nat_of_wsize sz))) (fun _ _ ->
    negb
      (eq_op coq_Datatypes_bool__canonical__eqtype_Equality
        (Obj.magic msb sz v) (Obj.magic cf))) v i0 cf

(** val coq_Ox86_RCR_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_RCR_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (ww8b_ty sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Ef x86_decl (S O) RCX) :: ((coq_F x86_decl
                                                               CF) :: [])));
    id_tout = (b2w_ty sz); id_out =
    ((coq_F x86_decl OF) :: ((coq_F x86_decl CF) :: ((coq_Eu x86_decl O) :: [])));
    id_semi = (sem_prod_ok (ww8b_ty sz) (Obj.magic x86_RCR sz));
    id_args_kinds = (check_ror sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "RCR" sz); id_safe = []; id_pp_asm = (pp_iname_w_8 "rcr" sz) }),
    ("RCR", (prim_8_64 (fun x -> RCR x))))

(** val x86_RCL :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> bool -> sem_tuple **)

let x86_RCL sz v i0 cf =
  x86_rotate_with_carry sz (rotl (S (nat_of_wsize sz))) (fun r0 c0 ->
    negb
      (eq_op coq_Datatypes_bool__canonical__eqtype_Equality
        (Obj.magic msb sz r0) (Obj.magic c0))) v i0 cf

(** val coq_Ox86_RCL_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_RCL_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (ww8b_ty sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Ef x86_decl (S O) RCX) :: ((coq_F x86_decl
                                                               CF) :: [])));
    id_tout = (b2w_ty sz); id_out =
    ((coq_F x86_decl OF) :: ((coq_F x86_decl CF) :: ((coq_Eu x86_decl O) :: [])));
    id_semi = (sem_prod_ok (ww8b_ty sz) (Obj.magic x86_RCL sz));
    id_args_kinds = (check_ror sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "RCL" sz); id_safe = []; id_pp_asm = (pp_iname_w_8 "rcl" sz) }),
    ("RCL", (prim_8_64 (fun x -> RCL x))))

(** val rflags_OF :
    wsize -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> bool ->
    bool -> sem_tuple **)

let rflags_OF s sz i0 r0 rc oF =
  let oF0 =
    if eq_op
         (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality
           (word s)) i0
         (GRing.one
           (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
             (word s)))
    then Some oF
    else None
  in
  let cF = Some rc in
  let sF = Some (coq_SF_of_word sz r0) in
  let pF = Some (coq_PF_of_word sz r0) in
  let zF = Some (coq_ZF_of_word sz r0) in
  Obj.magic (oF0, (cF, (sF, (pF, (zF, r0)))))

(** val x86_SHL :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_SHL sz v i0 =
  let i1 = Word0.wand U8 i0 (x86_shift_mask sz) in
  if eq_op
       (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality
         (word U8)) i1
       (GRing.zero
         (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
           (word U8)))
  then rflags_None_w sz v
  else let rc = msb sz (wshl sz v (Z.sub (wunsigned U8 i1) (Zpos Coq_xH))) in
       let r0 = wshl sz v (wunsigned U8 i1) in
       rflags_OF U8 sz i1 r0 rc (addb (msb sz r0) rc)

(** val coq_Ox86_SHL_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_SHL_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (ww8_ty sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Ef x86_decl (S O) RCX) :: [])); id_tout =
    (b5w_ty sz); id_out = (cat implicit_flags ((coq_Eu x86_decl O) :: []));
    id_semi = (sem_prod_ok (ww8_ty sz) (Obj.magic x86_SHL sz));
    id_args_kinds = (check_ror sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "SHL" sz); id_safe = []; id_pp_asm = (pp_iname_w_8 "shl" sz) }),
    ("SHL", (prim_8_64 (fun x -> SHL x))))

(** val x86_SHR :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_SHR sz v i0 =
  let i1 = Word0.wand U8 i0 (x86_shift_mask sz) in
  if eq_op
       (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality
         (word U8)) i1
       (GRing.zero
         (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
           (word U8)))
  then rflags_None_w sz v
  else let rc = lsb sz (wshr sz v (Z.sub (wunsigned U8 i1) (Zpos Coq_xH))) in
       let r0 = wshr sz v (wunsigned U8 i1) in
       rflags_OF U8 sz i1 r0 rc (msb sz v)

(** val coq_Ox86_SHR_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_SHR_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (ww8_ty sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Ef x86_decl (S O) RCX) :: [])); id_tout =
    (b5w_ty sz); id_out = (cat implicit_flags ((coq_Eu x86_decl O) :: []));
    id_semi = (sem_prod_ok (ww8_ty sz) (Obj.magic x86_SHR sz));
    id_args_kinds = (check_ror sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "SHR" sz); id_safe = []; id_pp_asm = (pp_iname_w_8 "shr" sz) }),
    ("SHR", (prim_8_64 (fun x -> SHR x))))

(** val coq_Ox86_SAL_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_SAL_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (ww8_ty sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Ef x86_decl (S O) RCX) :: [])); id_tout =
    (b5w_ty sz); id_out = (cat implicit_flags ((coq_Eu x86_decl O) :: []));
    id_semi = (sem_prod_ok (ww8_ty sz) (Obj.magic x86_SHL sz));
    id_args_kinds = (check_ror sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "SAL" sz); id_safe = []; id_pp_asm = (pp_iname_w_8 "sal" sz) }),
    ("SAL", (prim_8_64 (fun x -> SAL x))))

(** val x86_SAR :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_SAR sz v i0 =
  let i1 = Word0.wand U8 i0 (x86_shift_mask sz) in
  if eq_op
       (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality
         (word U8)) i1
       (GRing.zero
         (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
           (word U8)))
  then rflags_None_w sz v
  else let rc = lsb sz (wsar sz v (Z.sub (wunsigned U8 i1) (Zpos Coq_xH))) in
       let r0 = wsar sz v (wunsigned U8 i1) in rflags_OF U8 sz i1 r0 rc false

(** val coq_Ox86_SAR_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_SAR_instr =
  ((fun sz -> { id_valid = (size_8_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (ww8_ty sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Ef x86_decl (S O) RCX) :: [])); id_tout =
    (b5w_ty sz); id_out = (cat implicit_flags ((coq_Eu x86_decl O) :: []));
    id_semi = (sem_prod_ok (ww8_ty sz) (Obj.magic x86_SAR sz));
    id_args_kinds = (check_ror sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "SAR" sz); id_safe = []; id_pp_asm = (pp_iname_w_8 "sar" sz) }),
    ("SAR", (prim_8_64 (fun x -> SAR x))))

(** val check_shld : wsize -> arg_kind list list list **)

let check_shld _ =
  ((rm false) :: (r :: ((ri U8) :: []))) :: []

(** val safe_shxd : wsize -> safe_cond list **)

let safe_shxd = function
| U16 ->
  (InRangeMod32 (U8, Z0, (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO Coq_xH))))),
    (S (S O)))) :: []
| _ -> []

(** val x86_SHLD :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort
    -> sem_tuple exec **)

let x86_SHLD sz v1 v2 i0 =
  let i1 = Word0.wand U8 i0 (x86_shift_mask sz) in
  if eq_op
       (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality
         (word U8)) i1
       (GRing.zero
         (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
           (word U8)))
  then Ok (rflags_None_w sz v1)
  else let j = Z.sub (wsize_bits sz) (wunsigned U8 i1) in
       if Z.ltb j Z0
       then Error ErrArith
       else let rc =
              msb sz (wshl sz v1 (Z.sub (wunsigned U8 i1) (Zpos Coq_xH)))
            in
            let r1 = wshl sz v1 (wunsigned U8 i1) in
            let r2 = wshr sz v2 j in
            let r0 = Word0.wor sz r1 r2 in
            Ok (rflags_OF U8 sz i1 r0 rc (addb (msb sz r0) rc))

(** val coq_Ox86_SHLD_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_SHLD_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2w8_ty sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Ea x86_decl (S O)) :: ((coq_Ef x86_decl (S
                                                           (S O)) RCX) :: [])));
    id_tout = (b5w_ty sz); id_out =
    (cat implicit_flags ((coq_Eu x86_decl O) :: [])); id_semi =
    (Obj.magic x86_SHLD sz); id_args_kinds = (check_shld sz); id_nargs = (S
    (S (S O))); id_str_jas = (pp_sz "SHLD" sz); id_safe = (safe_shxd sz);
    id_pp_asm = (pp_iname_ww_8 "shld" sz) }), ("SHLD",
    (prim_16_64 (fun x -> SHLD x))))

(** val x86_SHRD :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort
    -> sem_tuple exec **)

let x86_SHRD sz v1 v2 i0 =
  let i1 = Word0.wand U8 i0 (x86_shift_mask sz) in
  if eq_op
       (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality
         (word U8)) i1
       (GRing.zero
         (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
           (word U8)))
  then Ok (rflags_None_w sz v1)
  else let j = Z.sub (wsize_bits sz) (wunsigned U8 i1) in
       if Z.ltb j Z0
       then Error ErrArith
       else let rc =
              lsb sz (wshr sz v1 (Z.sub (wunsigned U8 i1) (Zpos Coq_xH)))
            in
            let r1 = wshr sz v1 (wunsigned U8 i1) in
            let r2 = wshl sz v2 j in
            let r0 = Word0.wor sz r1 r2 in
            Ok (rflags_OF U8 sz i1 r0 rc (addb (msb sz r0) (msb sz v1)))

(** val coq_Ox86_SHRD_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_SHRD_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2w8_ty sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Ea x86_decl (S O)) :: ((coq_Ef x86_decl (S
                                                           (S O)) RCX) :: [])));
    id_tout = (b5w_ty sz); id_out =
    (cat implicit_flags ((coq_Eu x86_decl O) :: [])); id_semi =
    (Obj.magic x86_SHRD sz); id_args_kinds = (check_shld sz); id_nargs = (S
    (S (S O))); id_str_jas = (pp_sz "SHRD" sz); id_safe = (safe_shxd sz);
    id_pp_asm = (pp_iname_ww_8 "shrd" sz) }), ("SHRD",
    (prim_16_64 (fun x -> SHRD x))))

(** val check_rorx : wsize -> arg_kind list list list **)

let check_rorx _ =
  (r :: ((rm true) :: ((i U8) :: []))) :: []

(** val x86_RORX :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let x86_RORX sz v i0 =
  let i1 = Word0.wand U8 i0 (x86_shift_mask sz) in wror sz v (wunsigned U8 i1)

(** val coq_Ox86_RORX_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_RORX_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (ww8_ty sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Ea x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (ww8_ty sz) (Obj.magic x86_RORX sz)); id_args_kinds =
    (check_rorx sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "RORX" sz); id_safe = []; id_pp_asm = (pp_name "rorx" sz) }),
    ("RORX", (prim_32_64 (fun x -> RORX x))))

(** val x86_bmi_shift :
    wsize -> (GRing.ComRing.sort -> coq_Z -> GRing.ComRing.sort) ->
    GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let x86_bmi_shift sz op v i0 =
  let i1 = Z.coq_land (wunsigned sz i0) (wunsigned U8 (x86_shift_mask sz)) in
  op v i1

(** val check_sarx : wsize -> arg_kind list list list **)

let check_sarx _ =
  (r :: ((rm true) :: (r :: []))) :: []

(** val x86_SARX :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let x86_SARX sz =
  x86_bmi_shift sz (wsar sz)

(** val coq_Ox86_SARX_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_SARX_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_SARX sz)); id_args_kinds =
    (check_sarx sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "SARX" sz); id_safe = []; id_pp_asm = (pp_name "sarx" sz) }),
    ("SARX", (prim_32_64 (fun x -> SARX x))))

(** val x86_SHRX :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let x86_SHRX sz =
  x86_bmi_shift sz (wshr sz)

(** val coq_Ox86_SHRX_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_SHRX_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_SHRX sz)); id_args_kinds =
    (check_sarx sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "SHRX" sz); id_safe = []; id_pp_asm = (pp_name "shrx" sz) }),
    ("SHRX", (prim_32_64 (fun x -> SHRX x))))

(** val x86_SHLX :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let x86_SHLX sz =
  x86_bmi_shift sz (wshl sz)

(** val coq_Ox86_SHLX_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_SHLX_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_SHLX sz)); id_args_kinds =
    (check_sarx sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "SHLX" sz); id_safe = []; id_pp_asm = (pp_name "shlx" sz) }),
    ("SHLX", (prim_32_64 (fun x -> SHLX x))))

(** val x86_BSWAP : wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_BSWAP =
  wbswap

(** val coq_Ox86_BSWAP_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_BSWAP_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w_ty sz); id_in = ((coq_Eu x86_decl O) :: []); id_tout =
    (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_BSWAP sz)); id_args_kinds =
    ((r :: []) :: []); id_nargs = (S O); id_str_jas = (pp_sz "BSWAP" sz);
    id_safe = []; id_pp_asm = (pp_iname "bswap" sz) }), ("BSWAP",
    (prim_32_64 (fun x -> BSWAP x))))

(** val x86_POPCNT : wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_POPCNT sz v =
  let r0 = popcnt sz v in
  Obj.magic ((Some false), ((Some false), ((Some false), ((Some false),
    ((Some (coq_ZF_of_word sz v)), r0)))))

(** val coq_Ox86_POPCNT_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_POPCNT_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w_ty sz); id_in = ((coq_Eu x86_decl (S O)) :: []); id_tout =
    (b5w_ty sz); id_out = (cat implicit_flags ((coq_Eu x86_decl O) :: []));
    id_semi = (sem_prod_ok (w_ty sz) (Obj.magic x86_POPCNT sz));
    id_args_kinds = (r_rm :: []); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "POPCNT" sz); id_safe = []; id_pp_asm = (pp_name "popcnt" sz) }),
    ("POPCNT", (prim_16_64 (fun x -> POPCNT x))))

(** val x86_BTX :
    (wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_ot) -> wsize ->
    GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_BTX op sz x y =
  let bit = Z.modulo (wunsigned sz y) (wsize_bits sz) in
  Obj.magic ((Some (wbit_n sz x (Z.to_nat bit))),
    (op sz (wrepr sz (Z.pow (Zpos (Coq_xO Coq_xH)) bit)) x))

(** val coq_Ox86_BTR_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_BTR_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = MSB_MERGE; id_tin =
    (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])); id_tout =
    (bw_ty sz); id_out =
    ((coq_F x86_decl CF) :: ((coq_Ea x86_decl O) :: [])); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_BTX wandn sz)); id_args_kinds =
    (check_bt sz); id_nargs = (S (S O)); id_str_jas = (pp_sz "BTR" sz);
    id_safe = []; id_pp_asm = (pp_iname "btr" sz) }), ("BTR",
    (prim_16_64 (fun x -> BTR x))))

(** val coq_Ox86_BTS_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_BTS_instr =
  ((fun sz -> { id_valid = (size_16_64 sz); id_msb_flag = MSB_MERGE; id_tin =
    (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])); id_tout =
    (bw_ty sz); id_out =
    ((coq_F x86_decl CF) :: ((coq_Ea x86_decl O) :: [])); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_BTX Word0.wor sz));
    id_args_kinds = (check_bt sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "BTS" sz); id_safe = []; id_pp_asm = (pp_iname "bts" sz) }),
    ("BTS", (prim_16_64 (fun x -> BTS x))))

(** val x86_PEXT :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_PEXT =
  pextr

(** val coq_Ox86_PEXT_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_PEXT_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_PEXT sz)); id_args_kinds =
    ((r :: (r :: ((rm true) :: []))) :: []); id_nargs = (S (S (S O)));
    id_str_jas = (pp_sz "PEXT" sz); id_safe = []; id_pp_asm =
    (pp_name "pext" sz) }), ("PEXT", (prim_32_64 (fun x -> PEXT x))))

(** val x86_PDEP :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_PDEP =
  pdep

(** val coq_Ox86_PDEP_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_PDEP_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_PDEP sz)); id_args_kinds =
    ((r :: (r :: ((rm true) :: []))) :: []); id_nargs = (S (S (S O)));
    id_str_jas = (pp_sz "PDEP" sz); id_safe = []; id_pp_asm =
    (pp_name "pdep" sz) }), ("PDEP", (prim_32_64 (fun x -> PDEP x))))

(** val check_movd : wsize -> arg_kind list list list **)

let check_movd _ =
  (xmm :: ((rm true) :: [])) :: []

(** val x86_MOVD : wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_MOVD sz v =
  zero_extend U128 sz v

(** val coq_Ox86_MOVD_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_MOVD_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = MSB_MERGE; id_tin =
    (w_ty sz); id_in = ((coq_Eu x86_decl (S O)) :: []); id_tout = w128_ty;
    id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_MOVD sz)); id_args_kinds =
    (check_movd sz); id_nargs = (S (S O)); id_str_jas = (pp_sz "MOVD" sz);
    id_safe = []; id_pp_asm = (pp_movd "mov" (Obj.magic sz)) }), ("MOVD",
    (prim_32_64 (fun x -> MOVD x))))

(** val coq_Ox86_MOVV_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_MOVV_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = (reg_msb_flag sz);
    id_tin = (w_ty sz); id_in = ((coq_Eu x86_decl (S O)) :: []); id_tout =
    (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_MOVX sz)); id_args_kinds =
    (((rm false) :: (xmm :: [])) :: []); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "MOVV" sz); id_safe = []; id_pp_asm =
    (pp_movd "mov" (Obj.magic sz)) }), ("MOVV",
    (prim_32_64 (fun x -> MOVV x))))

(** val coq_Ox86_VMOV_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VMOV_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = MSB_CLEAR; id_tin =
    (w_ty sz); id_in = ((coq_Eu x86_decl (S O)) :: []); id_tout = w128_ty;
    id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_MOVD sz)); id_args_kinds =
    (check_movd sz); id_nargs = (S (S O)); id_str_jas = (pp_sz "VMOV" sz);
    id_safe = []; id_pp_asm = (pp_movd "vmov" (Obj.magic sz)) }), ("VMOV",
    (prim_32_64 (fun x -> VMOV x))))

(** val check_vmovdq : wsize -> arg_kind list list list **)

let check_vmovdq _ =
  xmm_xmmm :: (xmmm_xmm :: [])

(** val x86_VMOVDQ : wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_VMOVDQ _ v =
  v

(** val coq_Ox86_VMOVDQA_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VMOVDQA_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w_ty sz); id_in =
    ((coq_Ea x86_decl (S O)) :: []); id_tout = (w_ty sz); id_out =
    ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_VMOVDQ sz)); id_args_kinds =
    (check_vmovdq sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "VMOVDQA" sz); id_safe = []; id_pp_asm =
    (pp_name "vmovdqa" sz) }), ("VMOVDQA",
    (prim_128_256 (fun x -> VMOVDQA x))))

(** val coq_Ox86_VMOVDQU_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VMOVDQU_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w_ty sz); id_in =
    ((coq_Eu x86_decl (S O)) :: []); id_tout = (w_ty sz); id_out =
    ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_VMOVDQ sz)); id_args_kinds =
    (check_vmovdq sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "VMOVDQU" sz); id_safe = []; id_pp_asm =
    (pp_name "vmovdqu" sz) }), ("VMOVDQU",
    (prim_128_256 (fun x -> VMOVDQU x))))

(** val pp_vpmovx :
    string -> velem -> wsize -> velem -> wsize -> (register, register_ext,
    xmm_register, rflag, condt) asm_arg list -> (register, register_ext,
    xmm_register, rflag, condt) pp_asm_op **)

let pp_vpmovx name ve sz ve' sz' args =
  { pp_aop_name = name; pp_aop_ext = (PP_viname2 (ve, ve')); pp_aop_args =
    (zip (sz' :: (sz :: [])) args) }

(** val vector_size : velem -> wsize -> coq_Z option **)

let vector_size ve ws =
  let (q, r0) = Z.div_eucl (wsize_size ws) (wsize_size (wsize_of_velem ve)) in
  if eq_op coq_BinNums_Z__canonical__eqtype_Equality (Obj.magic r0)
       (Obj.magic Z0)
  then Some q
  else None

(** val check_vector_length : velem -> wsize -> velem -> wsize -> bool **)

let check_vector_length ve sz ve' sz' =
  match vector_size ve sz with
  | Some i0 ->
    (match vector_size ve' sz' with
     | Some j ->
       eq_op coq_BinNums_Z__canonical__eqtype_Equality (Obj.magic i0)
         (Obj.magic j)
     | None -> false)
  | None -> false

(** val x86_VPMOVSX :
    velem -> wsize -> velem -> wsize -> GRing.ComRing.sort ->
    GRing.ComRing.sort **)

let x86_VPMOVSX ve sz ve' sz' w =
  lift1_vec' (wsize_of_velem ve) (wsize_of_velem ve')
    (sign_extend (wsize_of_velem ve') (wsize_of_velem ve)) sz sz' w

(** val coq_Ox86_VPMOVSX_instr :
    (velem -> wsize -> velem -> wsize -> (register, register_ext,
    xmm_register, rflag, condt) instr_desc_t) * (string * x86_op
    prim_constructor) **)

let coq_Ox86_VPMOVSX_instr =
  let name = "VPMOVSX" in
  ((fun ve sz ve' sz' -> { id_valid =
  ((&&) (size_128_256 sz') (check_vector_length ve sz ve' sz'));
  id_msb_flag = MSB_CLEAR; id_tin = ((Coq_sword sz) :: []); id_in =
  ((coq_Eu x86_decl (S O)) :: []); id_tout = ((Coq_sword sz') :: []);
  id_out = ((coq_Eu x86_decl O) :: []); id_semi =
  (sem_prod_ok ((Coq_sword sz) :: []) (Obj.magic x86_VPMOVSX ve sz ve' sz'));
  id_args_kinds = ((xmm :: ((xmmm true) :: [])) :: []); id_nargs = (S (S O));
  id_str_jas = (pp_ve_sz_ve_sz name ve sz ve' sz'); id_safe = []; id_pp_asm =
  (pp_vpmovx "vpmovsx" ve sz ve' sz') }), (name,
  (prim_vv (fun x x0 x1 x2 -> VPMOVSX (x, x0, x1, x2)))))

(** val x86_VPMOVZX :
    velem -> wsize -> velem -> wsize -> GRing.ComRing.sort ->
    GRing.ComRing.sort **)

let x86_VPMOVZX ve sz ve' sz' w =
  lift1_vec' (wsize_of_velem ve) (wsize_of_velem ve')
    (zero_extend (wsize_of_velem ve') (wsize_of_velem ve)) sz sz' w

(** val coq_Ox86_VPMOVZX_instr :
    (velem -> wsize -> velem -> wsize -> (register, register_ext,
    xmm_register, rflag, condt) instr_desc_t) * (string * x86_op
    prim_constructor) **)

let coq_Ox86_VPMOVZX_instr =
  let name = "VPMOVZX" in
  ((fun ve sz ve' sz' -> { id_valid =
  ((&&) (size_128_256 sz') (check_vector_length ve sz ve' sz'));
  id_msb_flag = MSB_CLEAR; id_tin = ((Coq_sword sz) :: []); id_in =
  ((coq_Eu x86_decl (S O)) :: []); id_tout = ((Coq_sword sz') :: []);
  id_out = ((coq_Eu x86_decl O) :: []); id_semi =
  (sem_prod_ok ((Coq_sword sz) :: []) (Obj.magic x86_VPMOVZX ve sz ve' sz'));
  id_args_kinds = ((xmm :: ((xmmm true) :: [])) :: []); id_nargs = (S (S O));
  id_str_jas = (pp_ve_sz_ve_sz name ve sz ve' sz'); id_safe = []; id_pp_asm =
  (pp_vpmovx "vpmovzx" ve sz ve' sz') }), (name,
  (prim_vv (fun x x0 x1 x2 -> VPMOVZX (x, x0, x1, x2)))))

(** val check_xmm_xmm_xmmm : wsize -> arg_kind list list list **)

let check_xmm_xmm_xmmm _ =
  xmm_xmm_xmmm :: []

(** val x86_u128_binop :
    wsize -> (GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort)
    -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_u128_binop _ op =
  op

(** val x86_VPAND :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPAND sz =
  x86_u128_binop sz (Word0.wand sz)

(** val coq_Ox86_VPAND_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPAND_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPAND sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "VPAND" sz); id_safe = []; id_pp_asm = (pp_name "vpand" sz) }),
    ("VPAND", (prim_128_256 (fun x -> VPAND x))))

(** val x86_VPANDN :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPANDN sz =
  x86_u128_binop sz (wandn sz)

(** val coq_Ox86_VPANDN_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPANDN_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPANDN sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "VPANDN" sz); id_safe = []; id_pp_asm = (pp_name "vpandn" sz) }),
    ("VPANDN", (prim_128_256 (fun x -> VPANDN x))))

(** val x86_VPOR :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPOR sz =
  x86_u128_binop sz (Word0.wor sz)

(** val coq_Ox86_VPOR_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPOR_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPOR sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "VPOR" sz); id_safe = []; id_pp_asm = (pp_name "vpor" sz) }),
    ("VPOR", (prim_128_256 (fun x -> VPOR x))))

(** val x86_VPXOR :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPXOR sz =
  x86_u128_binop sz (Word0.wxor sz)

(** val coq_Ox86_VPXOR_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPXOR_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPXOR sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "VPXOR" sz); id_safe = []; id_pp_asm = (pp_name "vpxor" sz) }),
    ("VPXOR", (prim_128_256 (fun x -> VPXOR x))))

(** val x86_VPADD :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPADD ve sz =
  x86_u128_binop sz
    (lift2_vec (wsize_of_velem ve)
      (GRing.add
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
          (word (wsize_of_velem ve)))) sz)

(** val coq_Ox86_VPADD_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPADD_instr =
  ((fun ve sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPADD ve sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_ve_sz "VPADD" ve sz); id_safe = []; id_pp_asm =
    (pp_viname "vpadd" ve sz) }), ("VPADD",
    (primV (fun x x0 -> VPADD (x, x0)))))

(** val x86_VPSUB :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPSUB ve sz =
  x86_u128_binop sz
    (lift2_vec (wsize_of_velem ve) (fun x y ->
      GRing.add
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
          (word (wsize_of_velem ve))) x
        (GRing.opp
          (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Zmodule
            (word (wsize_of_velem ve))) y)) sz)

(** val coq_Ox86_VPSUB_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPSUB_instr =
  ((fun ve sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPSUB ve sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_ve_sz "VPSUB" ve sz); id_safe = []; id_pp_asm =
    (pp_viname "vpsub" ve sz) }), ("VPSUB",
    (primV (fun x x0 -> VPSUB (x, x0)))))

(** val x86_VPAVG :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPAVG ve sz v1 v2 =
  let avg = fun x y ->
    wrepr (wsize_of_velem ve)
      (Z.div
        (Z.add
          (Z.add (wunsigned (wsize_of_velem ve) x)
            (wunsigned (wsize_of_velem ve) y)) (Zpos Coq_xH)) (Zpos (Coq_xO
        Coq_xH)))
  in
  x86_u128_binop sz (lift2_vec (wsize_of_velem ve) avg sz) v1 v2

(** val coq_Ox86_VPAVG_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPAVG_instr =
  ((fun ve sz -> { id_valid =
    ((&&) (size_8_16 (wsize_of_velem ve)) (size_128_256 sz)); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPAVG ve sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_ve_sz "VPAVG" ve sz); id_safe = []; id_pp_asm =
    (pp_viname "vpavg" ve sz) }), ("VPAVG",
    (primV_8_16 (fun x x0 -> VPAVG (x, x0)))))

(** val x86_VPMULL :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPMULL ve sz v1 v2 =
  x86_u128_binop sz
    (lift2_vec (wsize_of_velem ve)
      (GRing.mul
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_SemiRing
          (word (wsize_of_velem ve)))) sz) v1 v2

(** val coq_Ox86_VPMULL_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPMULL_instr =
  ((fun ve sz -> { id_valid =
    ((&&) (size_16_32 (wsize_of_velem ve)) (size_128_256 sz)); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPMULL ve sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_ve_sz "VPMULL" ve sz); id_safe = []; id_pp_asm =
    (pp_viname "vpmull" ve sz) }), ("VPMULL",
    (primV_16_32 (fun x x0 -> VPMULL (x, x0)))))

(** val x86_VPMUL :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPMUL sz =
  x86_u128_binop sz (wpmul sz)

(** val coq_Ox86_VPMUL_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPMUL_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag = MSB_CLEAR;
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPMUL sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "VPMUL" sz); id_safe = []; id_pp_asm = (pp_name "vpmuldq" sz) }),
    ("VPMUL", (prim_128_256 (fun x -> VPMUL x))))

(** val x86_VPMULU :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPMULU sz =
  x86_u128_binop sz (wpmulu sz)

(** val coq_Ox86_VPMULU_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPMULU_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag = MSB_CLEAR;
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPMULU sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "VPMULU" sz); id_safe = []; id_pp_asm =
    (pp_name "vpmuludq" sz) }), ("VPMULU",
    (prim_128_256 (fun x -> VPMULU x))))

(** val x86_VPMULH :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPMULH sz v1 v2 =
  x86_u128_binop sz (lift2_vec U16 (wmulhs U16) sz) v1 v2

(** val coq_Ox86_VPMULH_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPMULH_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPMULH sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_ve_sz "VPMULH" VE16 sz); id_safe = []; id_pp_asm =
    (pp_viname "vpmulh" VE16 sz) }), ("VPMULH",
    (primV_16 (fun _ x -> VPMULH x))))

(** val x86_VPMULHU :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPMULHU sz v1 v2 =
  x86_u128_binop sz (lift2_vec U16 (wmulhu U16) sz) v1 v2

(** val coq_Ox86_VPMULHU_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPMULHU_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPMULHU sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_ve_sz "VPMULHU" VE16 sz); id_safe = []; id_pp_asm =
    (pp_viname "vpmulhu" VE16 sz) }), ("VPMULHU",
    (primV_16 (fun _ x -> VPMULHU x))))

(** val x86_VPMULHRS :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPMULHRS sz v1 v2 =
  x86_u128_binop sz (lift2_vec U16 (wmulhrs U16) sz) v1 v2

(** val coq_Ox86_VPMULHRS_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPMULHRS_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPMULHRS sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_ve_sz "VPMULHRS" VE16 sz); id_safe = []; id_pp_asm =
    (pp_viname "vpmulhrs" VE16 sz) }), ("VPMULHRS",
    (primV_16 (fun _ x -> VPMULHRS x))))

(** val check_vpextr : wsize -> arg_kind list list list **)

let check_vpextr _ =
  ((rm false) :: (xmm :: ((i U8) :: []))) :: []

(** val pp_viname_t :
    string -> velem -> wsize list -> (register, register_ext, xmm_register,
    rflag, condt) asm_arg list -> (register, register_ext, xmm_register,
    rflag, condt) pp_asm_op **)

let pp_viname_t name ve ts args =
  { pp_aop_name = name; pp_aop_ext = (PP_viname (ve, false)); pp_aop_args =
    (zip ts args) }

(** val x86_nelem_mask : wsize -> wsize -> GRing.ComRing.sort **)

let x86_nelem_mask sze szc =
  wrepr U8
    (Z.sub (two_power_nat (subn (wsize_log2 szc) (wsize_log2 sze))) (Zpos
      Coq_xH))

(** val x86_VPEXTR :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPEXTR ve v i0 =
  let i1 = Word0.wand U8 i0 (x86_nelem_mask ve U128) in
  nth
    (GRing.zero
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule (word ve)))
    (Obj.magic split_vec U128 (nat_of_wsize ve) v)
    (Z.to_nat (wunsigned U8 i1))

(** val coq_Ox86_VPEXTR_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPEXTR_instr =
  ((fun sz ->
    let ve = match sz with
             | U8 -> VE8
             | U16 -> VE16
             | U32 -> VE32
             | _ -> VE64 in
    { id_valid = (size_8_64 sz); id_msb_flag = MSB_CLEAR; id_tin = w128w8_ty;
    id_in = ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok w128w8_ty (Obj.magic x86_VPEXTR sz)); id_args_kinds =
    (check_vpextr sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "VPEXTR" sz); id_safe = []; id_pp_asm =
    (pp_viname_t "vpextr" ve
      ((if eq_op wsize_wsize__canonical__eqtype_Equality (Obj.magic sz)
             (Obj.magic U32)
        then U32
        else U64) :: (U128 :: (U8 :: [])))) }), ("VPEXTR",
    (prim_8_64 (fun x -> VPEXTR x))))

(** val pp_vpinsr :
    velem -> (register, register_ext, xmm_register, rflag, condt) asm_arg
    list -> (register, register_ext, xmm_register, rflag, condt) pp_asm_op **)

let pp_vpinsr ve args =
  let rs = match ve with
           | VE64 -> U64
           | _ -> U32 in
  { pp_aop_name = "vpinsr"; pp_aop_ext = (PP_viname (ve, false));
  pp_aop_args = (zip (U128 :: (U128 :: (rs :: (U8 :: [])))) args) }

(** val check_vpinsr : wsize -> arg_kind list list list **)

let check_vpinsr _ =
  (xmm :: (xmm :: ((rm true) :: ((i U8) :: [])))) :: []

(** val x86_VPINSR :
    velem -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort
    -> sem_tuple **)

let x86_VPINSR ve v1 v2 i0 =
  let i1 = Word0.wand U8 i0 (x86_nelem_mask (wsize_of_velem ve) U128) in
  wpinsr (wsize_of_velem ve) v1 v2 i1

(** val coq_Ox86_VPINSR_instr :
    (velem -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPINSR_instr =
  ((fun ve -> { id_valid = true; id_msb_flag = MSB_CLEAR; id_tin =
    (w128ww8_ty (wsize_of_velem ve)); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: ((coq_Eu
                                                                   x86_decl
                                                                   (S (S (S
                                                                   O)))) :: [])));
    id_tout = w128_ty; id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w128ww8_ty (wsize_of_velem ve)) (Obj.magic x86_VPINSR ve));
    id_args_kinds = (check_vpinsr (wsize_of_velem ve)); id_nargs = (S (S (S
    (S O)))); id_str_jas = (pp_ve_sz "VPINSR" ve U128); id_safe = [];
    id_pp_asm = (pp_vpinsr ve) }), ("VPINSR",
    (primV_128 (fun ve _ -> VPINSR ve))))

(** val check_xmm_xmm_imm8 : wsize -> arg_kind list list list **)

let check_xmm_xmm_imm8 _ =
  (xmm :: (xmm :: ((i U8) :: []))) :: []

(** val x86_u128_shift :
    wsize -> wsize -> (GRing.ComRing.sort -> coq_Z -> GRing.ComRing.sort) ->
    GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_u128_shift sz' sz op v c0 =
  lift1_vec sz' (fun v0 -> op v0 (wunsigned U128 c0)) sz v

(** val check_xmm_xmm_xmmmi : wsize -> arg_kind list list list **)

let check_xmm_xmm_xmmmi _ =
  (xmm_xmm_xmmmi U8) :: []

(** val x86_VPSLL :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPSLL ve sz =
  x86_u128_shift (wsize_of_velem ve) sz (wshl (wsize_of_velem ve))

(** val coq_Ox86_VPSLL_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPSLL_instr =
  ((fun ve sz -> { id_valid =
    ((&&) (size_16_64 (wsize_of_velem ve)) (size_128_256 sz)); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz U128); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz U128) (Obj.magic x86_VPSLL ve sz));
    id_args_kinds = (check_xmm_xmm_xmmmi sz); id_nargs = (S (S (S O)));
    id_str_jas = (pp_ve_sz "VPSLL" ve sz); id_safe = []; id_pp_asm =
    (pp_viname_ww_128 "vpsll" ve sz) }), ("VPSLL",
    (primV_16_64 (fun x x0 -> VPSLL (x, x0)))))

(** val x86_VPSRL :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPSRL ve sz =
  x86_u128_shift (wsize_of_velem ve) sz (wshr (wsize_of_velem ve))

(** val coq_Ox86_VPSRL_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPSRL_instr =
  ((fun ve sz -> { id_valid =
    ((&&) (size_16_64 (wsize_of_velem ve)) (size_128_256 sz)); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz U128); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz U128) (Obj.magic x86_VPSRL ve sz));
    id_args_kinds = (check_xmm_xmm_xmmmi sz); id_nargs = (S (S (S O)));
    id_str_jas = (pp_ve_sz "VPSRL" ve sz); id_safe = []; id_pp_asm =
    (pp_viname_ww_128 "vpsrl" ve sz) }), ("VPSRL",
    (primV_16_64 (fun x x0 -> VPSRL (x, x0)))))

(** val x86_VPSRA :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPSRA ve sz =
  x86_u128_shift (wsize_of_velem ve) sz (wsar (wsize_of_velem ve))

(** val coq_Ox86_VPSRA_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPSRA_instr =
  ((fun ve sz -> { id_valid =
    ((&&) (size_16_32 (wsize_of_velem ve)) (size_128_256 sz)); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz U128); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz U128) (Obj.magic x86_VPSRA ve sz));
    id_args_kinds = (check_xmm_xmm_xmmmi sz); id_nargs = (S (S (S O)));
    id_str_jas = (pp_ve_sz "VPSRA" ve sz); id_safe = []; id_pp_asm =
    (pp_viname_ww_128 "vpsra" ve sz) }), ("VPSRA",
    (primV_16_32 (fun x x0 -> VPSRA (x, x0)))))

(** val x86_u128_shift_variable :
    wsize -> wsize -> (GRing.ComRing.sort -> coq_Z -> GRing.ComRing.sort) ->
    GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_u128_shift_variable ve sz op v1 v2 =
  lift2_vec ve (fun v3 v4 -> op v3 (Z.min (wunsigned ve v4) (wsize_bits ve)))
    sz v1 v2

(** val x86_VPSLLV :
    wsize -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPSLLV ve sz =
  x86_u128_shift_variable ve sz (wshl ve)

(** val coq_Ox86_VPSLLV_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPSLLV_instr =
  ((fun ve sz -> { id_valid =
    ((&&) (size_16_64 (wsize_of_velem ve)) (size_128_256 sz)); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPSLLV (wsize_of_velem ve) sz));
    id_args_kinds = (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O)));
    id_str_jas = (pp_ve_sz "VPSLLV" ve sz); id_safe = []; id_pp_asm =
    (pp_viname "vpsllv" ve sz) }), ("VPSLLV",
    (primV_16_64 (fun x x0 -> VPSLLV (x, x0)))))

(** val x86_VPSRLV :
    wsize -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPSRLV ve sz =
  x86_u128_shift_variable ve sz (wshr ve)

(** val coq_Ox86_VPSRLV_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPSRLV_instr =
  ((fun ve sz -> { id_valid =
    ((&&) (size_16_64 (wsize_of_velem ve)) (size_128_256 sz)); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPSRLV (wsize_of_velem ve) sz));
    id_args_kinds = (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O)));
    id_str_jas = (pp_ve_sz "VPSRLV" ve sz); id_safe = []; id_pp_asm =
    (pp_viname "vpsrlv" ve sz) }), ("VPSRLV",
    (primV_16_64 (fun x x0 -> VPSRLV (x, x0)))))

(** val x86_vpsxldq :
    wsize -> (GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple) ->
    GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_vpsxldq _ op =
  op

(** val x86_VPSLLDQ :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPSLLDQ sz =
  x86_vpsxldq sz (wpslldq sz)

(** val coq_Ox86_VPSLLDQ_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPSLLDQ_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (ww8_ty sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Ea x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (ww8_ty sz) (Obj.magic x86_VPSLLDQ sz)); id_args_kinds =
    (check_xmm_xmm_imm8 sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "VPSLLDQ" sz); id_safe = []; id_pp_asm =
    (pp_name "vpslldq" sz) }), ("VPSLLDQ",
    (prim_128_256 (fun x -> VPSLLDQ x))))

(** val x86_VPSRLDQ :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPSRLDQ sz =
  x86_vpsxldq sz (wpsrldq sz)

(** val coq_Ox86_VPSRLDQ_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPSRLDQ_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (ww8_ty sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Ea x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (ww8_ty sz) (Obj.magic x86_VPSRLDQ sz)); id_args_kinds =
    (check_xmm_xmm_imm8 sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "VPSRLDQ" sz); id_safe = []; id_pp_asm =
    (pp_name "vpsrldq" sz) }), ("VPSRLDQ",
    (prim_128_256 (fun x -> VPSRLDQ x))))

(** val x86_VPSHUFB :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPSHUFB sz =
  x86_u128_binop sz (lift2_vec U128 (wpshufb U128) sz)

(** val coq_Ox86_VPSHUFB_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPSHUFB_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPSHUFB sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "VPSHUFB" sz); id_safe = []; id_pp_asm =
    (pp_name "vpshufb" sz) }), ("VPSHUFB",
    (prim_128_256 (fun x -> VPSHUFB x))))

(** val check_xmm_xmmm_imm8 : wsize -> arg_kind list list list **)

let check_xmm_xmmm_imm8 _ =
  (xmm :: ((xmmm true) :: ((i U8) :: []))) :: []

(** val x86_vpshuf :
    wsize -> (GRing.ComRing.sort -> coq_Z -> GRing.ComRing.sort) ->
    GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_vpshuf _ op v1 v2 =
  op v1 (wunsigned U8 v2)

(** val x86_VPSHUFHW :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPSHUFHW sz =
  x86_vpshuf sz (wpshufhw sz)

(** val coq_Ox86_VPSHUFHW_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPSHUFHW_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (ww8_ty sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Ea x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (ww8_ty sz) (Obj.magic x86_VPSHUFHW sz)); id_args_kinds =
    (check_xmm_xmmm_imm8 sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "VPSHUFHW" sz); id_safe = []; id_pp_asm =
    (pp_name "vpshufhw" sz) }), ("VPSHUFHW",
    (prim_128_256 (fun x -> VPSHUFHW x))))

(** val x86_VPSHUFLW :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPSHUFLW sz =
  x86_vpshuf sz (wpshuflw sz)

(** val coq_Ox86_VPSHUFLW_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPSHUFLW_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (ww8_ty sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Ea x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (ww8_ty sz) (Obj.magic x86_VPSHUFLW sz)); id_args_kinds =
    (check_xmm_xmmm_imm8 sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "VPSHUFLW" sz); id_safe = []; id_pp_asm =
    (pp_name "vpshuflw" sz) }), ("VPSHUFLW",
    (prim_128_256 (fun x -> VPSHUFLW x))))

(** val x86_VPSHUFD :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPSHUFD sz =
  x86_vpshuf sz (wpshufd sz)

(** val coq_Ox86_VPSHUFD_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPSHUFD_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (ww8_ty sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Ea x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (ww8_ty sz) (Obj.magic x86_VPSHUFD sz)); id_args_kinds =
    (check_xmm_xmmm_imm8 sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "VPSHUFD" sz); id_safe = []; id_pp_asm =
    (pp_name "vpshufd" sz) }), ("VPSHUFD",
    (prim_128_256 (fun x -> VPSHUFD x))))

(** val x86_VPUNPCKH :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPUNPCKH ve sz =
  x86_u128_binop sz (wpunpckh sz ve)

(** val coq_Ox86_VPUNPCKH_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPUNPCKH_instr =
  ((fun ve sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPUNPCKH ve sz));
    id_args_kinds = (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O)));
    id_str_jas = (pp_ve_sz "VPUNPCKH" ve sz); id_safe = []; id_pp_asm =
    (pp_viname_long "vpunpckh" ve sz) }), ("VPUNPCKH",
    (primV (fun x x0 -> VPUNPCKH (x, x0)))))

(** val x86_VPUNPCKL :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPUNPCKL ve sz =
  x86_u128_binop sz (wpunpckl sz ve)

(** val coq_Ox86_VPUNPCKL_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPUNPCKL_instr =
  ((fun ve sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPUNPCKL ve sz));
    id_args_kinds = (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O)));
    id_str_jas = (pp_ve_sz "VPUNPCKL" ve sz); id_safe = []; id_pp_asm =
    (pp_viname_long "vpunpckl" ve sz) }), ("VPUNPCKL",
    (primV (fun x x0 -> VPUNPCKL (x, x0)))))

(** val check_xmm_xmm_xmmm_imm8 : wsize -> arg_kind list list list **)

let check_xmm_xmm_xmmm_imm8 _ =
  (xmm :: (xmm :: ((xmmm true) :: ((i U8) :: [])))) :: []

(** val wpblendw :
    GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    GRing.ComRing.sort **)

let wpblendw m0 w1 w2 =
  let v1 = split_vec U128 (nat_of_wsize U16) w1 in
  let v2 = split_vec U128 (nat_of_wsize U16) w2 in
  let b = split_vec U8 (S O) m0 in
  let r0 =
    map3 (fun b0 v3 v4 ->
      if eq_op
           (GRing.Ring.Exports.coq_GRing_Ring__to__eqtype_Equality
             (reverse_coercion (word_word__canonical__GRing_Ring O) __))
           (Obj.magic b0)
           (GRing.one
             (GRing.Ring.Exports.coq_GRing_Ring__to__GRing_SemiRing
               (reverse_coercion (word_word__canonical__GRing_Ring O) __)))
      then v4
      else v3) b v1 v2
  in
  make_vec U16 U128 (Obj.magic r0)

(** val x86_VPBLEND :
    Equality.sort -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    GRing.ComRing.sort -> sem_tuple **)

let x86_VPBLEND ve sz v1 v2 m0 =
  if eq_op wsize_wsize__canonical__eqtype_Equality ve (Obj.magic U32)
  then wpblendd sz v1 v2 m0
  else lift2_vec U128 (wpblendw m0) sz v1 v2

(** val coq_Ox86_VPBLEND_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPBLEND_instr =
  ((fun ve sz -> { id_valid =
    (let ve0 = wsize_of_velem ve in (&&) (size_16_32 ve0) (size_128_256 sz));
    id_msb_flag = (reg_msb_flag sz); id_tin = (w2w8_ty sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: ((coq_Ea
                                                                   x86_decl
                                                                   (S (S (S
                                                                   O)))) :: [])));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2w8_ty sz) (Obj.magic x86_VPBLEND (wsize_of_velem ve) sz));
    id_args_kinds = (check_xmm_xmm_xmmm_imm8 sz); id_nargs = (S (S (S (S
    O)))); id_str_jas = (pp_ve_sz "VPBLEND" ve sz); id_safe = []; id_pp_asm =
    (pp_viname "vpblend" ve sz) }), ("VPBLEND",
    (primV_16_32 (fun x x0 -> VPBLEND (x, x0)))))

(** val check_xmm_xmm_xmmm_xmm : wsize -> arg_kind list list list **)

let check_xmm_xmm_xmmm_xmm _ =
  (xmm :: (xmm :: ((xmmm true) :: (xmm :: [])))) :: []

(** val x86_VPBLENDVB :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort
    -> sem_tuple **)

let x86_VPBLENDVB =
  wpblendvb

(** val coq_Ox86_VPBLENDVB_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPBLENDVB_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag = MSB_CLEAR;
    id_tin = (w3_ty sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: ((coq_Eu
                                                                   x86_decl
                                                                   (S (S (S
                                                                   O)))) :: [])));
    id_tout = (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w3_ty sz) (Obj.magic x86_VPBLENDVB sz)); id_args_kinds =
    (check_xmm_xmm_xmmm_xmm sz); id_nargs = (S (S (S (S O)))); id_str_jas =
    (pp_sz "VPBLENDVB" sz); id_safe = []; id_pp_asm =
    (pp_name "vpblendvb" sz) }), ("VPBLENDVB",
    (prim_128_256 (fun x -> VPBLENDVB x))))

(** val x86_BLENDV :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    GRing.ComRing.sort -> sem_tuple **)

let x86_BLENDV =
  blendv

(** val coq_Ox86_BLENDV_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_BLENDV_instr =
  ((fun ve sz -> { id_valid = (size_128_256 sz); id_msb_flag = MSB_CLEAR;
    id_tin = (w3_ty sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: ((coq_Ea
                                                                   x86_decl
                                                                   (S (S (S
                                                                   O)))) :: [])));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w3_ty sz) (Obj.magic x86_BLENDV ve sz)); id_args_kinds =
    (check_xmm_xmm_xmmm_xmm sz); id_nargs = (S (S (S (S O)))); id_str_jas =
    (pp_ve_sz "BLENDV" ve sz); id_safe = []; id_pp_asm =
    (pp_name
      (match ve with
       | VE8 -> "vpblendvb"
       | VE16 -> "<assert false>"
       | VE32 -> "vblendvps"
       | VE64 -> "vblendvpd") sz) }), ("BLENDV",
    (primV_range
      (flatten
        (map (fun ve -> map (fun sz -> PVv (ve, sz)) (U128 :: (U256 :: [])))
          (VE8 :: (VE32 :: (VE64 :: []))))) (fun x x0 -> BLENDV (x, x0)))))

(** val coq_SaturatedSignedToUnsigned :
    wsize -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let coq_SaturatedSignedToUnsigned sz1 sz2 w =
  let i1 = wsigned sz1 w in
  let i2 = cmp_max Z.compare Z0 (cmp_min Z.compare i1 (wmax_unsigned sz2)) in
  wrepr sz2 i2

(** val coq_SaturatedSignedToSigned :
    wsize -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let coq_SaturatedSignedToSigned sz1 sz2 w =
  let i1 = wsigned sz1 w in
  let i2 =
    cmp_max Z.compare (wmin_signed sz2)
      (cmp_min Z.compare i1 (wmax_signed sz2))
  in
  wrepr sz2 i2

(** val vpack2 :
    wsize -> wsize -> wsize -> (GRing.ComRing.sort -> GRing.ComRing.sort) ->
    GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let vpack2 sz1 sz2 sz op w1 w2 =
  make_vec sz2 sz
    (cat (map (Obj.magic op) (split_vec sz (nat_of_wsize sz1) w1))
      (map (Obj.magic op) (split_vec sz (nat_of_wsize sz1) w2)))

(** val x86_VPACKUS :
    Equality.sort -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    sem_tuple **)

let x86_VPACKUS ve sz v1 v2 =
  let doit = fun sz0 v3 v4 ->
    if eq_op wsize_wsize__canonical__eqtype_Equality ve (Obj.magic U32)
    then vpack2 U32 U16 sz0 (coq_SaturatedSignedToUnsigned U32 U16) v3 v4
    else vpack2 U16 U8 sz0 (coq_SaturatedSignedToUnsigned U16 U8) v3 v4
  in
  if eq_op wsize_wsize__canonical__eqtype_Equality (Obj.magic sz)
       (Obj.magic U128)
  then doit sz v1 v2
  else lift2_vec U128 (doit U128) sz v1 v2

(** val coq_Ox86_VPACKUS_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPACKUS_instr =
  ((fun ve sz -> { id_valid =
    (let ve0 = wsize_of_velem ve in (&&) (size_16_32 ve0) (size_128_256 sz));
    id_msb_flag = (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPACKUS (wsize_of_velem ve) sz));
    id_args_kinds = (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O)));
    id_str_jas = (pp_ve_sz "VPACKUS" ve sz); id_safe = []; id_pp_asm =
    (pp_name
      (if eq_op wsize_wsize__canonical__eqtype_Equality (Obj.magic U16)
            (Obj.magic wsize_of_velem ve)
       then "vpackuswb"
       else "vpackusdw") sz) }), ("VPACKUS",
    (primV_16_32 (fun x x0 -> VPACKUS (x, x0)))))

(** val x86_VPACKSS :
    Equality.sort -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    sem_tuple **)

let x86_VPACKSS ve sz v1 v2 =
  let doit = fun sz0 v3 v4 ->
    if eq_op wsize_wsize__canonical__eqtype_Equality ve (Obj.magic U32)
    then vpack2 U32 U16 sz0 (coq_SaturatedSignedToSigned U32 U16) v3 v4
    else vpack2 U16 U8 sz0 (coq_SaturatedSignedToSigned U16 U8) v3 v4
  in
  if eq_op wsize_wsize__canonical__eqtype_Equality (Obj.magic sz)
       (Obj.magic U128)
  then doit sz v1 v2
  else lift2_vec U128 (doit U128) sz v1 v2

(** val coq_Ox86_VPACKSS_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPACKSS_instr =
  ((fun ve sz -> { id_valid =
    (let ve0 = wsize_of_velem ve in (&&) (size_16_32 ve0) (size_128_256 sz));
    id_msb_flag = (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPACKSS (wsize_of_velem ve) sz));
    id_args_kinds = (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O)));
    id_str_jas = (pp_ve_sz "VPACKSS" ve sz); id_safe = []; id_pp_asm =
    (pp_name
      (if eq_op wsize_wsize__canonical__eqtype_Equality (Obj.magic U16)
            (Obj.magic wsize_of_velem ve)
       then "vpacksswb"
       else "vpackssdw") sz) }), ("VPACKSS",
    (primV_16_32 (fun x x0 -> VPACKSS (x, x0)))))

(** val wshufps_128 :
    GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    GRing.ComRing.sort **)

let wshufps_128 o s1 s2 =
  make_vec U32 U128
    ((Obj.magic wpshufd1 s1 o O) :: ((Obj.magic wpshufd1 s1 o (S O)) :: (
    (Obj.magic wpshufd1 s2 o (S (S O))) :: ((Obj.magic wpshufd1 s2 o (S (S (S
                                              O)))) :: []))))

(** val x86_VSHUFPS :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort
    -> sem_tuple **)

let x86_VSHUFPS sz s1 s2 o =
  lift2_vec U128 (wshufps_128 o) sz s1 s2

(** val coq_Ox86_VSHUFPS_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VSHUFPS_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w2w8_ty sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: ((coq_Ea
                                                                   x86_decl
                                                                   (S (S (S
                                                                   O)))) :: [])));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2w8_ty sz) (Obj.magic x86_VSHUFPS sz)); id_args_kinds =
    (check_xmm_xmm_xmmm_imm8 sz); id_nargs = (S (S (S (S O)))); id_str_jas =
    (pp_sz "VSHUFPS" sz); id_safe = []; id_pp_asm =
    (pp_name "vshufps" sz) }), ("VSHUFPS",
    (prim_128_256 (fun x -> VSHUFPS x))))

(** val pp_vpbroadcast :
    velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    asm_arg list -> (register, register_ext, xmm_register, rflag, condt)
    pp_asm_op **)

let pp_vpbroadcast ve sz args =
  { pp_aop_name = "vpbroadcast"; pp_aop_ext = (PP_viname (ve, false));
    pp_aop_args = (zip (sz :: ((wsize_of_velem ve) :: [])) args) }

(** val check_xmm_xmmm : wsize -> arg_kind list list list **)

let check_xmm_xmmm _ =
  (xmm :: ((xmmm true) :: [])) :: []

(** val x86_VPBROADCAST :
    wsize -> wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPBROADCAST =
  wpbroadcast

(** val coq_Ox86_VPBROADCAST_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPBROADCAST_instr =
  ((fun ve sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w_ty (wsize_of_velem ve)); id_in =
    ((coq_Eu x86_decl (S O)) :: []); id_tout = (w_ty sz); id_out =
    ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty (wsize_of_velem ve))
      (Obj.magic x86_VPBROADCAST (wsize_of_velem ve) sz)); id_args_kinds =
    (check_xmm_xmmm sz); id_nargs = (S (S O)); id_str_jas =
    (pp_ve_sz "VPBROADCAST" ve sz); id_safe = []; id_pp_asm =
    (pp_vpbroadcast ve sz) }), ("VPBROADCAST",
    (primV (fun x x0 -> VPBROADCAST (x, x0)))))

(** val x86_VMOVSHDUP : wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_VMOVSHDUP sz v =
  wdup_hi (wsize_of_velem VE32) sz v

(** val coq_Ox86_VMOVSHDUP_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VMOVSHDUP_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w_ty sz); id_in =
    ((coq_Eu x86_decl (S O)) :: []); id_tout = (w_ty sz); id_out =
    ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_VMOVSHDUP sz)); id_args_kinds =
    (check_xmm_xmmm sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "VMOVSHDUP" sz); id_safe = []; id_pp_asm =
    (pp_name "vmovshdup" sz) }), ("VMOVSHDUP",
    (prim_128_256 (fun x -> VMOVSHDUP x))))

(** val x86_VMOVSLDUP : wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_VMOVSLDUP sz v =
  wdup_lo (wsize_of_velem VE32) sz v

(** val coq_Ox86_VMOVSLDUP_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VMOVSLDUP_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag =
    (reg_msb_flag sz); id_tin = (w_ty sz); id_in =
    ((coq_Eu x86_decl (S O)) :: []); id_tout = (w_ty sz); id_out =
    ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_VMOVSLDUP sz)); id_args_kinds =
    (check_xmm_xmmm sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "VMOVSLDUP" sz); id_safe = []; id_pp_asm =
    (pp_name "vmovsldup" sz) }), ("VMOVSLDUP",
    (prim_128_256 (fun x -> VMOVSLDUP x))))

(** val x86_VPALIGNR128 :
    GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    GRing.ComRing.sort **)

let x86_VPALIGNR128 m0 v1 v2 =
  let v = make_vec U128 U256 (v2 :: (v1 :: [])) in
  let v' =
    wshr U256 v
      (Z.mul (wunsigned U8 m0) (Zpos (Coq_xO (Coq_xO (Coq_xO Coq_xH)))))
  in
  nth
    (GRing.zero
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule (word U128)))
    (Obj.magic split_vec U256 (nat_of_wsize U128) v') O

(** val x86_VPALIGNR :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort
    -> sem_tuple **)

let x86_VPALIGNR sz v1 v2 m0 =
  lift2_vec U128 (x86_VPALIGNR128 m0) sz v1 v2

(** val coq_Ox86_VPALIGNR_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPALIGNR_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag = MSB_CLEAR;
    id_tin = (w2w8_ty sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: ((coq_Eu
                                                                   x86_decl
                                                                   (S (S (S
                                                                   O)))) :: [])));
    id_tout = (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2w8_ty sz) (Obj.magic x86_VPALIGNR sz)); id_args_kinds =
    (check_xmm_xmm_xmmm_imm8 sz); id_nargs = (S (S (S (S O)))); id_str_jas =
    (pp_sz "VPALIGNR" sz); id_safe = []; id_pp_asm =
    (pp_name "vpalignr" sz) }), ("VPALIGNR",
    (prim_128_256 (fun x -> VPALIGNR x))))

(** val coq_Ox86_VBROADCASTI128_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_VBROADCASTI128_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = w128_ty; id_in =
    ((coq_Eu x86_decl (S O)) :: []); id_tout = w256_ty; id_out =
    ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok w128_ty (Obj.magic x86_VPBROADCAST U128 U256));
    id_args_kinds = ((xmm :: ((m true) :: [])) :: []); id_nargs = (S (S O));
    id_str_jas = (pp_s "VPBROADCAST_2u128"); id_safe = []; id_pp_asm =
    (pp_name_ty "vbroadcasti128" (U256 :: (U128 :: []))) },
    ("VPBROADCAST_2u128", (primM VBROADCASTI128)))

(** val check_xmmm_xmm_imm8 : wsize -> arg_kind list list list **)

let check_xmmm_xmm_imm8 _ =
  ((xmmm false) :: (xmm :: ((i U8) :: []))) :: []

(** val x86_VEXTRACTI128 :
    GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VEXTRACTI128 v i0 =
  let r0 = if lsb U8 i0 then wshr U256 v (Z.of_nat (nat_of_wsize U128)) else v
  in
  zero_extend U128 U256 r0

(** val coq_Ox86_VEXTRACTI128_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_VEXTRACTI128_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = w256w8_ty; id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = w128_ty; id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok w256w8_ty (Obj.magic x86_VEXTRACTI128)); id_args_kinds =
    (check_xmmm_xmm_imm8 U256); id_nargs = (S (S (S O))); id_str_jas =
    (pp_s "VEXTRACTI128"); id_safe = []; id_pp_asm =
    (pp_name_ty "vextracti128" (U128 :: (U256 :: (U8 :: [])))) },
    ("VEXTRACTI128", (primM VEXTRACTI128)))

(** val x86_VINSERTI128 :
    GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    sem_tuple **)

let x86_VINSERTI128 =
  winserti128

(** val coq_Ox86_VINSERTI128_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_VINSERTI128_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = w256w128w8_ty;
    id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: ((coq_Eu
                                                                   x86_decl
                                                                   (S (S (S
                                                                   O)))) :: [])));
    id_tout = w256_ty; id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok w256w128w8_ty (Obj.magic x86_VINSERTI128)); id_args_kinds =
    (check_xmm_xmm_xmmm_imm8 U256); id_nargs = (S (S (S (S O))));
    id_str_jas = (pp_s "VINSERTI128"); id_safe = []; id_pp_asm =
    (pp_name_ty "vinserti128" (U256 :: (U256 :: (U128 :: (U8 :: []))))) },
    ("VINSERTI128", (primM VINSERTI128)))

(** val x86_VPERM2I128 :
    GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort ->
    sem_tuple **)

let x86_VPERM2I128 =
  wperm2i128

(** val coq_Ox86_VPERM2I128_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_VPERM2I128_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = w256x2w8_ty; id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: ((coq_Eu
                                                                   x86_decl
                                                                   (S (S (S
                                                                   O)))) :: [])));
    id_tout = w256_ty; id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok w256x2w8_ty (Obj.magic x86_VPERM2I128)); id_args_kinds =
    (check_xmm_xmm_xmmm_imm8 U256); id_nargs = (S (S (S (S O))));
    id_str_jas = (pp_s "VPERM2I128"); id_safe = []; id_pp_asm =
    (pp_name_ty "vperm2i128" (U256 :: (U256 :: (U256 :: (U8 :: []))))) },
    ("VPERM2I128", (primM VPERM2I128)))

(** val x86_VPERMD : GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPERMD v1 v2 =
  wpermd U256 v1 v2

(** val coq_Ox86_VPERMD_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_VPERMD_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = (w2_ty U256 U256);
    id_in = ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = w256_ty; id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty U256 U256) (Obj.magic x86_VPERMD)); id_args_kinds =
    (check_xmm_xmm_xmmm U256); id_nargs = (S (S (S O))); id_str_jas =
    (pp_s "VPERMD"); id_safe = []; id_pp_asm = (pp_name "vpermd" U256) },
    ("VPERMD", (primM VPERMD)))

(** val x86_VPERMQ : GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPERMQ =
  wpermq

(** val coq_Ox86_VPERMQ_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_VPERMQ_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = w256w8_ty; id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = w256_ty; id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok w256w8_ty (Obj.magic x86_VPERMQ)); id_args_kinds =
    (check_xmm_xmmm_imm8 U256); id_nargs = (S (S (S O))); id_str_jas =
    (pp_s "VPERMQ"); id_safe = []; id_pp_asm =
    (pp_name_ty "vpermq" (U256 :: (U256 :: (U8 :: [])))) }, ("VPERMQ",
    (primM VPERMQ)))

(** val x86_VPMOVMSKB : wsize -> wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPMOVMSKB ssz dsz v =
  wpmovmskb dsz ssz v

(** val coq_Ox86_PMOVMSKB_instr :
    (wsize -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_PMOVMSKB_instr =
  ((fun ssz dsz -> { id_valid = ((&&) (size_32_64 dsz) (size_128_256 ssz));
    id_msb_flag = MSB_CLEAR; id_tin = (w_ty ssz); id_in =
    ((coq_Eu x86_decl (S O)) :: []); id_tout = (w_ty dsz); id_out =
    ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty ssz) (Obj.magic x86_VPMOVMSKB ssz dsz));
    id_args_kinds = ((r :: (xmm :: [])) :: []); id_nargs = (S (S O));
    id_str_jas = (pp_sz_sz "VPMOVMSKB" false ssz dsz); id_safe = [];
    id_pp_asm = (pp_name_ty "vpmovmskb" (dsz :: (ssz :: []))) }),
    ("VPMOVMSKB", (primX (fun x x0 -> VPMOVMSKB (x, x0)))))

(** val x86_VPCMPEQ :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPCMPEQ ve sz v1 v2 =
  wpcmpeq (wsize_of_velem ve) sz v1 v2

(** val coq_Ox86_VPCMPEQ_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPCMPEQ_instr =
  ((fun ve sz -> { id_valid =
    ((&&) (size_8_64 (wsize_of_velem ve)) (size_128_256 sz)); id_msb_flag =
    MSB_CLEAR; id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPCMPEQ ve sz));
    id_args_kinds = (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O)));
    id_str_jas = (pp_ve_sz "VPCMPEQ" ve sz); id_safe = []; id_pp_asm =
    (pp_viname "vpcmpeq" ve sz) }), ("VPCMPEQ",
    (primV (fun x x0 -> VPCMPEQ (x, x0)))))

(** val x86_VPCMPGT :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPCMPGT ve sz v1 v2 =
  wpcmpgt (wsize_of_velem ve) sz v1 v2

(** val coq_Ox86_VPCMPGT_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPCMPGT_instr =
  ((fun ve sz -> { id_valid =
    ((&&) (size_8_64 (wsize_of_velem ve)) (size_128_256 sz)); id_msb_flag =
    MSB_CLEAR; id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPCMPGT ve sz));
    id_args_kinds = (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O)));
    id_str_jas = (pp_ve_sz "VPCMPGT" ve sz); id_safe = []; id_pp_asm =
    (pp_viname "vpcmpgt" ve sz) }), ("VPCMPGT",
    (primV (fun x x0 -> VPCMPGT (x, x0)))))

(** val x86_VPSIGN :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPSIGN ve sz v1 v2 =
  lift2_vec (wsize_of_velem ve) (fun x m0 ->
    match wsigned (wsize_of_velem ve) m0 with
    | Z0 ->
      GRing.zero
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
          (word (wsize_of_velem ve)))
    | Zpos _ -> x
    | Zneg _ ->
      GRing.opp
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Zmodule
          (word (wsize_of_velem ve))) x) sz v1 v2

(** val coq_Ox86_VPSIGN_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPSIGN_instr =
  ((fun ve sz -> { id_valid =
    ((&&) (size_8_32 (wsize_of_velem ve)) (size_128_256 sz)); id_msb_flag =
    MSB_CLEAR; id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPSIGN ve sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_ve_sz "VPSIGN" ve sz); id_safe = []; id_pp_asm =
    (pp_viname "vpsign" ve sz) }), ("VPSIGN",
    (primV_8_32 (fun x x0 -> VPSIGN (x, x0)))))

(** val x86_VPMADDUBSW :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPMADDUBSW =
  wpmaddubsw

(** val coq_Ox86_VPMADDUBSW_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPMADDUBSW_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag = MSB_CLEAR;
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPMADDUBSW sz));
    id_args_kinds = (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O)));
    id_str_jas = (pp_sz "VPMADDUBSW" sz); id_safe = []; id_pp_asm =
    (pp_name_ty "vpmaddubsw" (sz :: (sz :: (sz :: [])))) }), ("VPMADDUBSW",
    (prim_128_256 (fun x -> VPMADDUBSW x))))

(** val x86_VPMADDWD :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPMADDWD =
  wpmaddwd

(** val coq_Ox86_VPMADDWD_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPMADDWD_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag = MSB_CLEAR;
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPMADDWD sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_sz "VPMADDWD" sz); id_safe = []; id_pp_asm =
    (pp_name_ty "vpmaddwd" (sz :: (sz :: (sz :: [])))) }), ("VPMADDWD",
    (prim_128_256 (fun x -> VPMADDWD x))))

(** val check_movpd : arg_kind list list list **)

let check_movpd =
  ((m false) :: (xmm :: [])) :: []

(** val x86_VMOVLPD : GRing.ComRing.sort -> sem_tuple **)

let x86_VMOVLPD v =
  zero_extend U64 U128 v

(** val coq_Ox86_VMOVLPD_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_VMOVLPD_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = (w_ty U128); id_in =
    ((coq_Eu x86_decl (S O)) :: []); id_tout = (w_ty U64); id_out =
    ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty U128) (Obj.magic x86_VMOVLPD)); id_args_kinds =
    check_movpd; id_nargs = (S (S O)); id_str_jas = (pp_s "VMOVLPD");
    id_safe = []; id_pp_asm = (pp_name_ty "vmovlpd" (U64 :: (U128 :: []))) },
    ("VMOVLPD", (primM VMOVLPD)))

(** val x86_VMOVHPD : GRing.ComRing.sort -> sem_tuple **)

let x86_VMOVHPD v =
  zero_extend U64 U128
    (wshr U128 v (Zpos (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO (Coq_xO
      Coq_xH))))))))

(** val coq_Ox86_VMOVHPD_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_VMOVHPD_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = (w_ty U128); id_in =
    ((coq_Eu x86_decl (S O)) :: []); id_tout = (w_ty U64); id_out =
    ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty U128) (Obj.magic x86_VMOVHPD)); id_args_kinds =
    check_movpd; id_nargs = (S (S O)); id_str_jas = (pp_s "VMOVHPD");
    id_safe = []; id_pp_asm = (pp_name_ty "vmovhpd" (U64 :: (U128 :: []))) },
    ("VMOVHPD", (primM VMOVHPD)))

(** val x86_VPMINS :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPMINS ve sz x y =
  wmin Signed (wsize_of_velem ve) sz x y

(** val coq_Ox86_VPMINS_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPMINS_instr =
  ((fun ve sz -> { id_valid =
    (let ve0 = wsize_of_velem ve in (&&) (size_8_32 ve0) (size_128_256 sz));
    id_msb_flag = (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPMINS ve sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_ve_sz "VPMINS" ve sz); id_safe = []; id_pp_asm =
    (pp_viname "vpmins" ve sz) }), ("VPMINS",
    (primV_8_32 (fun x x0 -> VPMINS (x, x0)))))

(** val x86_VPMINU :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPMINU ve sz x y =
  wmin Unsigned (wsize_of_velem ve) sz x y

(** val coq_Ox86_VPMINU_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPMINU_instr =
  ((fun ve sz -> { id_valid =
    (let ve0 = wsize_of_velem ve in (&&) (size_8_32 ve0) (size_128_256 sz));
    id_msb_flag = (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPMINU ve sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_ve_sz "VPMINU" ve sz); id_safe = []; id_pp_asm =
    (pp_viname "vpminu" ve sz) }), ("VPMINU",
    (primV_8_32 (fun x x0 -> VPMINU (x, x0)))))

(** val x86_VPMAXS :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPMAXS ve sz x y =
  wmax Signed (wsize_of_velem ve) sz x y

(** val coq_Ox86_VPMAXS_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPMAXS_instr =
  ((fun ve sz -> { id_valid =
    (let ve0 = wsize_of_velem ve in (&&) (size_8_32 ve0) (size_128_256 sz));
    id_msb_flag = (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPMAXS ve sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_ve_sz "VPMAXS" ve sz); id_safe = []; id_pp_asm =
    (pp_viname "vpmaxs" ve sz) }), ("VPMAXS",
    (primV_8_32 (fun x x0 -> VPMAXS (x, x0)))))

(** val x86_VPMAXU :
    velem -> wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPMAXU ve sz x y =
  wmax Unsigned (wsize_of_velem ve) sz x y

(** val coq_Ox86_VPMAXU_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPMAXU_instr =
  ((fun ve sz -> { id_valid =
    (let ve0 = wsize_of_velem ve in (&&) (size_8_32 ve0) (size_128_256 sz));
    id_msb_flag = (reg_msb_flag sz); id_tin = (w2_ty sz sz); id_in =
    ((coq_Ea x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPMAXU ve sz)); id_args_kinds =
    (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O))); id_str_jas =
    (pp_ve_sz "VPMAXU" ve sz); id_safe = []; id_pp_asm =
    (pp_viname "vpmaxu" ve sz) }), ("VPMAXU",
    (primV_8_32 (fun x x0 -> VPMAXU (x, x0)))))

(** val x86_VPABS : velem -> wsize -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPABS =
  wabs

(** val coq_Ox86_VPABS_instr :
    (velem -> wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPABS_instr =
  ((fun ve sz -> { id_valid =
    (let ve0 = wsize_of_velem ve in (&&) (size_8_32 ve0) (size_128_256 sz));
    id_msb_flag = (reg_msb_flag sz); id_tin = (w_ty sz); id_in =
    ((coq_Eu x86_decl (S O)) :: []); id_tout = (w_ty sz); id_out =
    ((coq_Ea x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty sz) (Obj.magic x86_VPABS ve sz)); id_args_kinds =
    (check_xmm_xmmm sz); id_nargs = (S (S O)); id_str_jas =
    (pp_ve_sz "VPABS" ve sz); id_safe = []; id_pp_asm =
    (pp_viname "vpabs" ve sz) }), ("VPABS",
    (primV_8_32 (fun x x0 -> VPABS (x, x0)))))

(** val check_vptest : wsize -> arg_kind list list list **)

let check_vptest _ =
  xmm_xmmm :: []

(** val x86_VPTEST :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_VPTEST sz x y =
  Obj.magic ((Some false), ((Some
    (eq_op
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__eqtype_Equality (word sz))
      (wandn sz x y)
      (GRing.zero
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule (word sz))))),
    ((Some false), ((Some false), (Some
    (coq_ZF_of_word sz (Word0.wand sz x y)))))))

(** val coq_Ox86_VPTEST_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPTEST_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag = MSB_MERGE;
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: [])); id_tout =
    b5_ty; id_out = implicit_flags; id_semi =
    (sem_prod_ok (w2_ty sz sz) (Obj.magic x86_VPTEST sz)); id_args_kinds =
    (check_vptest sz); id_nargs = (S (S O)); id_str_jas =
    (pp_sz "VPTEST" sz); id_safe = []; id_pp_asm = (pp_name "vptest" sz) }),
    ("VPTEST", (prim_128_256 (fun x -> VPTEST x))))

(** val coq_Ox86_RDTSC_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_RDTSC_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = MSB_CLEAR; id_tin =
    []; id_in = []; id_tout = (w2_ty sz sz); id_out =
    ((coq_R x86_decl RDX) :: ((coq_R x86_decl RAX) :: [])); id_semi =
    (Obj.magic (Error ErrSemUndef)); id_args_kinds = ([] :: []); id_nargs =
    O; id_str_jas = (pp_sz "RDTSC" sz); id_safe = (ScFalse :: []);
    id_pp_asm = (pp_name_ty "rdtsc" (sz :: (sz :: []))) }), ("RDTSC",
    (prim_32_64 (fun x -> RDTSC x))))

(** val coq_Ox86_RDTSCP_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_RDTSCP_instr =
  ((fun sz -> { id_valid = (size_32_64 sz); id_msb_flag = MSB_CLEAR; id_tin =
    []; id_in = []; id_tout = (w3_ty sz); id_out =
    ((coq_R x86_decl RDX) :: ((coq_R x86_decl RAX) :: ((coq_R x86_decl RCX) :: [])));
    id_semi = (Obj.magic (Error ErrSemUndef)); id_args_kinds = ([] :: []);
    id_nargs = O; id_str_jas = (pp_sz "RDTSCP" sz); id_safe =
    (ScFalse :: []); id_pp_asm =
    (pp_name_ty "rdtscp" (sz :: (sz :: (sz :: [])))) }), ("RDTSCP",
    (prim_32_64 (fun x -> RDTSCP x))))

(** val coq_Ox86_CLFLUSH_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_CLFLUSH_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = ((Coq_sword
    (arch_pd x86_decl)) :: []); id_in = ((coq_Ec x86_decl O) :: []);
    id_tout = []; id_out = []; id_semi =
    (sem_prod_ok ((Coq_sword (arch_pd x86_decl)) :: [])
      (Obj.magic (fun _ -> ()))); id_args_kinds = (((m true) :: []) :: []);
    id_nargs = (S O); id_str_jas = (pp_s "CLFLUSH"); id_safe = [];
    id_pp_asm = (pp_name "clflush" U8) }, ("CLFLUSH", (primM CLFLUSH)))

(** val coq_Ox86_PREFETCHT0_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_PREFETCHT0_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = ((Coq_sword
    (arch_pd x86_decl)) :: []); id_in = ((coq_Ec x86_decl O) :: []);
    id_tout = []; id_out = []; id_semi =
    (sem_prod_ok ((Coq_sword (arch_pd x86_decl)) :: [])
      (Obj.magic (fun _ -> ()))); id_args_kinds = (((m true) :: []) :: []);
    id_nargs = (S O); id_str_jas = (pp_s "PREFETCHT0"); id_safe = [];
    id_pp_asm = (pp_name "prefetcht0" U8) }, ("PREFETCHT0",
    (primM PREFETCHT0)))

(** val coq_Ox86_PREFETCHT1_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_PREFETCHT1_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = ((Coq_sword
    (arch_pd x86_decl)) :: []); id_in = ((coq_Ec x86_decl O) :: []);
    id_tout = []; id_out = []; id_semi =
    (sem_prod_ok ((Coq_sword (arch_pd x86_decl)) :: [])
      (Obj.magic (fun _ -> ()))); id_args_kinds = (((m true) :: []) :: []);
    id_nargs = (S O); id_str_jas = (pp_s "PREFETCHT1"); id_safe = [];
    id_pp_asm = (pp_name "prefetcht1" U8) }, ("PREFETCHT1",
    (primM PREFETCHT1)))

(** val coq_Ox86_PREFETCHT2_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_PREFETCHT2_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = ((Coq_sword
    (arch_pd x86_decl)) :: []); id_in = ((coq_Ec x86_decl O) :: []);
    id_tout = []; id_out = []; id_semi =
    (sem_prod_ok ((Coq_sword (arch_pd x86_decl)) :: [])
      (Obj.magic (fun _ -> ()))); id_args_kinds = (((m true) :: []) :: []);
    id_nargs = (S O); id_str_jas = (pp_s "PREFETCHT2"); id_safe = [];
    id_pp_asm = (pp_name "prefetcht2" U8) }, ("PREFETCHT2",
    (primM PREFETCHT2)))

(** val coq_Ox86_PREFETCHNTA_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_PREFETCHNTA_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = ((Coq_sword
    (arch_pd x86_decl)) :: []); id_in = ((coq_Ec x86_decl O) :: []);
    id_tout = []; id_out = []; id_semi =
    (sem_prod_ok ((Coq_sword (arch_pd x86_decl)) :: [])
      (Obj.magic (fun _ -> ()))); id_args_kinds = (((m true) :: []) :: []);
    id_nargs = (S O); id_str_jas = (pp_s "PREFETCHNTA"); id_safe = [];
    id_pp_asm = (pp_name "prefetchnta" U8) }, ("PREFETCHNTA",
    (primM PREFETCHNTA)))

(** val coq_Ox86_LFENCE_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_LFENCE_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = []; id_in = [];
    id_tout = []; id_out = []; id_semi = (sem_prod_ok [] (Obj.magic ()));
    id_args_kinds = ([] :: []); id_nargs = O; id_str_jas = (pp_s "LFENCE");
    id_safe = []; id_pp_asm = (pp_name "lfence" U8) }, ("LFENCE",
    (primM LFENCE)))

(** val coq_Ox86_MFENCE_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_MFENCE_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = []; id_in = [];
    id_tout = []; id_out = []; id_semi = (sem_prod_ok [] (Obj.magic ()));
    id_args_kinds = ([] :: []); id_nargs = O; id_str_jas = (pp_s "MFENCE");
    id_safe = []; id_pp_asm = (pp_name "mfence" U8) }, ("MFENCE",
    (primM MFENCE)))

(** val coq_Ox86_SFENCE_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_SFENCE_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = []; id_in = [];
    id_tout = []; id_out = []; id_semi = (sem_prod_ok [] (Obj.magic ()));
    id_args_kinds = ([] :: []); id_nargs = O; id_str_jas = (pp_s "SFENCE");
    id_safe = []; id_pp_asm = (pp_name "sfence" U8) }, ("SFENCE",
    (primM SFENCE)))

(** val x86_AESDEC : GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_AESDEC =
  wAESDEC

(** val x86_AESDECLAST :
    GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_AESDECLAST =
  wAESDECLAST

(** val x86_AESENC : GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_AESENC =
  wAESENC

(** val x86_AESENCLAST :
    GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_AESENCLAST =
  wAESENCLAST

(** val x86_AESIMC : GRing.ComRing.sort -> sem_tuple **)

let x86_AESIMC =
  coq_InvMixColumns

(** val x86_AESKEYGENASSIST :
    GRing.ComRing.sort -> GRing.ComRing.sort -> sem_tuple **)

let x86_AESKEYGENASSIST =
  wAESKEYGENASSIST

(** val mk_instr_aes2 :
    string -> string -> x86_op -> sem_tuple sem_prod -> msb_flag ->
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let mk_instr_aes2 jname aname constr x86_sem msb_flag0 =
  ({ id_valid = true; id_msb_flag = msb_flag0; id_tin = (w2_ty U128 U128);
    id_in = ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: []));
    id_tout = (w_ty U128); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty U128 U128) x86_sem); id_args_kinds =
    (check_xmm_xmmm U128); id_nargs = (S (S O)); id_str_jas = (pp_s jname);
    id_safe = []; id_pp_asm = (pp_name_ty aname (U128 :: (U128 :: []))) },
    (jname, (primM constr)))

(** val mk_instr_aes3 :
    string -> string -> (wsize -> x86_op) -> (GRing.ComRing.sort ->
    GRing.ComRing.sort -> GRing.ComRing.sort) -> (wsize -> (register,
    register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let mk_instr_aes3 jname aname constr x86_sem =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag = MSB_CLEAR;
    id_tin = (w2_ty sz sz); id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty sz sz)
      (Obj.magic x86_u128_binop sz (lift2_vec U128 x86_sem sz)));
    id_args_kinds = (check_xmm_xmm_xmmm sz); id_nargs = (S (S (S O)));
    id_str_jas = (pp_sz jname sz); id_safe = []; id_pp_asm =
    (pp_name_ty aname (sz :: (sz :: (sz :: [])))) }), (jname,
    (prim_128_256 constr)))

(** val coq_Ox86_AESDEC_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_AESDEC_instr =
  mk_instr_aes2 "AESDEC" "aesdec" AESDEC (Obj.magic x86_AESDEC) MSB_MERGE

(** val coq_Ox86_VAESDEC_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VAESDEC_instr =
  mk_instr_aes3 "VAESDEC" "vaesdec" (fun x -> VAESDEC x) wAESDEC

(** val coq_Ox86_AESDECLAST_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_AESDECLAST_instr =
  mk_instr_aes2 "AESDECLAST" "aesdeclast" AESDECLAST
    (Obj.magic x86_AESDECLAST) MSB_MERGE

(** val coq_Ox86_VAESDECLAST_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VAESDECLAST_instr =
  mk_instr_aes3 "VAESDECLAST" "vaesdeclast" (fun x -> VAESDECLAST x)
    wAESDECLAST

(** val coq_Ox86_AESENC_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_AESENC_instr =
  mk_instr_aes2 "AESENC" "aesenc" AESENC (Obj.magic x86_AESENC) MSB_MERGE

(** val coq_Ox86_VAESENC_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VAESENC_instr =
  mk_instr_aes3 "VAESENC" "vaesenc" (fun x -> VAESENC x) wAESENC

(** val coq_Ox86_AESENCLAST_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_AESENCLAST_instr =
  mk_instr_aes2 "AESENCLAST" "aesenclast" AESENCLAST
    (Obj.magic x86_AESENCLAST) MSB_MERGE

(** val coq_Ox86_VAESENCLAST_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VAESENCLAST_instr =
  mk_instr_aes3 "VAESENCLAST" "vaesenclast" (fun x -> VAESENCLAST x)
    wAESENCLAST

(** val coq_Ox86_AESIMC_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_AESIMC_instr =
  ({ id_valid = true; id_msb_flag = MSB_MERGE; id_tin = (w_ty U128); id_in =
    ((coq_Eu x86_decl (S O)) :: []); id_tout = (w_ty U128); id_out =
    ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty U128) (Obj.magic x86_AESIMC)); id_args_kinds =
    (check_xmm_xmmm U128); id_nargs = (S (S O)); id_str_jas =
    (pp_s "AESIMC"); id_safe = []; id_pp_asm =
    (pp_name_ty "aesimc" (U128 :: (U128 :: []))) }, ("AESIMC",
    (primM AESIMC)))

(** val coq_Ox86_VAESIMC_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_VAESIMC_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = (w_ty U128); id_in =
    ((coq_Eu x86_decl (S O)) :: []); id_tout = (w_ty U128); id_out =
    ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w_ty U128) (Obj.magic x86_AESIMC)); id_args_kinds =
    (check_xmm_xmmm U128); id_nargs = (S (S O)); id_str_jas =
    (pp_s "VAESIMC"); id_safe = []; id_pp_asm =
    (pp_name_ty "vaesimc" (U128 :: (U128 :: []))) }, ("VAESIMC",
    (primM VAESIMC)))

(** val coq_Ox86_AESKEYGENASSIST_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_AESKEYGENASSIST_instr =
  ({ id_valid = true; id_msb_flag = MSB_MERGE; id_tin = (w2_ty U128 U8);
    id_in = ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty U128); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty U128 U8) (Obj.magic x86_AESKEYGENASSIST));
    id_args_kinds = (check_xmm_xmmm_imm8 U128); id_nargs = (S (S (S O)));
    id_str_jas = (pp_s "AESKEYGENASSIST"); id_safe = []; id_pp_asm =
    (pp_name_ty "aeskeygenassist" (U128 :: (U128 :: (U8 :: [])))) },
    ("AESKEYGENASSIST", (primM AESKEYGENASSIST)))

(** val coq_Ox86_VAESKEYGENASSIST_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_VAESKEYGENASSIST_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = (w2_ty U128 U8);
    id_in = ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: []));
    id_tout = (w_ty U128); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok (w2_ty U128 U8) (Obj.magic x86_AESKEYGENASSIST));
    id_args_kinds = (check_xmm_xmmm_imm8 U128); id_nargs = (S (S (S O)));
    id_str_jas = (pp_s "VAESKEYGENASSIST"); id_safe = []; id_pp_asm =
    (pp_name_ty "vaeskeygenassist" (U128 :: (U128 :: (U8 :: [])))) },
    ("VAESKEYGENASSIST", (primM VAESKEYGENASSIST)))

(** val wclmulq :
    GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort **)

let wclmulq x1 x2 =
  let x = zero_extend U128 U64 x1 in
  foldr (fun k r0 ->
    Word0.wxor U128
      (if wbit_n U64 x2 k
       then wshl U128 x (Z.of_nat k)
       else GRing.zero
              (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
                (word U128))) r0)
    (GRing.zero
      (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule (word U128)))
    (iota O (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
      (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
      (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S (S
      O)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))

(** val wVPCLMULDQD :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort
    -> GRing.ComRing.sort **)

let wVPCLMULDQD sz w1 w2 k =
  let get1 = fun w ->
    nth
      (Obj.magic GRing.zero
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
          (word U64))) (split_vec U128 (nat_of_wsize U64) w)
      (nat_of_bool (wbit_n U8 k O))
  in
  let get2 = fun w ->
    nth
      (Obj.magic GRing.zero
        (GRing.ComRing.Exports.coq_GRing_ComRing__to__GRing_Nmodule
          (word U64))) (split_vec U128 (nat_of_wsize U64) w)
      (nat_of_bool (wbit_n U8 k (S (S (S (S O))))))
  in
  let f = fun w3 w4 -> wclmulq (Obj.magic get1 w3) (Obj.magic get2 w4) in
  make_vec U128 sz
    (map2 (Obj.magic f) (split_vec sz (nat_of_wsize U128) w1)
      (split_vec sz (nat_of_wsize U128) w2))

(** val x86_VPCLMULQDQ :
    wsize -> GRing.ComRing.sort -> GRing.ComRing.sort -> GRing.ComRing.sort
    -> sem_tuple **)

let x86_VPCLMULQDQ =
  wVPCLMULDQD

(** val coq_Ox86_PCLMULQDQ_instr :
    (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t * (string * x86_op prim_constructor) **)

let coq_Ox86_PCLMULQDQ_instr =
  ({ id_valid = true; id_msb_flag = MSB_CLEAR; id_tin = ((Coq_sword
    U128) :: ((Coq_sword U128) :: ((Coq_sword U8) :: []))); id_in =
    ((coq_Eu x86_decl O) :: ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S
                                                           (S O))) :: [])));
    id_tout = (w_ty U128); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok ((Coq_sword U128) :: ((Coq_sword U128) :: ((Coq_sword
      U8) :: []))) (Obj.magic x86_VPCLMULQDQ U128)); id_args_kinds =
    (check_xmm_xmmm_imm8 U128); id_nargs = (S (S (S O))); id_str_jas =
    (pp_s "PCLMULQDQ"); id_safe = []; id_pp_asm =
    (pp_name_ty "pclmulqdq" (U128 :: (U128 :: (U8 :: [])))) }, ("PCLMULQDQ",
    (primM PCLMULQDQ)))

(** val coq_Ox86_VPCLMULQDQ_instr :
    (wsize -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t) * (string * x86_op prim_constructor) **)

let coq_Ox86_VPCLMULQDQ_instr =
  ((fun sz -> { id_valid = (size_128_256 sz); id_msb_flag = MSB_CLEAR;
    id_tin = ((Coq_sword sz) :: ((Coq_sword sz) :: ((Coq_sword U8) :: [])));
    id_in =
    ((coq_Eu x86_decl (S O)) :: ((coq_Eu x86_decl (S (S O))) :: ((coq_Eu
                                                                   x86_decl
                                                                   (S (S (S
                                                                   O)))) :: [])));
    id_tout = (w_ty sz); id_out = ((coq_Eu x86_decl O) :: []); id_semi =
    (sem_prod_ok ((Coq_sword sz) :: ((Coq_sword sz) :: ((Coq_sword
      U8) :: []))) (Obj.magic x86_VPCLMULQDQ sz)); id_args_kinds =
    (check_xmm_xmm_xmmm_imm8 sz); id_nargs = (S (S (S (S O)))); id_str_jas =
    (pp_sz "VPCLMULQDQ" sz); id_safe = []; id_pp_asm =
    (pp_name "vpclmulqdq" sz) }), ("VPCLMULQDQ",
    (prim_128_256 (fun x -> VPCLMULQDQ x))))

(** val x86_instr_desc :
    x86_op -> (register, register_ext, xmm_register, rflag, condt)
    instr_desc_t **)

let x86_instr_desc = function
| MOV sz -> fst coq_Ox86_MOV_instr sz
| MOVSX (sz, sz') -> fst coq_Ox86_MOVSX_instr sz sz'
| MOVZX (sz, sz') -> fst coq_Ox86_MOVZX_instr sz sz'
| CMOVcc sz -> fst coq_Ox86_CMOVcc_instr sz
| XCHG sz -> fst coq_Ox86_XCHG_instr sz
| ADD sz -> fst coq_Ox86_ADD_instr sz
| SUB sz -> fst coq_Ox86_SUB_instr sz
| MUL sz -> fst coq_Ox86_MUL_instr sz
| IMUL sz -> fst coq_Ox86_IMUL_instr sz
| IMULr sz -> fst coq_Ox86_IMULr_instr sz
| IMULri sz -> fst coq_Ox86_IMULri_instr sz
| DIV sz -> fst coq_Ox86_DIV_instr sz
| IDIV sz -> fst coq_Ox86_IDIV_instr sz
| CQO sz -> fst coq_Ox86_CQO_instr sz
| ADC sz -> fst coq_Ox86_ADC_instr sz
| SBB sz -> fst coq_Ox86_SBB_instr sz
| NEG sz -> fst coq_Ox86_NEG_instr sz
| INC sz -> fst coq_Ox86_INC_instr sz
| DEC sz -> fst coq_Ox86_DEC_instr sz
| LZCNT sz -> fst coq_Ox86_LZCNT_instr sz
| TZCNT sz -> fst coq_Ox86_TZCNT_instr sz
| SETcc -> fst coq_Ox86_SETcc_instr
| BT sz -> fst coq_Ox86_BT_instr sz
| CLC -> fst coq_Ox86_CLC_instr
| STC -> fst coq_Ox86_STC_instr
| LEA sz -> fst coq_Ox86_LEA_instr sz
| TEST sz -> fst coq_Ox86_TEST_instr sz
| CMP sz -> fst coq_Ox86_CMP_instr sz
| AND sz -> fst coq_Ox86_AND_instr sz
| ANDN sz -> fst coq_Ox86_ANDN_instr sz
| OR sz -> fst coq_Ox86_OR_instr sz
| XOR sz -> fst coq_Ox86_XOR_instr sz
| NOT sz -> fst coq_Ox86_NOT_instr sz
| ROR sz -> fst coq_Ox86_ROR_instr sz
| ROL sz -> fst coq_Ox86_ROL_instr sz
| RCR sz -> fst coq_Ox86_RCR_instr sz
| RCL sz -> fst coq_Ox86_RCL_instr sz
| SHL sz -> fst coq_Ox86_SHL_instr sz
| SHR sz -> fst coq_Ox86_SHR_instr sz
| SAL sz -> fst coq_Ox86_SAL_instr sz
| SAR sz -> fst coq_Ox86_SAR_instr sz
| SHLD sz -> fst coq_Ox86_SHLD_instr sz
| SHRD sz -> fst coq_Ox86_SHRD_instr sz
| RORX sz -> fst coq_Ox86_RORX_instr sz
| SARX sz -> fst coq_Ox86_SARX_instr sz
| SHRX sz -> fst coq_Ox86_SHRX_instr sz
| SHLX sz -> fst coq_Ox86_SHLX_instr sz
| MULX_lo_hi sz -> fst coq_Ox86_MULX_lo_hi_instr sz
| ADCX sz -> fst coq_Ox86_ADCX_instr sz
| ADOX sz -> fst coq_Ox86_ADOX_instr sz
| BSWAP sz -> fst coq_Ox86_BSWAP_instr sz
| POPCNT sz -> fst coq_Ox86_POPCNT_instr sz
| BTR sz -> fst coq_Ox86_BTR_instr sz
| BTS sz -> fst coq_Ox86_BTS_instr sz
| PEXT sz -> fst coq_Ox86_PEXT_instr sz
| PDEP sz -> fst coq_Ox86_PDEP_instr sz
| MOVX sz -> fst coq_Ox86_MOVX_instr sz
| POR -> fst coq_Ox86_POR_instr
| MOVD sz -> fst coq_Ox86_MOVD_instr sz
| MOVV sz -> fst coq_Ox86_MOVV_instr sz
| VMOV sz -> fst coq_Ox86_VMOV_instr sz
| VMOVDQA sz -> fst coq_Ox86_VMOVDQA_instr sz
| VMOVDQU sz -> fst coq_Ox86_VMOVDQU_instr sz
| VPMOVSX (ve, sz, ve', sz') -> fst coq_Ox86_VPMOVSX_instr ve sz ve' sz'
| VPMOVZX (ve, sz, ve', sz') -> fst coq_Ox86_VPMOVZX_instr ve sz ve' sz'
| VPAND sz -> fst coq_Ox86_VPAND_instr sz
| VPANDN sz -> fst coq_Ox86_VPANDN_instr sz
| VPOR sz -> fst coq_Ox86_VPOR_instr sz
| VPXOR sz -> fst coq_Ox86_VPXOR_instr sz
| VPADD (sz, sz') -> fst coq_Ox86_VPADD_instr sz sz'
| VPSUB (sz, sz') -> fst coq_Ox86_VPSUB_instr sz sz'
| VPAVG (sz, sz') -> fst coq_Ox86_VPAVG_instr sz sz'
| VPMULL (sz, sz') -> fst coq_Ox86_VPMULL_instr sz sz'
| VPMULH sz -> fst coq_Ox86_VPMULH_instr sz
| VPMULHU sz -> fst coq_Ox86_VPMULHU_instr sz
| VPMULHRS sz -> fst coq_Ox86_VPMULHRS_instr sz
| VPMUL sz -> fst coq_Ox86_VPMUL_instr sz
| VPMULU sz -> fst coq_Ox86_VPMULU_instr sz
| VPEXTR ve -> fst coq_Ox86_VPEXTR_instr ve
| VPINSR sz -> fst coq_Ox86_VPINSR_instr sz
| VPSLL (sz, sz') -> fst coq_Ox86_VPSLL_instr sz sz'
| VPSRL (sz, sz') -> fst coq_Ox86_VPSRL_instr sz sz'
| VPSRA (sz, sz') -> fst coq_Ox86_VPSRA_instr sz sz'
| VPSLLV (sz, sz') -> fst coq_Ox86_VPSLLV_instr sz sz'
| VPSRLV (sz, sz') -> fst coq_Ox86_VPSRLV_instr sz sz'
| VPSLLDQ sz -> fst coq_Ox86_VPSLLDQ_instr sz
| VPSRLDQ sz -> fst coq_Ox86_VPSRLDQ_instr sz
| VPSHUFB sz -> fst coq_Ox86_VPSHUFB_instr sz
| VPSHUFD sz -> fst coq_Ox86_VPSHUFD_instr sz
| VPSHUFHW sz -> fst coq_Ox86_VPSHUFHW_instr sz
| VPSHUFLW sz -> fst coq_Ox86_VPSHUFLW_instr sz
| VPBLEND (ve, sz) -> fst coq_Ox86_VPBLEND_instr ve sz
| VPBLENDVB sz -> fst coq_Ox86_VPBLENDVB_instr sz
| BLENDV (ve, sz) -> fst coq_Ox86_BLENDV_instr ve sz
| VPACKUS (ve, sz) -> fst coq_Ox86_VPACKUS_instr ve sz
| VPACKSS (ve, sz) -> fst coq_Ox86_VPACKSS_instr ve sz
| VSHUFPS sz -> fst coq_Ox86_VSHUFPS_instr sz
| VPBROADCAST (sz, sz') -> fst coq_Ox86_VPBROADCAST_instr sz sz'
| VMOVSHDUP sz -> fst coq_Ox86_VMOVSHDUP_instr sz
| VMOVSLDUP sz -> fst coq_Ox86_VMOVSLDUP_instr sz
| VPALIGNR sz -> fst coq_Ox86_VPALIGNR_instr sz
| VBROADCASTI128 -> fst coq_Ox86_VBROADCASTI128_instr
| VPUNPCKH (sz, sz') -> fst coq_Ox86_VPUNPCKH_instr sz sz'
| VPUNPCKL (sz, sz') -> fst coq_Ox86_VPUNPCKL_instr sz sz'
| VEXTRACTI128 -> fst coq_Ox86_VEXTRACTI128_instr
| VINSERTI128 -> fst coq_Ox86_VINSERTI128_instr
| VPERM2I128 -> fst coq_Ox86_VPERM2I128_instr
| VPERMD -> fst coq_Ox86_VPERMD_instr
| VPERMQ -> fst coq_Ox86_VPERMQ_instr
| VPMOVMSKB (sz, sz') -> fst coq_Ox86_PMOVMSKB_instr sz sz'
| VPCMPEQ (ve, sz) -> fst coq_Ox86_VPCMPEQ_instr ve sz
| VPCMPGT (ve, sz) -> fst coq_Ox86_VPCMPGT_instr ve sz
| VPSIGN (ve, sz) -> fst coq_Ox86_VPSIGN_instr ve sz
| VPMADDUBSW sz -> fst coq_Ox86_VPMADDUBSW_instr sz
| VPMADDWD sz -> fst coq_Ox86_VPMADDWD_instr sz
| VMOVLPD -> fst coq_Ox86_VMOVLPD_instr
| VMOVHPD -> fst coq_Ox86_VMOVHPD_instr
| VPMINU (ve, sz) -> fst coq_Ox86_VPMINU_instr ve sz
| VPMINS (ve, sz) -> fst coq_Ox86_VPMINS_instr ve sz
| VPMAXU (ve, sz) -> fst coq_Ox86_VPMAXU_instr ve sz
| VPMAXS (ve, sz) -> fst coq_Ox86_VPMAXS_instr ve sz
| VPABS (ve, sz) -> fst coq_Ox86_VPABS_instr ve sz
| VPTEST sz -> fst coq_Ox86_VPTEST_instr sz
| CLFLUSH -> fst coq_Ox86_CLFLUSH_instr
| PREFETCHT0 -> fst coq_Ox86_PREFETCHT0_instr
| PREFETCHT1 -> fst coq_Ox86_PREFETCHT1_instr
| PREFETCHT2 -> fst coq_Ox86_PREFETCHT2_instr
| PREFETCHNTA -> fst coq_Ox86_PREFETCHNTA_instr
| LFENCE -> fst coq_Ox86_LFENCE_instr
| MFENCE -> fst coq_Ox86_MFENCE_instr
| SFENCE -> fst coq_Ox86_SFENCE_instr
| RDTSC sz -> fst coq_Ox86_RDTSC_instr sz
| RDTSCP sz -> fst coq_Ox86_RDTSCP_instr sz
| AESDEC -> fst coq_Ox86_AESDEC_instr
| VAESDEC sz -> fst coq_Ox86_VAESDEC_instr sz
| AESDECLAST -> fst coq_Ox86_AESDECLAST_instr
| VAESDECLAST sz -> fst coq_Ox86_VAESDECLAST_instr sz
| AESENC -> fst coq_Ox86_AESENC_instr
| VAESENC sz -> fst coq_Ox86_VAESENC_instr sz
| AESENCLAST -> fst coq_Ox86_AESENCLAST_instr
| VAESENCLAST sz -> fst coq_Ox86_VAESENCLAST_instr sz
| AESIMC -> fst coq_Ox86_AESIMC_instr
| VAESIMC -> fst coq_Ox86_VAESIMC_instr
| AESKEYGENASSIST -> fst coq_Ox86_AESKEYGENASSIST_instr
| VAESKEYGENASSIST -> fst coq_Ox86_VAESKEYGENASSIST_instr
| PCLMULQDQ -> fst coq_Ox86_PCLMULQDQ_instr
| VPCLMULQDQ sz -> fst coq_Ox86_VPCLMULQDQ_instr sz

(** val x86_prim_string : (string * x86_op prim_constructor) list **)

let x86_prim_string =
  (snd coq_Ox86_MOV_instr) :: ((snd coq_Ox86_MOVSX_instr) :: ((snd
                                                                coq_Ox86_MOVZX_instr) :: (
    (snd coq_Ox86_CMOVcc_instr) :: ((snd coq_Ox86_XCHG_instr) :: ((snd
                                                                    coq_Ox86_BSWAP_instr) :: (
    (snd coq_Ox86_POPCNT_instr) :: ((snd coq_Ox86_BTR_instr) :: ((snd
                                                                   coq_Ox86_BTS_instr) :: (
    (snd coq_Ox86_PEXT_instr) :: ((snd coq_Ox86_PDEP_instr) :: ((snd
                                                                  coq_Ox86_CQO_instr) :: (
    (snd coq_Ox86_ADD_instr) :: ((snd coq_Ox86_SUB_instr) :: ((snd
                                                                coq_Ox86_MUL_instr) :: (
    (snd coq_Ox86_IMUL_instr) :: ((snd coq_Ox86_IMULr_instr) :: ((snd
                                                                   coq_Ox86_IMULri_instr) :: (
    (snd coq_Ox86_DIV_instr) :: ((snd coq_Ox86_IDIV_instr) :: ((snd
                                                                 coq_Ox86_ADC_instr) :: (
    (snd coq_Ox86_ADCX_instr) :: ((snd coq_Ox86_ADOX_instr) :: ((snd
                                                                  coq_Ox86_MULX_lo_hi_instr) :: (
    (snd coq_Ox86_SBB_instr) :: ((snd coq_Ox86_NEG_instr) :: ((snd
                                                                coq_Ox86_INC_instr) :: (
    (snd coq_Ox86_DEC_instr) :: ((snd coq_Ox86_LZCNT_instr) :: ((snd
                                                                  coq_Ox86_TZCNT_instr) :: (
    (snd coq_Ox86_SETcc_instr) :: ((snd coq_Ox86_BT_instr) :: ((snd
                                                                 coq_Ox86_CLC_instr) :: (
    (snd coq_Ox86_STC_instr) :: ((snd coq_Ox86_LEA_instr) :: ((snd
                                                                coq_Ox86_TEST_instr) :: (
    (snd coq_Ox86_CMP_instr) :: ((snd coq_Ox86_AND_instr) :: ((snd
                                                                coq_Ox86_ANDN_instr) :: (
    (snd coq_Ox86_OR_instr) :: ((snd coq_Ox86_XOR_instr) :: ((snd
                                                               coq_Ox86_NOT_instr) :: (
    (snd coq_Ox86_ROL_instr) :: ((snd coq_Ox86_ROR_instr) :: ((snd
                                                                coq_Ox86_RCL_instr) :: (
    (snd coq_Ox86_RCR_instr) :: ((snd coq_Ox86_SHL_instr) :: ((snd
                                                                coq_Ox86_SHR_instr) :: (
    (snd coq_Ox86_SAR_instr) :: ((snd coq_Ox86_SAL_instr) :: ((snd
                                                                coq_Ox86_SHLD_instr) :: (
    (snd coq_Ox86_SHRD_instr) :: ((snd coq_Ox86_RORX_instr) :: ((snd
                                                                  coq_Ox86_SARX_instr) :: (
    (snd coq_Ox86_SHRX_instr) :: ((snd coq_Ox86_SHLX_instr) :: ((snd
                                                                  coq_Ox86_MOVX_instr) :: (
    (snd coq_Ox86_POR_instr) :: ((snd coq_Ox86_MOVD_instr) :: ((snd
                                                                 coq_Ox86_MOVV_instr) :: (
    (snd coq_Ox86_VMOV_instr) :: ((snd coq_Ox86_VPMOVSX_instr) :: ((snd
                                                                    coq_Ox86_VPMOVZX_instr) :: (
    (snd coq_Ox86_VPINSR_instr) :: ((snd coq_Ox86_VEXTRACTI128_instr) :: (
    (snd coq_Ox86_VMOVDQA_instr) :: ((snd coq_Ox86_VMOVDQU_instr) :: (
    (snd coq_Ox86_VPAND_instr) :: ((snd coq_Ox86_VPANDN_instr) :: ((snd
                                                                    coq_Ox86_VPOR_instr) :: (
    (snd coq_Ox86_VPXOR_instr) :: ((snd coq_Ox86_VPADD_instr) :: ((snd
                                                                    coq_Ox86_VPSUB_instr) :: (
    (snd coq_Ox86_VPAVG_instr) :: ((snd coq_Ox86_VPMULL_instr) :: ((snd
                                                                    coq_Ox86_VPMUL_instr) :: (
    (snd coq_Ox86_VPMULU_instr) :: ((snd coq_Ox86_VPMULH_instr) :: ((snd
                                                                    coq_Ox86_VPMULHU_instr) :: (
    (snd coq_Ox86_VPMULHRS_instr) :: ((snd coq_Ox86_VPSLL_instr) :: (
    (snd coq_Ox86_VPSRL_instr) :: ((snd coq_Ox86_VPSRA_instr) :: ((snd
                                                                    coq_Ox86_VPSLLV_instr) :: (
    (snd coq_Ox86_VPSRLV_instr) :: ((snd coq_Ox86_VPSLLDQ_instr) :: (
    (snd coq_Ox86_VPSRLDQ_instr) :: ((snd coq_Ox86_VPSHUFB_instr) :: (
    (snd coq_Ox86_VPSHUFHW_instr) :: ((snd coq_Ox86_VPSHUFLW_instr) :: (
    (snd coq_Ox86_VPSHUFD_instr) :: ((snd coq_Ox86_VSHUFPS_instr) :: (
    (snd coq_Ox86_VPUNPCKH_instr) :: ((snd coq_Ox86_VPUNPCKL_instr) :: (
    (snd coq_Ox86_VPBLEND_instr) :: ((snd coq_Ox86_VPBLENDVB_instr) :: (
    (snd coq_Ox86_BLENDV_instr) :: ((snd coq_Ox86_VPACKUS_instr) :: (
    (snd coq_Ox86_VPACKSS_instr) :: ((snd coq_Ox86_VPBROADCAST_instr) :: (
    (snd coq_Ox86_VMOVSHDUP_instr) :: ((snd coq_Ox86_VMOVSLDUP_instr) :: (
    (snd coq_Ox86_VPALIGNR_instr) :: ((snd coq_Ox86_VBROADCASTI128_instr) :: (
    (snd coq_Ox86_VPERM2I128_instr) :: ((snd coq_Ox86_VPERMD_instr) :: (
    (snd coq_Ox86_VPERMQ_instr) :: ((snd coq_Ox86_VINSERTI128_instr) :: (
    (snd coq_Ox86_VPEXTR_instr) :: ((snd coq_Ox86_PMOVMSKB_instr) :: (
    (snd coq_Ox86_VPCMPEQ_instr) :: ((snd coq_Ox86_VPCMPGT_instr) :: (
    (snd coq_Ox86_VPSIGN_instr) :: ((snd coq_Ox86_VPMADDUBSW_instr) :: (
    (snd coq_Ox86_VPMADDWD_instr) :: ((snd coq_Ox86_VMOVLPD_instr) :: (
    (snd coq_Ox86_VMOVHPD_instr) :: ((snd coq_Ox86_VPMINU_instr) :: (
    (snd coq_Ox86_VPMINS_instr) :: ((snd coq_Ox86_VPMAXU_instr) :: ((snd
                                                                    coq_Ox86_VPMAXS_instr) :: (
    (snd coq_Ox86_VPABS_instr) :: ((snd coq_Ox86_VPTEST_instr) :: ((snd
                                                                    coq_Ox86_CLFLUSH_instr) :: (
    (snd coq_Ox86_PREFETCHT0_instr) :: ((snd coq_Ox86_PREFETCHT1_instr) :: (
    (snd coq_Ox86_PREFETCHT2_instr) :: ((snd coq_Ox86_PREFETCHNTA_instr) :: (
    (snd coq_Ox86_LFENCE_instr) :: ((snd coq_Ox86_MFENCE_instr) :: ((snd
                                                                    coq_Ox86_SFENCE_instr) :: (
    (snd coq_Ox86_RDTSC_instr) :: ((snd coq_Ox86_RDTSCP_instr) :: ((snd
                                                                    coq_Ox86_AESDEC_instr) :: (
    (snd coq_Ox86_VAESDEC_instr) :: ((snd coq_Ox86_AESDECLAST_instr) :: (
    (snd coq_Ox86_VAESDECLAST_instr) :: ((snd coq_Ox86_AESENC_instr) :: (
    (snd coq_Ox86_VAESENC_instr) :: ((snd coq_Ox86_AESENCLAST_instr) :: (
    (snd coq_Ox86_VAESENCLAST_instr) :: ((snd coq_Ox86_AESIMC_instr) :: (
    (snd coq_Ox86_VAESIMC_instr) :: ((snd coq_Ox86_AESKEYGENASSIST_instr) :: (
    (snd coq_Ox86_VAESKEYGENASSIST_instr) :: ((snd coq_Ox86_PCLMULQDQ_instr) :: (
    (snd coq_Ox86_VPCLMULQDQ_instr) :: (("VPMAX",
    (primSV_8_32 (fun signedness0 ve sz ->
      match signedness0 with
      | Signed -> VPMAXS (ve, sz)
      | Unsigned -> VPMAXU (ve, sz)))) :: (("VPMIN",
    (primSV_8_32 (fun signedness0 ve sz ->
      match signedness0 with
      | Signed -> VPMINS (ve, sz)
      | Unsigned -> VPMINU (ve, sz)))) :: []))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))

(** val eqC_x86_op : x86_op eqTypeC **)

let eqC_x86_op =
  { beq = x86_op_beq; ceqP = x86_op_eq_axiom }

(** val x86_op_decl :
    (register, register_ext, xmm_register, rflag, condt, x86_op) asm_op_decl **)

let x86_op_decl =
  { Arch_decl._eqT = eqC_x86_op; instr_desc_op = x86_instr_desc;
    Arch_decl.prim_string = x86_prim_string }

type x86_prog =
  (register, register_ext, xmm_register, rflag, condt, x86_op) asm_prog
